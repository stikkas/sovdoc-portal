package ru.insoft.sdportal.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.web.table.WebImages;


@Entity
@Table(name="WEB_NEWS")
public class News {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "newsidgen")
	@SequenceGenerator(name = "newsidgen", sequenceName = "SEQ_WEB_NEWS")
	@Column(name = "NEWS_ID",nullable = false)
	private Long id;
	@Column(name = "NEWS_DATE",nullable=false)
	private Date date;
	@Column(name = "NAME",nullable=false)
	private String name;
	
	
	@ManyToMany
	@JoinTable(name = "WEB_NEWS_TO_IMAGES", joinColumns = { @JoinColumn(name = "news_id") }, inverseJoinColumns = { @JoinColumn(name = "image_id") })
	private Set<WebImages> images = new HashSet<WebImages>();
	
	public News(){
		
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Set<WebImages> getImages() {
		return images;
	}

	public void setImages(Set<WebImages> images) {
		this.images = images;
	}
	
	public String printableDate(){
		return new SimpleDateFormat("dd.MM.yyyy").format(date);
	}
}
