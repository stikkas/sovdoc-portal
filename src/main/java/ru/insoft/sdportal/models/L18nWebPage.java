package ru.insoft.sdportal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.table.DescLanguage;

/**
 * Page's data, localized on "lang" language
 * 
 * @author sorokin
 */
@Entity
@Table(name = "WEB_PAGES_LOCALIZED")
public class L18nWebPage {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "l18npageidgen")
	@SequenceGenerator(name = "l18npageidgen", sequenceName = "SEQ_WEB_PAGES_LOCALIZED")
	@Column(name = "localized_page_id")
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "page_id")
	private WebPage page;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "news_id")
	private News news;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "language_code")
	private DescLanguage lang;
	@Lob
	@Column(name = "content", nullable = false)
	private String pageData;
	@Column(name = "name", nullable = false)
	private String pageName;

	@Column(name = "CONTENT_TYPE_ID", nullable = false)
	private Long contentType;

	public Long getContentType() {
		return contentType;
	}

	public void setContentType(Long contentType) {
		this.contentType = contentType;
	}

	public L18nWebPage() {
		this.pageName = "";
		this.pageData = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WebPage getPage() {
		return page;
	}

	public void setPage(WebPage page) {
		this.page = page;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public DescLanguage getLang() {
		return lang;
	}

	public void setLang(DescLanguage lang) {
		this.lang = lang;
	}

	public String getPageData() {
		return pageData;
	}

	public void setPageData(String pageData) {
		this.pageData = pageData;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
}
