package ru.insoft.sdportal.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WEB_USER")
public class WebUser {
	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE,generator="useridgen")
	@SequenceGenerator(name="useridgen",sequenceName="SEQ_WEB_USER")
	@Column(name = "web_user_id", nullable = false)
	private Integer webUserId;
	@Column(name = "user_id")
	private Long userId;

	@Column(name = "last_name", nullable = false, length = 150)
	private String lastName;

	@Column(name = "first_name", nullable = false, length = 150)
	private String firstName;

	@Column(name = "middle_name", nullable = true, length = 150)
	private String middleName;

	@Column(name = "birth_date", nullable = false)
	private Date birthDate;

	@Column(name="CONFIRMATION_UUID")
	private String confirmationUUID;
	
	@Column(name="STATUS",nullable=false)
	private Long descValueStatusId;
	// FK to descriptor value!
	//гражданство

	@Column(name = "citizenship_id")
	private Long citizenshipId;

	@Column(name = "occupation_info", nullable = true)
	private String occupationInfo;

	// FK to descriptor value!

	@Column(name = "education_level_id", nullable = true)
	private Long educationLevelId;

	// FK to descriptor value!

	@Column(name = "rank_id", nullable = true)
	private Long rankId;

	@Column(name = "phone", nullable = true, length = 20)
	private String phone;

	@Column(name = "email", nullable = false, length = 20)
	private String email;

	public Integer getWebUserId() {
		return webUserId;
	}

	public void setWebUserId(Integer webUserId) {
		this.webUserId = webUserId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Long getCitizenshipId() {
		return citizenshipId;
	}

	public void setCitizenshipId(Long citizenshipId) {
		this.citizenshipId = citizenshipId;
	}

	public String getOccupationInfo() {
		return occupationInfo;
	}

	public void setOccupationInfo(String occupationInfo) {
		this.occupationInfo = occupationInfo;
	}

	public Long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public Long getRankId() {
		return rankId;
	}

	public void setRankId(Long rankId) {
		this.rankId = rankId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setConfirmationUUID(String confirmationUUID) {
		this.confirmationUUID = confirmationUUID;
	}

	public void setDescValueStatusId(Long descValueStatusId) {
		this.descValueStatusId = descValueStatusId;
	}

	public String getConfirmationUUID() {
		return confirmationUUID;
	}

	public Long getDescValueStatusId() {
		return descValueStatusId;
	}
	
	

}
