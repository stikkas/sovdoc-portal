package ru.insoft.sdportal.models.ui;

import ru.insoft.archive.extcommons.json.JsonOut;
import ru.insoft.sovdoc.model.adm.table.AdmUser;

public class UserRegOkResponse implements JsonOut{

	private static final long serialVersionUID = -4286469280615974035L;

	private boolean success = true;
	
	private String fullName;
	private Long userId;
	
	
	public UserRegOkResponse(){
		
	}

	
	public void initMessage(AdmUser au){
		fullName = au.getDisplayedName();
		userId = au.getUserId();
	}
}
