package ru.insoft.sdportal.models.ui;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author melnikov
 */
public class TematicRootSection 
{
    private Long id;
    private String section;
    private List<TematicRootValue> values;
    private List<TematicRootSection> sections;
    
    public TematicRootSection()
    {
        values   = new ArrayList<>();
        sections = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public List<TematicRootValue> getValues() {
        return values;
    }

    public void setValues(List<TematicRootValue> values) {
        this.values = values;
    }

    public List<TematicRootSection> getSections() {
        return sections;
    }

    public void setSections(List<TematicRootSection> sections) {
        this.sections = sections;
    }
}
