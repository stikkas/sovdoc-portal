package ru.insoft.sdportal.models.ui;

import ru.insoft.archive.extcommons.json.JsonOut;

public class ImageInfo implements JsonOut {

	private String fileName;
	private String url;
	private String mapUrl;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMapUrl() {
		return mapUrl;
	}

	public void setMapUrl(String mapUrl) {
		this.mapUrl = mapUrl;
	}
}
