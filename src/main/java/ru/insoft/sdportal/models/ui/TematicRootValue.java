package ru.insoft.sdportal.models.ui;

/**
 *
 * @author melnikov
 */
public class TematicRootValue 
{
    private Long id;
    private String name;
    private String archCode;
    private String dates;
    private Integer childrenCnt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArchCode() {
        return archCode;
    }

    public void setArchCode(String archCode) {
        this.archCode = archCode;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public Integer getChildrenCnt() {
        return childrenCnt;
    }

    public void setChildrenCnt(Integer childrenCnt) {
        this.childrenCnt = childrenCnt;
    }
}
