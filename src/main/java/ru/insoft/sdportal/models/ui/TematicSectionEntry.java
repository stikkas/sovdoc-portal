package ru.insoft.sdportal.models.ui;

public class TematicSectionEntry {
	String name;
	Boolean hasRoots;
	Long id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getHasRoots() {
		return hasRoots;
	}
	public void setHasRoots(Boolean hasRoots) {
		this.hasRoots = hasRoots;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
