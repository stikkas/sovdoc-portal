package ru.insoft.sdportal.models.ui;

import java.text.SimpleDateFormat;
import ru.insoft.archive.extcommons.json.JsonOut;
import ru.insoft.sdportal.ejb.SysUtils;
import ru.insoft.sdportal.models.WebUser;

public class UserInfoResponse implements JsonOut{


	private boolean success=true;
	private String mail;
	private String middleName;
	private String firstName;
	private String secondName;
	private String birthday;
	private Long citizenchipId;
	private String workPlace;
	private Long education;
	private Long rank;
	private String phone;

	public void initMessage(WebUser wu,SysUtils su){
		mail = wu.getEmail();
		middleName = wu.getMiddleName();
		firstName = wu.getFirstName();
		secondName = wu.getLastName();
		SimpleDateFormat sdf = su.getSdf();
		birthday = sdf.format(wu.getBirthDate());
		citizenchipId = wu.getCitizenshipId();
		workPlace = wu.getOccupationInfo();
		education = wu.getEducationLevelId();
		rank = wu.getRankId();
		phone = wu.getPhone();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Long getCitizenchipId() {
		return citizenchipId;
	}

	public void setCitizenchipId(Long citizenchipId) {
		this.citizenchipId = citizenchipId;
	}

	public String getWorkPlace() {
		return workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	public Long getEducation() {
		return education;
	}

	public void setEducation(Long education) {
		this.education = education;
	}

	public Long getRank() {
		return rank;
	}

	public void setRank(Long rank) {
		this.rank = rank;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
	
	
	
	
}
