package ru.insoft.sdportal.models.ui;

import java.util.ArrayList;

import ru.insoft.archive.extcommons.json.JsonOut;

public class TreeItem implements JsonOut{
	private Long id;
	private String text;
	private Boolean leaf;
	private boolean isDescValue=true;
	private ArrayList<TreeItem> children;

	public TreeItem() {
		children = new ArrayList<>();
		leaf = true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ArrayList<TreeItem> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<TreeItem> children) {
		this.children = children;
	}

	public void addChild(TreeItem ti) {
		children.add(ti);
		leaf = false;
	}
	public void setLeaf(boolean b ){
		this.leaf = b;
	}
	public boolean isLeaf(){
		return leaf; 
	}
}
