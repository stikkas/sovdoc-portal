
package ru.insoft.sdportal.models.ui;

/**
 *
 * @author sorokin
 */
public class FastTreeItem {
	private boolean leaf;
	private boolean isDescValue=false;
//	private boolean loaded = true;
	private String text;
	private String id;
	
	private FastTreeItem[] children;
	
	public void initChildrenSize(int size){
		children = new FastTreeItem[size];
	}
	
	public void addChildren(int index,FastTreeItem child){
		leaf = false;
		children[index]=child;
	}

	public boolean isIsLeaf() {
		return leaf;
	}

	public void setIsLeaf(boolean isLeaf) {
		this.leaf = isLeaf;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public FastTreeItem[] getChildren() {
		return children;
	}

	public void setChildren(FastTreeItem[] children) {
		this.children = children;
	}

}
