package ru.insoft.sdportal.models.ui;

import java.io.Serializable;

public class ComboItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -818003928984669651L;

	Long id;

	String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
