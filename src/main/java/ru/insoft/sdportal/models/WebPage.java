package ru.insoft.sdportal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "WEB_PAGES")
public class WebPage {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pageidgen")
	@SequenceGenerator(name = "pageidgen", sequenceName = "SEQ_WEB_PAGES")
	@Column(name = "page_id",nullable=false)
	private Long id;
	@Column(name = "page_code", nullable = true, length = 30)
	private String pageCode;
	@Column(name = "internal_name", nullable = false, length = 300)
	private String internalName;
	@Column(name = "PAGE_TYPE_ID")
	Long pageTypeId;
	@Column(name = "SORT_ORDER", nullable = false)
	Long sortOrder;

	public WebPage() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPageCode() {
		return pageCode;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public Long getPageTypeId() {
		return pageTypeId;
	}

	public void setPageTypeId(Long pageTypeId) {
		this.pageTypeId = pageTypeId;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}
}
