package ru.insoft.sdportal.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author melnikov
 */
@Entity
public class TematicRootResult implements Serializable
{
    @Column(name = "PARENT_UNIT_ID")
    private Long parentUnitId;
    
    @Id
    @Column(name = "SECTION_ID")
    private Long sectionId;
    
    @Column(name = "SECTION_NAME")
    private String sectionName;
    
    @Id
    @Column(name = "UNIV_DATA_UNIT_ID")
    private Long univDataUnitId;
    
    @Column(name = "UNIT_NAME")
    private String unitName;
    
    @Column(name = "CODE")
    private String archCode;
    
    @Column(name = "CHILDREN_CNT")
    private Integer childrenCnt;
    
    @Column(name = "DATES")
    private String dates;

    public Long getParentUnitId() {
        return parentUnitId;
    }

    public void setParentUnitId(Long parentUnitId) {
        this.parentUnitId = parentUnitId;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public Long getUnivDataUnitId() {
        return univDataUnitId;
    }

    public void setUnivDataUnitId(Long univDataUnitId) {
        this.univDataUnitId = univDataUnitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getArchCode() {
        return archCode;
    }

    public void setArchCode(String archCode) {
        this.archCode = archCode;
    }

    public Integer getChildrenCnt() {
        return childrenCnt;
    }

    public void setChildrenCnt(Integer childrenCnt) {
        this.childrenCnt = childrenCnt;
    }

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }
}
