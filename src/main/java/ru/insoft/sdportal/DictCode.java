package ru.insoft.sdportal;

/**
 * Коды различных данных базы
 *
 * @author stikkas<stikkas@yandex.ru>
 */
public class DictCode {

	/**
	 * Электронная книга
	 */
	public final static String EBOOK = "EBOOK";
	/**
	 * Раздел
	 */
	public final static String SECTION = "SECTION";
	/**
	 * Фонд
	 */
	public final static String FUND = "FUND";
	/**
	 * Опись
	 */
	public final static String SERIES = "SERIES";
	/**
	 * Дело на бумажной основе
	 */
	public final static String PAPER_FILE = "PAPER_FILE";
	/**
	 * Документ на бумажной основе
	 */
	public final static String PAPER_DOC = "PAPER_DOC";
	/**
	 * Фонодокумент
	 */
	public final static String PHONODOC = "PHONODOC";
}
