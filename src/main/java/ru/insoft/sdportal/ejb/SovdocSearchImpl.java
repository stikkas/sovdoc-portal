package ru.insoft.sdportal.ejb;

import com.google.common.collect.Lists;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import ru.insoft.sdportal.models.TematicRootResult;
import ru.insoft.sdportal.models.ui.TematicRootSection;
import ru.insoft.sdportal.models.ui.TematicRootValue;
import ru.insoft.sdportal.ws.ExtSearchReqParm;
import ru.insoft.sdportal.ws.ImageItemInfo;
import ru.insoft.sdportal.ws.SearchResultShortItem;
import ru.insoft.sdportal.ws.SearchResults;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.table.UnivImagePath;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Stateless(name = "SovdocSearchImpl")
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SovdocSearchImpl {

	private Logger log;

	private HashMap<String, String> elibAttribs;

	@Inject
	private EntityManager em;

	private String imagesReplacePath;
	private String imagesWebPath;

	@EJB
	private HibernateHelper hh;

	public SovdocSearchImpl() {
		log = Logger.getLogger(getClass().getCanonicalName());
		elibAttribs = new HashMap<String, String>();
		elibAttribs.put("publYear", "ARCH_EBOOK.PUBLISHING_YEAR");
		elibAttribs.put("seriesInfo", "ARCH_EBOOK.SERIES_ID");
		elibAttribs.put("isbn", "ARCH_EBOOK.ISBN");
	}

	// @thx to Melnikov
	/*
	protected String fillArchNumberCode(Integer fundNumber, String fundLetter,
			Integer opisNumber, String opisLitera, Integer deloNumber,
			String deloLitera) {
		String res = "";
		String fundNumber_ = fundNumber == null ? "%\u0001" : "%1$08d\u0001";
		String fundLetter_ = "%1$s\u0001";
		String opisNumber_ = opisNumber == null ? "%\u0001" : "%1$08d\u0001";
		String opisLitera_ = "%1$s\u0001";
		String deloNumber_ = deloNumber == null ? "%\u0001" : "%1$08d\u0001";
		String deloLitera_ = "%1$s\u0001";
		if (fundNumber != null) {
			res += String.format(fundNumber_, fundNumber);
		} else {
			res += fundNumber_;
		}

		if (fundLetter == null) {
			fundLetter = "";
		}
		res += String.format(fundLetter_, fundLetter);

		if (opisNumber != null) {
			res += String.format(opisNumber_, opisNumber);
		} else {
			res += opisNumber_;
		}
		if (opisLitera == null) {
			opisLitera = "";
		}
		res += String.format(opisLitera_, opisLitera);

		if (deloNumber != null) {
			res += String.format(deloNumber_, deloNumber);
		} else {
			res += deloNumber_;
		}

		if (deloLitera == null || "".equals(deloLitera)) {
			return res;
		}

		res += String.format(deloLitera_, deloLitera);
		return res;
	}
	 */
	/**
	 * Считает количество результатов выполнения запроса. (селектит count из
	 * базы)
	 *
	 * @param query
	 * @return
	 */
	private Integer getQueryResultSize(String query) {
		String countQuery = "select count(*) from (";
		countQuery += query;
		countQuery += ")";
		Query q = em.createNativeQuery(countQuery);
		Integer count = Integer.parseInt(q.getSingleResult().toString());
		return count;
	}

	/**
	 * Проверка, является ли запрос, запросом на поиск из раздела "Электронная
	 * библиотека"
	 *
	 * @param parm
	 * @return
	 */
	private boolean isElibRequest(ExtSearchReqParm parm) {
		return parm.fieldParamKeys().contains("elibQuery");
	}

	/**
	 * Проверка, является ли запрос запросом на получение всех книг из
	 * электронной библиотеки
	 *
	 * @param parm
	 * @return
	 */
	private boolean isElibGetAllQuery(ExtSearchReqParm parm) {
		return parm.fieldParamKeys().contains("elibGetAll");
	}

	/**
	 * Проверка, является ли запрос запросом на получение корневых элементов
	 * тематических разделов
	 *
	 * @param parm
	 * @return
	 */
	private boolean isTematicSectionRootsRequest(ExtSearchReqParm parm) {
		return parm.fieldParamKeys().contains("tematicSectionRootRequest");
	}

	/**
	 * Проверка, является ли запрос запросом дочерних элементов корневого
	 * элемента тематического раздела
	 *
	 * @param parm
	 * @return
	 */
	private boolean isTematicSectionChildsRequest(ExtSearchReqParm parm) {
		return parm.fieldParamKeys().contains("tematicSectionChildsRequest");
	}

	/**
	 * Новая версия функции для запаковки результатов поиска для клиента.
	 * Особенность - ограничения на размер страницы ответа зашивается в сам
	 * запрос, т.о. эти параметры передавать функцию более не нужно.
	 *
	 * @param q
	 * @return
	 */
	private SearchResults packSearchResult(Query q) {
		SearchResults results = new SearchResults();
		List<SearchResultShortItem> l = new LinkedList<SearchResultShortItem>();
		Object[] rawRes = q.getResultList().toArray();
		Long allResultsCount = null;
		for (int i = 0; i < rawRes.length; i++) {
			Object[] resultRow = (Object[]) rawRes[i];
			Long unitId = Long.parseLong(resultRow[0].toString());
			String name = (String) resultRow[1];
			String archCode = (String) resultRow[2];
			String dates = (String) resultRow[5];
			if (allResultsCount == null) {
				allResultsCount = ((BigDecimal) resultRow[6]).longValue();
			}
			SearchResultShortItem curIt = new SearchResultShortItem();
			curIt.setDataUnitId(unitId);
			curIt.setDataUnitName(name);
			curIt.setMathesTextBlock(dates);
			if (archCode != null) {
				archCode = archCode.replaceAll("\u0001", "");
			}
			curIt.setArchiveCode(archCode);
			l.add(curIt);
		}
		results.setFindedItems(l);
		if (allResultsCount != null) {
			results.setTotalSearchResultsCount(allResultsCount.intValue());
		}
		return results;
	}

	/**
	 * Pack query results into List of SearchResultShortItem objects, query must
	 * return values of these columns: univ_data_unit_id,unit_name,ebook_author,
	 * ebook_publisher,ebook-publisher year Что бы не менять архитектуру, и
	 * получение книг электронной библиотеки для отображения в списке работал
	 * через вебсервис и в рамках существующей архитектуры, в одно из полей,
	 * (имя единицы хранения), заталкивается столбец с кодированными в json
	 * данными, для электронных книг. (Там ВНЕЗАПНО, решили сделать другой набор
	 * столбцов, поэтому структура SearchResultShortItem не подходит).
	 *
	 * @param q
	 * @param limit
	 * @param startWith
	 * @return
	 */
	private List<SearchResultShortItem> packElibSearchResults(Query q,
			Integer limit, Integer startWith) {
		List<SearchResultShortItem> result = new ArrayList<SearchResultShortItem>();
		q.setFirstResult(startWith);
		q.setMaxResults(limit);
		List<Object[]> rawResult = (List<Object[]>) q.getResultList();
		for (Object[] rawRow : rawResult) {
			SearchResultShortItem tempItem = new SearchResultShortItem();
			Long id = Long.parseLong(rawRow[0].toString());
			String bookName = rawRow[1].toString();
			String author = "-";
			if (rawRow[2] != null) {
				author = rawRow[2].toString();
			}
			String publisher = rawRow[3].toString();
			Integer publYear = Integer.parseInt(rawRow[4].toString());
			String volume = "нет";
			if (rawRow[5] != null) {
				volume = rawRow[5].toString();
			}
			String url = "";
			if (rawRow.length > 6) {
				if (rawRow[6] != null) {
					url = rawRow[6].toString();
					if (imagesReplacePath != null && imagesWebPath != null) {
						url = url.replaceAll(imagesReplacePath, imagesWebPath);
					}
				}
			}
			tempItem.setArchiveCode("нет");
			tempItem.setDataUnitId(id);
			tempItem.setDatesInfoBlock("нет");
			tempItem.setMathesTextBlock("нет");
			JsonObjectBuilder encodedElibItem = Json.createObjectBuilder();
			encodedElibItem.add("dataUnitId", id);
			encodedElibItem.add("bookName", bookName);
			encodedElibItem.add("author", author);
			encodedElibItem.add("publisher", publisher);
			encodedElibItem.add("publYear", publYear);
			encodedElibItem.add("tom", volume);
			encodedElibItem.add("coverurl", url);
			String jsonElibItem = encodedElibItem.build().toString();
			tempItem.setDataUnitName(jsonElibItem);
			result.add(tempItem);
		}
		return result;
	}

	/**
	 * Получить список электронных книг. (Баг 6069, п5).
	 *
	 * @param limit
	 * @param startWith
	 * @return
	 */
	private SearchResults getAllElibBooks(Integer limit, Integer startWith) {
		imagesReplacePath = hh.getWebImagesReplacePath();
		imagesWebPath = hh.getPublicWebImagesPath();

		SearchResults rs = new SearchResults();
		String getAllBooksQuery = "select ARCH_EBOOK.UNIV_DATA_UNIT_ID,UNIV_DATA_UNIT.UNIT_NAME, sovdocportal.getAuthors(ARCH_EBOOK.UNIV_DATA_UNIT_ID), "
				+ "PUBLISHER,PUBLISHING_YEAR,VOLUME1_INFO, "
				+ "(select concat(FILE_PATH,FILE_NAME) from univ_image_path where UNIV_DATA_UNIT_ID=ARCH_EBOOK.UNIV_DATA_UNIT_ID and IMAGE_CATEGORY_ID = "
				+ "(select descriptor_value_id from descriptor_value where value_code='BOOK_COVER'))"
				+ "  from ARCH_EBOOK, UNIV_DATA_UNIT where UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID=ARCH_EBOOK.UNIV_DATA_UNIT_ID ";
		Integer resultRowCount = getQueryResultSize(getAllBooksQuery);
		getAllBooksQuery += "order by UNIV_DATA_UNIT.UNIT_NAME";
		rs.setTotalSearchResultsCount(resultRowCount);

		log.log(Level.FINEST, "ELIB GET EBOOK LIST QUERY: " + getAllBooksQuery);
		Query q = em.createNativeQuery(getAllBooksQuery);
		rs.setFindedItems(packElibSearchResults(q, limit, startWith));
		return rs;
	}

	private List<SearchResultShortItem> packTematicRootsResult(Query q,
			Integer limit, Integer startWith) {
		q.setFirstResult(startWith);
		q.setMaxResults(limit);
		List<SearchResultShortItem> items = new ArrayList<SearchResultShortItem>();
		for (Object rawRow : q.getResultList()) {
			SearchResultShortItem item = new SearchResultShortItem();
			Object[] row = (Object[]) rawRow;
			Long id = Long.parseLong(row[0].toString());
			String name = row[1].toString();
			String code = row[2].toString();
			Long childCount = Long.parseLong(row[3].toString());
			String dates = "";
			if (row[4] != null) {
				dates = row[4].toString();
			}
			item.setArchiveCode("нет");
			item.setDataUnitId(id);
			item.setDatesInfoBlock("нет");
			item.setMathesTextBlock("нет");

			JsonObjectBuilder obj = Json.createObjectBuilder();
			obj.add("dataUnitId", id);
			obj.add("unitName", name);
			obj.add("archiveCode", code);
			obj.add("goToCard", id);
			obj.add("goToChilds", childCount);
			obj.add("mathesTextBlock", dates);
			item.setDataUnitName(obj.build().toString());
			items.add(item);
		}
		return items;
	}

	//Старая функция получения корневых элементов тематического раздела (без иерархии)
	//Оставлено для истории
	/*private SearchResults getTematicSectionRoots(Integer limit,
			Integer startWith, ExtSearchReqParm params) {
		Long sectionId = Long.parseLong(params
				.getFieldParam("tematicSectionId").toString());
		log.log(Level.FINEST, "TEMATIC SECTION ROOT REQUEST: sectionId: "
				+ sectionId);
		String query = "select a.UNIV_DATA_UNIT_ID, a.UNIT_NAME, ";
		query += "sovdocportal.getFullArchiveCode(a.UNIV_DATA_UNIT_ID) code, ";
		query += "sovdocportal.getChildsCount(a.UNIV_DATA_UNIT_ID),complex_pack.get_date_intervals(a.UNIV_DATA_UNIT_ID) from ";
		query += "UNIV_DATA_UNIT a, UNIV_DATA_UNIT b where ";
		query += "b.UNIV_DATA_UNIT_ID(+) = a.PARENT_UNIT_ID and ";
		query += "a.PORTAL_SECTION_ID = " + sectionId + " and ";
		query += "((a.PORTAL_SECTION_ID <> b.PORTAL_SECTION_ID) OR ";
		query += "(b.portal_section_id is null)) order by code";
		log.log(Level.FINEST, "QUERY " + query);
		int resultSize = getQueryResultSize(query);
		SearchResults results = new SearchResults();
		results.setTotalSearchResultsCount(resultSize);
		Query q = em.createNativeQuery(query);
		log.log(Level.FINEST, "AMOUNT OF FOUNDED ROOT ELEMENTS: " + resultSize);
		results.setFindedItems(packTematicRootsResult(q, limit, startWith));
		return results;
	}*/
	private SearchResults getTematicSectionChilds(Integer limit,
			Integer startWith, ExtSearchReqParm params) {
		Long rootId = Long.parseLong(params.getFieldParam(
				"tematicSectionRootId").toString());
		String query = "select UNIV_DATA_UNIT_ID,UNIT_NAME,sovdocportal.getChildsCount(UNIV_DATA_UNIT_ID),complex_pack.get_date_intervals(UNIV_DATA_UNIT_ID) from UNIV_DATA_UNIT where ";
		query += "PORTAL_SECTION_ID IS NOT NULL and  PARENT_UNIT_ID = "
				+ rootId;
		int resultCount = getQueryResultSize(query);
		query += " order by NUMBER_NUMBER, NUMBER_TEXT, UNIT_NAME";
		log.info("TEMATIC SECTION CHILDS REQUEST");
		log.info("QUERY: " + query);
		log.info("RESULTS row count: " + resultCount);
		Query q = em.createNativeQuery(query);
		log.info("setMaxResult: " + limit);
		log.info("setFirstResult: " + startWith);
		q.setMaxResults(limit);
		q.setFirstResult(startWith);
		List<SearchResultShortItem> findedItems = new ArrayList<SearchResultShortItem>();
		ArrayList<Object> rawResult = new ArrayList<Object>(q.getResultList());
		for (Object o : rawResult) {
			Object[] row = (Object[]) o;
			Long univDataUnitId = Long.parseLong(row[0].toString());
			String univDataUnitName = row[1].toString();
			Long childCount = Long.parseLong(row[2].toString());
			String dates = "";
			if (row[3] != null) {
				dates = row[3].toString();
			}
			JsonObjectBuilder obj = Json.createObjectBuilder();
			obj.add("dataUnitId", univDataUnitId);
			obj.add("unitName", univDataUnitName);
			obj.add("goToCard", univDataUnitId);
			obj.add("goToChilds", childCount);
			obj.add("mathesTextBlock", dates);
			SearchResultShortItem item = new SearchResultShortItem();
			item.setDataUnitId(univDataUnitId);
			item.setDataUnitName(obj.build().toString());
			findedItems.add(item);
		}
		SearchResults results = new SearchResults();
		results.setFindedItems(findedItems);
		results.setTotalSearchResultsCount(resultCount);
		return results;
	}

	/**
	 * Получить значения тематического раздела портала с учётом иерархии
	 * тематических разделов АИС
	 *
	 * @param sectionId - ID тематического раздела портала
	 * @return иерархическая структура значений с учётом разделов
	 */
	public TematicRootSection getTematicSectionHierarchy(Long sectionId) {
		String query = "  with T as (select UDU1.UNIV_DATA_UNIT_ID, UDU1.UNIT_NAME,"
				+ "                    SOVDOCPORTAL.GETFULLARCHIVECODE(UDU1.UNIV_DATA_UNIT_ID) as CODE,"
				+ "                    SOVDOCPORTAL.GETCHILDSCOUNT(UDU1.UNIV_DATA_UNIT_ID) as CHILDREN_CNT,"
				+ "                    COMPLEX_PACK.GET_DATE_INTERVALS(UDU1.UNIV_DATA_UNIT_ID) as DATES,"
				+ "                    UDU1.ARCH_NUMBER_CODE,"
				+ "                    VCUH.PARENT_UNIT_ID as SECTION_ID,"
				+ "                    VCUH.PARENT_UNIT_NAME as SECTION_NAME,"
				+ "                    VCUH.PARENT_LEVEL as SECTION_LEVEL,"
				+ "                    UDU3.PARENT_UNIT_ID"
				+ "               from UNIV_DATA_UNIT UDU1"
				+ "              inner join DESCRIPTOR_VALUE DV"
				+ "                      on DV.VALUE_CODE = 'TOPIC_SECTION'"
				+ "              inner join DESCRIPTOR_GROUP DG"
				+ "                      on DG.GROUP_CODE = 'DESC_LEVEL'"
				+ "                     and DV.DESCRIPTOR_GROUP_ID = DG.DESCRIPTOR_GROUP_ID"
				+ "               left join UNIV_DATA_UNIT UDU2"
				+ "                      on UDU1.PARENT_UNIT_ID = UDU2.UNIV_DATA_UNIT_ID"
				+ "               left join V_COMPLEX_UNIT_HIERARCHY VCUH"
				+ "                      on UDU1.UNIV_DATA_UNIT_ID = VCUH.UNIV_DATA_UNIT_ID"
				+ "                     and VCUH.PARENT_LEVEL_ID = DV.DESCRIPTOR_VALUE_ID"
				+ "               left join UNIV_DATA_UNIT UDU3"
				+ "                      on VCUH.PARENT_UNIT_ID = UDU3.UNIV_DATA_UNIT_ID"
				+ "              where (nvl(UDU2.PORTAL_SECTION_ID, -1) != UDU1.PORTAL_SECTION_ID or UDU2.DESCRIPTION_LEVEL_ID = DV.DESCRIPTOR_VALUE_ID)"
				+ "                and UDU1.PORTAL_SECTION_ID = " + sectionId.toString()
				+ "                and UDU1.DESCRIPTION_LEVEL_ID != DV.DESCRIPTOR_VALUE_ID) "
				+ "select T.PARENT_UNIT_ID, T.SECTION_ID, T.SECTION_NAME,"
				+ "       decode(T.SECTION_LEVEL, T1.MIN_LEVEL, T.UNIV_DATA_UNIT_ID, null) as UNIV_DATA_UNIT_ID,"
				+ "       decode(T.SECTION_LEVEL, T1.MIN_LEVEL, T.UNIT_NAME, null) as UNIT_NAME,"
				+ "       decode(T.SECTION_LEVEL, T1.MIN_LEVEL, T.CODE, null) as CODE,"
				+ "       decode(T.SECTION_LEVEL, T1.MIN_LEVEL, T.CHILDREN_CNT, null) as CHILDREN_CNT,"
				+ "       decode(T.SECTION_LEVEL, T1.MIN_LEVEL, T.DATES, null) as DATES"
				+ "  from T"
				+ " inner join (select T.UNIV_DATA_UNIT_ID, min(T.SECTION_LEVEL) as MIN_LEVEL"
				+ "               from T"
				+ "              group by T.UNIV_DATA_UNIT_ID) T1"
				+ "         on T.UNIV_DATA_UNIT_ID = T1.UNIV_DATA_UNIT_ID"
				+ " order by PARENT_UNIT_ID nulls first, SECTION_NAME, ARCH_NUMBER_CODE";
		Query q = em.createNativeQuery(query, TematicRootResult.class);
		List<TematicRootResult> queryRes = q.getResultList();
		return getSectionDataFromQuery(null, queryRes);
	}

	private TematicRootSection getSectionDataFromQuery(Long parentId, List<TematicRootResult> queryRes) {
		TematicRootSection trs = new TematicRootSection();
		trs.setId(parentId);
		Set<Long> subsections = new HashSet<>();
		for (TematicRootResult trr : queryRes) {
			if (parentId == null ? trr.getSectionId() == null : parentId.equals(trr.getSectionId())) {
				trs.setSection(trr.getSectionName());
				if (trr.getUnivDataUnitId() != null) {
					TematicRootValue trv = new TematicRootValue();
					trv.setId(trr.getUnivDataUnitId());
					trv.setName(trr.getUnitName());
					trv.setArchCode(trr.getArchCode());
					trv.setChildrenCnt(trr.getChildrenCnt());
					trv.setDates(trr.getDates());
					trs.getValues().add(trv);
				}
			}
			if ((parentId == null ? trr.getParentUnitId() == null : parentId.equals(trr.getParentUnitId()))
					&& !subsections.contains(trr.getSectionId())) {
				subsections.add(trr.getSectionId());
				trs.getSections().add(getSectionDataFromQuery(trr.getSectionId(), queryRes));
			}
		}
		return trs;
	}

	/**
	 * Поисковый запрос, не содержащий в критериях поиска ключевых слов.
	 *
	 * @param parm
	 * @param limit
	 * @param startWith
	 * @return
	 */
	private SearchResults getExtSearchResultWithoutDesc(ExtSearchReqParm parm,
			Integer limit, Integer startWith) {
		String fastQuery = "select UNIV_DATA_UNIT_ID, UNIT_NAME, SOVDOCPORTAL.GETFULLARCHIVECODE(UNIV_DATA_UNIT_ID), "
				+ "INSERT_DATE,LAST_UPDATE_DATE, COMPLEX_PACK.GET_DATE_INTERVALS(UNIV_DATA_UNIT_ID),ROW_COUNT "
				+ "  from (select UNIV_DATA_UNIT_ID, UNIT_NAME, ROW_COUNT,INSERT_DATE,LAST_UPDATE_DATE"
				+ "          from (select UNIV_DATA_UNIT_ID, UNIT_NAME, ROW_COUNT, rownum as RN, ARCH_NUMBER_CODE, INSERT_DATE,LAST_UPDATE_DATE "
				+ "                  from (select UNIV_DATA_UNIT_ID, UNIT_NAME, "
				+ "                               count(*) over () as ROW_COUNT, INSERT_DATE,LAST_UPDATE_DATE,ARCH_NUMBER_CODE "
				+ "                          from UNIV_DATA_UNIT "
				+ "                          where PORTAL_SECTION_ID is not null ";

		for (String s : parm.fieldParamKeys()) {
			Object value = parm.getFieldParam(s);
			if ("name".equals(s)) {
				fastQuery += " AND ";
				fastQuery += getNameCondition((String) value);
				continue;
			}
			if ("archiveCode".equals(s)) {
				fastQuery += " AND ";
				fastQuery += getArchiveCodeCondition(parm);
				continue;
			}
			if (("fromDate").equals(s)) {
				try {
					String dateFromOracleFormat = getDateInOracleFormat(parm.getFieldParam("fromDate").toString());
					if (parm.getFieldParam("toDate") != null) {
						String dateToOracleFormat = getDateInOracleFormat(parm.getFieldParam("toDate").toString());
						fastQuery += " AND (sovdocportal.checkInterval(TO_DATE('" + dateFromOracleFormat + "','dd-mm-yyyy'),TO_DATE('" + dateToOracleFormat + "','dd-mm-yyyy'),UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID)=1) ";
					} else {
						fastQuery += "AND (sovdocportal.chekIntervalAonly(TO_DATE('" + dateFromOracleFormat + "','dd-mm-yyyy'),UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID)=1) ";
					}
				} catch (Exception e) {
					log.log(Level.SEVERE,
							"Could not parse into date value with pattern: yyyy-M-dd: "
							+ (String) parm.getFieldParam("date"));
				}

			}
			if ("elemType".equals(s)) {
				fastQuery += " AND ";
				fastQuery += " (UNIV_DATA_UNIT.UNIT_TYPE_ID="
						+ parm.getFieldParam("elemType") + ") ";
				continue;
			}
			if ("unitLang".equals(s)) {
				fastQuery += " AND ";
				String langPagm = getLangCondition(parm
						.getFieldParam("unitLang"));
				fastQuery += langPagm;
				continue;
			}
		}
		Integer maxw = startWith + limit;
		fastQuery += "                        order by ARCH_NUMBER_CODE) "
				+ "                  where rownum <= " + maxw + ") "
				+ "         where RN >" + startWith + ") ";
		Query q = em.createNativeQuery(fastQuery);
		SearchResults res = packSearchResult(q);
		return res;
	}

	private String addIntersectForDescValues(String query,
			ExtSearchReqParm params) {
		Object[] keys = params.descValueKeys().toArray();
		String resultBigQuery = "";

		if (keys.length == 1) {
			resultBigQuery += query;
			Long l = (Long) keys[0];
			resultBigQuery += " AND V_DESC_ALL_RELATIONS.DESCRIPTOR_VALUE_ID="
					+ params.getDescValue(l);
			resultBigQuery += " ";
		} else {
			resultBigQuery += query;
			String in = "AND V_DESC_ALL_RELATIONS.DESCRIPTOR_VALUE_ID in (";
			for (int i = 0; i < keys.length; i++) {
				Long l = (Long) keys[i];
				in += params.getDescValue(l);
				if (i != keys.length - 1) {
					in += ",";
				}
			}
			in += ")";
			resultBigQuery += in;
		}
		return resultBigQuery;
	}

	private String getDateInOracleFormat(String dateFromRequest)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
		Date dd = sdf.parse(dateFromRequest);
		sdf.applyPattern("dd-M-yyyy");
		String dateInDefOracleFormat = sdf.format(dd);
		return dateInDefOracleFormat;
	}

	/**
	 * Обработка запроса расширенного поиска содержащего как значения полей, так
	 * и значения указателя
	 *
	 * @param params
	 * @param limit
	 * @param startWith
	 * @return
	 */
	private SearchResults getExtSearchResult(ExtSearchReqParm params,
			Integer limit, Integer startWith) {

		String longSearchQueryRequest = "select UNIV_DATA_UNIT_ID, UNIT_NAME, SOVDOCPORTAL.GETFULLARCHIVECODE(UNIV_DATA_UNIT_ID),";
		longSearchQueryRequest += "INSERT_DATE,LAST_UPDATE_DATE, COMPLEX_PACK.GET_DATE_INTERVALS(UNIV_DATA_UNIT_ID),ROW_COUNT ";
		longSearchQueryRequest += "  from (select UNIV_DATA_UNIT_ID, UNIT_NAME, ROW_COUNT,INSERT_DATE,LAST_UPDATE_DATE,  rownum as RN ";
		longSearchQueryRequest += "           from (select UNIV_DATA_UNIT_ID, UNIT_NAME,  INSERT_DATE,LAST_UPDATE_DATE, count(*) over () as ROW_COUNT, sovdocportal.getFullArchiveCode(UNIV_DATA_UNIT_ID), ";
		longSearchQueryRequest += " complex_pack.get_date_intervals(UNIV_DATA_UNIT_ID) ";
		longSearchQueryRequest += "                  from (";
		longSearchQueryRequest += "select distinct UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID, UNIV_DATA_UNIT.UNIT_NAME,"
				+ " UNIV_DATA_UNIT.INSERT_DATE, UNIV_DATA_UNIT.LAST_UPDATE_DATE,";
		longSearchQueryRequest += "  UNIV_DATA_UNIT.ARCH_NUMBER_CODE from V_DESC_ALL_RELATIONS, UNIV_DESCRIPTOR, UNIV_DATA_UNIT where";
		longSearchQueryRequest += " V_DESC_ALL_RELATIONS.RELATED_VALUE_ID=UNIV_DESCRIPTOR.DESCRIPTOR_VALUE_ID1 and";
		longSearchQueryRequest += " UNIV_DESCRIPTOR.UNIV_DATA_UNIT_ID=UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID ";
		// name of UNIV_DATA_UNIT value, search with oracle text
		String nameParm = (String) params.getFieldParam("name");
		if (nameParm != null) {
			nameParm = textRequest(nameParm);
			if (params.fieldParamKeys().contains("name")) {
				longSearchQueryRequest += "and  contains(UNIV_DATA_UNIT.UNIT_NAME,'"
						+ nameParm + "',0)>0";
			}
		}
		// fondNumber is a part of ARCHIVE_CODE from UNIV_DATA_UNIT
		// if it exist, construct full archive code with user's data
		if (params.fieldParamKeys().contains("fondNumber")) {
			longSearchQueryRequest += " AND ";
			longSearchQueryRequest += getArchiveCodeCondition(params);
		}
		if (params.fieldParamKeys().contains("elemType")) {
			longSearchQueryRequest += " AND UNIT_TYPE_ID="
					+ params.getFieldParam("elemType") + " ";
		}

		if (params.fieldParamKeys().contains("fromDate")) {
			try {
				String dateFromOracleFormat = getDateInOracleFormat(params.getFieldParam("fromDate").toString());
				if (params.getFieldParam("toDate") != null) {
					String dateToOracleFormat = getDateInOracleFormat(params.getFieldParam("toDate").toString());
					longSearchQueryRequest += " AND (sovdocportal.checkInterval(TO_DATE('" + dateFromOracleFormat + "','dd-mm-yyyy'),TO_DATE('" + dateToOracleFormat + "','dd-mm-yyyy'),UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID)=1) ";
				} else {
					longSearchQueryRequest += "AND (sovdocportal.chekIntervalAonly(TO_DATE('" + dateFromOracleFormat + "','dd-mm-yyyy'),UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID)=1) ";
				}
			} catch (Exception e) {
				log.log(Level.SEVERE,
						"Could not parse into date value with pattern: yyyy-M-dd: "
						+ (String) params.getFieldParam("date"));
			}
		}
		if (params.fieldParamKeys().contains("unitLang")) {
			longSearchQueryRequest += " AND ";
			String langPagm = "(SELECT COUNT(*) from UNIV_DATA_LANGUAGE where UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID=UNIV_DATA_LANGUAGE.UNIV_DATA_UNIT_ID";
			langPagm += " AND UNIV_DATA_LANGUAGE.DOC_LANGUAGE_ID="
					+ params.getFieldParam("unitLang") + ")>0 ";
			longSearchQueryRequest += langPagm;
		}
		longSearchQueryRequest += " AND UNIV_DATA_UNIT.PORTAL_SECTION_ID IS NOT NULL ";
		longSearchQueryRequest = addIntersectForDescValues(
				longSearchQueryRequest, params);
		longSearchQueryRequest += " order by ARCH_NUMBER_CODE ";
		Integer maxw = startWith + limit;
		longSearchQueryRequest += ")";
		longSearchQueryRequest += ")                   where rownum <= " + maxw;
		longSearchQueryRequest += ")         where RN >" + startWith;
		Query q = em.createNativeQuery(longSearchQueryRequest);
		SearchResults sr = packSearchResult(q);
		return sr;
	}

	/**
	 * Запрос поиска в электронной библиотеке (без значений указателей в
	 * критериях)
	 *
	 * @param parm
	 * @param limit
	 * @param startWith
	 * @return
	 */
	private SearchResults getElibSearchResultWithotDesc(ExtSearchReqParm parm,
			Integer limit, Integer startWith) {
		log.log(Level.FINEST,
				"ELIBRARY SEARCH REQUEST WITHOUT DESCRIPTOR VALUE PARAMS");
		SearchResults rs = new SearchResults();
		String query = ""
				+ "select UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID, UNIV_DATA_UNIT.UNIT_NAME,"
				+ "sovdocportal.getAuthors(UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID),ARCH_EBOOK.PUBLISHER, ARCH_EBOOK.PUBLISHING_YEAR "
				+ ", ARCH_EBOOK.VOLUME1_INFO, "
				+ " (select concat(FILE_PATH,FILE_NAME) from univ_image_path where UNIV_DATA_UNIT_ID=UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID and IMAGE_CATEGORY_ID = "
				+ " (select descriptor_value_id from descriptor_value where value_code='BOOK_COVER'))"
				+ " from UNIV_DATA_UNIT,ARCH_EBOOK where "
				+ "UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID=ARCH_EBOOK.UNIV_DATA_UNIT_ID ";
		query += getElibFieldsCondition(parm);
		log.log(Level.FINEST, "QUERY: " + query);
		Integer s = getQueryResultSize(query);
		log.log(Level.FINEST, "Количество строк в результатах поиска: " + s);
		rs.setTotalSearchResultsCount(s);
		query += " ORDER BY UNIV_DATA_UNIT.UNIT_NAME";
		log.log(Level.FINEST, "QUERY: " + query);
		Query q = em.createNativeQuery(query);
		rs.setFindedItems(packElibSearchResults(q, limit, startWith));
		return rs;
	}

	private String getArchiveCodeCondition(ExtSearchReqParm parm) {
		String condition = "";
		List<String> conds = new ArrayList<>();

		Long archiveCodeS = null;
		if (!"null".equals(parm.getFieldParam("archiveCode"))
				&& !"".equals(parm.getFieldParam("archiveCode"))) {
			archiveCodeS = Long.parseLong(parm.getFieldParam("archiveCode")
					.toString());
		}

		String fondNum = parm.getFieldParam("fondNumber").toString();

		if (fondNum != null && !"".equals(fondNum) && !"null".equals(fondNum)) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 1) = " + Integer.parseInt(fondNum));
		}

		String fondPrefix = (String) parm.getFieldParam("fondPrefix");
		if (fondPrefix != null && !fondPrefix.isEmpty()) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 0) = '" + fondPrefix + "'");
		}

		String fondLitera = (String) parm.getFieldParam("fondLitera");
		if (fondLitera != null && !fondLitera.isEmpty()) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 2) = '" + fondLitera + "'");
		}

		String opNum = parm.getFieldParam("opisNumber").toString();
		if (opNum != null && !"".equals(opNum) && !"null".equals(opNum)) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 3) = " + Integer.parseInt(opNum));
		}
		String opisLitera = (String) parm.getFieldParam("opisLitera");
		if (opisLitera != null && !opisLitera.isEmpty()) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 4) = '" + opisLitera + "'");
		}

		String delNum = parm.getFieldParam("deloNumber").toString();
		if (parm.getFieldParam("deloNumber") != null && !"".equals(delNum)
				&& !"null".equals(delNum)) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 5) = " + Integer.parseInt(delNum));
		}

		String deloLitera = (String) parm.getFieldParam("deloLitera");
		if (deloLitera != null && !deloLitera.isEmpty()) {
			conds.add("complex_pack.extract_arch_code_part(UNIV_DATA_UNIT.ARCH_NUMBER_CODE, 6) = '" + deloLitera + "'");
		}

//		String constructedArchCode = fillArchNumberCode(fondNumber, fondLitera,
//				opisNumber, opisLitera, deloNumber, deloLitera);
		boolean isArchNumberNotNull = false;

		if (!conds.isEmpty()) {
//			condition += "  (lower(UNIV_DATA_UNIT.ARCH_NUMBER_CODE) like lower('"
//					+ constructedArchCode + "%'))";
			condition = " (";
			int last = conds.size() - 1;
			for (int i = 0; i < last + 1; ++i) {
				condition += conds.get(i);
				if (i < last) {
					condition += " AND ";
				}
			}
			condition += ")";
			isArchNumberNotNull = true;
		}
		if (!"".equals(archiveCodeS) && archiveCodeS != null) {
			if (isArchNumberNotNull) {
				condition += " AND ";
			}
			condition += " UNIV_DATA_UNIT.RES_OWNER_ID=" + archiveCodeS;
		}
		return condition;
	}

	private String getLangCondition(Object id) {
		String langPagm = " (SELECT COUNT(*) from UNIV_DATA_LANGUAGE where UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID=UNIV_DATA_LANGUAGE.UNIV_DATA_UNIT_ID";
		langPagm += " AND UNIV_DATA_LANGUAGE.DOC_LANGUAGE_ID=" + id + ")>0 ";
		return langPagm;
	}

	private String getNameCondition(String nameValue) {
		String cond = " contains(UNIV_DATA_UNIT.UNIT_NAME,'"
				+ textRequest(nameValue) + "')>0 ";
		return cond;
	}

	private String getAuthorCondition(Object authorId) {
		String sqlCondition = ""
				+ "(select count(*) from ARCH_EBOOK_AUTHOR where "
				+ "ARCH_EBOOK_AUTHOR.UNIV_DATA_UNIT_ID=UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID AND "
				+ "ARCH_EBOOK_AUTHOR.AUTHOR_ID=" + authorId.toString() + " )>0";
		return sqlCondition;
	}

	private String textRequest(String src) {
		if (src == null || src.trim().isEmpty()) {
			return null;
		}

		String textDigits = "";
		for (Character ch : src.toCharArray()) {

			if (Character.isLetterOrDigit(ch) || ch.equals(' ') || ch.equals('№') || ch.equals('-') || ch.equals('_') || ch.equals('/')) {
				if (ch.equals('-') || ch.equals('_') || ch.equals('/')) {
					textDigits = textDigits + "\\" + ch;
				} else {
					textDigits = textDigits + ch;
				}
			} else {
				textDigits = textDigits + " ";
			}
		}
		src = textDigits;

		List<String> words = Lists.newArrayList(src.split(" "));
		for (int i = 0;
				i < words.size();
				i++) {
			if (words.get(i).trim().equals("")) {
				words.remove(words.get(i));
				i--;
			}
		}

		String res = null;
		// boolean quoted = false;
		for (String word : words) {
			word = word.trim();

			String part = word;
			if (word.length() >= 3) {
				part = "$" + part + "%";
			}

			if (res == null) {
				res = part;
			} else if (word.length() > 0) {
				res += " AND " + part;
			}
		}
		return res;
	}

	/**
	 * Формирование условия where для параметров - полей, сформированных
	 * пользователем в разделе "Электронная библиотека"
	 *
	 * @param parm
	 * @return
	 */
	private String getElibFieldsCondition(ExtSearchReqParm parm) {
		String condition = "";
		for (String s : parm.fieldParamKeys()) {
			String value = parm.getFieldParam(s).toString();

			if ("name".equals(s)) {
				condition += " and ";
				String txtRequest = textRequest(value);
				log.info("Prepared Oracle Text request: " + txtRequest);
				condition += getNameCondition(value);
				continue;
			}
			if ("unitLang".equals(s)) {
				condition += " and ";
				condition += getLangCondition(parm.getFieldParam(s));
				continue;
			}
			if ("author".equals(s)) {
				condition += " and ";
				condition += getAuthorCondition(parm.getFieldParam(s));
				continue;
			}

			if ("publisher".equals(s)) {
				condition += " and ";
				condition += " contains(ARCH_EBOOK.PUBLISHER, '";
				condition += textRequest(value);
				condition += "',0)>0 ";
			}

			if ("publYear".equals(s)) {
				condition += " and ";
				condition += " " + elibAttribs.get(s) + "='"
						+ parm.getFieldParam(s) + "' ";
				continue;
			}

			if ("isbn".equals(s)) {
				condition += " and ";
				condition += " " + "complex_pack.getnumfromstr(" + elibAttribs.get(s) + ")" + "='"
						+ parm.getFieldParam(s).toString().replaceAll("\\D+", "") + "' ";
				continue;
			}

			if ("seriesInfo".equals(s)) {
				condition += " and ";
				condition += " " + elibAttribs.get(s) + "="
						+ parm.getFieldParam(s) + " ";
				continue;
			}
		}
		return condition;
	}

	private SearchResults getElibSearchResult(ExtSearchReqParm params,
			Integer limit, Integer startWith) {
		String elibQuery = ""
				+ "select distinct UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID, UNIV_DATA_UNIT.UNIT_NAME,"
				+ "sovdocportal.getAuthors(UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID), ARCH_EBOOK.PUBLISHER, ARCH_EBOOK.PUBLISHING_YEAR, ARCH_EBOOK.VOLUME1_INFO, "
				+ " (select concat(FILE_PATH,FILE_NAME) from univ_image_path where UNIV_DATA_UNIT_ID=UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID and IMAGE_CATEGORY_ID = "
				+ " (select descriptor_value_id from descriptor_value where value_code='BOOK_COVER'))"
				+ "from V_DESC_ALL_RELATIONS, UNIV_DESCRIPTOR, UNIV_DATA_UNIT,ARCH_EBOOK where "
				+ "V_DESC_ALL_RELATIONS.RELATED_VALUE_ID=UNIV_DESCRIPTOR.DESCRIPTOR_VALUE_ID1 and "
				+ "UNIV_DESCRIPTOR.UNIV_DATA_UNIT_ID=UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID and "
				+ "UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID=ARCH_EBOOK.UNIV_DATA_UNIT_ID ";
		elibQuery += getElibFieldsCondition(params);
		elibQuery = addIntersectForDescValues(elibQuery, params);
		log.log(Level.FINEST, "ELIBRARY SEARCH REQUEST");
		elibQuery += " ORDER BY UNIV_DATA_UNIT.UNIT_NAME";
		log.log(Level.FINEST, "QUERY: " + elibQuery);
		SearchResults searchResult = new SearchResults();
		try {
			Integer s = getQueryResultSize(elibQuery);
			Query q = em.createNativeQuery(elibQuery);
			searchResult.setTotalSearchResultsCount(s);
			searchResult.setFindedItems(packElibSearchResults(q, limit,
					startWith));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return searchResult;
	}

	public SearchResults getExtendedSearchResult(ExtSearchReqParm params,
			Integer limit, Integer startWith) {
		log.log(Level.FINEST, "EXTENDED SEARCH REQUEST:");
		for (String s : params.fieldParamKeys()) {
			log.log(Level.FINEST, "FIELD PARAM: {0}:{1}", new Object[]{s, params.getFieldParam(s)});
		}
		for (Long l : params.descValueKeys()) {
			log.log(Level.FINEST, "DESC VALUE ID: {0}", params.getDescValue(l));
		}
		/*if (isTematicSectionRootsRequest(params)) {
			return getTematicSectionRoots(startWith, limit, params);
		}*/
		if (isTematicSectionChildsRequest(params)) {
			return getTematicSectionChilds(startWith, limit, params);
		}
		if (isElibRequest(params)) {
			if (isElibGetAllQuery(params)) {
				return getAllElibBooks(limit, startWith);
			}
			if (params.isHasDescValueParams()) {
				return getElibSearchResult(params, limit, startWith);
			} else {
				return getElibSearchResultWithotDesc(params, limit, startWith);
			}
		} else if (params.isHasDescValueParams()) {
			return getExtSearchResult(params, limit, startWith);
		} else {
			return getExtSearchResultWithoutDesc(params, limit, startWith);
		}
	}

	public SearchResults getSearchResult(String query, Integer resultCount,
			Integer startWith) {
		query = textRequest(query);
		String stermQuery = query;
		Integer maxw = startWith + resultCount;
		String fastQuery = "select UNIV_DATA_UNIT_ID, UNIT_NAME, SOVDOCPORTAL.GETFULLARCHIVECODE(UNIV_DATA_UNIT_ID), "
				+ "INSERT_DATE,LAST_UPDATE_DATE, COMPLEX_PACK.GET_DATE_INTERVALS(UNIV_DATA_UNIT_ID),ROW_COUNT "
				+ "  from (select UNIV_DATA_UNIT_ID, UNIT_NAME, ROW_COUNT,INSERT_DATE,LAST_UPDATE_DATE "
				+ "          from (select UNIV_DATA_UNIT_ID, UNIT_NAME, ROW_COUNT, rownum as RN, INSERT_DATE,LAST_UPDATE_DATE "
				+ "                  from (select UNIV_DATA_UNIT_ID, UNIT_NAME, "
				+ "                               count(*) over () as ROW_COUNT, INSERT_DATE,LAST_UPDATE_DATE,ARCH_NUMBER_CODE "
				+ "                          from UNIV_DATA_UNIT "
				+ "                         where PORTAL_SECTION_ID is not null "
				+ "                           and  contains(CONTENTS,'"
				+ stermQuery
				+ "',0)>0 "
				+ "                         order by ARCH_NUMBER_CODE) "
				+ "                  where rownum <= "
				+ maxw
				+ ") "
				+ "         where RN >" + startWith + ") ";
		Query q = em.createNativeQuery(fastQuery);
		SearchResults searchResult = packSearchResult(q);
		return searchResult;
	}

	public String getFullElementInfo(Long elId) {
		UnivDataUnit du = (UnivDataUnit) em.find(UnivDataUnit.class, elId);
		String duData = "<h1>";
		duData += du.getUnitName();
		duData += "</h1>";
		return duData;
	}

	public List<ImageItemInfo> getImagesInfo(Long dataUnitId, Integer limit, Integer start) throws Exception {
		List<ImageItemInfo> l = new ArrayList<ImageItemInfo>();
		UnivDataUnit unit = (UnivDataUnit) em.find(UnivDataUnit.class,
				dataUnitId);
		DescriptorValue previewCategory = hh.getDescValueByCode("PREVIEW");
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivImagePath> criteriaQuery = cb
				.createQuery(UnivImagePath.class);
		Root<UnivImagePath> root = criteriaQuery.from(UnivImagePath.class);
		criteriaQuery.select(root);
		Predicate r = cb.equal(root.get("dataUnit"), unit);
		Predicate r2 = cb.equal(root.get("category"), previewCategory);
		Predicate and = cb.and(r, r2);
		criteriaQuery.where(and).orderBy(cb.asc(root.get("sortOrder")));
		Query q = em.createQuery(criteriaQuery);
		q.setFirstResult(start).setMaxResults(limit);
		for (Object upo : q.getResultList()) {
			UnivImagePath up = (UnivImagePath) upo;
			ImageItemInfo iii = new ImageItemInfo();
			iii.setFileFormat(up.getDvFormat().getFullValue());
			iii.setHasPreview(false);
			iii.setFileName(up.getCaption());
			iii.setId(up.getId());
			iii.setOrderId(up.getSortOrder());
			iii.setPreviewData(new byte[0]);
			try {
				String fPath = up.getFilePath() + "\\" + up.getFileName();
				log.log(Level.FINEST, "PACK FILE FROM STORAGE: " + fPath);
				// Картинки более не копируем. Поле теперь используется для
				// передачи реального пути
				byte[] previewData = fPath.getBytes("UTF-8");
				iii.setPreviewData(previewData);
			} catch (Exception e) {
				if (e instanceof FileNotFoundException) {
					log.log(Level.FINEST, e.getLocalizedMessage());
				} else {
					e.printStackTrace();
				}
			}
			l.add(iii);
		}
		return l;
	}

	@Deprecated
	public DescriptorValue getDescValueByCode(String code) {
		return hh.getDescValueByCode(code);
	}

	public JsonObject getSectionInfo(Long univDataUnit) {
		String sql = "select portal_section_id, "
				+ "(select full_value from descriptor_value where descriptor_value_id = portal_section_id) as sectionName,"
				+ "unit_name, sovdocportal.getFullArchiveCode("
				+ univDataUnit
				+ ") as archiveCode from univ_data_unit where univ_data_unit_id = "
				+ univDataUnit;
		Query sq = em.createNativeQuery(sql);
		Object[] row = (Object[]) sq.getResultList().get(0);
		Long sectionId = ((BigDecimal) row[0]).longValue();
		String sectionName = row[1].toString();
		String unitName = row[2].toString();
		JsonObjectBuilder jo = Json.createObjectBuilder();
		jo.add("sectionId", sectionId);
		jo.add("sectionName", sectionName);
		jo.add("unitName", unitName);
		jo.add("unitArchiveCode", row[3].toString());
		return jo.build();
	}

	public boolean isCardOfKomintern(Long uvId) {

		if (uvId == null || uvId.equals(0L)) {
			return false;
		}

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivDataUnit> cq = cb.createQuery(UnivDataUnit.class);
		Root<UnivDataUnit> root = cq.from(UnivDataUnit.class);
		cq.select(root).where(cb.equal(root.get("univDataUnitId"), uvId));
		return em.createQuery(cq).getSingleResult().isFullAccessToImages();
	}

}
