package ru.insoft.sdportal.ejb;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import ru.insoft.sdportal.models.ui.TematicSectionEntry;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class TematicSections {

	private Logger log;

	private String sectionsCache;
	@Inject
	private EntityManager em;
	
	@EJB
	private HibernateHelper hh;

	@EJB
	private SysUtils su;

	public TematicSections() {
		log = Logger.getLogger(getClass().getCanonicalName());
		sectionsCache = "";
	}

	public String getTematicSectionTitle(Long sectionId) {
		DescriptorValue dv = (DescriptorValue)em.find(DescriptorValue.class, sectionId);
		String fullValue = dv.getFullValue();
		return fullValue;

	}

	public String getTematicSections2() throws Exception {
		if ("".equals(sectionsCache)) {
			String sectionsDescriptorGroupCode = "PORTAL_SECTION";
			String query = "select DESCRIPTOR_VALUE_ID, FULL_VALUE, ";
			query += " (select count(*) from UNIV_DATA_UNIT where PORTAL_SECTION_ID=DESCRIPTOR_VALUE_ID) ";
			query += " from DESCRIPTOR_VALUE where DESCRIPTOR_GROUP_ID = ";
			query += " (select DESCRIPTOR_GROUP_ID from DESCRIPTOR_GROUP where GROUP_CODE = '"
					+ sectionsDescriptorGroupCode + "' )";
			query += " order by SORT_ORDER";
			log.log(Level.FINEST, "TEMATIC SECTIONS REQUEST");
			log.log(Level.FINEST, "query: " + query);
			Query q = em.createNativeQuery(query);
			List<TematicSectionEntry> sectionsList = new ArrayList<TematicSectionEntry>();
			for (Object o : q.getResultList()) {
				TematicSectionEntry entry = new TematicSectionEntry();
				Object[] rawRow = (Object[]) o;
				Long l = Long.parseLong(rawRow[0].toString());
				String name = rawRow[1].toString();
				Long count = Long.parseLong(rawRow[2].toString());
				entry.setId(l);
				entry.setName(name);
				boolean b = count != 0l;
				entry.setHasRoots(b);
				sectionsList.add(entry);
			}
			Template tp = Velocity
					.getTemplate("tematicsections.html");
			VelocityContext ctx = new VelocityContext();
			ctx.put("sectionList", sectionsList);
			StringWriter stW = new StringWriter();
			tp.merge(ctx, stW);
			sectionsCache = stW.toString();
		}
		return sectionsCache;
	}
	
	public JsonObject handleTematicSectionRootParamsRequest2(Long rootId){
		String query = "select UNIV_DATA_UNIT_ID, UNIT_NAME, PORTAL_SECTION_ID,";
		query +=" (select FULL_VALUE from DESCRIPTOR_VALUE where DESCRIPTOR_VALUE_ID = PORTAL_SECTION_ID), ";
		query+=" sovdocportal.getFullArchiveCode(UNIV_DATA_UNIT_ID) ";
		query +="from UNIV_DATA_UNIT where UNIV_DATA_UNIT_ID = "+rootId;
		log.log(Level.FINEST,"TEMATIC SECTION ROOT PARAMS REQUEST");
		log.log(Level.FINEST,"query: "+query);
		Query q = em.createNativeQuery(query);
		Object [] row =(Object[]) q.getResultList().get(0);
		JsonObjectBuilder obj = Json.createObjectBuilder();
		obj.add("sectionTitle", row[3].toString());
		obj.add("sectionId", row[2].toString());
		obj.add("rootTitle", row[1].toString());
		obj.add("rootId", row[0].toString());
		obj.add("rootArchiveCode", row[4].toString());
		return obj.build();
	}

}
