package ru.insoft.sdportal.ejb;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import ru.insoft.sdportal.logic.MainPageRenderer;
import ru.insoft.sdportal.logic.NewsRenderer;
import ru.insoft.sdportal.models.L18nWebPage;
import ru.insoft.sdportal.models.WebPage;
import ru.insoft.sovdoc.model.desc.table.DescLanguage;
import ru.insoft.sovdoc.model.desc.table.DescValueInternational;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Stateless
public class L10nUtils {

	@EJB
	private HibernateHelper hh;
	
	@Inject
	private EntityManager em;
	
	// map for all of DescLanguage objects with localeCode as key
	private static HashMap<String, DescLanguage> langMap;
	// map for all of DescLanguage objects with languageCode as key
	private static HashMap<String, DescLanguage> langMapSys;
	private static HashMap<String, HashMap<String, String>> portalConstants;
	// localization constants, used on the server
	private static HashMap<String, HashMap<String, String>> portalConstsCards;
	private MainPageRenderer mpr;
	private NewsRenderer nr;
	public static final String userLangCode = "ru_RU";
	// localized chapter's names for portal
	private static final String defaultLocale = "ru_RU";
	
	private HashMap<String,L18nWebPage> pages;
	@EJB
	private SysUtils su;
	
	private Logger log;

	public L10nUtils() {
		log = Logger.getLogger(getClass().getCanonicalName());
		pages=new HashMap<>();
                Properties p = new Properties();
                p.setProperty("resource.loader", "sdportal");
                p.setProperty("sdportal.resource.loader.class", "ru.insoft.sdportal.PrefixedClasspathResourceLoader");
                p.setProperty("sdportal.resource.loader.prefix", "/META-INF/templates/");
                p.setProperty("input.encoding","UTF-8");
                p.setProperty("output.encoding","UTF-8");
                Velocity.init(p);
	}
	
	public MainPageRenderer getMainPageRenderer(){
		if (mpr==null){
			mpr = new MainPageRenderer(this, hh, su,em);
		}
		return mpr;
	}
	
	public NewsRenderer getNewsRenderer(){
		if (nr==null){
			nr = new NewsRenderer(this, hh,em);
		}
		return nr;
	}
	
	public String getDefaultLocale() {
		return defaultLocale;
	}
	
	public DescLanguage getDefaultLocaleDl(){
		return getDescLangByBrowserCode(defaultLocale);
	}

	private HashMap<String, HashMap<String, String>> getLocalizationConstants(
			String groupCode) {
		HashMap<String, HashMap<String, String>> result = new HashMap<String, HashMap<String, String>>();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroup> criteriaQuery2 = cb.createQuery(DescriptorGroup.class);
		Root<DescriptorGroup> root = criteriaQuery2.from(DescriptorGroup.class);
		criteriaQuery2.where(cb.equal(root.get("groupCode"), groupCode));
		Query q2 = em.createQuery(criteriaQuery2);
		if (q2.getResultList().size()== 0) {
			String err = "DESCRIPTOR GROUP WITH CODE: " + groupCode
					+ " NOT FOUND!";
			throw new IllegalStateException(err);
		}
		DescriptorGroup constValuesGroup = (DescriptorGroup) q2.getResultList().get(0);
		CriteriaQuery<DescriptorValue> criteriaQuery3 = cb.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root3 = criteriaQuery3.from(DescriptorValue.class);
		criteriaQuery3.where(cb.equal(root3.get("descriptorGroup"), constValuesGroup));
		Query q3 = em.createQuery(criteriaQuery3);
		for (String s : langMap.keySet()) {
			result.put(s, new HashMap<String, String>());
		}
		for (Object o : q3.getResultList()) {
			DescriptorValue val = (DescriptorValue) o;
			Object[] values = val.getMultilangList().toArray();
			if (values.length != 0) {
				Vector<String> existValuesForLocales = new Vector<String>();
				for (Object dvlo : values) {
					DescValueInternational dvl = (DescValueInternational)dvlo;
					String localeCode = langMapSys.get(dvl.getLanguageCode())
							.getLocaleCode();
					HashMap<String, String> sameLangMap = result
							.get(localeCode);
					sameLangMap.put(val.getValueCode(),
							dvl.getInternationalFullValue());
					existValuesForLocales.add(localeCode);
				}
				if (val.getMultilangList().size() != langMap.keySet().size()) {
					for (DescLanguage l : langMap.values()) {
						if (!existValuesForLocales.contains(l.getLocaleCode())) {
							HashMap<String, String> sameLangMap = result.get(l
									.getLocaleCode());
							sameLangMap.put(val.getValueCode(),
									val.getFullValue());
						}
					}
				}
			} else {
				for (String s : langMap.keySet()) {
					String localeCode = langMap.get(s).getLocaleCode();
					HashMap<String, String> sameLangMap = result
							.get(localeCode);
					sameLangMap.put(val.getValueCode(), val.getFullValue());
				}
			}
		}
		return result;
	}
	@PostConstruct
	public void initMaps() {
		langMap = new HashMap<String, DescLanguage>();
		langMapSys = new HashMap<String, DescLanguage>();
		CriteriaQuery<DescLanguage> criteriaQuery = em.getCriteriaBuilder().createQuery(DescLanguage.class);
		Root<DescLanguage> root = criteriaQuery.from(DescLanguage.class);
		Query q = em.createQuery(criteriaQuery);
		for (Object o : q.getResultList().toArray()) {
			DescLanguage dl = (DescLanguage) o;
			langMap.put(dl.getLocaleCode(), dl);
			langMapSys.put(dl.getLanguageCode(), dl);
		}
		portalConstants = getLocalizationConstants("l10n-constants");
		portalConstsCards = getLocalizationConstants("l10n-contstants-cards");
	}

	/**
	 * Get constants for client's JavaScript code
	 * 
	 * @param browserLocaleCode
	 * @return
	 */
	public HashMap<String, String> getLocalizedConstants(
			String browserLocaleCode) {
		return portalConstants.get(browserLocaleCode);
	}

	/**
	 * Get constants for data unit's views (generated on server, see
	 * SearchHandler.java)
	 * 
	 * @param browserLocaleCode
	 * @return
	 */
	public HashMap<String, String> getCardLocalizedConstsnts(
			String browserLocaleCode) {
		return portalConstsCards.get(browserLocaleCode);
	}

	public WebPage getPageByPageCode(String pageCode) {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<WebPage> criteriaQuery = cb.createQuery(WebPage.class);
			Root<WebPage> root = criteriaQuery.from(WebPage.class);
			criteriaQuery.where(cb.equal(root.get("pageCode"), pageCode));
			Query q = em.createQuery(criteriaQuery);
			List<Object> result = q.getResultList();
			if (result.size() == 0) {
				String errMsg = "PAGE WITH CODE: " + pageCode + " NOT FOUND!";
				throw new IllegalStateException(errMsg);
			}
			Object o = result.get(0);

			return (WebPage) o;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String chaptersHtml = "";
        private String footerHtml = "";
        private HashMap<String, String> chaptersNames;
	
	private String getChaptersHtml(String browserLocaleCode,String templateFilePath){
			Template chaptersTemplate = Velocity
					.getTemplate(templateFilePath);
			String resultHtml="";
			chaptersNames = getChapterNames(browserLocaleCode);
			VelocityContext vContext = new VelocityContext();
			for (String s : chaptersNames.keySet()) {
				vContext.put(s, chaptersNames.get(s));
			}
			StringWriter stW = new StringWriter();
			chaptersTemplate.merge(vContext, stW);
			resultHtml = stW.toString();
			return resultHtml;
	}
	
	public String getMainMenuHtml(String browserLocaleCode){
		if ("".equals(chaptersHtml)){
			chaptersHtml = getChaptersHtml(browserLocaleCode, "chaptersmenu.html");
		}
		return chaptersHtml;
	}
        
        public String getFooterHtml(String browserLocaleCode)
        {
            if ("".equals(footerHtml))
            {
                footerHtml = getChaptersHtml(browserLocaleCode, "pagefooter.html");
            }
            return footerHtml;
        }
	
	/**
	 * increasing performance
	 * 
	 * @param browserLocaleCode
	 * @return
	 */
	private HashMap<String, String> getChapterNames(String browserLocaleCode) 
        {
            if (chaptersNames == null)
            {
		String systemLangCode = getDescLangByBrowserCode(browserLocaleCode)
				.getLanguageCode();
		chaptersNames = new HashMap<String, String>();
		String query = ""
				+ "select PAGE_CODE, "
				+ "nvl((select WEB_PAGES_LOCALIZED.NAME from web_pages_localized "
				+ "where WEB_PAGES_LOCALIZED.PAGE_ID=WEB_PAGES.PAGE_ID and WEB_PAGES_LOCALIZED.LANGUAGE_CODE='"
				+ systemLangCode
				+ "')," 
				+ "(select WEB_PAGES_LOCALIZED.NAME from web_pages_localized "
				+ "where WEB_PAGES_LOCALIZED.PAGE_ID=WEB_PAGES.PAGE_ID and WEB_PAGES_LOCALIZED.LANGUAGE_CODE='RUS')"
				+")"
				+ " as name from web_pages where WEB_PAGES.PAGE_CODE IS NOT NULL";

		Query q = em.createNativeQuery(query);
		for (Object o : q.getResultList().toArray()) {
			Object[] a = (Object[]) o;
			String code = (String) a[0];
			String parm = (String) a[1];
			chaptersNames.put(code, parm);
		}
            }
            return chaptersNames;
	}

	/**
	 * Get localized page
	 * 
	 * @param browserLocale
	 *            request.getLocale().toString()
	 * @param pageCode
	 *            pageCode (chapter's name or page_code from db)
	 * @return L18nWebPage object
	 */
	public L18nWebPage getLocalizedPage(String browserLocale, String pageCode) {
		log.log(Level.FINEST, "REQUEST PAGE: " + pageCode + " LOCALE: "
				+ browserLocale);
		if (pages.get(pageCode) == null) {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<WebPage> criteriaQuery = cb.createQuery(WebPage.class);
			Root<WebPage> root = criteriaQuery.from(WebPage.class);
			criteriaQuery.where(cb.equal(root.get("pageCode"), pageCode));
			Query cr = em.createQuery(criteriaQuery);
			List<Object> result = cr.getResultList();
			if (cr.getResultList().size() == 0) {
				String errMsg = "PAGE WITH CODE: " + pageCode + " NOT FOUND!";
				throw new IllegalStateException(errMsg);
			}
			Object o = result.get(0);
			WebPage p = (WebPage) o;
			if (browserLocale == null) {
				String errMsg = "browserLocale can not be null!\n cannot define user's language";
				log.log(Level.SEVERE, errMsg);
				throw new IllegalArgumentException(errMsg);
			}
			
			
			CriteriaQuery<L18nWebPage> cr2 = cb.createQuery(L18nWebPage.class);
			Root<L18nWebPage> root2 = cr2.from(L18nWebPage.class);
			DescLanguage userLang = getDescLangByBrowserCode(browserLocale);
			cr2.where(cb.and(cb.equal(root2.get("lang"), userLang),cb.equal(root2.get("page"), p)));
			Query q2 = em.createQuery(cr2);

			// it is must be one too!
			List<L18nWebPage> result2 = q2.getResultList();
			if (result2.size() == 0) {
				String errNotFound = "PAGE WITH CODE: " + pageCode
						+ " AND LANGUAGE: " + browserLocale + " NOT FOUND";
				log.log(Level.FINEST, errNotFound);
				// try load page on default language - russian
				return getLocalizedPage(defaultLocale, pageCode);
				// throw new IllegalStateException(errNotFound);
				// return getLocalizedPage(defaultLocale, pageCode);
			}
			L18nWebPage localizedMainPage = (L18nWebPage) result2.get(0);

			pages.put(pageCode, localizedMainPage);
		}
		return pages.get(pageCode);
	}

	/**
	 * retrieve DescLanguage instance for user's browser's locale code
	 * 
	 * @param browserCode
	 *            usually result of: request.getLocale().toString()
	 * @return
	 */
	public DescLanguage getDescLangByBrowserCode(String browserCode) {
		return langMap.get(browserCode);
	}

	/**
	 * Get all available languages, supported by system
	 * 
	 * @return
	 */
	public Vector<DescLanguage> getAvailableSystemLangs() {
		Vector<DescLanguage> result = new Vector<DescLanguage>(langMap.values());
		return result;
	}

	/**
	 * Get list of supported locale names
	 * 
	 * @return
	 */
	public Vector<String> getAvailableSystemLocales() {
		return new Vector<String>(langMap.keySet());
	}

	/**
	 * Check: is locale supported by system.
	 * 
	 * @param localeCode
	 *            locale code from browser
	 * @return
	 */
	public Boolean isLocaleSupportedBySystem(String localeCode) {
		return langMap.get(localeCode) != null;
	}
	
	/**
	 * Get localized value for Descriptor value
	 * 
	 * @v to language
	 * @systemLangCode
	 * 
	 * @param v
	 *            descriptor value
	 * @param systemLangCode
	 *            system's language code NOT BROWSER LOCALE CODE!
	 * @return DescValueInternational object or null, if localization does not
	 *         exist
	 */
	public DescValueInternational getL10nValueIfExist(DescriptorValue v,
			String systemLangCode) {
		if (v == null) {
			throw new IllegalArgumentException("value can not be null here");
		}
		if (systemLangCode == "RUS") {
			// Default system's language. For russian, values stored into
			// DescriptorValue's fields
			return null;
		}
		boolean isLocalizedValueExist = false;
		int i = 0;
		// http://www.sql.ru/forum/actualthread.aspx?tid=591393 fix of crashing
		// with lazy loading for detached object
		// https://forum.hibernate.org/viewtopic.php?f=1&t=992697

			em.refresh(v);
			
			int l10nValuesCount = v.getMultilangList().size();
			if (l10nValuesCount == 0) {
				return null;
			}
			while ((!isLocalizedValueExist) && (i < l10nValuesCount)) {
				DescValueInternational curIntValue = v.getMultilangList()
						.get(i);
				isLocalizedValueExist = (systemLangCode.equals(curIntValue
						.getLanguageCode()));
				i++;
			}
		
		if (isLocalizedValueExist) {
			return v.getMultilangList().get(i - 1);
		} else {
			return null;
		}
	}
}
