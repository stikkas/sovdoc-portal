package ru.insoft.sdportal.ejb;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import ru.insoft.archive.extcommons.ejb.JsonTools;
import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.sdportal.logic.mailing.MailSender;
import ru.insoft.sdportal.logic.mailing.PostMessage;
import ru.insoft.sdportal.models.WebUser;
import ru.insoft.sdportal.models.ui.UserInfoResponse;
import ru.insoft.sdportal.models.ui.UserRegOkResponse;
import ru.insoft.sovdoc.model.adm.table.AdmUser;

@Stateless
public class UserManager {

	private Logger log;
	
	@EJB
	private SovdocSearchImpl searchImpl;

	@EJB
	private HibernateHelper hh;

	@EJB
	private L10nUtils l10nUtils;

	@EJB
	private SysUtils su;

	@Inject
	private EntityManager em;

	@EJB
	private JsonTools jsonTools;

	public UserManager() {
		this.log = Logger.getLogger(this.getClass().getCanonicalName());
	}

	/**
	 * Проверка корректности пароля пользователя
	 * 
	 * @param admUserId
	 *            id пользователя (таблица AdmUser)
	 * @param pass
	 *            пароль (в незашифрованном виде)
	 * @return
	 */
	public boolean isUserPasswordCorrect(Long admUserId, String pass) {
		String encryptedPass = su.getEncryptedPass(pass);
		boolean result = false;
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> cr = cb.createQuery(AdmUser.class);
		Root<AdmUser> rootAdmUser = cr.from(AdmUser.class);
		cr.where(cb.and(cb.equal(rootAdmUser.get("userId"), admUserId),
				cb.equal(rootAdmUser.get("password"), encryptedPass)));
		Query q = em.createQuery(cr);
		int amount = q.getResultList().size();
		log.log(Level.FINEST, "AMOUNT OF USERS WITH id: " + admUserId
				+ " AND PASSWORD: " + encryptedPass + " IS " + amount);
		result = amount == 1;
		return result;
	}

	/**
	 * Проверка, свободен ли данный логин для регистрации в системе
	 * 
	 * @param login
	 *            логин
	 * @return
	 * @throws Exception
	 */
	public boolean isLoginFree(String login) throws Exception {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> cr = cb.createQuery(AdmUser.class);
		Root<AdmUser> root = cr.from(AdmUser.class);
		cr.where(cb.equal(root.get("login"), login));
		Query q = em.createQuery(cr);
		boolean result = (q.getResultList().size() == 0);
		return result;
	}

	/**
	 * Изменение пароля пользователя
	 * 
	 * @param userId
	 *            id пользователя
	 * @param oldPass
	 *            старый пароль
	 * @param newPass
	 *            новый пароль
	 * @return
	 * @throws Exception
	 */
	public JsonObject changePasswd(Long userId, String oldPass, String newPass)
			throws Exception {
		FailMessage fm = null;
		if (!isUserPasswordCorrect(userId, oldPass)) {
			fm = new FailMessage(false, "Введен неверный текущий пароль!");
		} else {
			// Все проверки пройдены, можно обновлять пароль пользователю
			String newEncryptedPass = su.getEncryptedPass(newPass);
			String hqlUpd = "update ADM_USER au set au.password= '"
					+ newEncryptedPass + "' where au.userId = " + userId;
			Query q = em.createNativeQuery(hqlUpd);
			int updatedAmount = q.executeUpdate();
			if (updatedAmount != 1) {
				log.log(Level.WARNING, "WARNING!!! WARNING!!! WARNING!!!");
				log.log(Level.WARNING, "updated more than one record "
						+ (updatedAmount));
				log.log(Level.WARNING,
						"e.g. adm_user table contains duplicate user's ids");
			}
			fm = new FailMessage(true, "Пароль успешно изменен");
		}
		JsonObject jsonResult = jsonTools.getJsonForEntity(fm);
		return jsonResult;
	}

	/**
	 * Получение информации об аккаунте по его id
	 * 
	 * @param userId
	 *            id аккаунта
	 * @return
	 * @throws Exception
	 */
	public JsonObject getAccountInfo(Long userId) throws Exception {
		UserInfoResponse userInfo = new UserInfoResponse();
		WebUser wu = em.find(WebUser.class, userId);
		userInfo.initMessage(wu, su);
		JsonObject result = jsonTools.getJsonForEntity(userInfo);
		return result;
	}

	/**
	 * Обновление сведений аккаунта
	 * 
	 * @param userId
	 *            айди пользователя
	 * @param userInfoResp
	 *            новые данные
	 * @return
	 * @throws Exception
	 */
	public JsonObject updateAccountInfo(Long userId,
			UserInfoResponse userInfoResp) throws Exception {
		WebUser wu = (WebUser) em.find(WebUser.class, userId);
		wu.setEmail(userInfoResp.getMail());
		wu.setLastName(userInfoResp.getSecondName());
		wu.setFirstName(userInfoResp.getFirstName());
		wu.setMiddleName(userInfoResp.getMiddleName());
		wu.setOccupationInfo(userInfoResp.getWorkPlace());
		wu.setEducationLevelId(userInfoResp.getEducation());
		wu.setRankId(userInfoResp.getRank());
		wu.setPhone(userInfoResp.getPhone());
		wu.setBirthDate(su.getSdf().parse(userInfoResp.getBirthday()));
		wu.setCitizenshipId(userInfoResp.getCitizenchipId());
		FailMessage fm = new FailMessage(true, "Изменения успешно сохранены");
		JsonObject result = jsonTools.getJsonForEntity(fm);
		return result;
	}

	public boolean isWebUserFieldUnique(String fieldName, String fieldValue)
			throws Exception {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebUser> criteriaQuery = cb.createQuery(WebUser.class);
		Root<WebUser> root = criteriaQuery.from(WebUser.class);
		criteriaQuery.where(cb.equal(root.get(fieldName), fieldValue));
		Query q = em.createQuery(criteriaQuery);
		boolean result = (q.getResultList().size() == 0);
		return result;
	}

	/**
	 * Проверка логина/пароля пользователя
	 * 
	 * @param login
	 * @param pass
	 * @return
	 * @throws Exception
	 */
	public JsonObject checkPassword(String login, String pass) throws Exception {
		String encPass = su.getEncryptedPass(pass);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AdmUser> criteriaQuery = cb.createQuery(AdmUser.class);
		Root<AdmUser> root = criteriaQuery.from(AdmUser.class);
		criteriaQuery.where(cb.and(cb.equal(root.get("login"), login),
				cb.equal(root.get("password"), encPass)));
		Query q = em.createQuery(criteriaQuery);
		FailMessage result = null;
		if (q.getResultList().size() == 0) {
			result = new FailMessage(false, "Неверный логин/пароль");
			log.log(Level.FINEST, "LOGIN FAIL, USER: " + login
					+ " ENCRYPTED PASS: " + encPass);
			JsonObject jo = jsonTools.getJsonForEntity(result);
			return jo;
		} else {
			UserRegOkResponse r = new UserRegOkResponse();
			AdmUser loginedUser = (AdmUser) q.getResultList().get(0);
			r.initMessage(loginedUser);
			JsonObject jo = jsonTools.getJsonForEntity(r);
			log.log(Level.FINEST, "LOGIN OK, USER: " + loginedUser.getLogin());
			return jo;
		}
	}

	/**
	 * Обработка подтверждения регистрации на портале
	 * 
	 * @param req
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public boolean isRegistrationConfirmed(String code) throws Exception {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebUser> criteriaQuery = cb.createQuery(WebUser.class);
		Root<WebUser> root = criteriaQuery.from(WebUser.class);
		Long dvStatus = su.getAccountStatus("account_status_require_confirm")
				.getDescriptorValueId();
		criteriaQuery.where(cb.and(
				cb.equal(root.get("descValueStatusId"), dvStatus),
				cb.equal(root.get("confirmationUUID"), code)));
		Query q = em.createQuery(criteriaQuery);
		boolean isConfirm = q.getResultList().size() == 1;
		if (isConfirm) {
			WebUser confirmedUser = (WebUser) q.getResultList().get(0);
			confirmedUser.setDescValueStatusId(su.getAccountStatus(
					"account_status_active").getDescriptorValueId());
			confirmedUser.setConfirmationUUID("");

			AdmUser us = (AdmUser) em.find(AdmUser.class,
					confirmedUser.getUserId());
			us.setBlocked(false);
		}
		return isConfirm;
	}

	private boolean isEmailFree(String email) throws Exception {
		return isWebUserFieldUnique("email", email);
	}
	
	//TODO: Нужно сделать корректные параметры, а не передавать HttpServletRequest
	/**
	 * 
	 * Регистрация пользователя в системе
	 * @param req
	 * @return
	 * @throws Exception
	 */
	public JsonObject regUser(HttpServletRequest req) throws Exception {
		String login = req.getParameter("login");
		String pass = req.getParameter("pass");
		String email = req.getParameter("email");
		String last_name = req.getParameter("last_name");
		String first_name = req.getParameter("first_name");
		String middle_name = req.getParameter("middle_name");
		String work_place = req.getParameter("work_place");
		String education = req.getParameter("education");
		String rank = req.getParameter("rank");
		String phone = req.getParameter("phone");
		String birthday = req.getParameter("birthday");

		Date bdDate = su.getSdf().parse(birthday);
		String citizenship = req.getParameter("citizenship");

		String userLangLocale = l10nUtils.getDefaultLocale();
		log.log(Level.FINEST, "USER REGISTRATION REQUEST: userLang is "
				+ userLangLocale);
		HashMap<String, String> consts = l10nUtils
				.getCardLocalizedConstsnts(userLangLocale);

		String errMsg = "";
		boolean hasErrors = false;
		if (!isEmailFree(email)) {
			errMsg += "<p>";
			errMsg += consts.get("srv_user_email_exist");
			errMsg += "</p>";
			hasErrors = true;
		}
		if (!isLoginFree(login)) {
			errMsg += consts.get("srv_user_login_occupied");
			hasErrors = true;
		}
		if (hasErrors) {
			FailMessage fm = new FailMessage(false, errMsg);
			return jsonTools.getJsonForEntity(fm);
		} else {
			WebUser user = new WebUser();
			user.setBirthDate(bdDate);
			if (citizenship != null) {
				user.setCitizenshipId(Long.parseLong(citizenship));
			}
			String confirmationPattern = UUID.randomUUID().toString();
			user.setConfirmationUUID(confirmationPattern);
			user.setDescValueStatusId(su.getAccountStatus(
					"account_status_require_confirm").getDescriptorValueId());
			if (education != null) {
				user.setEducationLevelId(Long.parseLong(education));
			}
			user.setEmail(email);
			user.setFirstName(first_name);
			user.setLastName(last_name);
			user.setMiddleName(middle_name);
			user.setOccupationInfo(work_place);
			user.setPhone(phone);
			if (rank != null) {
				user.setRankId(Long.parseLong(rank));
			}
			user.setUserId(141l); // TODO: разобраться, что нужно здесь сделать
			AdmUser au = new AdmUser();
			au.setBlocked(true);
			au.setDisplayedName(last_name + " " + middle_name + " "
					+ first_name);
			au.setLogin(login);
			au.setPassword(su.getEncryptedPass(pass));

			au.setUserTypeId(hh.getDescValueByCode("WEB_USER")
					.getDescriptorValueId());
			em.persist(au);
			user.setUserId(au.getUserId());
			em.persist(user);
			log.log(Level.FINEST, "ACCOUNT FOR USER WITH LOGIN: " + login
					+ " SAVED INTO DB");

			PostMessage pm = new PostMessage();
			String fio = last_name + " " + first_name + " " + middle_name;
			String url = req.getScheme() + "://" + req.getServerName() + ":"
					+ req.getServerPort()
					+ req.getServletContext().getContextPath();
			url += req.getServletPath();
			url += "?action=confirmRegistration&code=" + confirmationPattern;
			pm.configureMessage(PostMessage.msgTypes.MAIL_CONFIRM, email,
					userLangLocale, url, fio);
			MailSender ms = new MailSender();
			ms.setMessage(pm);
			ms.start();
			FailMessage fm = new FailMessage(true, "Операция выполнена успешно");
			JsonObject o = jsonTools.getJsonForEntity(fm);
			return o;
		}

	}

}
