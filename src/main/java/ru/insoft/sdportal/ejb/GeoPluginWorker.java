package ru.insoft.sdportal.ejb;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
/**
 * Бин для проверки кода страны на вхождение в список разрешенных стран, из
 * которых разрешен просмотр образов документов на портале.
 * Отправляет запрос к сервису http://www.geoplugin.net/ для получения кода страны по IP,
 * получает ответ в json, парсит его и проверяет вхождение кода в список разрешенных.
 */
@Stateless
public class GeoPluginWorker {

	private Logger log;
	
	
	private ArrayList<String> allowedCountries;
	
	@EJB
	private HibernateHelper hh;
	
	
	public GeoPluginWorker(){
		log = Logger.getLogger(this.getClass().getCanonicalName());
	}
	
	private String getCountryCodeByIp(String ip) throws Exception{
		String fullUrl = "http://www.geoplugin.net/json.gp?ip="
				+ ip;
		URL u = new URL(fullUrl);
		HttpURLConnection huc = (HttpURLConnection) u.openConnection();
		String USER_AGENT = "Mozilla/5.0";
		huc.setRequestMethod("GET");
		huc.setRequestProperty("User-Agent", USER_AGENT);
		int respCode = huc.getResponseCode();
		log.info("url: " + fullUrl + " responseCode: " + respCode);

		BufferedReader bin = new BufferedReader(new InputStreamReader(
				huc.getInputStream()));
		String inLine = "";
		String serviceREsponse = "";
		while ((inLine = bin.readLine()) != null) {
			serviceREsponse += inLine;
		}
		bin.close();
		StringReader str = new StringReader(serviceREsponse);
		JsonReader jr = Json.createReader(str);
		JsonObject obj = jr.readObject();
		for (String s: obj.keySet()){
			log.info("key: "+s+" value:"+obj.get(s).toString());
		}
        String regionCode = obj.getString("geoplugin_region");
        System.out.println("regionCode=           " + regionCode);
        if(regionCode.equals ("Avtonomna Respublika Krym")){
          return "KRYM";
        }
        else {
          String countryCode = obj.getString("geoplugin_countryCode");          
          return countryCode;
        }
	}
	
	/**
	 * Проверка, может ли пользователь с данным ip просматривать
	 * образы на портале.
	 * @param ip ип адрес (строка)
	 * @return
	 * @throws Exception
	 */
	public boolean isImagesAllowed(String ip)throws Exception
        {
            InetAddress address = InetAddress.getByName(ip);
            
		//if ("127.0.0.1".equals(ip)){
            
            if (address.isLoopbackAddress() || address.isSiteLocalAddress())
		return true;
		
        if (allowedCountries==null){
		String rawCountries = hh.getImagesAllowcountries();
		String[] cplitted = rawCountries.split(",");
		allowedCountries = new ArrayList<String>();
		for (int i = 0 ; i< cplitted.length;i++){
			allowedCountries.add(cplitted[i]);
		}
            }
            String countryCode = getCountryCodeByIp(ip);
            if(countryCode.equals("KRYM")) {
              return true;
            }
            else{
              return allowedCountries.contains(countryCode);
            }
	}
	
	
}