package ru.insoft.sdportal.ejb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import ru.insoft.sdportal.models.L18nWebPage;
import ru.insoft.sdportal.models.WebPage;
import ru.insoft.sovdoc.model.desc.table.DescLanguage;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroupAttr;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.desc.table.DescriptorValueAttr;

/**
 * Создание страницы
 */
@Stateless(name = "PageMaker")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PageMaker {

	@Inject
	private EntityManager em;
	@EJB
	private HibernateHelper hh;

	private Logger logger;

	public PageMaker() {
		this.logger = Logger.getLogger(this.getClass().getCanonicalName());
	}

	/**
	 * get DescriptorGroup instance by code
	 * 
	 * @param code
	 * @return
	 */
	private DescriptorGroup getDescriptorGroupByCode(String code) {
		String nativeQuerys = "select descriptor_group_id from descriptor_group where group_code = '"
				+ code + "'";
		Query nativeQuery = em.createNativeQuery(nativeQuerys);
		Long id = ((BigDecimal) nativeQuery.getSingleResult()).longValue();
		DescriptorGroup gr = (DescriptorGroup) em.find(DescriptorGroup.class,
				id);
		return gr;
	}

	private DescriptorGroupAttr getGroupAttrByCode(String attrCode, Long groupId) {
		String sql = "select descriptor_group_attr_id from descriptor_group_attr where attr_code='"
				+ attrCode + "' and descriptor_group_id=" + groupId.toString();
		Query q = em.createNativeQuery(sql);
		Long id = ((BigDecimal) q.getSingleResult()).longValue();
		DescriptorGroupAttr atr = (DescriptorGroupAttr) em.find(
				DescriptorGroupAttr.class, id);
		return atr;
	}

	private DescriptorValue getDescriptorValueByCode(String code) {
		String sql = "select descriptor_value_id from descriptor_value where value_code='"
				+ code + "'";
		Query sq = em.createNativeQuery(sql);
		Long id = ((BigDecimal) sq.getSingleResult()).longValue();
		DescriptorValue dv = (DescriptorValue) em.find(DescriptorValue.class,
				id);
		return dv;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void createPage(String pageCode, String pageName) {
		logger.info("pagecode: " + pageCode);
		logger.info("pagename: " + pageName);
		DescriptorGroup pageTypeGroup = getDescriptorGroupByCode("WEB_PAGE_TYPE");
		DescriptorGroupAttr isCollectionAttribute = getGroupAttrByCode(
				"IS_COLLECTION", pageTypeGroup.getDescriptorGroupId());
		DescriptorValue contentTypeValue = getDescriptorValueByCode("PAGE_BODY");
		DescriptorValue newPageType = new DescriptorValue();
		newPageType.setDescriptorGroup(pageTypeGroup);
		newPageType.setValueCode("page_" + pageCode);
		newPageType.setFullValue(pageName);
		newPageType.setSortOrder(-1l);
		em.persist(newPageType);
		DescriptorValueAttr newPageCollectionAtr = new DescriptorValueAttr();
		Boolean b = false;
		newPageCollectionAtr.setAttrValue(b.toString());
		newPageCollectionAtr.setDescriptorGroupAttrId(isCollectionAttribute
				.getDescriptorGroupAttrId());
		newPageCollectionAtr.setDescriptorValue(newPageType);

		em.persist(newPageCollectionAtr);

		WebPage newPage = new WebPage();
		newPage.setInternalName(pageName);
		newPage.setPageCode(pageCode);
		newPage.setPageTypeId(newPageType.getDescriptorValueId());
		newPage.setSortOrder(-1l);
		em.persist(newPage);
		logger.info("Saved page with code " + pageCode + " id: "
				+ newPage.getId());
		DescLanguage rusLag = (DescLanguage) em.find(DescLanguage.class, "RUS");
		L18nWebPage rusPage = new L18nWebPage();
		rusPage.setPage(newPage);
		rusPage.setPageData("Данные страницы: " + pageName);
		rusPage.setPageName(pageName);
		rusPage.setLang(rusLag);
		rusPage.setContentType(contentTypeValue.getDescriptorValueId());

		em.persist(rusPage);
		DescLanguage engLag = (DescLanguage) em.find(DescLanguage.class, "ENG");

		L18nWebPage engPage = new L18nWebPage();
		engPage.setPage(newPage);
		engPage.setPageData("Data for page: " + pageName);
		engPage.setPageName(pageName);
		engPage.setLang(engLag);
		engPage.setContentType(contentTypeValue.getDescriptorValueId());

		em.persist(engPage);
		DescLanguage espLag = (DescLanguage) em.find(DescLanguage.class, "ESP");
		L18nWebPage espPage = new L18nWebPage();
		espPage.setPage(newPage);
		espPage.setPageData("ESP page data: " + pageName);
		espPage.setPageName(pageName);
		espPage.setLang(espLag);
		espPage.setContentType(contentTypeValue.getDescriptorValueId());

		em.persist(espPage);

		// newPage.setPageTypeId()
	}

	/**
	 * Удаление страницы по её коду (включая все локализованные страницы и
	 * значение справочника "Тип страницы")
	 * 
	 * @param pageCode
	 */
	public void dropPage(String pageCode) {
		String getPageIdSql = "select page_id from web_pages where page_code='"
				+ pageCode + "'";
		Query sq = em.createNativeQuery(getPageIdSql);
		ArrayList<Long> list = new ArrayList<>();
		for (Object o : sq.getResultList()) {
			Long l = ((BigDecimal) o).longValue();
			WebPage wp = (WebPage) em.find(WebPage.class, l);
			Long idForPageTypeForDrop = wp.getPageTypeId();
			String droplocalizedPagesSQl = "delete from web_pages_localized where page_id="
					+ wp.getId();
			Query dropLocalizedPage = em
					.createNativeQuery(droplocalizedPagesSQl);
			// удаляются локализованные страницы
			dropLocalizedPage.executeUpdate();
			// удаляется сама страница
			String dropWP = "delete from web_pages where page_id = "
					+ wp.getId();
			Query ropWPQuery = em.createNativeQuery(dropWP);
			ropWPQuery.executeUpdate();
			String dropAttributesSql = "delete from descriptor_value_attr where descriptor_value_id = "
					+ idForPageTypeForDrop;
			Query dropAttrs = em.createNativeQuery(dropAttributesSql);
			// удаляются атрибуты значения справочника "Тип страницы" для
			// удаляемой
			// страницы
			dropAttrs.executeUpdate();
			list.add(idForPageTypeForDrop);
		}
		for (Long idForPageTypeForDrop : list) {
			String dropDescValuePageType = "delete from descriptor_value where descriptor_value_id = "
					+ idForPageTypeForDrop;
			Query dropDvQuery = em.createNativeQuery(dropDescValuePageType);
			// Удаляется само значение справочника "Тип страницы"
			dropDvQuery.executeUpdate();
		}
	}

}
