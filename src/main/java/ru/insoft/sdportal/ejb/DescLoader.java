/**
 * @author sorokin sovdoc-portal 29.10.2013 11:14:30
 */
package ru.insoft.sdportal.ejb;

import java.io.StringReader;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.StringEscapeUtils;

import ru.insoft.archive.extcommons.ejb.JsonTools;
import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.archive.extcommons.webmodel.ScalarItem;
import ru.insoft.sdportal.models.ui.TreeItem;
import ru.insoft.sovdoc.model.desc.table.DescValueInternational;
import ru.insoft.sovdoc.model.desc.table.DescriptorGroup;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;
import java.util.List;
import javax.persistence.criteria.Expression;

@Stateless
public class DescLoader {

	private XStream xs;

	@Inject
	private EntityManager em;

	@EJB
	private HibernateHelper hh;

	@EJB
	private L10nUtils l10nUtils;

	@EJB
	private SysUtils su;

	@EJB
	private JsonTools jsonTools;

	private Logger log;

	public DescLoader() {
		log = Logger.getLogger(this.getClass().getCanonicalName());
		xs = new XStream(new JsonHierarchicalStreamDriver() {
			public HierarchicalStreamWriter createWriter(Writer writer) {
				return new JsonWriter(writer, JsonWriter.DROP_ROOT_MODE);
			}
		});
	}

	public JsonObject getGroups() throws Exception {
		JsonArrayBuilder jabdr = Json.createArrayBuilder();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroup> criteriaQuery = cb
				.createQuery(DescriptorGroup.class);
		Root<DescriptorGroup> root = criteriaQuery.from(DescriptorGroup.class);
		criteriaQuery.where(cb.not(cb.equal(root.get("isSystem"), true)));
		Query q = em.createQuery(criteriaQuery);
		for (Object o : q.getResultList().toArray()) {
			DescriptorGroup g = (DescriptorGroup) o;
			JsonObjectBuilder job = Json.createObjectBuilder();
			job.add("id", g.getDescriptorGroupId());
			job.add("namee", g.getGroupName());
			jabdr.add(job);
		}
		JsonObjectBuilder resultBuilder = Json.createObjectBuilder();
		resultBuilder.add("items", jabdr);
		return resultBuilder.build();
	}

	public JsonObject getValuesTree(Long groupId, String userLang)
			throws Exception {
		String query = "";
		query += "select LEVEL,PARENT_VALUE_ID,DESCRIPTOR_VALUE_ID, nvl((select INTERNATIONAL_SHORT_VALUE from ";
		query += "DESC_VALUE_INTERNATIONAL where DESC_VALUE_INTERNATIONAL.DESCRIPTOR_VALUE_ID=DESCRIPTOR_VALUE.DESCRIPTOR_VALUE_ID and ";
		query += "LANGUAGE_CODE='" + userLang + "'),FULL_VALUE) as l10nValue ";
		query += "from DESCRIPTOR_VALUE where DESCRIPTOR_GROUP_ID = "
				+ groupId
				+ " connect by prior DESCRIPTOR_VALUE_ID=PARENT_VALUE_ID order by LEVEL, L10NVALUE";
		Query q = em.createNativeQuery(query);
		JsonObject result = readTreeQueryDict(q);
		return result;
	}

	/**
	 * Загрузка значений справочников (Значения на русском).
	 *
	 * @param groupId id справочника (если нулл, то грузятся значения всех
	 * несистемных справочников).
	 * @param q - поисковая подстрака для осуществления поиска по значениям Код
	 * обеспечивает работоспособность диалога выбора значения справочника
	 * @return
	 */
	public JsonObject getValuesGrid(Long groupId, String q) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorValue> criteriaQuery = cb
				.createQuery(DescriptorValue.class);
		Root<DescriptorValue> root = criteriaQuery.from(DescriptorValue.class);
		criteriaQuery.select(root);
		if (groupId != null) {
			DescriptorGroup gr = em.find(DescriptorGroup.class, groupId);
			if ((q != null) && !"\"\"".equals(q)) {
				q = StringEscapeUtils.unescapeJava(q);
				q = q.substring(1, q.length() - 1).toUpperCase();
				q += "%";
				log.info("qliks: " + q);
				criteriaQuery.where(cb.and(cb.equal(
						root.<DescriptorGroup>get("descriptorGroup"), gr), cb
						.like(root.<String>get("fullValue"), q)));
			} else {
				criteriaQuery.where(cb.equal(
						root.<DescriptorGroup>get("descriptorGroup"), gr));
			}
		} else {
			Subquery<DescriptorGroup> subquery = criteriaQuery
					.subquery(DescriptorGroup.class);
			Root<DescriptorGroup> subqueryRoot = subquery
					.from(DescriptorGroup.class);
			subquery.where(cb.not(cb.equal(subqueryRoot.get("isSystem"), true)));
			subquery.select(subqueryRoot);

			if ((q != null) && q != "") {
				q = StringEscapeUtils.unescapeJava(q);
				q = q.substring(1, q.length() - 1).toUpperCase();
				q += "%";
				Predicate likeFull = cb.like(root.<String>get("fullValue"), q);
				Predicate groupIn = root.<DescriptorGroup>get(
						"descriptorGroup").in(subquery);
				Predicate fullpredicate = cb.and(likeFull, groupIn);
				criteriaQuery.where(fullpredicate);
			} else {
				Predicate wherePredicate = root.<DescriptorGroup>get(
						"descriptorGroup").in(subquery);
				criteriaQuery.where(wherePredicate);
			}
		}
		ArrayList<Order> arr = new ArrayList<>();
		arr.add(cb.asc(root.get("fullValue")));
		criteriaQuery.orderBy(arr);
		Query qq = em.createQuery(criteriaQuery);
		JsonArrayBuilder itemsArrayBuilder = Json.createArrayBuilder();
		Object[] resuuults = qq.getResultList().toArray();
		for (Object o : resuuults) {
			JsonObjectBuilder job = Json.createObjectBuilder();
			DescriptorValue v = (DescriptorValue) o;
			DescValueInternational l10nV = l10nUtils.getL10nValueIfExist(v,
					"RUS");
			if (l10nV == null) {
				job.add("text", v.getFullValue());
			} else {
				job.add("text", l10nV.getInternationalFullValue());
			}
			job.add("id", v.getDescriptorValueId());

			itemsArrayBuilder.add(job);
		}
		JsonObjectBuilder resultBuilder = Json.createObjectBuilder();
		resultBuilder.add("items", itemsArrayBuilder);
		return resultBuilder.build();
	}

	/**
	 * Обработка результата выполнения sql запроса на получение иерархичной
	 * структуры данных. Используется для обработки запроса на формирования
	 * организационной структуры + запрос иерархии при загрузки значений
	 * справочника в диалоге выбора значения справочника в расширенном поиске.
	 *
	 * @param q
	 * @return
	 * @throws Exception
	 */
	private JsonObject readTreeQueryDict(Query q) throws Exception {
		HashMap<Long, TreeItem> cache = new HashMap<Long, TreeItem>();
		TreeItem rootItem = new TreeItem();
		rootItem.setId(-2l);
		rootItem.setLeaf(false);
		rootItem.setText("Default root element");
		Object[] allResult = q.getResultList().toArray();
		for (int i = 0; i < allResult.length; i++) {
			Object rawRowObj = allResult[i];
			Object[] rawRow = (Object[]) rawRowObj;
			Long PARENT_VALUE_ID = null;
			if (rawRow[1] != null) {
				PARENT_VALUE_ID = Long.parseLong(rawRow[1].toString());
			}
			Long DESCRIPTOR_VALUE_ID = Long.parseLong(rawRow[2].toString());
			String l10nValue = (String) rawRow[3];
			TreeItem ti = new TreeItem();
			ti.setId(DESCRIPTOR_VALUE_ID);
			ti.setText(l10nValue);
			if (PARENT_VALUE_ID == null) {
				rootItem.addChild(ti);
			} else {
				TreeItem parentItem = null;
				if (cache.get(PARENT_VALUE_ID) != null) {
					parentItem = cache.get(PARENT_VALUE_ID);
				} else {
					parentItem = rootItem;
				}
				parentItem.addChild(ti);
			}
			cache.put(ti.getId(), ti);
		}
		JsonObject jo = Json.createReader(new StringReader(xs.toXML(rootItem)))
				.readObject();
		return jo;
	}

	/**
	 * Запрос пути к значению в иерархии справочника.
	 *
	 * @param valueId id значения справочника
	 * @return
	 * @throws Exception
	 */
	public JsonObject getPath(Long valueId) throws Exception {
		DescriptorValue val = (DescriptorValue) em.find(DescriptorValue.class,
				valueId);
		Vector<DescriptorValue> path = new Vector<DescriptorValue>();
		while (val != null) {
			path.add(val);
			val = val.getParentValueId();
		}
		String strPath = path.get(0).getDescriptorGroupId().getGroupName();
		for (int i = path.size() - 1; i >= 0; i--) {
			strPath += "/";
			DescriptorValue v = path.get(i);
			DescValueInternational l10nV = l10nUtils.getL10nValueIfExist(v,
					"RUS");
			if (l10nV == null) {
				strPath += path.get(i).getFullValue();
			} else {
				strPath += l10nV.getInternationalFullValue();
			}
		}
		FailMessage fm = new FailMessage(true, strPath);
		return jsonTools.getJsonForEntity(fm);
	}

	/**
	 * Возвращает список серий книг (для соответствующего критерия поиска в
	 * "Электронной библиотеке")
	 *
	 * @return
	 * @throws Exception
	 */
	public JsonArray getEbookSeries() throws Exception {
		String hql = "" + "select a.series_id, a.series_name from "
				+ " ARCH_EBOOK_SERIES " + " a order by a.series_name ";
		ArrayList<ScalarItem> comboItems = new ArrayList<ScalarItem>();
		Query q = em.createNativeQuery(hql);
		for (Object obj : q.getResultList().toArray()) {
			Object[] objArr = (Object[]) obj;
			ScalarItem ci = new ScalarItem();
			ci.setId(((BigDecimal) objArr[0]).longValue());
			ci.setName((String) objArr[1]);
			comboItems.add(ci);
		}
		JsonArray arr = jsonTools.getJsonEntitiesList(comboItems);
		return arr;
	}

	public JsonObject getLocalizedDescValuesForGroup(String groupCode,
			String userLang) throws Exception {
		return getLocalizedDescValuesForGroup(groupCode, userLang, null);
	}

	/**
	 * Запрос значений справочника с кодом groupCode на языке userLang
	 *
	 * @param groupCode код справочника
	 * @param userLang код языка "RUS"||"ENG"||"ESP"
	 * @param exludeCodes коды значений справочников, которые не отдавать
	 * @return
	 * @throws Exception
	 */
	public JsonObject getLocalizedDescValuesForGroup(String groupCode,
			String userLang, List<String> exludeCodes) throws Exception {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DescriptorGroup> criteriaQuery = cb
				.createQuery(DescriptorGroup.class);
		Root<DescriptorGroup> root = criteriaQuery.from(DescriptorGroup.class);
		criteriaQuery.where(cb.equal(root.get("groupCode"), groupCode));
		Query cr = em.createQuery(criteriaQuery);
		if (cr.getResultList().isEmpty()) {
			String err = "ERROR: DESCRIPTOR GROUP WITH CODE " + groupCode
					+ " NOT FOUND!";
			log.severe(err);
			throw new IllegalArgumentException(err);
		}
		DescriptorGroup group = (DescriptorGroup) cr.getResultList().get(0);
		CriteriaQuery<DescriptorValue> criteriaQuery2 = cb
				.createQuery(DescriptorValue.class);

		Root<DescriptorValue> root2 = criteriaQuery2
				.from(DescriptorValue.class);

		Expression<Boolean> expr = cb.equal(root2.get("descriptorGroup"), group);

		if (exludeCodes != null) {
			for (String code : exludeCodes) {
				expr = cb.and(expr, cb.notEqual(root2.get("valueCode"), code));
			}
		}

		criteriaQuery2.where(expr);

		Query q2 = em.createQuery(criteriaQuery2);
		ArrayList<ScalarItem> arrItems = new ArrayList<>();
		for (Object o : q2.getResultList()) {
			DescriptorValue dv = (DescriptorValue) o;
			ScalarItem si = new ScalarItem();
			si.setId(dv.getDescriptorValueId());
			DescValueInternational locValue = l10nUtils.getL10nValueIfExist(dv,
					userLang);
			if (locValue != null) {
				si.setName(locValue.getInternationalFullValue());
			} else {
				si.setName(dv.getFullValue());
			}
			arrItems.add(si);
		}
		JsonArray arr = jsonTools.getJsonEntitiesList(arrItems);
		JsonObjectBuilder job = Json.createObjectBuilder();
		job.add("items", arr);
		return job.build();
	}

	/**
	 * Возвращает список с архивами для поля "Архив" в расширенном поиске
	 *
	 * @return
	 */
	public JsonObject getOrgStructure() {
		String orgStructQuery = "select  DESCRIPTOR_VALUE_ID,SHORT_VALUE as l10nValue "
				+ "from DESCRIPTOR_VALUE where "
				+ "( "
				+ "( "
				+ "        select VALUE_CODE from DESCRIPTOR_VALUE a where a.DESCRIPTOR_VALUE_ID= "
				+ "    ( "
				+ "    select REF_DESCRIPTOR_VALUE_ID from DESCRIPTOR_VALUE_ATTR where DESCRIPTOR_VALUE_ATTR.DESCRIPTOR_VALUE_ID=DESCRIPTOR_VALUE.DESCRIPTOR_VALUE_ID and "
				+ "    DESCRIPTOR_GROUP_ATTR_ID=(select DESCRIPTOR_GROUP_ATTR_ID from DESCRIPTOR_GROUP_ATTR where ATTR_CODE='ORG_STRUCTURE_TYPE') "
				+ "    ) "
				+ ")='ARCHIVE' "
				+ ") and parent_value_id = 430 order by L10nValue ";
		Query q = em.createNativeQuery(orgStructQuery);
		JsonArrayBuilder ab = Json.createArrayBuilder();
		JsonObjectBuilder notSelectedObjectBuilder = Json.createObjectBuilder();
		notSelectedObjectBuilder.add("name", "&nbsp;");
		notSelectedObjectBuilder.add("id", -1);
		ab.add(notSelectedObjectBuilder);
		Object[] rawResults = q.getResultList().toArray();
		for (int i = 0; i < rawResults.length; i++) {
			Object rawRow = rawResults[i];
			Object[] rawRowArr = (Object[]) rawRow;
			Long id = ((BigDecimal) rawRowArr[0]).longValue();
			String name = rawRowArr[1].toString();
			JsonObjectBuilder curItemBuilder = Json.createObjectBuilder();
			curItemBuilder.add("name", name);
			curItemBuilder.add("id", id);
			ab.add(curItemBuilder.build());
		}
		JsonObjectBuilder resultItemBuilder = Json.createObjectBuilder();
		resultItemBuilder.add("items", ab.build());
		return resultItemBuilder.build();
	}

}
