package ru.insoft.sdportal.ejb;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.picketlink.idm.impl.api.PasswordCredential;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Stateless
public class SysUtils {

	private HashMap<String, DescriptorValue> accountStatuses;
	
	@Inject
	private EntityManager em;
	
	@EJB
	private HibernateHelper hh;
	
	public SysUtils() {
		accountStatuses = new HashMap<String, DescriptorValue>();
	}
	
	public String getEncryptedPass(String pass){
		return PasswordCredential.md5AsHexString(pass).toUpperCase();
	}
	
	public String getStackTraceString(Exception ee){
		String resultStr = "";
		for (StackTraceElement e: ee.getStackTrace()){
			resultStr+=" \n";
			resultStr+=e.toString();
		}
		return resultStr;
	}
	@PostConstruct
	private void initAccountStatuses() {
		String[] codeArrays = {"account_status_active", "account_status_require_confirm"};
		for (String code : codeArrays) {
			
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<DescriptorValue> criteriaQuery = cb.createQuery(DescriptorValue.class);
			Root<DescriptorValue> root = criteriaQuery.from(DescriptorValue.class);
			criteriaQuery.where(cb.equal(root.get("valueCode"), code));
			Query q = em.createQuery(criteriaQuery);
			if (q.getResultList().size() == 0) {
				throw new IllegalStateException("�������� ��������� � �����: " + code + " �� �������! ");
			}
			DescriptorValue val = (DescriptorValue)q.getResultList().get(0);
			accountStatuses.put(code, val);
		}
	}

	public DescriptorValue getAccountStatus(String code){
		return accountStatuses.get(code);
	}

	public SimpleDateFormat getSdf(){
		return new SimpleDateFormat("dd.MM.yyyy");
	}
	
}
