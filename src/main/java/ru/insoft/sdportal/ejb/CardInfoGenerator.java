package ru.insoft.sdportal.ejb;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import ru.insoft.archive.extcommons.ejb.JsonTools;
import ru.insoft.sdportal.logic.ArchiveCodeParser;
import ru.insoft.sdportal.models.ui.ImageInfo;
import ru.insoft.sdportal.ws.ImageItemInfo;
import ru.insoft.sovdoc.model.complex.table.ApplicationToDocument;
import ru.insoft.sovdoc.model.complex.table.ArchDocument;
import ru.insoft.sovdoc.model.complex.table.ArchFund;
import ru.insoft.sovdoc.model.complex.table.ArchPhonoStorageUnit;
import ru.insoft.sovdoc.model.complex.table.ArchPhonodoc;
import ru.insoft.sovdoc.model.complex.table.ArchSeries;
import ru.insoft.sovdoc.model.complex.table.ArchStorageUnit;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;
import ru.insoft.sovdoc.model.complex.table.UnivDateInterval;
import ru.insoft.sovdoc.model.complex.table.UnivImagePath;
import ru.insoft.sovdoc.model.complex.view.VComplexCardLinks;
import ru.insoft.sovdoc.model.desc.table.DescValueInternational;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;
import ru.insoft.sovdoc.model.ebook.ArchEbook;
import ru.insoft.sovdoc.model.ebook.ArchEbookSeries;

@Stateless
public class CardInfoGenerator {

	@EJB
	private JsonTools jsonTools;

	@Inject
	private EntityManager em;

	@EJB
	private L10nUtils l10nUtils;

	@EJB
	private SovdocSearchImpl searchImpl;

	@EJB
	private SysUtils su;

	private Logger log;

	public CardInfoGenerator() {
		log = Logger.getLogger(getClass().getCanonicalName());
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	private final static Map<Character, Character> letters = new HashMap<Character, Character>() {
		{
			put('Р', 'R');
			put('Г', 'G');
			put('А', 'A');
			put('С', 'S');
			put('П', 'P');
			put('И', 'I');
			put('Ф', 'F');
			put('О', 'O');
			put('п', 'p');
			put('Д', 'D');
			put('Л', 'L');
			put('л', 'l');
		}
	};

	@EJB
	private HibernateHelper hh;

	/**
	 * Возвращает html блок с датами для единицы хранения с заданным id
	 *
	 * @param dataUnit id единицы хранения
	 * @param userLang - язык пользователя
	 * @return
	 */
	private String getDateIntervalsForDataUnit(UnivDataUnit dataUnit,
			String userLang) {
		String html = "";
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UnivDateInterval> criteriaQuery = cb.createQuery(UnivDateInterval.class);
		Root<UnivDateInterval> root = criteriaQuery.from(UnivDateInterval.class);
		criteriaQuery.where(cb.equal(root.get("dataUnit"), dataUnit));
		TypedQuery<UnivDateInterval> typedQuery = em.createQuery(criteriaQuery);
		List<UnivDateInterval> l = typedQuery.getResultList();
		if (l.isEmpty()) {
			return ""; // не показываю пустую таблицу с одним только заголовком
			// "Даты"
		}
		Template tmpl = Velocity.getTemplate("datetable.html");
		VelocityContext ctx = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(userLang));
		ctx.put("dateList", l);
		StringWriter stw = new StringWriter();
		tmpl.merge(ctx, stw);
		html = stw.getBuffer().toString();
		return html;
	}

	private String getLocalizedAuthors(Long dataUnitId, String userLang) {
		String lang = l10nUtils.getDescLangByBrowserCode(userLang)
				.getLanguageCode();
		String query = ""
				+ "select nvl((select INTERNATIONAL_FULL_VALUE from DESC_VALUE_INTERNATIONAL where "
				+ "DESCRIPTOR_VALUE_ID = author_id and LANGUAGE_CODE='" + lang
				+ "'),DESCRIPTOR_VALUE.FULL_VALUE)"
				+ "l10nValue from ARCH_EBOOK_AUTHOR, DESCRIPTOR_VALUE "
				+ "where ARCH_EBOOK_AUTHOR.UNIV_DATA_UNIT_ID="
				+ dataUnitId.toString()
				+ " AND DESCRIPTOR_VALUE.DESCRIPTOR_VALUE_ID=AUTHOR_ID"
				+ " ORDER BY l10nValue";
		log.log(Level.FINEST, "LOAD LOCALIZED AUTHORS INFO FOR UNIT, QUERY: "
				+ query);
		String authors = "";
		try {
			Query q = em.createNativeQuery(query);
			ArrayList<Object> result = new ArrayList<>(q.getResultList());
			for (Object o : result) {
				authors += o.toString() + " ";
			}
		} catch (Exception e) {
			authors = e.getLocalizedMessage();
			e.printStackTrace();
		}
		return authors;
	}

	private List<DescriptorValue> getLangsForUnivDataUnit(long uduId) {
		String hql = "select distinct l from DescriptorValue l join l.langToDocs d where d.univDataUnitId=" + uduId;
		Query q = em.createQuery(hql);
		List<DescriptorValue> result = (List<DescriptorValue>) q.getResultList();
		return result;
	}

	private String getLangsStrForUnivDataUnit(Long univDataUnitId) {
		List<DescriptorValue> langs = getLangsForUnivDataUnit(univDataUnitId);
		String langStr = "";
		if (!langs.isEmpty()) {

			for (DescriptorValue v : langs) {
				langStr += v.getFullValue();
				langStr += " ";
			}
		}
		return langStr;
	}

	/**
	 * Считает количество образов (только превьюх), для заданной единицы
	 * хранения
	 *
	 * @param univDataUnitId
	 * @return
	 */
	private Long getImagesCountForUnit(Long univDataUnitId) {
		String sql = "select count(*) from UNIV_IMAGE_PATH where UNIV_DATA_UNIT_ID="
				+ univDataUnitId;
		sql += " and IMAGE_CATEGORY_ID = (select DESCRIPTOR_VALUE_ID from DESCRIPTOR_VALUE where VALUE_CODE='PREVIEW')";
		Query q = em.createNativeQuery(sql);
		Long l = ((BigDecimal) q.getSingleResult()).longValue();
		return l;
	}

	/**
	 * Запрос на подсчет количества дочерних элементов (с использованием функции
	 * в бд)
	 *
	 * @param univDataUnitId
	 * @return
	 */
	private Long getChildsCount(Long univDataUnitId) {
		String sql = "select sovdocportal.getChildsCount(" + univDataUnitId
				+ ") from dual";
		Query q = em.createNativeQuery(sql);
		Long l = ((BigDecimal) q.getSingleResult()).longValue();
		return l;
	}

	/**
	 * Формирование блока навигации по родительским элементам
	 *
	 * @param univDataUnitId
	 * @return
	 */
	private String getCardNavigationBlock(Long univDataUnitId) {
		UnivDataUnit unit = (UnivDataUnit) em.find(UnivDataUnit.class, univDataUnitId);
		ArrayList<UrlData> urls = new ArrayList<UrlData>();
		UnivDataUnit targetUnit = unit;
		boolean first = true;
		while (unit.getParentUnitId() != null) {
			UrlData ud = new UrlData();
			String code = unit.getUnitName();
			if (unit.getUnitType().getDescriptorValueId() != 241l) {
				ArchiveCodeParser.getShortCodeForNavigation(unit.getUnitType().getDescriptorValueId(), unit.getArchNumberCode());
			}
			ud.setName(code);
			if (!first) {
				if (unit.getSection() != null) {
					//Если у родительского элемента - тематический раздел внезапно нулл, то ссылку не формируем.
					//на портале не должны показываться карточки, с нулловым тематическим разделом
					ud.setUrl("#showunit&id=" + unit.getUnivDataUnitId());
				}
			} else {
				first = false;
			}
			if (unit.getSection() != null) {
				urls.add(ud);
			}
			long curUnitParentId = unit.getParentUnitId();
			unit = (UnivDataUnit) em.find(UnivDataUnit.class, curUnitParentId);
		}
		UrlData sectionUrl = new UrlData();
		sectionUrl.setName(targetUnit.getSection().getFullValue());
		sectionUrl.setUrl("#!tematicsection&sectionId="
				+ targetUnit.getSection().getDescriptorValueId());
		urls.add(sectionUrl);
		ArrayList<UrlData> urlsInCorrectOrder = new ArrayList<UrlData>();
		for (int i = urls.size() - 1; i >= 0; i--) {
			urlsInCorrectOrder.add(urls.get(i));
		}
		Template tt = Velocity
				.getTemplate("cardnavigationblock.html");
		VelocityContext ctx = new VelocityContext();
		ctx.put("allurls", urlsInCorrectOrder);
		StringWriter stw = new StringWriter();
		tt.merge(ctx, stw);
		String result = stw.getBuffer().toString();
		return result;
	}

	/**
	 * Формируют нижний блок для отображения данных, по вопросу 7301
	 *
	 * @param univDataUnitId
	 * @return
	 */
	private String getCardFooter(Long univDataUnitId) {
		Template tm = Velocity.getTemplate("cardfooter.html");
		VelocityContext vc = new VelocityContext();
		vc.put("imgcount", getImagesCountForUnit(univDataUnitId));
		vc.put("childscount", getChildsCount(univDataUnitId));
		vc.put("id", univDataUnitId);

		StringWriter st = new StringWriter();
		tm.merge(vc, st);
		String result = st.getBuffer().toString();
		return result;
	}

	private String getEbookHtml(ArchEbook eb, String userLang) {
		Template tm = Velocity.getTemplate("ebookview.html");
		VelocityContext vc = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(userLang));
		ArchEbook ebook = eb;
		Long dataUnitId = ebook.getUnivDataUnitId();
		String ebook_author = getLocalizedAuthors(dataUnitId, userLang);
		String ebook_publisher = ebook.getPublisher();
		String ebook_publisher_place = ebook.getPublishingPlace();
		String ebook_publisher_year = ebook.getPublishingYear().toString();
		String ebook_volume = ebook.getEditionSize();
		String ebook_isbn = "Нет данных";

		String no = "Нет";

		String ebook_editor = no;
		String ebook_compilers = no;
		String ebook_editorialBoard = no;
		String ebook_bbk = no;
		String ebook_volume1Info = no;
		String ebook_atopData = no;

		String ebook_seriesName = no;
		String ebook_seriesInfo = no;

		String ebook_lang = getLangsStrForUnivDataUnit(dataUnitId);
		String ebook_editionType = no;
		Long edTypeId = ebook.getEditionTypeId();
		if (edTypeId != null) {
			DescriptorValue dv = (DescriptorValue) em.find(DescriptorValue.class, edTypeId);
			DescValueInternational l = l10nUtils.getL10nValueIfExist(dv,
					userLang);
			if (l != null) {
				ebook_editionType = l.getInternationalFullValue();
			} else {
				ebook_editionType = dv.getFullValue();
			}
		}

		if (ebook.getSeriesId() != null) {
			ArchEbookSeries aes = (ArchEbookSeries) em.find(ArchEbookSeries.class, ebook.getSeriesId());
			if (aes.getSeries_name() != null) {
				ebook_seriesName = aes.getSeries_name();
			}
			if (aes.getSeries_info() != null) {
				ebook_seriesInfo = aes.getSeries_info();
			}
		}
		if (ebook.getEditor() != null) {
			ebook_editor = ebook.getEditor();
		}
		if (ebook.getCompilers() != null) {
			ebook_compilers = ebook.getCompilers();
		}
		if (ebook.getEditorialBoard() != null) {
			ebook_editorialBoard = ebook.getEditorialBoard();
		}
		if (ebook.getBBC() != null) {
			ebook_bbk = ebook.getBBC();
		}
		if (ebook.getVolume1Info() != null) {
			ebook_volume1Info = ebook.getVolume1Info();
		}

		if (ebook.getAtoptitleData() != null) {
			ebook_atopData = ebook.getAtoptitleData();
		}
		vc.put("ebook_seriesName", ebook_seriesName);
		vc.put("ebook_seriesInfo", ebook_seriesInfo);
		vc.put("ebook_editor", ebook_editor);
		vc.put("ebook_compilers", ebook_compilers);
		vc.put("ebook_editorialBoard", ebook_editorialBoard);
		vc.put("ebook_bbk", ebook_bbk);
		vc.put("ebook_volume1Info", ebook_volume1Info);
		vc.put("ebook_atopData", ebook_atopData);
		vc.put("ebook_lang", ebook_lang);
		vc.put("ebook_editionType", ebook_editionType);
		String url = ebook.getUrl();
		if (url == null) {
			url = "";
		}
		vc.put("ebook_href", url);
		if (ebook.getIsbn() != null) {
			ebook_isbn = ebook.getIsbn();
		}
		String ebook_udk = "Нет данных";
		if (ebook.getUdc() != null) {
			ebook_udk = ebook.getUdc();
		}
		String ebook_note = "Нет данных";
		if (ebook.getNotes() != null) {
			ebook_note = ebook.getNotes();
		}
		StringWriter stw = new StringWriter();
		vc.put("ebook_author", ebook_author);
		vc.put("ebook_publisher", ebook_publisher);
		vc.put("ebook_publication_place", ebook_publisher_place);
		vc.put("ebook_publication_year", ebook_publisher_year);
		vc.put("ebook_volume", ebook_volume);
		vc.put("ebook_isbn", ebook_isbn);
		vc.put("ebook_udk", ebook_udk);
		vc.put("ebook_note", ebook_note);
		tm.merge(vc, stw);
		return stw.getBuffer().toString();
	}

	private String getArchDocHtml(Long dataUnitId, String userLang) {
		Template tmpl = Velocity.getTemplate("docview.html");
		VelocityContext vc = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(userLang));
		ArchDocument doc = em.find(ArchDocument.class, dataUnitId);

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ApplicationToDocument> cq = cb.createQuery(ApplicationToDocument.class);
		Root<ApplicationToDocument> root = cq.from(ApplicationToDocument.class);
		cq.select(root);
		cq.where(cb.equal(root.get("univDataUnitId"), dataUnitId));
		cq.orderBy(cb.asc(root.get("applToDocId")));
		List<ApplicationToDocument> applList = em.createQuery(cq).getResultList();

		String doc_lang = getLangsStrForUnivDataUnit(doc.getUnivDataUnitId());
		vc.put("applList", applList);
		vc.put("doc_lang", doc_lang);
		StringWriter stw = new StringWriter();
		tmpl.merge(vc, stw);
		return stw.getBuffer().toString();
	}

	/**
	 * Возвращает HTML код специфичный для фонодокументов
	 *
	 * @param dataUnitId идентификатор единицы хранения
	 * @param templateContext переменные для подстановки в шаблон
	 * @return строка с html кодом
	 */
	private String getArchPhonoHtml(Long dataUnitId, VelocityContext templateContext) {
		Template tmpl = Velocity.getTemplate("phonodocview.html");

		final ArchPhonodoc doc = em.find(ArchPhonodoc.class, dataUnitId);
		UnivDataUnit unit = em.find(UnivDataUnit.class, dataUnitId);

		templateContext.put("phonodoc_date", sdf.format(doc.getRecordDate()));
		templateContext.put("phonodoc_author", doc.getAuthors());
		templateContext.put("phonodoc_place", doc.getPlace());
		templateContext.put("phonodoc_anno_doc", unit.getAnnotation());
		List<ArchPhonoStorageUnit> units = doc.getStorageUnits();
		Collections.sort(units);
		templateContext.put("phonodoc_units", units);
		StringWriter resultData = new StringWriter();
		tmpl.merge(templateContext, resultData);
		return resultData.getBuffer().toString();
	}

	private String getArchStorageHtml(Long dataUnitId, String userLang) {
		Template tmpl = Velocity.getTemplate("storageunitview.html");
		VelocityContext vc = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(userLang));
		ArchStorageUnit stUnit = (ArchStorageUnit) em.find(ArchStorageUnit.class, dataUnitId);
		if (stUnit == null) {
			String errMsg = "<h1>";
			errMsg += "Data incorrect! Found UnivDataUnit with type ArchStorageUnit and id: "
					+ dataUnitId.toString();
			errMsg += "but ArchStorageUnit with same id NOT FOUND!";
			log.log(Level.SEVERE, errMsg);
			return errMsg;
		}
		// vc.put("su_docs_amount",
		// new Integer(stUnit.getDocumentCount()).toString());
		String su_docs_language = "Нет данных";
		su_docs_language = getLangsStrForUnivDataUnit(stUnit
				.getUnivDataUnitId());

		vc.put("su_docs_language", su_docs_language);
		StringWriter stW = new StringWriter();
		tmpl.merge(vc, stW);
		return stW.getBuffer().toString();
	}

	private String getFormatedArchFundInfo(Long dataUnitId, String userLang)
			throws Exception {
		ArchFund fund = (ArchFund) em.find(ArchFund.class, dataUnitId);
		String v_fund_type = fund.getFundType().getFullValue();
		String v_series_count = Integer.toString(fund.getSeriesCount());
		String v_store_unit_count = Integer.toString(fund.getStorageUnitCount());
		String v_comment = "";
		if (fund.getNotes() != null) {
			v_comment = fund.getNotes();
		}
		String v_history_info = "";
		if (fund.getHistoricalNote() != null) {
			v_history_info = fund.getHistoricalNote();
		}
		Template tpl = Velocity.getTemplate("archfundview.html");

		VelocityContext vc = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(userLang));
		vc.put("v_fund_type", v_fund_type);
		vc.put("v_series_count", v_series_count);
		vc.put("v_store_unit_count", v_store_unit_count);
		vc.put("v_comment", v_comment);
		vc.put("v_history_info", v_history_info);
		StringWriter stw = new StringWriter();
		tpl.merge(vc, stw);
		return stw.getBuffer().toString();
	}

	private String getFormatedArchSeriesInfo(Long dataUnitId, String userLang)
			throws Exception {
		ArchSeries archSeries = (ArchSeries) em.find(ArchSeries.class, dataUnitId);
		if (archSeries == null) {
			String warnMsg = "ArchSeries with same ID not found! "
					+ dataUnitId.toString();
			log.log(Level.WARNING, warnMsg);
			String tbl = "<table><tr><td>";
			tbl += "<h2>ArchSeries with id" + dataUnitId.toString()
					+ " NOT FOUND. BUT UnivDataUnit with same ID exist</h2>";
			tbl += "</td></tr></table>";
			return tbl;
		}
		String v_series_type = "";
		v_series_type = archSeries.getDocumentKind().getFullValue();
		String v_series_name = archSeries.getSeriesName();
		String v_data_unit_amount = archSeries.getStorageUnitCount().toString();
		String v_copy_method = "";
		if (archSeries.getReproductionType() != null) {
			v_copy_method = archSeries.getReproductionType().getFullValue();
		}
//		String v_data_unit_langs = "";
//		v_data_unit_langs = getLangsStrForUnivDataUnit(archSeries
//				.getUnivDataUnitId());
		String v_series_comment = "";
		if (archSeries.getNotes() != null) {
			v_series_comment = archSeries.getNotes();
		}
		Template tpl = Velocity.getTemplate("archseriesviev.html");
		VelocityContext vc = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(userLang));
		vc.put("v_series_type", v_series_type);
		vc.put("v_series_name", v_series_name);
		vc.put("v_data_unit_amount", v_data_unit_amount);
		vc.put("v_copy_method", v_copy_method);
//		vc.put("v_data_unit_langs", v_data_unit_langs);
		vc.put("v_series_comment", v_series_comment);
		StringWriter strW = new StringWriter();
		tpl.merge(vc, strW);
		return strW.getBuffer().toString();
	}

	public JsonObject getImagesInfo(Long univDataUnitId, Integer limit, Integer start) throws Exception {
		List<ImageItemInfo> imagesForUnit = searchImpl
				.getImagesInfo(univDataUnitId, limit, start);
		Integer fullCountOfImages = searchImpl
				.getImagesInfo(univDataUnitId, 99999999, 0).size();

		String msg = "Size of images list for item with id: " + univDataUnitId
				+ " is: " + imagesForUnit.size();
		log.log(Level.FINEST, msg);

		// converting to json and adding URL parameter for preview
		ArrayList<ImageInfo> infArr = new ArrayList<>();
		for (ImageItemInfo info : imagesForUnit) {
			ImageInfo i = new ImageInfo();
			i.setFileName(info.getFileName());
			String realPath = new String(info.getPreviewData(), "UTF-8");
			String userUrl = "";
			String replaceStr = hh.getWebImagesReplacePath();
			String replaceTo = hh.getPublicWebImagesPath();
			userUrl = realPath.replaceFirst(replaceStr, replaceTo);
//			  В базе почему то пути лежат в виде:
//			  F:/Images/Final_s/CPSU00017/DIR0007\IMG0027.JPG, правлю это
//			  руками
			userUrl = userUrl.replace('\\', '/');
			i.setUrl(userUrl);
			if (info.getFileFormat().equals("Карта JPEG")) {
				i.setMapUrl(replaceTo + "Maps/" + rename(info.getFileName()) + "/index.html");
			}
			infArr.add(i);
		}
		JsonObjectBuilder resultBuilder = Json.createObjectBuilder();
		JsonArray jsonInfArr = jsonTools.getJsonEntitiesList(infArr);

		resultBuilder.add("items", jsonInfArr);
		resultBuilder.add("itemsCount", fullCountOfImages);
		return resultBuilder.build();
	}

	/**
	 * Формирует объект карты документа для отображения на портале
	 *
	 * @param univDataUnitId идентификатор единицы
	 * @return объект, содержащий html
	 * @throws Exception
	 */
	public JsonObject getCardHtml(Long univDataUnitId) throws Exception {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

		UnivDataUnit universalDataUnit = (UnivDataUnit) em.find(UnivDataUnit.class, univDataUnitId);

		VelocityContext templateContext = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(L10nUtils.userLangCode));
		String v_du_name = universalDataUnit.getUnitName();

		String v_du_type = "Неизвестно";
		if (universalDataUnit.getUnitType().getFullValue() != null) {
			v_du_type = universalDataUnit.getUnitType().getFullValue();
		} else if (universalDataUnit.getUnitType().getShortValue() != null) {
			v_du_type = universalDataUnit.getUnitType().getShortValue();
		} else {
			log.log(Level.FINEST,
					"DATA_UNIT TYPE DOES NOT CONTAIN FULL or SHORT values, id of DescValue: "
					+ universalDataUnit.getUnitType().getDescriptorValueId());
		}
		String v_annotation = null;
		if (universalDataUnit.getAnnotation() != null) {
			v_annotation = universalDataUnit.getAnnotation();
		}
		String v_du_contents = "Нет данных";
		if (universalDataUnit.getContents() != null) {
			v_du_contents = universalDataUnit.getContents();
		}

		templateContext.put("v_du_name", v_du_name);
		templateContext.put("v_du_type", v_du_type);
		templateContext.put("v_annotation", v_annotation);
		templateContext.put("v_du_contents", v_du_contents);

		String rawCode = universalDataUnit.getArchNumberCode();
		if (rawCode != null) {
			templateContext.put("v_du_archive_code",
					ArchiveCodeParser.getArchiveCodePrintable(universalDataUnit.getUnitType().getValueCode(),
							rawCode, universalDataUnit.getResOwner()));
		}

		// Различная информация для разных типов единиц
		String customTypeInfo = "";
		boolean isPhonoDoc = false;
		boolean isEbook = false;
		boolean isSeries = false;
		boolean isPaperFile = false;
		boolean isPaperDoc = false;

		String tmplatePath = "dataunitviev.html";
		String unitTypeName = (String) templateContext.get("unitView_DescLevel");
		switch (universalDataUnit.getUnitType().getValueCode()) {
			// опись
			case "SERIES":
				isSeries = true;
				customTypeInfo = getFormatedArchSeriesInfo(universalDataUnit.getUnivDataUnitId(),
						L10nUtils.userLangCode);
				break;
			// фонд
			case "FUND":
				customTypeInfo = getFormatedArchFundInfo(universalDataUnit.getUnivDataUnitId(),
						L10nUtils.userLangCode);
				break;
			// Дело
			case "PAPER_FILE":
				isPaperFile = true;
				customTypeInfo = getArchStorageHtml(universalDataUnit.getUnivDataUnitId(),
						L10nUtils.userLangCode);
				break;
			// документ
			case "PAPER_DOC":
				isPaperDoc = true;
				customTypeInfo = getArchDocHtml(universalDataUnit.getUnivDataUnitId(),
						L10nUtils.userLangCode);
				break;
			// фонодокумент
			case "PHONODOC":
				isPhonoDoc = true;
				unitTypeName = (String) templateContext.get("unitView_UnitType");
				customTypeInfo = getArchPhonoHtml(universalDataUnit.getUnivDataUnitId(), templateContext);
				break;
			// электронная книга
			case "EBOOK":
				isEbook = true;
				tmplatePath = "dataunitviewebook.html";
				ArchEbook ebook = (ArchEbook) em.find(ArchEbook.class, universalDataUnit.getUnivDataUnitId());
				customTypeInfo = getEbookHtml(ebook, L10nUtils.userLangCode);
				String url = ebook.getUrl();
				if (url == null) {
					url = "";
				}
				jsonBuilder.add("ebookurl", url);
				break;
		}

		templateContext.put("unitType", unitTypeName);

		String v_du_description_level = "";
		if (isPhonoDoc) {
			if (universalDataUnit.getUnitType() != null) {
				v_du_description_level = universalDataUnit.getUnitType().getFullValue();
			}
		} else if (universalDataUnit.getDescriptionLevel() != null) {
			v_du_description_level = universalDataUnit.getDescriptionLevel().getFullValue();
		}
		templateContext.put("v_du_description_level", v_du_description_level);

		StringWriter univDataUnitInfo = new StringWriter();
		Velocity.getTemplate(tmplatePath).merge(templateContext, univDataUnitInfo);

		String htmlCode = "";
		// для всех элементов, кроме электронных книг добавляем "панель"
		// навигации
		if (!isEbook) {
			htmlCode += getCardNavigationBlock(univDataUnitId);
		}
		htmlCode += univDataUnitInfo.getBuffer().toString()
				+ (isPhonoDoc ? ""
						: getDateIntervalsForDataUnit(universalDataUnit, L10nUtils.userLangCode))
				+ customTypeInfo;

		// блок "ссылки" для описей, дел, документов
		if (isSeries || isPaperFile || isPaperDoc) {
			htmlCode += getReferencesHtml(univDataUnitId);
		}

		// добавляем блок для всех карточек, кроме электронных книг
		if (!(isEbook || isPhonoDoc)) {
			htmlCode += getCardFooter(univDataUnitId);
		}

		jsonBuilder.add("cardhtml", htmlCode);
		jsonBuilder.add("isPhonoDoc", isPhonoDoc);
		return jsonBuilder.build();
	}

	private String getReferencesHtml(Long univDataUnitId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VComplexCardLinks> cq = cb.createQuery(VComplexCardLinks.class);
		Root<VComplexCardLinks> root = cq.from(VComplexCardLinks.class);
		cq.select(root);
		cq.where(cb.and(
				cb.equal(root.get("unitIdOriginal"), univDataUnitId),
				cb.isNotNull(root.get("portalSectionId"))
		));
		cq.orderBy(cb.asc(root.get("idLinkes")));
		List<VComplexCardLinks> refs = em.createQuery(cq).getResultList();

		Template tmpl = Velocity.getTemplate("references.html");
		VelocityContext ctx = new VelocityContext();
		ctx.put("refs", refs);
		StringWriter stw = new StringWriter();
		tmpl.merge(ctx, stw);
		return stw.getBuffer().toString();
	}

	/**
	 * Возвращает HTML код для отображения информации по единицы хранения
	 * фонодокумента
	 *
	 * @param phonoStorageUnitId идентификатор единицы хранения для
	 * фонодокумента
	 * @return json объект для отображения на клиенте
	 */
	public JsonObject getPhonoStorageUnitInfo(Long phonoStorageUnitId) {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

		VelocityContext templateContext = new VelocityContext(
				l10nUtils.getCardLocalizedConstsnts(L10nUtils.userLangCode));

		ArchPhonoStorageUnit unit = em.find(ArchPhonoStorageUnit.class, phonoStorageUnitId);
		String annoPath = "";
		String unitTitle = "";
		Long pathId = unit.getPathId();
		if (pathId != null) {
			UnivImagePath unitFilePath = em.find(UnivImagePath.class, pathId);
			if (unitFilePath != null) {
				annoPath = unitFilePath.getFileName();
				unitTitle = unitFilePath.getCaption();
			}
		}

		templateContext.put("unitnumber", unit.getFullNumber());
		templateContext.put("annos", em.createNamedQuery("ArchPerformanceAnnotation.findAllAnnosByUnit")
				.setParameter("suid", phonoStorageUnitId).getResultList());
		templateContext.put("annopath", annoPath);
		templateContext.put("unittitle", unitTitle);
		templateContext.put("unitid", phonoStorageUnitId);
		String server = hh.getPublicWebImagesPath();
		templateContext.put("us_path_audio", server + hh.getPhonoUnitStoreImagePath());
		templateContext.put("usa_path_audio", server + hh.getPhonoUnitStoreAnnoImagePath());

		StringWriter rawData = new StringWriter();
		Velocity.getTemplate("annosview.html").merge(templateContext, rawData);

		jsonBuilder.add("cardhtml", rawData.getBuffer().toString());
		jsonBuilder.add("isPhonoDoc", true);
		return jsonBuilder.build();
	}

	private String rename(String name) {
		StringBuilder sb = new StringBuilder();
		for (Character c : name.toCharArray()) {
			Character letter = letters.get(c);
			if (letter == null) {
				sb.append(c);
			} else {
				sb.append(letter);
			}
		}
		return sb.toString();
	}
}
