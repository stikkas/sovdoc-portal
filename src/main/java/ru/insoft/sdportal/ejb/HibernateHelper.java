package ru.insoft.sdportal.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Stateful
public class HibernateHelper {

	private Long code_news_body = 0l;
	private Long code_news_annotation = 0l;
	@Inject
	private EntityManager em;

	public Long getNewsTypeBodyCode() {
		return code_news_body;
	}

	public Long getNewsTypeAnnotationCode() {
		return code_news_annotation;
	}

	public DescriptorValue getDescValueByCode(String code) {
		DescriptorValue result = null;
		if (code == null) {
			throw new IllegalArgumentException();
		}
		try {

			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<DescriptorValue> criteriaQuery = cb
					.createQuery(DescriptorValue.class);
			Root<DescriptorValue> root = criteriaQuery
					.from(DescriptorValue.class);

			criteriaQuery.where(cb.equal(root.get("valueCode"), code));

			TypedQuery<DescriptorValue> typedQuery = em
					.createQuery(criteriaQuery);

			DescriptorValue v = typedQuery.getSingleResult();
			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@PostConstruct
	private void initContentTypeIds() {
		String newsAnonsCode = "ANNOUNCE";
		String newsBodyCode = "NEWS_BODY";
		code_news_annotation = getDescValueByCode(newsAnonsCode)
				.getDescriptorValueId();
		code_news_body = getDescValueByCode(newsBodyCode)
				.getDescriptorValueId();
	}

	private String getStringCoreParameterByCode(String parameterCode) {
		String sql = "select parameter_value from core_parameter where parameter_code='"
				+ parameterCode + "'";
		Query sq = em.createNativeQuery(sql);
		if (sq.getResultList().size() == 0) {
			return null;
		}
//		if (sq.getSingleResult() == null) {
//			return null;
//		}
		String paramValue = sq.getSingleResult().toString();
		return paramValue;
	}

	public String getWebImagesReplacePath() {
		String replacePath = getStringCoreParameterByCode("DOCUMENT_ROOT");
		return replacePath;
	}

	/**
	 * Публичный url для доступа к каталогу где хранятся превьюхи
	 *
	 * @return
	 */
	public String getPublicWebImagesPath() {
		return getStringCoreParameterByCode("SERVER_NET_ADDRESS");
	}

	/**
	 * Относительный путь до аудиозаписей единицы хранения
	 *
	 * @return
	 */
	public String getPhonoUnitStoreImagePath() {
		return getStringCoreParameterByCode("RELATIVE_PATH_AUDIO_UNITS");
	}

	/**
	 * Относительный путь до аудиозаписей аннотаций
	 *
	 * @return
	 */
	public String getPhonoUnitStoreAnnoImagePath() {
		return getStringCoreParameterByCode("RELATIVE_PATH_AUDIO_ANNOTA");
	}

	/**
	 * Сохранение в бд параметра системы, в котором хранится список
	 *
	 * @param list
	 */
	public void setImagesAllowcountries(String list) {
		if (list == null) {
			throw new IllegalArgumentException();
		}
		String sql = "update core_parameter set PARAMETER_VALUE='" + list
				+ "' where PARAMETER_CODE='WEB_IMAGES_ALLOW'";

		Query sq = em.createNativeQuery(sql);
		sq.executeUpdate();
	}

	public String getImagesAllowcountries() {
		String list = getStringCoreParameterByCode("WEB_IMAGES_ALLOW");
		if (list == null) {
			String sql = "insert into core_parameter (parameter_code,SUBSySTEM_NUMBER,PARAMETER_NAME,PARAMETER_VALUE) values ";
			sql += "('WEB_IMAGES_ALLOW',9,'Страны, из которых доступны образы','default_value')";
			Query sq = em.createNativeQuery(sql);
			sq.executeUpdate();
			return "";
		} else {
			return list;
		}

	}

}
