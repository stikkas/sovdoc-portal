/**
 * @author sorokin
 * sovdoc-portal
 * 30.10.2013 13:26:55
 */
package ru.insoft.sdportal.ejb;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import ru.insoft.sdportal.models.L18nWebPage;
import ru.insoft.sdportal.models.News;
import ru.insoft.sdportal.models.WebPage;
import ru.insoft.sovdoc.model.desc.table.DescLanguage;


@Stateless
public class PortalData {

	@EJB
	private HibernateHelper hh;
	
	@EJB
	private L10nUtils l10nUtils;
	
	@EJB
	private TematicSections ts;
	
	@EJB
	private SysUtils su;
	
	@Inject
	private EntityManager em;
	
	private Logger log;
	
	
	public PortalData(){
		log = Logger.getLogger(this.getClass().getName());
	}
	
	public JsonObject getTematicSectionRootTitle(Long id){
			String title = ts.getTematicSectionTitle(id);
			JsonObjectBuilder jobuilder = Json.createObjectBuilder();
			jobuilder.add("titleText", title);
			return jobuilder.build();
	}
	
	public JsonObject getLocaleConsts(String langCode){
		HashMap<String, String> l10nConsts = l10nUtils
				.getLocalizedConstants(langCode);
		JsonObjectBuilder locObjBdr = Json.createObjectBuilder();
		for (String s : l10nConsts.keySet()) {
			if (s != null) {
				locObjBdr.add(s,l10nConsts.get(s));
			}
		}
		return locObjBdr.build();
	}
	
	
	public String getOneNews(Long newsId,String userLang) throws Exception{
		L18nWebPage news = getLocaliezdNews(userLang, newsId);
		Template tpl = Velocity.getTemplate("singlenews.html");
		VelocityContext ctx = new VelocityContext();
		ctx.put("singlenews", news);
		StringWriter st = new StringWriter();
		tpl.merge(ctx, st);
		String result = st.getBuffer().toString();
		return result;
	}
	
	
	
	private L18nWebPage getLocaliezdNews(String browserLocaleCode, Long newsId) {
		News n = (News) em.find(News.class, newsId);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<L18nWebPage> criteriaQuery = cb.createQuery(L18nWebPage.class);
		Root<L18nWebPage> root = criteriaQuery.from(L18nWebPage.class);
		DescLanguage dl = l10nUtils.getDescLangByBrowserCode(browserLocaleCode);
		criteriaQuery.where(cb.and(cb.equal(root.get("news"), n),cb.equal(root.get("lang"), dl),cb.equal(root.get("contentType"), hh.getNewsTypeBodyCode())));
		Query q = em.createQuery(criteriaQuery);
		L18nWebPage resultPage = null;
		if (q.getResultList().size() == 0) {
			CriteriaQuery<L18nWebPage> criteriaQuery2 = cb.createQuery(L18nWebPage.class);
			Root<L18nWebPage> root2 = criteriaQuery2.from(L18nWebPage.class);
			criteriaQuery2.where(cb.and(cb.equal(root2.get("news"), n),cb.equal(root2.get("lang"), l10nUtils.getDefaultLocaleDl())));
			Query q2 = em.createQuery(criteriaQuery2);
			resultPage = (L18nWebPage) q2.getResultList().get(0);
		} else {
			resultPage = (L18nWebPage) q.getResultList().get(0);

		}
		if (resultPage.getPageName()==null){
			resultPage.setPageName("");
		}
		return resultPage;
	}
	
	
	/**
	 * Обработка запроса списка страниц "Выставок" для формирования списка для
	 * раздела "Выставки"
	 * @param req
	 * @param resp
	 * @param userLang
	 * @throws Exception
	 */
	public void getExhibitionsList(HttpServletRequest req,
			HttpServletResponse resp, String userLang) throws Exception {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebPage> criteriaQuery = cb.createQuery(WebPage.class);
		Root<WebPage> root = criteriaQuery.from(WebPage.class);
		criteriaQuery.where(cb.isNull(root.get("pageCode")));
		ArrayList<Order> orders = new ArrayList<>();
		orders.add(cb.asc(root.get("sortOrder")));
		criteriaQuery.orderBy(orders);
		Query q = em.createQuery(criteriaQuery);
		DescLanguage l = l10nUtils.getDescLangByBrowserCode(
				userLang);
		String html = "";
		
		Object[] pages = q.getResultList().toArray();

		for (int i = 0; i < pages.length; i++) {
			CriteriaQuery<L18nWebPage> criteriaQuery2 = cb.createQuery(L18nWebPage.class);
			Root<L18nWebPage> root2 = criteriaQuery2.from(L18nWebPage.class);
			criteriaQuery2.where(cb.and(cb.equal(root2.get("lang"), l),cb.equal(root2.get("page"), pages[i])));
			Query q2 = em.createQuery(criteriaQuery2);
			L18nWebPage exhPage = null;
			if (q2.getResultList().size() == 0) {
				// На языке пользователя выставок нет, показываем ему русские
				CriteriaQuery<L18nWebPage> criteriaQuery3 = cb.createQuery(L18nWebPage.class);
				Root<L18nWebPage> root3 = criteriaQuery3.from(L18nWebPage.class);
				DescLanguage lRus = l10nUtils
						.getDescLangByBrowserCode(
								l10nUtils.getDefaultLocale());
				criteriaQuery3.where(cb.and(cb.equal(root3.get("lang"), lRus),cb.equal(root3.get("page"), pages[i])));
				
				Query q3 = em.createQuery(criteriaQuery3);
				// Русская страница должна быть в базе!
				exhPage = (L18nWebPage) q3.getResultList().get(0);
			} else {
				exhPage = (L18nWebPage) q2.getResultList().get(0);
			}
			Long id = exhPage.getPage().getId();
			String link = "<p><a href=\"#showexbpage&id=" + id
					+ "\" onclick=\"window.vp.navigate(this.href)\">";
			link += exhPage.getPageName() + "</a></p>";
			html += link;
			
			
		}
		JsonObjectBuilder jobuilder = Json.createObjectBuilder();
		jobuilder.add("list", html);
		String staticContent = l10nUtils.getLocalizedPage(userLang, "exhb_static").getPageData();
		jobuilder.add("staticInfo", staticContent);
		resp.getWriter().write(jobuilder.build().toString());
	}
	
	
	
	public String getTemplateContent(ServletContext ctx,String templateName) throws Exception {
		String p = ctx.getRealPath("/templates/");
		FileInputStream fInp = new FileInputStream(p + "\\" + templateName);
		byte[] arr = new byte[fInp.available()];
		fInp.read(arr);
		fInp.close();
		String result = new String(arr, "UTF-8");
		return result;
	}
	
	
	/**
	 * Обработка запроса на получение страницы "Выставка" с заданным id
	 * 
	 * @param req
	 * @param resp
	 * @param userLang
	 * @throws Exception
	 */
	public void getOneExhibition(HttpServletRequest req,
			HttpServletResponse resp, String userLang) throws Exception {
		String idStr = req.getParameter("id");
		if (idStr == "" || idStr == null) {
			resp.sendError(500, "Нет идентификатора страницы для отображения");
			return;
		}
		Long idLong = Long.parseLong(idStr);
		WebPage p = (WebPage)em.find(WebPage.class, idLong);
		DescLanguage l = (DescLanguage) l10nUtils
				.getDescLangByBrowserCode(userLang);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<L18nWebPage> criteriaQuery = cb.createQuery(L18nWebPage.class);
		Root<L18nWebPage> root = criteriaQuery.from(L18nWebPage.class);
		criteriaQuery.where(cb.and(cb.equal(root.get("lang"), l),cb.equal(root.get("page"), p)));
		Query cr = em.createQuery(criteriaQuery);
		int resultCount = cr.getResultList().size();
		L18nWebPage exbresult = null;
		if (resultCount == 0) {
			CriteriaQuery<L18nWebPage> criteriaQuery2 = cb.createQuery(L18nWebPage.class);
			Root<L18nWebPage> root2 = criteriaQuery2.from(L18nWebPage.class);
			DescLanguage dl = l10nUtils.getDescLangByBrowserCode(
					l10nUtils.getDefaultLocale());
			criteriaQuery2.where(cb.and(cb.equal(root2.get("lang"), dl),cb.equal(root2.get("page"), p)));
			Query cc = em.createQuery(criteriaQuery2);
			exbresult = (L18nWebPage) cc.getResultList().get(0);

		} else {
			exbresult = (L18nWebPage) cr.getResultList().get(0);
		}
		Template tp = Velocity.getTemplate("singlepage.html");
		VelocityContext ctx = new VelocityContext();
		ctx.put("page", exbresult);
		tp.merge(ctx, resp.getWriter());
	}
	
	
	public void getNsaPage(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		log.log(Level.FINEST,"NSA PAGE REQUEST");
		Template tp = Velocity.getTemplate("singlepage.html");
		L18nWebPage p = l10nUtils.getLocalizedPage(
				l10nUtils.userLangCode, "nsa");
		VelocityContext ctx = new VelocityContext();
		ctx.put("page", p);
		tp.merge(ctx, resp.getWriter());
	}
	
	public String getMembers(){
		Template tp = Velocity.getTemplate("singlepage.html");
		L18nWebPage p = l10nUtils.getLocalizedPage("ru_RU",
				"members");
		VelocityContext ctx = new VelocityContext();

		ctx.put("page", p);
		StringWriter st = new StringWriter();
		tp.merge(ctx, st);
		return st.getBuffer().toString();

	}
	
	
}
