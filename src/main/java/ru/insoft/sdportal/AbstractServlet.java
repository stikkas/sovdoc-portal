package ru.insoft.sdportal;

import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ru.insoft.sdportal.ejb.SysUtils;
/**
 * Родительский класс сервлета для всех сервлетов системы.
 * Был разработан до появления такового в Ext-commons, но теперь наследуется от аналогичного 
 * в ext-commons, добавляя только необходимые функции по проверки авторизации пользователя
 */
public abstract class AbstractServlet extends ru.insoft.archive.extcommons.servlet.AbstractServlet {

	protected Logger log;
	@EJB
	private SysUtils su;

	public AbstractServlet() {
		log = Logger.getLogger(getClass().getCanonicalName());
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 468979486997390510L;

	protected Long getLoginedUser(HttpServletRequest req) {
		if (!isUserLogined(req)) {
			return null;
		} else {
			return Long.parseLong(req.getSession().getAttribute("loginedUser")
					.toString());
		}
	}

	/**
	 * Проверка, авторизован ли пользователь
	 * 
	 * @param req
	 * @return
	 */
	protected boolean isUserLogined(HttpServletRequest req) {
		HttpSession htSess = req.getSession();
		return htSess.getAttribute("loginedUser") != null;
	}

	/**
	 * Проверка, авторизован ли юзер
	 * 
	 * @param req
	 *            запрос
	 * @param userId
	 *            ид пользователя
	 */
	protected boolean isSameUserLogined(HttpServletRequest req, Long userId) {
		if (userId == null) {
			throw new IllegalArgumentException();
		}
		Object attrObj = req.getSession().getAttribute("loginedUser");
		if (attrObj != null) {
			Long authUserId = (Long) attrObj;
			boolean b = authUserId.equals(userId);
			return b;
		} else {
			return false;
		}
	}
	
	protected String getUserLang(HttpServletRequest req) {
		String userLang = "RUS";
		// http://buntic.insoft03.lan/redmine/issues/6069 пункт 2)
		// "Пока никто не осуществил перевод сайта" - выключаю автоматическое
		// определение локали пользователя
		// и загрузку версии на языке пользователя, если он поддерживается
		// системой.
		/**
		 * String userLangBr = (String) req.getSession().getAttribute("lang");
		 * if (userLangBr == null) { String locale = req.getLocale().toString();
		 * if (L10nUtils.getInstance().isLocaleSupportedBySystem(locale)) {
		 * userLang = L10nUtils.getInstance()
		 * .getDescLangByBrowserCode(locale).getLanguageCode(); } }
		 */
		return userLang;
	}
}
