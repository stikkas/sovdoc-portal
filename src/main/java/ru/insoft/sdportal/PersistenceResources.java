package ru.insoft.sdportal;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class PersistenceResources {

	@Produces
	@PersistenceContext(name="OracleEntityManager")
	private EntityManager em;
}
