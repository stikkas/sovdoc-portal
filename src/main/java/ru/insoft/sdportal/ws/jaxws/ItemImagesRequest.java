
package ru.insoft.sdportal.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class was generated by Apache CXF 2.6.4
 * Mon May 20 17:20:24 MSD 2013
 * Generated source version: 2.6.4
 */

@XmlRootElement(name = "ItemImagesRequest", namespace = "http://ws.sdportal.insoft.ru/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemImagesRequest", namespace = "http://ws.sdportal.insoft.ru/")

public class ItemImagesRequest {

    @XmlElement(name = "arg0")
    private java.lang.Long arg0;

    public java.lang.Long getArg0() {
        return this.arg0;
    }

    public void setArg0(java.lang.Long newArg0)  {
        this.arg0 = newArg0;
    }

}

