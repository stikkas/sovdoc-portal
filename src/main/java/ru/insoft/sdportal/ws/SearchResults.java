package ru.insoft.sdportal.ws;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SovdocSearchResults")
public class SearchResults {
	
	public SearchResults(){
		findedItems = new ArrayList<SearchResultShortItem>();
		totalSearchResultsCount = 0;
	}
	
	@XmlElement(required=true)
	private Integer totalSearchResultsCount;
	@XmlElement(required=true)
	private List<SearchResultShortItem> findedItems;
	public Integer getTotalSearchResultsCount() {
		return totalSearchResultsCount;
	}
	public void setTotalSearchResultsCount(Integer totalSearchResultsCount) {
		this.totalSearchResultsCount = totalSearchResultsCount;
	}
	public List<SearchResultShortItem> getFindedItems() {
		return findedItems;
	}
	public void setFindedItems(List<SearchResultShortItem> findedItems) {
		this.findedItems = findedItems;
	}
	
}
