package ru.insoft.sdportal.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.insoft.archive.extcommons.json.JsonOut;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SearchResultShortItem")
public class SearchResultShortItem implements JsonOut{
	@XmlElement(required=true)
    private Long dataUnitId;
    @XmlElement(required = true)
    private  String dataUnitName;
    @XmlElement(required = true)
    private String mathesTextBlock;
    @XmlElement(required=true)
    private String datesInfoBlock;
    @XmlElement(required = true)
    private String archiveCode;
	public Long getDataUnitId() {
		return dataUnitId;
	}
	public void setDataUnitId(Long dataUnitId) {
		this.dataUnitId = dataUnitId;
	}
	public String getDataUnitName() {
		return dataUnitName;
	}
	public void setDataUnitName(String dataUnitName) {
		this.dataUnitName = dataUnitName;
	}
	public String getMathesTextBlock() {
		return mathesTextBlock;
	}
	public void setMathesTextBlock(String mathesTextBlock) {
		this.mathesTextBlock = mathesTextBlock;
	}
	public String getDatesInfoBlock() {
		return datesInfoBlock;
	}
	public void setDatesInfoBlock(String datesInfoBlock) {
		this.datesInfoBlock = datesInfoBlock;
	}
	public String getArchiveCode() {
		return archiveCode;
	}
	public void setArchiveCode(String archiveCode) {
		this.archiveCode = archiveCode;
	}
    
    
}
