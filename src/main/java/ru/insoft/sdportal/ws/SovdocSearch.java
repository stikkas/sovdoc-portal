package ru.insoft.sdportal.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ru.insoft.sdportal.ejb.SovdocSearchImpl;

/**
 * Класс - предоставляющий интерфейс веб-сервиса для фукнций поиска Интерфейс
 * остался прежним, и предназначен только для внешних систем. (внешние системы
 * могут искать с помощью своей реализации клиента нашего веб-сервиса). Сам
 * портал теперь будет напрямую использовать ejb.
 * 
 * @author sorokin
 * 
 */
@WebService(name = "SovdocSearch")
public class SovdocSearch {

	@EJB
	private SovdocSearchImpl searchImpl;

	public SovdocSearch() {

	}

	@WebMethod(operationName = "SearchResultsRequest")
	public SearchResults getSearchResult(String query, Integer resultCount,
			Integer startWith) {
		return searchImpl.getSearchResult(query, resultCount, startWith);
	}

	@WebMethod(operationName = "ExtSearchResultsRequest")
	public synchronized SearchResults getExtendedSearchResult(
			ExtSearchReqParm params, Integer limit, Integer startWith) {
		return searchImpl.getExtendedSearchResult(params, limit, startWith);
	}

	@WebMethod(operationName = "ItemFullDataRequest")
	public String getFullElementInfo(Long elId) {
		return searchImpl.getFullElementInfo(elId);
	}

	@WebMethod(operationName = "ItemImagesRequest")
	public List<ImageItemInfo> getImagesInfo(Long dataUnitId, Integer limit, Integer start) {
		try {
			List<ImageItemInfo> l = searchImpl.getImagesInfo(dataUnitId, limit, start);
			return l;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<ImageItemInfo>();
		}

	}
}
