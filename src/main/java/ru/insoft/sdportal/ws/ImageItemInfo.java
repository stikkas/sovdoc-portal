package ru.insoft.sdportal.ws;

import ru.insoft.archive.extcommons.json.JsonOut;

public class ImageItemInfo implements JsonOut{
	private Long id;
	private Long orderId;
	private Boolean hasPreview;
	private String fileFormat;
	private String fileName;
	private byte[] previewData;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Boolean getHasPreview() {
		return hasPreview;
	}
	public void setHasPreview(Boolean hasPreview) {
		this.hasPreview = hasPreview;
	}
	public String getFileFormat() {
		return fileFormat;
	}
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	public byte[] getPreviewData() {
		return previewData;
	}
	public void setPreviewData(byte[] previewData) {
		this.previewData = previewData;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	
	
}
