package ru.insoft.sdportal.ws;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
@XmlAccessorType(XmlAccessType.FIELD)
public class StringMapWr implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2898173300354170517L;
	private HashMap<String, String>map = new HashMap<String, String>();

	public String get(Object key) {
		return map.get(key);
	}

	public String put(String key, String value) {
		return map.put(key, value);
	}

	public int size() {
		return map.size();
	}

	public Set<String> keySet() {
		return map.keySet();
	}
	
	
}
