package ru.insoft.sdportal.ws;

import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ExtendedSearchRequestParams")
public class ExtSearchReqParm {
	@XmlElement(required=true)
	private StringMapWr fieldParams;
	@XmlElement(required=true)
	private LongMapWr descValueParams;

	public ExtSearchReqParm() {
		fieldParams = new StringMapWr();
		descValueParams = new LongMapWr();
	}

	public void addFieldParam(String key,Object value){
		fieldParams.put(key, value.toString());
	}
	
	public Object getFieldParam(String fieldKey) {
		return fieldParams.get(fieldKey);
	}

	public Set<String> fieldParamKeys() {
		return fieldParams.keySet();
	}

	public void addDescValueParm(Long descValueGroupId, Long descValueCode) {
		descValueParams.put(descValueGroupId, descValueCode);
	}

	public Long getDescValue(Long descValueGroupId) {
		return descValueParams.get(descValueGroupId);
	}

	public Set<Long> descValueKeys() {
		return descValueParams.keySet();
	}
	
	public boolean isHasDescValueParams(){
		return descValueParams.keySet().size()!=0;
	}
}
