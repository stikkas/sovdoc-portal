package ru.insoft.sdportal.ws;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class LongMapWr implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2642330861352988995L;
	public HashMap<Long,Long> map = new HashMap<Long, Long>();

	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	public Long get(Object key) {
		return map.get(key);
	}

	public Long put(Long key, Long value) {
		return map.put(key, value);
	}

	public int size() {
		return map.size();
	}

	public Set<Long> keySet() {
		return map.keySet();
	}
	
}
