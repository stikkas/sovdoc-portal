/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.insoft.sdportal;

import java.io.InputStream;
import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

/**
 *
 * @author melnikov
 */
public class PrefixedClasspathResourceLoader extends ClasspathResourceLoader
{
    private String prefix = "";

    @Override
    public InputStream getResourceStream(String name) throws ResourceNotFoundException 
    {
        return super.getResourceStream(prefix + name);
    }

    @Override
    public void init(ExtendedProperties configuration) 
    {
        prefix = configuration.getString("prefix", "");
        super.init(configuration);
    }
    
}
