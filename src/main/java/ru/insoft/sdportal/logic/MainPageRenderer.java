package ru.insoft.sdportal.logic;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import ru.insoft.sdportal.ejb.HibernateHelper;
import ru.insoft.sdportal.ejb.L10nUtils;
import ru.insoft.sdportal.ejb.SysUtils;
import ru.insoft.sdportal.models.L18nWebPage;
import ru.insoft.sdportal.models.WebPage;
import ru.insoft.sovdoc.model.desc.table.DescLanguage;

public class MainPageRenderer {
	private static int SHORT_NEWS_ON_MAIN = 2;

	public MainPageRenderer(L10nUtils l10nUtils, HibernateHelper hh,
			SysUtils su, EntityManager em) {
		this.l10nUtils = l10nUtils;
		this.hh = hh;
		this.su = su;
		this.em = em;
	}

	private EntityManager em;

	private L10nUtils l10nUtils;
	private HibernateHelper hh;
	private SysUtils su;

	public String getShortNewsHtml(String lang) {
		NewsRenderer nr = new NewsRenderer(l10nUtils, hh, em);
		Vector<Long> newses = nr.getLocalizedNewsAnonsesIds(lang);
		String shortNews = "";
		SimpleDateFormat sdf = su.getSdf();

		int max;
		if (newses.size() > SHORT_NEWS_ON_MAIN) {
			max = SHORT_NEWS_ON_MAIN;
		} else {
			max = newses.size();
		}
		for (int i = 0; i < max; i++) {
			L18nWebPage p = (L18nWebPage) em.find(L18nWebPage.class,
					newses.get(i));
			shortNews += "<p><b>" + sdf.format(p.getNews().getDate())
					+ "</p></b>";
			// здесь будет только анонс
			shortNews += "<p>" + p.getPageData() + " ";
			String onclick = "onclick=\"window.vp.navigate(this.href,"
					+ p.getNews().getId() + ")\" ";
			shortNews += "<a " + onclick + " href=\"#shownews&id="
					+ p.getNews().getId() + "\"> &#45&#62 Подробнее</a>";
			shortNews += "</p>";
			shortNews += "<hr>";
		}
		return shortNews;
	}

	public String getExhibitionShort(String lang) {
		String pageLinks = "";
		String exhF1 = "<h1><p>"
				+ l10nUtils.getCardLocalizedConstsnts(lang).get(
						"sys_exhibition") + "</p></h1>";
		pageLinks += exhF1;
		for (L18nWebPage p : getPagesForLocale(lang)) {
			pageLinks += "<div><p>";
			pageLinks += p.getPageName();
			pageLinks += "</p>";
			Long newsId = p.getPage().getId();
			String onclick = "onclick=\"window.vp.navigate(this.href)\" ";
			pageLinks += "<a " + onclick + "href=\"#showexbpage&id="
					+ newsId + "\">&#45&#62 Подробнее</a>";
			pageLinks += "</div><hr>";
		}
		return pageLinks;
	}

	public String getMainPageHtml(String lang) {
		String result = "<p>";
		// это кешируеются внутри l10nUtils;
		result += l10nUtils.getLocalizedPage(lang, "main").getPageData();
		result += "</p>";
		return result;
	}

	/**
	 * get all user's pages for current browserLocale specified as
	 * 
	 * @param browserLocaleCode
	 *            which usually result of request.getLocale().toString()
	 * @return
	 */
	private Vector<L18nWebPage> getPagesForLocale(String browserLocaleCode) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WebPage> criteriaQuery = cb.createQuery(WebPage.class);
		Root<WebPage> root = criteriaQuery.from(WebPage.class);
		criteriaQuery.where(cb.isNull(root.get("pageCode")));
		criteriaQuery.orderBy(new Order[] { cb.asc(root.get("sortOrder")) });
		Query q = em.createQuery(criteriaQuery);
		Object[] pages = q.getResultList().toArray();
		Vector<WebPage> resultV = new Vector<WebPage>();
		for (int i = 0; i < pages.length; i++) {
			resultV.add((WebPage) pages[i]);
		}
		return getUserPages(resultV, browserLocaleCode);
	}

	/**
	 * get user's localized web pages
	 * 
	 * @param pages
	 *            vector of WebPage objects
	 * @param browserLocaleCode
	 *            usually result of request.getLocale().toString();
	 * @return
	 */
	private Vector<L18nWebPage> getUserPages(Vector<WebPage> pages,
			String browserLocaleCode) {
		Vector<L18nWebPage> pagesLoc = new Vector<L18nWebPage>();
		for (int j = 0; j < pages.size(); j++) {

			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<L18nWebPage> criteriaQuery = cb
					.createQuery(L18nWebPage.class);
			Root<L18nWebPage> root = criteriaQuery.from(L18nWebPage.class);
			criteriaQuery.where(cb.and(
					cb.equal(root.get("page"), pages.get(j)), cb.equal(root
							.get("lang"), l10nUtils
							.getDescLangByBrowserCode(browserLocaleCode))));
			Query q = em.createQuery(criteriaQuery);
			if (q.getResultList().size() == 0) {

				CriteriaQuery<L18nWebPage> criteriaQuery2 = cb
						.createQuery(L18nWebPage.class);
				Root<L18nWebPage> root2 = criteriaQuery2
						.from(L18nWebPage.class);
				DescLanguage l = l10nUtils.getDescLangByBrowserCode(l10nUtils
						.getDefaultLocale());
				criteriaQuery2.where(cb.and(
						cb.equal(root2.get("page"), pages.get(j)),
						cb.equal(root2.get("lang"), l)));
				Query q2 = em.createQuery(criteriaQuery2);

				pagesLoc.add((L18nWebPage) q2.getResultList().get(0));
			} else {
				pagesLoc.add((L18nWebPage) q.getResultList().get(0));
			}
		}
		return pagesLoc;
	}

}
