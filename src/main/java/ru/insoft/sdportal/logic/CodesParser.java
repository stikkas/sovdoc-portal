package ru.insoft.sdportal.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Парсер
 */
public class CodesParser {
	private static String parseRow(String input) {
		String regexp = ".*[(]\\d+[,]\\s[']([\\D|\\']+)['][,]\\s['][\\D|\\']+['][,]\\s[']([\\D|\\']+)['][)].*";
		Pattern p = Pattern.compile(regexp);
		Matcher matcher = p.matcher(input);
		boolean found = matcher.matches();
		// System.out.println("matches: "+found);
		// System.out.println("group count: "+matcher.groupCount());
		// System.out.println("0: "+matcher.group(0));
		// System.out.println("1: "+matcher.group(1));
		// System.out.println("2: "+matcher.group(2));
		String output = "Запись не соответствует регулярному выражению";
		if (found) {
			String countryCode = matcher.group(2);
			String countryRuName = matcher.group(1);
			output = "array['" + countryCode + "']='" + countryRuName + "';";
		}
		return output;
	}

	public static void main(String[] args) throws Exception {
		File f = new File("D:\\workspace\\sovdoc-portal\\CountryCodes.txt");
		BufferedReader br = new BufferedReader(new FileReader(f));
		String temp;
		String result = "var array = new Array();";
		while ((temp = br.readLine()) != null) {
			result += parseRow(temp);
		}
		br.close();
		File fo = new File(
				"D:\\workspace\\sovdoc-portal\\src\\main\\webapp\\admin\\js\\countrycodes.js");
		if (fo.exists()) {
			System.out.println("Целевой файл уже существует, удаляю");
			fo.delete();
			fo.createNewFile();
		}
		FileWriter fw = new FileWriter(fo);
		fw.write(result);
		fw.close();
		System.out.println("Операция успешно завершена");
	}
}
