package ru.insoft.sdportal.logic.mailing;

import java.io.StringWriter;
import java.util.Properties;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import ru.insoft.sdportal.ejb.L10nUtils;
import ru.insoft.sdportal.models.L18nWebPage;

/**
 *
 * @author sorokin
 */
public class PostMessage {
	
	
	public PostMessage(L10nUtils l10nUtils){
		this.l10nUtils = l10nUtils;

	}
	
	private L10nUtils l10nUtils;
	
	private String recepientAddr;
	private String messageTopic;
	private String messageBody;
	
	public String getRecepient(){
		return this.recepientAddr;
	}
	public String getTopic(){
		return this.messageTopic;
	}
	
	public String getBody(){
		return this.messageBody;
	}
	
	
	public enum msgTypes{
		MAIL_CONFIRM,MAIL_FEEDBACK
	}
	
	public PostMessage(){
		Properties props = new Properties();
		Velocity.init(props);
	}
	
	public void configureMessage(msgTypes type,String ... params)throws Exception{
		if (type==msgTypes.MAIL_CONFIRM){
			configureMailConfirmMessage(params);
			return;
		}
		if (type==msgTypes.MAIL_FEEDBACK){
			confitureMailFeedBack(params);
		}
	}
	protected void confitureMailFeedBack(String ... params){
		recepientAddr = params[0];
		messageTopic = "SOVDOC-PORTAL FEEDBACK";
		messageBody = params[1];
	}
	
	protected void configureMailConfirmMessage(String ... params)throws Exception{
		recepientAddr = params[0];
		String locale = params[1];
		String confirmUrl = params[2];
		String userName = params[3];
		L18nWebPage page = l10nUtils.getLocalizedPage(locale,"USER_MAIL_CONFIRM_MSG");
		messageTopic = page.getPageName();
		String templateBody = page.getPageData();
		VelocityContext ctx = new VelocityContext();
		StringWriter stw = new StringWriter();
		ctx.put("username", userName);
		ctx.put("url", confirmUrl);
		Velocity.evaluate(ctx, stw,"USER_MAIL_CONFIRM_MSG", templateBody);
		messageBody=stw.toString();
	}
	
}
