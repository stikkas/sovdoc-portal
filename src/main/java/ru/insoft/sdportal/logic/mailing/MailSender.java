package ru.insoft.sdportal.logic.mailing;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sorokin
 */
public class MailSender extends Thread {

	private Logger log;
	private PostMessage msg = null;

	public MailSender(){
		log =(Logger) Logger.getLogger(this.getClass().getCanonicalName());
	}
	public void setMessage(PostMessage msg) {
		this.msg = msg;
	}

	private void send() {
		if (this.msg == null) {
			throw new IllegalStateException(""
					+ "Message for sending is not set. "
					+ "You must set Message with setMessage method "
					+ "before starting this thread");
		}
		final String username = "sovdoc.notify@gmail.com";
		final String password = "sovdocnotify";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session;
		session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(username, password);
			}
		});
		try {
			log.log(Level.FINE,"SENDING MAIL:");
			log.log(Level.FINE,"RECEPIENT: "+msg.getRecepient());
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("sovdoc-notify@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(msg.getRecepient()));
			message.setHeader("charset", "utf-8");
			message.setSubject(msg.getTopic());
			message.setContent(msg.getBody(), "text/html;charset=utf-8");
			Transport.send(message);
			log.log(Level.FINE,"SENDED");
		} catch (MessagingException e) {
			log.log(Level.FINE,"SENDING OF MAIL WAS FAILED");
			log.log(Level.FINE,"RECEPIENT: "+msg.getRecepient());
			log.log(Level.FINE,"BODY: "+msg.getBody());
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		this.send();
	}
}
