package ru.insoft.sdportal.logic;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import ru.insoft.sdportal.ejb.HibernateHelper;
import ru.insoft.sdportal.ejb.L10nUtils;
import ru.insoft.sdportal.models.L18nWebPage;


public class NewsRenderer {

	private EntityManager em;
	
	private static int NEWS_BY_PAGE = 4;

	private L10nUtils l10nUtils;
	private HibernateHelper hh;

	public NewsRenderer(L10nUtils l10nUtils, HibernateHelper hh,EntityManager em) {
		this.l10nUtils = l10nUtils;
		this.hh = hh;
		this.em = em;
	}

	private Long getNewsCount() {
		String hql = "select count(distinct web_news.news_id) from web_news,web_pages_localized  ";
		hql += " where  web_news.news_id=web_pages_localized.news_id";
//		hql += "CONTENT_TYPE_ID = " + hh.getNewsTypeAnnotationCode();
		Query q = em.createNativeQuery(hql);
		Long i = ((BigDecimal) q.getSingleResult()).longValue();
		return i;
	}
	
	public Vector<Long> getLocalizedNewsAnonsesIds(String browserLocalecode){
		return getLocalizedNewsDataIds(browserLocalecode,hh.getNewsTypeAnnotationCode());
	}
	
	public Vector<Long> getLocalizedNewsDataIds(String browserlocalecode){
		return getLocalizedNewsDataIds(browserlocalecode, hh.getNewsTypeBodyCode());
	}
	
	/**
	 * Возвращает айдишники страниц-анонсов новостей
	 * @param browserLocaleCode
	 *            string value (possible values are "en_US" or "ru_RU"
	 * @return
	 */
	private Vector<Long> getLocalizedNewsDataIds(String browserLocaleCode,Long contentTypeCode) {

		String defSysLang = l10nUtils.getDefaultLocaleDl().getLanguageCode();
		// String reqLang
		// =l10nUtils.getDescLangByBrowserCode(browserLocaleCode).getLanguageCode();
		String nativeQuery = ""
				+ "select news_date, "
				+ "("
				// + "(select LOCALIZED_PAGE_ID from web_pages_localized where "
				// + "WEB_PAGES_LOCALIZED.NEWS_ID=WEB_NEWS.NEWS_ID "
				// + "and WEB_PAGES_LOCALIZED.LANGUAGE_CODE='" + reqLang
				// + "'),"
				+ "select LOCALIZED_PAGE_ID from web_pages_localized where "
				+ "WEB_PAGES_LOCALIZED.NEWS_ID=WEB_NEWS.NEWS_ID "
				+ "and WEB_PAGES_LOCALIZED.LANGUAGE_CODE='" + defSysLang + "' "
				+ "and CONTENT_TYPE_ID=" + contentTypeCode.toString()
				+ ") as lpid" + " from web_news order by news_date desc, news_id desc";
		Vector<Long> ids = new Vector<Long>();

		Query q = em.createNativeQuery(nativeQuery);
		Object[] r = q.getResultList().toArray();
		for (int i = 0; i < r.length; i++) {
			Object[] row = (Object[]) r[i];
			if (row[1] != null) {
				ids.add(Long.parseLong(row[1].toString()));
			}
		}

		return ids;
	}

	public Long getPagesCount(Integer newsPerPage, String browserLocale) {
		return (getNewsCount() / newsPerPage) + 1;
	}

	public String getTemplatePath() {
		return "pagefornews.html";
	}

	/**
	 * @param browserLocaleCode
	 * @param page
	 * @return
	 */
	public String getNewsPage(String browserLocaleCode, Integer page) {
		Template newsTemplate = Velocity.getTemplate("news.html");
		HashMap<String, String> allConsts = l10nUtils
				.getLocalizedConstants(browserLocaleCode);
		String p_page = allConsts.get("p_page");
		String p_from = allConsts.get("p_from");
		HashMap<String, Object> values = getTemplateValues(browserLocaleCode,
				page);
		VelocityContext ctx = new VelocityContext();
		for (String s : values.keySet()) {
//			if ("newsList".equals(s)) {
//				Object[] arr = (Object[]) values.get(s);
//				for (Object o : arr) {
//					em.refresh(o);
//				}
//			}
			ctx.put(s, values.get(s));
		}
		ctx.put("p_page", p_page);
		ctx.put("p_from", p_from);
		StringWriter stW = new StringWriter();
		newsTemplate.merge(ctx, stW);
		String result = stW.toString();
		return result;
	}

	private HashMap<String, Object> getTemplateValues(String browserLocaleCode,
			Integer p) {
		if (!(p != null && !p.equals(""))) {
			p = 1;
		}
		Vector<Long> ids = getLocalizedNewsDataIds(browserLocaleCode);
		// Vector<L18nWebPage> newsLocalizedPages = getLocalizedNewsPages(ids);
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("p", p);
		String pageLinks = "";
		Long pcount = getPagesCount(NEWS_BY_PAGE, browserLocaleCode);
		for (int i = 1; i < pcount + 1; i++) {
			if (i != p) {
				String onClick = " onclick=window.vp.navigate(this.href," + i
						+ ") ";
				pageLinks += "<a" + onClick + " href=#news&p=" + i + ">" + i
						+ "</a> ";
			} else {
				pageLinks += " <b>" + i + "</b> ";
			}
		}
		result.put("pageLinks", pageLinks);
		Integer startNewsIndex = p - 1;
		int from = startNewsIndex * NEWS_BY_PAGE;
		int to = from + NEWS_BY_PAGE;
		if (to > ids.size()) {
			to = ids.size();
		}
		List<Long> requestedLocalizedNews = ids.subList(from, to);
		Vector<Object> els = new Vector<Object>();
		for (Long l : requestedLocalizedNews) {
			L18nWebPage loadedpage = (L18nWebPage) em.find(L18nWebPage.class, l);
			//fix #7963
			if (loadedpage.getPageName()==null){
				loadedpage.setPageName("");
			}
			els.add(loadedpage);
		}
		result.put("newsList", els.toArray());
		result.put("pcount", pcount);
		return result;
	}
}
