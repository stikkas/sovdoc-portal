/**
 * @author sorokin
 * sovdoc-portal
 * 10.10.2013 14:59:01
 */
package ru.insoft.sdportal.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang.math.NumberUtils;
import ru.insoft.sdportal.DictCode;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

public class ArchiveCodeParser {

    private String rawCode;
    List<String> codeArray = new ArrayList<>();
    private int size;

    public ArchiveCodeParser(String rawArchiveCode) {
        rawCode = rawArchiveCode;
        String[] arr = rawCode.split("\u0001");
        if (1 <= arr.length) {
            if (!rawCode.startsWith("\u0001") && arr[0].length() > 2) {
                codeArray.add("");
            }
            codeArray.addAll(Arrays.asList(arr));
        }
        size = codeArray.size();
    }

    public String getFundPrefix() {
        if (size >= 1) {
            return codeArray.get(0);
        }
        return "";
    }

    public Integer getFund() {
        if (size >= 2) {
            String fundStr = codeArray.get(1);
            if (!"".equals(fundStr) && NumberUtils.isNumber(fundStr)) {
                return Integer.parseInt(fundStr);
            }
        }
        return 0;
    }

    public String getFundLitera() {
        if (size >= 3) {
            return codeArray.get(2);
        }
        return "";
    }

    public Integer getOpis() {
        if (size >= 4) {
            String opisString = codeArray.get(3);
            if (!"".equals(opisString)) {
                return Integer.parseInt(opisString);
            }
        }
        return 0;
    }

    public String getOpisLitera() {
        if (size >= 5) {
            return codeArray.get(4);
        }
        return "";
    }

    public Integer getDelo() {
        if (size >= 6) {
            String deloStr = codeArray.get(5);
            if (!"".equals(deloStr)) {
                return Integer.parseInt(deloStr);
            }
        }
        return 0;
    }

    public String getDeloLitera() {
        if (size >= 7) {
            return codeArray.get(6);
        }
        return "";
    }

    public String getDeloVolume() {
        if (size >= 8) {
            return codeArray.get(7);
        }
        return "";
    }

    public String getDeloPart() {
        if (size >= 9) {
            return codeArray.get(8);
        }
        return "";
    }

    public Integer getListStart() {
        if (size >= 10) {
            String docStr = codeArray.get(9);
            if (!"".equals(docStr)) {
                return Integer.parseInt(docStr);
            }
        }
        return 0;
    }

    public String getLists() {
        if (size >= 11) {
            return codeArray.get(10);
        }
        return "";
    }

    public static String getShortCodeForNavigation(long unitType, String rawCode) {
        ArchiveCodeParser ap = new ArchiveCodeParser(rawCode);
        String code = "";
        // фонд
        if (unitType == 242l) {
            code = "Фонд " + ap.getFundPrefix() + ap.getFund().toString() + ap.getFundLitera();
        }
        // опись
        if (unitType == 243l) {
            code = "Опись " + ap.getOpis().toString() + ap.getOpisLitera();
        }
        // дело
        if (unitType == 244l) {
            code = "Дело " + ap.getDelo().toString() + ap.getDeloLitera()
                    + ap.getDeloVolume() + ap.getDeloPart();
        }
        // Документ
        if (unitType == 245l) {
            if (!"".equals(ap.getLists())) {
                code = "Лист " + ap.getLists();
            } else {
                code = "Лист" + ap.getListStart();
            }
        }
        return code;
    }

    /**
     * Формирует архивный шифр в строковом формате
     *
     * @param typeCode код типа элемента
     * @param rawCode архивный шифр как он хранится в базе
     * @param archive архив-владелец ресурса
     * @return архивный шифр
     */
    public static String getArchiveCodePrintable(String typeCode, String rawCode,
            DescriptorValue archive) {
        String owner = (archive == null) ? "" : archive.getShortValue() + ". ";
        String code = "";

        ArchiveCodeParser parser = new ArchiveCodeParser(rawCode);

        Integer number = parser.getFund();
        if (number != 0) {
            String prefix = parser.getFundPrefix();
            code += String.format("Ф. %s%d%s", prefix.isEmpty() ? "" : prefix + "-", number, parser.getFundLitera());
        }
        number = parser.getOpis();
        if (number != 0) {
            if (!code.isEmpty()) {
                code += ". ";
            }
            code += String.format("Оп. %d%s", number, parser.getOpisLitera());
        }
        number = parser.getDelo();
        if (number != 0) {
            if (!code.isEmpty()) {
                code += ". ";
            }
            if (DictCode.PHONODOC.equals(typeCode)) {
                code += String.format("Ед.уч. %d%s", number, parser.getDeloLitera());
            } else {
                code += String.format("Д. %d%s%s%s", number, parser.getDeloLitera(),
                        parser.getDeloVolume(), parser.getDeloPart());
            }
        }
        number = parser.getListStart();
        String lists = parser.getLists();
        if (number != 0 || !lists.isEmpty()) {
            if (!code.isEmpty()) {
                code += ". ";
            }
            code += "Л. ";
//            if (number != 0) {
//                code += number;
//            } else {
                code += lists;
//            }
        }

        return owner + code;
    }

}
