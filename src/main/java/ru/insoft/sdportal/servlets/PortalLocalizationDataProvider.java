package ru.insoft.sdportal.servlets;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.sdportal.AbstractServlet;

public class PortalLocalizationDataProvider extends AbstractServlet {

	private static final long serialVersionUID = -5202435459548214266L;



	@Override
	protected void handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String action = req.getParameter("action");
		if ("changeLocale".equals(action)) {
			String chosedLocale = req.getParameter("lang");
			req.getSession().setAttribute("lang", chosedLocale);
		}
		
	}

}
