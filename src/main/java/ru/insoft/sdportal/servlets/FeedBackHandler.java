package ru.insoft.sdportal.servlets;

import java.util.logging.Level;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.HibernateHelper;
import ru.insoft.sdportal.ejb.SysUtils;
import ru.insoft.sdportal.logic.mailing.MailSender;
import ru.insoft.sdportal.logic.mailing.PostMessage;

/**
 *
 * @author sorokin
 */
public class FeedBackHandler extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3544088481375795668L;

	@Override
	public String getServletInfo() {
		return "Обратная связь: обработка клиентских запросов";
	}
	@EJB
	private HibernateHelper hh;
	
	@EJB
	private SysUtils su;

	@Override
	protected void handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		log.log(Level.FINEST,"Отзыв успешно отправлен");
		FailMessage fm = null;
		try {
			PostMessage pm = new PostMessage();
			String userMessage = req.getParameter("msg");
			String mailRecepients = "support@archives.ru,natalya@mail.rgantd.ru";
			pm.configureMessage(PostMessage.msgTypes.MAIL_FEEDBACK,mailRecepients,userMessage);
			MailSender ms = new MailSender();
			ms.setMessage(pm);
			ms.start();
			fm = new FailMessage(true, "Сообщение успешно отправлено");
		} catch (Exception e) {
			e.printStackTrace();
		}
		String jsonResp = jsonTools.getJsonForEntity(fm).toString();
		resp.getWriter().write(jsonResp);
	}
}
