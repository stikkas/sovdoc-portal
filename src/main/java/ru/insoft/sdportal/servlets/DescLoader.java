package ru.insoft.sdportal.servlets;

import java.util.Arrays;
import javax.ejb.EJB;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.sdportal.AbstractServlet;

/**
 * Servlet implementation class DescLoader
 */
@WebServlet("/DescLoader")
public class DescLoader extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -232511896164046275L;
	@EJB
	private ru.insoft.sdportal.ejb.DescLoader dl;

	public DescLoader() {
		super();
	}

	@Override
	public void handleRequest(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		String action = req.getParameter("action");

		String userLang = getUserLang(req);

		if (action.equals("getGroups")) {
			JsonObject result = dl.getGroups();
			resp.getWriter().write(result.toString());
		}
		if (action.equals("getValuesTree")) {
			String groupIdStr = req.getParameter("groupId");
			if (groupIdStr == null) {
				return;
			}
			Long groupId = Long.parseLong(groupIdStr);
			JsonObject result = dl.getValuesTree(groupId, "RUS");
			resp.getWriter().write(result.toString());
			return;
		}
		if (action.equals("getValuesGrid")) {
			String groupIdStr = req.getParameter("groupId");
			Long groupId = null;
			if (groupIdStr != null && !"".equals(groupIdStr)) {
				groupId = Long.parseLong(groupIdStr);
			}
			JsonObject result = dl
					.getValuesGrid(groupId, req.getParameter("q"));
			resp.getWriter().write(result.toString());
		}
		if ("getPath".equals(action)) {
			String valueIdStr = req.getParameter("valueId");
			Long valueId = Long.parseLong(valueIdStr);
			JsonObject result = dl.getPath(valueId);
			resp.getWriter().write(result.toString());
		}

		if ("getEbookSeries".equals(action)) {
			JsonArray arr = dl.getEbookSeries();
			resp.getWriter().write(arr.toString());
		}

		if ("getCitizenship".equals(action)) {
			resp.getWriter().write(
					dl.getLocalizedDescValuesForGroup("user_citizenship",
							userLang).toString());
			return;
		}
		if ("getDegriees".equals(action)) {
			resp.getWriter().write(
					dl.getLocalizedDescValuesForGroup("user_degree", userLang)
							.toString());
			return;
		}
		if ("getRanks".equals(action)) {
			resp.getWriter().write(
					dl.getLocalizedDescValuesForGroup("user_rank", userLang)
							.toString());
			return;
		}
		if ("getElementType".equals(action)) {
			resp.getWriter().write(
					dl.getLocalizedDescValuesForGroup("UNIV_UNIT_TYPE", userLang, Arrays.asList("EBOOK"))
							.toString());
			return;
		}
		if ("getElibAuthors".equals(action)) {
			resp.getWriter().write(
					dl.getLocalizedDescValuesForGroup("LITERATURE_AUTHOR",
							userLang).toString());
			return;
		}
		if ("getUnitLanguages".equals(action)) {
			resp.getWriter().write(
					dl.getLocalizedDescValuesForGroup("DOC_LANGUAGE", userLang)
							.toString());

		}
		if ("getOrgStructure".equals(action)) {
			JsonObject result = dl.getOrgStructure();
			resp.getWriter().write(result.toString());
		}

	}

}
