package ru.insoft.sdportal.servlets;

import java.io.IOException;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.archive.extcommons.ejb.JsonTools;
import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.UserManager;
import ru.insoft.sdportal.models.ui.UserInfoResponse;

/**
 * 
 * @author sorokin
 */
public class UsersManager extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7951945847557941003L;
	@EJB
	private UserManager um;
	@EJB
	private JsonTools jsonTools;

	@Override
	public void handleRequest(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, Exception {
		String action = req.getParameter("action");
		if (action == null) {
			resp.sendError(500, "Action parameter can not be null!");
		}
		log.log(Level.FINEST, "USERMANAGER SERVLET, aciton: " + action);
		if (action.equals("regUser")) {
			regUser(req, resp);
		}
		if (action.equals("confirmRegistration")) {
			confirmRegistration(req, resp);
		}
		if (action.equals("login")) {
			checkPassword(req, resp);
		}
		if (action.equals("logout")) {
			logout(req, resp);
		}
		if (action.equals("getAccountInfo")) {
			getAccountInfo(req, resp);
		}
		if (action.equals("updateAccountInfo")) {
			updateAccountInfo(req, resp);
		}
		if (action.equals("changePass")) {
			changePasswd(req, resp);
		}
	}

	private void changePasswd(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		String oldPasswd = req.getParameter("oldPwd");
		String newPasswd = req.getParameter("newPwd");
		if (isUserLogined(req)) {
			Long userId = getLoginedUser(req);
			JsonObject result = um.changePasswd(userId, oldPasswd, newPasswd);
			resp.getWriter().write(result.toString());
		}
	}

	private void updateAccountInfo(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String userIdS = req.getParameter("userId");
		log.log(Level.FINEST, "UPDATE ACCOUNT INFO, USERID " + userIdS);
		if (userIdS != null) {
			Long userId = Long.parseLong(userIdS);
			JsonObject result = null;
			if (isSameUserLogined(req, userId)) {
				log.log(Level.FINEST, "UPDATE ACCOUNT INFO, ACCESS OK");				
				UserInfoResponse userInfoResp = new UserInfoResponse();
				userInfoResp.setMail(req.getParameter("email").toString());
				userInfoResp.setSecondName(req.getParameter("last_name"));
				userInfoResp.setFirstName(req.getParameter("first_name"));
				userInfoResp.setMiddleName(req.getParameter("middle_name"));
				userInfoResp.setWorkPlace(req.getParameter("work_place"));
				userInfoResp.setEducation(Long.parseLong(req.getParameter("education")));
				userInfoResp.setRank(Long.parseLong(req.getParameter("rank")));
				userInfoResp.setPhone(req.getParameter("phone"));
				userInfoResp.setBirthday(req.getParameter("birthday"));
				userInfoResp.setCitizenchipId(Long.parseLong(req.getParameter("citizenship")));
				result = um.updateAccountInfo(userId, userInfoResp);
			}else{
				FailMessage fm = new FailMessage(false,"Вы не авторизованы в системе");
				result = jsonTools.getJsonForEntity(fm);
			}
			resp.getWriter().write(result.toString());
		}
		
	}

	private void getAccountInfo(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		Long userId = Long.parseLong(req.getParameter("userId"));
		boolean isAccessOk = isSameUserLogined(req, userId);
		JsonObject result = null; 
		if (isAccessOk) {
			result = um.getAccountInfo(userId);
		} else {
			FailMessage fm = new FailMessage(false,"Доступ запрещен");
			result = jsonTools.getJsonForEntity(fm);
		}
		resp.getWriter().write(result.toString());

	}

	private void logout(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		FailMessage fm = null;
		log.log(Level.FINEST, "LOGOUT");
		if (isUserLogined(req)) {
			req.getSession().removeAttribute("loginedUser");
			fm = new FailMessage(true, "Успешно");
		} else {
			fm = new FailMessage(false, "Ошибка");
		}
		resp.getWriter().write(jsonTools.getJsonForEntity(fm).toString());
		resp.sendRedirect(getSystemAddr(req));
	}

	private void checkPassword(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		String login = req.getParameter("login");
		String pass = req.getParameter("pass");
		JsonObject result = um.checkPassword(login, pass);
		resp.getWriter().write(result.toString());
	}

	private String getSystemAddr(HttpServletRequest req) {
		String redirectUrl = req.getScheme() + "://" + req.getServerName()
				+ ":" + req.getServerPort() + req.getContextPath() + "/";
		return redirectUrl;
	}

	private void confirmRegistration(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String redirectUrl = req.getScheme() + "://" + req.getServerName()
				+ ":" + req.getServerPort() + req.getContextPath();
		String confirmationCode = req.getParameter("code");
		boolean isConfirm = um.isRegistrationConfirmed(confirmationCode);

		if (isConfirm) {
			resp.sendRedirect(redirectUrl + "#regconfirmok");
		} else {
			resp.sendRedirect(redirectUrl + "#regconfirmfail");
		}
	}

	private void regUser(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		JsonObject o = um.regUser(req);
		resp.getWriter().write(o.toString());
	}





}
