package ru.insoft.sdportal.servlets;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.PageMaker;


public class AdminDelPage extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1944349609699582667L;
	@EJB
	private PageMaker pm;


	@Override
	protected void handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		req.setCharacterEncoding("UTF-8");
		String pagecode = req.getParameter("pagecode");
		log.info("Запрос на удаление страницы с кодом: " + pagecode);
		try {
			pm.dropPage(pagecode);
			resp.getWriter().write("Удаление страницы успешно выполнено");
		} catch (Exception e) {
			e.printStackTrace();
			resp.getWriter().write("Ошибка при удалении страницы");
		}
		
	}

}
