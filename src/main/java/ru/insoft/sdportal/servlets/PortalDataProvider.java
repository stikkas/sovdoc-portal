package ru.insoft.sdportal.servlets;

import java.io.IOException;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.L10nUtils;
import ru.insoft.sdportal.ejb.PortalData;
import ru.insoft.sdportal.ejb.TematicSections;
import ru.insoft.sdportal.logic.MainPageRenderer;

/**
 * Servlet implementation class PortalDataProvider
 */
@WebServlet(description = "provide data for portal's pages (news,pages data, etc)", urlPatterns = { "/PortalDataProvider" })
public class PortalDataProvider extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7274807818198800913L;

	@EJB
	private TematicSections ts;

	@EJB
	private L10nUtils l10nUtils;

	@EJB
	private PortalData pdp;

	public PortalDataProvider() {
		super();
		initActionToPageCodeMap();
	}

	private HashMap<String, String> actionToPageCode;

	private void initActionToPageCodeMap() {
		actionToPageCode = new HashMap<String, String>();
		actionToPageCode.put("getRegOkPage", "USER_REG_OK");
		actionToPageCode.put("getActivationOkPage", "USER_MAIL_CONFIRMED");
		actionToPageCode.put("getActivationFail", "USER_ACC_ACTIVATION_FAIL");
        actionToPageCode.put("portal_doccomplex", "portal_doccomplex");
		actionToPageCode.put("SEARCH_HELP", "SEARCH_HELP");
		actionToPageCode.put("aboutSitePage", "ABOUT_SITE");
		actionToPageCode.put("aboutProjectPage", "ABOUT_PROJECT");
		actionToPageCode.put("usingConditions", "portal_using_conditions");
		actionToPageCode.put("help", "sys_help");
	}

	@Override
	public void handleRequest(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, Exception {
		String action = req.getParameter("action");
		String userLang = "ru_RU";

		if ("getTematicSectionRootTitle".equals(action)) {
			String sectionIdStr = req.getParameter("sectionId");
			JsonObject p = pdp.getTematicSectionRootTitle(Long
					.parseLong(sectionIdStr));
			resp.getWriter().write(p.toString());
			return;
		}

		if ("getLocaleConsts".equals(action)) {
			JsonObject o = pdp.getLocaleConsts(userLang);
			resp.getWriter().write(o.toString());
			return;
		}

		if ("getChapters".equals(action)) {
			String mainMenu = l10nUtils.getMainMenuHtml(userLang);
			resp.getWriter().write(mainMenu);
			return;
		}
                
                if ("getFooter".equals(action))
                {
                    String footer = l10nUtils.getFooterHtml(userLang);
                    resp.getWriter().write(footer);
                    return;
                }

		if ("getNewsBlock".equals(action)) {
			HashMap<String, String> localeConsts = l10nUtils
					.getLocalizedConstants(userLang);
			MainPageRenderer mpr = l10nUtils.getMainPageRenderer();
			String shortNewsBlock = mpr.getShortNewsHtml("RUS");
			String newsStr = localeConsts.get("sys_news");
			resp.getWriter().write("<h1><p>" + newsStr + "</p></h1>");
			resp.getWriter().write(shortNewsBlock);
			return;
		}

		if ("getMainContent".equals(action)) {
			MainPageRenderer mpr = l10nUtils.getMainPageRenderer();
			resp.getWriter().write(mpr.getMainPageHtml(userLang));
			return;
		}

		if ("getExhibitionShort".equals(action)) {
			MainPageRenderer mpr = l10nUtils.getMainPageRenderer();
			resp.getWriter().write(mpr.getExhibitionShort(userLang));
			return;
		}

		if ("getOneNews".equals(action)) {
			String idStr = req.getParameter("id");
			String result = pdp.getOneNews(Long.parseLong(idStr), "ru_RU");
			resp.getWriter().write(result);
			return;
		}

		if ("getNews".equals(action)) {
			String p = req.getParameter("p");
			Integer pint = 1;
			if (p != null && !"".equals(p)) {
				pint = Integer.parseInt(p);
			}
			String html = l10nUtils.getNewsRenderer().getNewsPage(userLang,
					pint);
			resp.getWriter().write(html);
			return;
		}

		if ("getExhibitionsList".equals(action)) {
			pdp.getExhibitionsList(req, resp, userLang);
			return;
		}
		if ("getOneExhibition".equals(action)) {
			pdp.getOneExhibition(req, resp, userLang);
			return;
		}
		if ("getMembers".equals(action)) {
			String members = pdp.getMembers();
			resp.getWriter().write(members);
		}
		// запрос данных для центрального блока "Ромб"
		if ("getMainLinksBlock".equals(action)) {
			String blockContent = l10nUtils.getLocalizedPage(userLang,
					"main_links_block").getPageData();
			resp.getWriter().write(blockContent);
			return;
		}
		if ("getBanners".equals(action)) {
			String banners = l10nUtils.getLocalizedPage(userLang,
					"banners_block").getPageData();
			resp.getWriter().write(banners);
			return;
		}
		if ("getUserHomeChapters".equals(action)) {
			resp.getWriter().write(
					pdp.getTemplateContent(getServletContext(),
							"userhomechapters.html"));
			return;
		}
		if ("getTematicSections".equals(action)) {
			String sections = ts.getTematicSections2();
			resp.getWriter().write(sections);
			return;
		}
		if ("getLoginPageText".equals(action)) {
			resp.getWriter().write(
					pdp.getTemplateContent(getServletContext(),
							"usernotloginederror.html"));
			return;
		}

		if (actionToPageCode.keySet().contains(action)) {
			String pageData = l10nUtils.getLocalizedPage(userLang,
					actionToPageCode.get(action)).getPageData();
			resp.getWriter().write(pageData);
		}

		if ("getNsaPage".equals(action)) {
			pdp.getNsaPage(req, resp);
		}

		if ("getSectionRootParm".equals(action)) {
			String rootIdStr = req.getParameter("sectionRootId");
			try {
				Long rootId = Long.parseLong(rootIdStr);
				JsonObject obj = ts
						.handleTematicSectionRootParamsRequest2(rootId);
				resp.getWriter().write(obj.toString());
			} catch (Exception e) {
				FailMessage fm = new FailMessage(false, e.getLocalizedMessage());
				e.printStackTrace();
				JsonObject o = jsonTools.getJsonForEntity(fm);
				resp.getWriter().write(o.toString());
			}
			return;
		}
	}

}
