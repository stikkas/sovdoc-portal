package ru.insoft.sdportal.servlets;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.PageMaker;

@WebServlet(name = "AdmMkPage", urlPatterns = "admin/MakePage")
public class AdmMkPage extends AbstractServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3359589552804190416L;
	@EJB
	private PageMaker pm;

	@Override
	protected void handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		req.setCharacterEncoding("UTF-8");
		String pagename = req.getParameter("pagename");
		String pagecode = req.getParameter("pagecode");
		if (pagename == null || "".equals(pagename) || pagecode == null
				|| "".equals(pagecode)) {
			resp.getWriter().write(
					"Параметры не могут быть нуллами или пустой строкой");
		} else {
			resp.getWriter().write("pagename: " + pagename);
			resp.getWriter().write("pagecode: " + pagecode);
			try {
				pm.createPage(pagecode, pagename);
				resp.getWriter().write("успешно");
			} catch (Exception e) {
				e.printStackTrace();
				resp.getWriter().write("<br>");
				resp.getWriter().write(e.getLocalizedMessage());
			}
			
		}
		
	}

}
