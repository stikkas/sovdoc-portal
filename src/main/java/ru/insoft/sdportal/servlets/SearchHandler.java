package ru.insoft.sdportal.servlets;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.CardInfoGenerator;
import ru.insoft.sdportal.ejb.GeoPluginWorker;
import ru.insoft.sdportal.ejb.L10nUtils;
import ru.insoft.sdportal.ejb.SovdocSearchImpl;
import ru.insoft.sdportal.ejb.SysUtils;
import ru.insoft.sdportal.models.L18nWebPage;
import ru.insoft.sdportal.models.ui.TematicRootSection;
import ru.insoft.sdportal.models.ui.TematicRootValue;
import ru.insoft.sdportal.ws.ExtSearchReqParm;
import ru.insoft.sdportal.ws.SearchResultShortItem;
import ru.insoft.sdportal.ws.SearchResults;

/**
 * Сервлет для обработки поисковых запросов
 */
@WebServlet("/SearchHandler")
public class SearchHandler extends AbstractServlet {

	private static final long serialVersionUID = 1L;
	private Logger log;

	@EJB
	private SysUtils su;

	@EJB
	private CardInfoGenerator cig;

	@EJB
	private L10nUtils l10nUtils;

	@EJB
	private SovdocSearchImpl searchImpl;

	public SearchHandler() {
		log = Logger.getLogger(getClass().getCanonicalName());
	}

	private ExtSearchReqParm packRequestParams(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String descValueParms = req.getParameter("descValueParams");
		JsonObject fieldParms = Json.createReader(new StringReader(
				req.getParameter("params")
		)).readObject();
		ExtSearchReqParm packedRequestForWs = new ExtSearchReqParm();
		for (Object so1 : fieldParms.keySet()) {
			String s = (String) so1;
			if (fieldParms.get(s) == null) {
				packedRequestForWs.addFieldParam(s, "");
			} else {
				packedRequestForWs.addFieldParam(s, fieldParms.get(s));
			}
		}
		if (descValueParms != null) {
			JsonObject descParms = Json.createReader(new StringReader(descValueParms)).readObject();
			for (Object o : descParms.keySet()) {
				packedRequestForWs.addDescValueParm(Long.parseLong(o.toString()),
						Long.parseLong(descParms.get(o).toString()));
			}
		}
		return packedRequestForWs;
	}

	private void handleElibSearchRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		log.log(Level.FINEST,
				"HANDLE ELIBRARY SEARCH REQUEST handleElibSearchRequest()");
		String startSt = req.getParameter("start");
		String limitStr = req.getParameter("limit");
		Integer start = 0;
		if (startSt != null) {
			start = Integer.parseInt(startSt);
		}
		Integer limit = null;
		if (limitStr != null) {
			limit = Integer.parseInt(limitStr);
		}
		ExtSearchReqParm packedRequestForWs = packRequestParams(req, resp);
		SearchResults results = searchImpl.getExtendedSearchResult(
				packedRequestForWs, limit, start);
		JsonObjectBuilder resultbuilder = Json.createObjectBuilder();
		JsonArrayBuilder jabu = Json.createArrayBuilder();
		for (SearchResultShortItem item : results.getFindedItems()) {
			JsonObject tempObj = Json.createReader(new StringReader(item.getDataUnitName())).readObject();
			jabu.add(tempObj);
		}
		resultbuilder.add("items", jabu.build());
		resultbuilder.add("itemsCount", results.getTotalSearchResultsCount());
		resp.getWriter().write(resultbuilder.build().toString());
	}

	private void handleAdvancedSearchRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String startSt = req.getParameter("start");
		String limitStr = req.getParameter("limit");
		Integer start = 0;
		if (startSt != null) {
			start = Integer.parseInt(startSt);
		}
		Integer limit = null;
		if (limitStr != null) {
			limit = Integer.parseInt(limitStr);
		}
		ExtSearchReqParm packedRequestForWs = packRequestParams(req, resp);
		SearchResults extSearchResults = searchImpl.getExtendedSearchResult(
				packedRequestForWs, limit, start);
		JsonObjectBuilder packedResult = packSearchResultShortItems(extSearchResults
				.getFindedItems());
		packedResult.add("itemsCount", extSearchResults.getTotalSearchResultsCount());
		resp.getWriter().write(packedResult.build().toString());
	}

	@Override
	public void handleRequest(HttpServletRequest req, HttpServletResponse resp)
			throws Exception {
		try {
			String action = req.getParameter("action");
			String startSt = req.getParameter("start");
			String limitStr = req.getParameter("limit");
			Integer start = 0;
			if (startSt != null) {
				start = Integer.parseInt(startSt);
			}
			Integer limit = null;
			if (limitStr != null) {
				limit = Integer.parseInt(limitStr);
			}
			// обработка для расширенного поиска и "электронной библиотеки"
			// идентична
			if ("advsearch".equals(action)) {
				handleAdvancedSearchRequest(req, resp);
				return;
			}

			if ("elib".equals(action)) {
				handleElibSearchRequest(req, resp);
				return;
			}
			if ("getTematicRoots".equals(action)) {
				handleTematicSectionHierarchyRequest(req, resp);
				//Старый вариант получения элементов тематического раздела (без иерархии)
				//handleTematicSectionRootsRequest(req, resp);
				return;
			}
			if ("getTematicChilds".equals(action)) {
				handleTematicSectionChildsRequest(req, resp);
				return;
			}

			if ("getUnitInfo".equals(action)) {
				String idStr = req.getParameter("unitId");
				if (idStr == null || "".equals(idStr)) {
					resp.sendError(500, "Не указан идентификатор единицы хранения");
					return;
				}
				Long univDataUnitId = Long.parseLong(idStr);
				resp.getWriter().write(
						cig.getCardHtml(univDataUnitId).toString());
				return;
			}

			if ("getPhonoSUInfo".equals(action)) {
				String idStr = req.getParameter("unitId");
				if (idStr == null || idStr.isEmpty()) {
					resp.sendError(500, "Не указан идентификатор аннотации единицы хранения");
				} else {
					Long univDataUnitId = Long.parseLong(idStr);
					resp.getWriter().write(cig.getPhonoStorageUnitInfo(univDataUnitId).toString());
				}
				return;
			}

			if ("getImageInfo".equals(action)) {
				handleImagesInfoRequest(req, resp);
				return;
			}

			String encodedQuery = req.getParameter("q");
			String query = StringEscapeUtils.unescapeJava(encodedQuery);
			if (query == null) {
				resp.sendError(500);
				return;
			}
			query = query.replaceAll("\"", "");
			log.log(Level.FINEST, "Decoded simple search query: " + query);

			SearchResults list = searchImpl
					.getSearchResult(query, limit, start);

			List<SearchResultShortItem> findedItems = list.getFindedItems();
			JsonObjectBuilder result = packSearchResultShortItems(findedItems);
			result.add("itemsCount", list.getTotalSearchResultsCount());
			resp.getWriter().write(result.build().toString());
		} catch (Exception e) {
			e.printStackTrace();
			resp.sendError(500);
		}
	}

	@EJB
	private GeoPluginWorker gpw;

	private String getForwardedForIp(HttpServletRequest req) {
		String headername = "x-forwarded-for";
		String ip = req.getHeader(headername);
		log.info("forwarded for ip: " + ip);
		if (ip == null) {
			ip = req.getRemoteAddr();
		}
		log.info("ip: " + req.getRemoteAddr());
		return ip;
	}

	private void handleImagesInfoRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		boolean b = true;
		boolean isKomintern = false;

		String startSt = req.getParameter("start");
		String limitStr = req.getParameter("limit");
		Integer start = 0;
		if (startSt != null) {
			start = Integer.parseInt(startSt);
		}
		Integer limit = null;
		if (limitStr != null) {
			limit = Integer.parseInt(limitStr);
		}

		String idCard = req.getParameter("unitId");
		Long uvId = Long.parseLong(idCard);
		isKomintern = searchImpl.isCardOfKomintern(uvId);

		if (isKomintern == false) {
			try {
				String ipChain = getForwardedForIp(req);
				for (String ip : ipChain.split(",")) {
					b = gpw.isImagesAllowed(ip.trim());
					if (!b) {
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.warning("Ошибка при обращении к сервису GeoPlugin.net; Разрешен доступ к образам клиенту с ip: " + req.getRemoteAddr());
			}
		}
		if (b) {
			String idStr = req.getParameter("unitId");
			Long univDataUnitId = Long.parseLong(idStr);
			JsonObject result = cig.getImagesInfo(univDataUnitId, limit, start);
			resp.getWriter().write(result.toString());
		} else {
			L18nWebPage wp = l10nUtils.getLocalizedPage("ru_RU", "block_images");
			FailMessage f = new FailMessage(false, wp.getPageData());
			String response = jsonTools.getJsonForEntity(f).toString();
			resp.getWriter().write(response);
		}
	}

	private void handleTematicSectionChildsRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String rootIdSt = req.getParameter("tematicSectionRootId");
		Long rootId = Long.parseLong(rootIdSt);
		String startS = req.getParameter("start");
		Integer start = 0;
		if (startS != null) {
			start = Integer.parseInt(startS);
		}
		String limitS = req.getParameter("limit");
		Integer limit = 50;
		if (limitS != null) {
			limit = Integer.parseInt(limitS);
		}
		ExtSearchReqParm parm = new ExtSearchReqParm();
		parm.addFieldParam("tematicSectionRootId", rootId);
		parm.addFieldParam("tematicSectionChildsRequest", 1);
		SearchResults results = searchImpl.getExtendedSearchResult(parm, start,
				limit);
		JsonObjectBuilder result = Json.createObjectBuilder();
		result.add("itemsCount", results.getTotalSearchResultsCount());
		JsonArrayBuilder items = Json.createArrayBuilder();
		for (SearchResultShortItem item : results.getFindedItems()) {
			JsonObject obj = Json.createReader(new StringReader(item.getDataUnitName())).readObject();
			items.add(obj);
		}
		result.add("sectionInfo", searchImpl.getSectionInfo(rootId).toString());
		result.add("items", items.build());
		resp.getWriter().write(result.build().toString());
	}

	/**
	 * Обработка запроса на получение списка корневых элементов для
	 * тематического раздела
	 *
	 * @param req HttpServletRequest
	 * @param resp HttpServletResponse
	 * @throws Exception
	 */
	/*private void handleTematicSectionRootsRequest(HttpServletRequest req,
	 HttpServletResponse resp) throws Exception {
	 String sectionId = req.getParameter("tematicSectionId");
	 log.log(Level.FINEST, "TEMATIC SECTION ROOTS REQUEST");
	 log.log(Level.FINEST, "TEMACTIC SECION ID: " + sectionId);
	 String startS = req.getParameter("start");
	 Integer start = 0;
	 if (startS != null) {
	 start = Integer.parseInt(startS);
	 }

	 String limitS = req.getParameter("limit");
	 Integer limit = 50;
	 if (limitS != null) {
	 limit = Integer.parseInt(limitS);
	 }
	 if (sectionId == null)
	 throw new Exception("Не определен Id тематического раздела");
	 ExtSearchReqParm parm = new ExtSearchReqParm();
	 parm.addFieldParam("tematicSectionId", sectionId);
	 parm.addFieldParam("tematicSectionRootRequest", "1");
	 SearchResults results = searchImpl.getExtendedSearchResult(parm, start,
	 limit);
	 JsonObjectBuilder jsonResultBdr = Json.createObjectBuilder();
	 jsonResultBdr.add("itemsCount", results.getTotalSearchResultsCount());
	 JsonArrayBuilder itemsArrBuilder = Json.createArrayBuilder();
	 List<SearchResultShortItem> resultItems = results.getFindedItems();
	 for (int i = 0; i < resultItems.size(); i++) {
	 SearchResultShortItem item = resultItems.get(i);
	 JsonObject jsItem = Json.createReader(new StringReader(item.getDataUnitName())).readObject();
	 itemsArrBuilder.add(jsItem);
	 }
	 jsonResultBdr.add("items", itemsArrBuilder.build());
	 resp.getWriter().write(jsonResultBdr.build().toString());
	 }*/
	private void handleTematicSectionHierarchyRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		Long sectionId = Long.parseLong(req.getParameter("tematicSectionId"));
		TematicRootSection root = searchImpl.getTematicSectionHierarchy(sectionId);
		JsonObject jo = trs2Json(root).build();
		resp.getWriter().write(jo.toString());
	}

	private JsonObjectBuilder trs2Json(TematicRootSection trs) {
		JsonObjectBuilder secObj = Json.createObjectBuilder();
		if (trs.getSection() != null) {
			secObj.add("section", trs.getSection());
		}
		JsonArrayBuilder valArr = Json.createArrayBuilder();
		for (TematicRootValue trv : trs.getValues()) {
			JsonObjectBuilder valObj = Json.createObjectBuilder();
			valObj.add("id", trv.getId());
			valObj.add("name", trv.getName());
			valObj.add("archCode", trv.getArchCode());
			if (trv.getDates() != null) {
				valObj.add("dates", trv.getDates());
			}
			valObj.add("childrenCnt", trv.getChildrenCnt());
			valArr.add(valObj);
		}
		secObj.add("values", valArr);
		JsonArrayBuilder secArr = Json.createArrayBuilder();
		for (TematicRootSection subsection : trs.getSections()) {
			secArr.add(trs2Json(subsection));
		}
		secObj.add("sections", secArr);
		return secObj;
	}

	/**
	 * Pack to json search results
	 *
	 * @param searchResult
	 * @return
	 */
	private JsonObjectBuilder packSearchResultShortItems(
			List<SearchResultShortItem> searchResult) throws Exception {
		JsonObjectBuilder jobuilder = Json.createObjectBuilder();

		ArrayList<SearchResultShortItem> findedItems = new ArrayList<>();

		if (searchResult.size() != 0) {
			for (SearchResultShortItem it : searchResult) {
				findedItems.add(it);
			}
		}
		JsonArray items = jsonTools.getJsonEntitiesList(findedItems);
		jobuilder.add("items", items);
		return jobuilder;
	}

}
