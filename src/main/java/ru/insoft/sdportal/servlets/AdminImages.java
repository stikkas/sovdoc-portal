package ru.insoft.sdportal.servlets;

import javax.ejb.EJB;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ru.insoft.archive.extcommons.webmodel.FailMessage;
import ru.insoft.sdportal.AbstractServlet;
import ru.insoft.sdportal.ejb.GeoPluginWorker;
import ru.insoft.sdportal.ejb.HibernateHelper;
import ru.insoft.sdportal.ejb.SysUtils;

/**
 * Обработка запросов на получение и сохранение списка кодов стран, 
 * из которых разрешен просмотр образов документов на портале
 * 18.10.2013
 */
public class AdminImages extends AbstractServlet {

	@EJB
	private SysUtils su;

	@EJB
	private GeoPluginWorker gpw;

	@EJB
	HibernateHelper hh;
	/**
	 * 
	 */
	private static final long serialVersionUID = 882668644477199645L;


	@Override
	protected void handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		String action = req.getParameter("action");
		if ("save".equals(action)) {
			String countries = req.getParameter("countries");
			String[] arr = countries.split(",");
			for (int i = 0; i < arr.length; i++) {
				log.info("country: " + i + ": " + arr[i]);
			}
			try {
				log.info("allow list: " + hh.getImagesAllowcountries());
				hh.setImagesAllowcountries(countries);
				FailMessage fm = new FailMessage(true,"Изменения успешно сохранены" );
				JsonObject fmJson = jsonTools.getJsonForEntity(fm);
				resp.getWriter().write(fmJson.toString());
			} catch (Exception e) {
				FailMessage fm = new FailMessage(false, e.getLocalizedMessage());
				JsonObject jo = jsonTools.getJsonForEntity(fm);
				resp.getWriter().write(jo.toString());
			}
		}
		if ("get".equals(req.getParameter("action"))) {
			FailMessage fm = null;
			try {
				String countries = hh.getImagesAllowcountries();
				log.info("countries from database: " + countries);
				fm = new FailMessage(true, countries);
			} catch (Exception e) {
				fm = new FailMessage(false, e.getLocalizedMessage());
			}
			String encoded = jsonTools.getJsonForEntity(fm).toString();
			resp.getWriter().write(encoded);
		}
		log.info("is images allowed: "
				+ gpw.isImagesAllowed(req.getRemoteAddr()));
		
	}
}
