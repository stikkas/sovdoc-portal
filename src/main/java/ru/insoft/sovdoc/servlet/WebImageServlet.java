package ru.insoft.sovdoc.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.insoft.sdportal.ejb.HibernateHelper;
import ru.insoft.sovdoc.model.web.table.WebImages;

@SuppressWarnings("serial")
public class WebImageServlet extends HttpServlet {

	@EJB
	private HibernateHelper hh;
	
	@Inject
	private EntityManager em;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		WebImages image;
		response.setCharacterEncoding("UTF-8");
		Long id = Long.valueOf(request.getParameter("id"));
		try {
			image = (WebImages) em.find(WebImages.class, id);
		} catch (Exception e) {
			response.setContentType("text/plain; charset=UTF-8");
			response.getWriter().print("Изображение не найдено");
			return;
		}
		ServletOutputStream out = response.getOutputStream();
		response.setContentType(image.getFormat().getValueCode());
		response.setContentLength(image.getImageData().length);
		out.write(image.getImageData());
		out.close();
	}
}
