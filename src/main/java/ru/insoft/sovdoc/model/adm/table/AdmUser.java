package ru.insoft.sovdoc.model.adm.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "ADM_USER")
public class AdmUser {

	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE,generator="admuseridgen")
	@SequenceGenerator(name="admuseridgen",sequenceName="SEQ_ADM_USER")
	@Column(name = "USER_ID")
	private Long userId;
	
	@Column(name = "USER_TYPE_ID")
	private Long userTypeId;
	
	@Column(name = "LOGIN")
	private String login;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "IS_BLOCKED")
	private boolean isBlocked;
	
	@Column(name = "DISPLAYED_NAME")
	private String displayedName;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public String getDisplayedName() {
		return displayedName;
	}

	public void setDisplayedName(String displayedName) {
		this.displayedName = displayedName;
	}

	public String getName() 
	{
		return displayedName;
	}
}
