package ru.insoft.sovdoc.model.ebook;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_EBOOK")
public class ArchEbook {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "EDITION_TYPE_ID")
	private Long editionTypeId;
	
	@Column(name = "UDC")
	private String udc;
	
	@Column(name = "BBC")
	private String BBC;
	
	@Column(name = "ISBN")
	private String isbn;
	
	@Column(name = "EDITOR")
	private String editor;
	
	@Column(name = "EDITORIAL_BOARD")
	private String editorialBoard;
	
	@Column(name = "COMPILERS")
	private String compilers;
	
	@Column(name = "VOLUME1_INFO")
	private String volume1Info;
	
	@Column(name = "VOLUME2_INFO")
	private String volume2Info;
	
	@Column(name = "ATOPTITLE_DATA")
	private String atoptitleData;
	
	@Column(name = "SERIES_ID")
	private Long seriesId;
	
	@Column(name = "EDITION_NUMBER")
	private String editionNumber;
	
	@Column(name = "EDITION_NOTE")
	private String editionNote;
	
	@Column(name = "PUBLISHING_PLACE")
	private String publishingPlace;
	
	@Column(name = "PUBLISHER")
	private String publisher;
	
	@Column(name = "PUBLISHING_YEAR")
	private Integer publishingYear;
	
	@Column(name = "EDITION_SIZE")
	private String editionSize;
	
	@Column(name = "NOTES")
	private String notes;
	
	@Column(name="EXTERNAL_URL")
	private String url;
	
	@OneToMany(mappedBy = "ebook")
	private List<ArchEbookAuthor> authors;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getEditionTypeId() {
		return editionTypeId;
	}

	public void setEditionTypeId(Long editionTypeId) {
		this.editionTypeId = editionTypeId;
	}

	public String getUdc() {
		return udc;
	}

	public void setUdc(String udc) {
		this.udc = udc;
	}

	public String getBBC() {
		return BBC;
	}

	public void setBBC(String bBC) {
		BBC = bBC;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getEditorialBoard() {
		return editorialBoard;
	}

	public void setEditorialBoard(String editorialBoard) {
		this.editorialBoard = editorialBoard;
	}

	public String getCompilers() {
		return compilers;
	}

	public void setCompilers(String compilers) {
		this.compilers = compilers;
	}

	public String getVolume1Info() {
		return volume1Info;
	}

	public void setVolume1Info(String volume1Info) {
		this.volume1Info = volume1Info;
	}

	public String getVolume2Info() {
		return volume2Info;
	}

	public void setVolume2Info(String volume2Info) {
		this.volume2Info = volume2Info;
	}

	public String getAtoptitleData() {
		return atoptitleData;
	}

	public void setAtoptitleData(String atoptitleData) {
		this.atoptitleData = atoptitleData;
	}

	public Long getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Long seriesId) {
		this.seriesId = seriesId;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getEditionNote() {
		return editionNote;
	}

	public void setEditionNote(String editionNote) {
		this.editionNote = editionNote;
	}

	public String getPublishingPlace() {
		return publishingPlace;
	}

	public void setPublishingPlace(String publishingPlace) {
		this.publishingPlace = publishingPlace;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Integer getPublishingYear() {
		return publishingYear;
	}

	public void setPublishingYear(Integer publishingYear) {
		this.publishingYear = publishingYear;
	}

	public String getEditionSize() {
		return editionSize;
	}

	public void setEditionSize(String editionSize) {
		this.editionSize = editionSize;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<ArchEbookAuthor> getAuthors() {
		return authors;
	}

	public void setAuthors(List<ArchEbookAuthor> authors) {
		this.authors = authors;
	}
}
