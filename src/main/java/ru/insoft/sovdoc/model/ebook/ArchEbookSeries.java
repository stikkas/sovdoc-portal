package ru.insoft.sovdoc.model.ebook;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="ARCH_EBOOK_SERIES")
public class ArchEbookSeries {
	@Id
	@Column(name = "SERIES_ID")
	Long series_id;
	
	@Column(name="SERIES_NAME",length=300)
	String series_name;
	
	@Column(name="SERIES_INFO",length=2500)
	String series_info;

	public Long getSeries_id() {
		return series_id;
	}

	public void setSeries_id(Long series_id) {
		this.series_id = series_id;
	}

	public String getSeries_name() {
		return series_name;
	}

	public void setSeries_name(String series_name) {
		this.series_name = series_name;
	}

	public String getSeries_info() {
		return series_info;
	}

	public void setSeries_info(String series_info) {
		this.series_info = series_info;
	}	

}
