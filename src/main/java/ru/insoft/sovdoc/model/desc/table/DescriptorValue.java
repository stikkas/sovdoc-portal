package ru.insoft.sovdoc.model.desc.table;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.complex.table.UnivDataUnit;

@Entity
@Table(name = "DESCRIPTOR_VALUE")
public class DescriptorValue {

	@Id
	@GeneratedValue(generator="dvidgen")
	@SequenceGenerator(allocationSize=1,sequenceName="SEQ_DESCRIPTOR_VALUE", name = "dvidgen")
	@Column(name = "DESCRIPTOR_VALUE_ID",nullable=false)
	private Long descriptorValueId;

	@OneToOne
	@JoinColumn(name = "DESCRIPTOR_GROUP_ID")
	private DescriptorGroup descriptorGroup;

	@Column(name = "FULL_VALUE")
	private String fullValue;

	@Column(name = "SHORT_VALUE")
	private String shortValue;

	@Column(name = "VALUE_CODE")
	private String valueCode;

	@Column(name = "IS_ACTUAL")
	private boolean isActual;
	
	@Column(name="SORT_ORDER",nullable=false)
	private Long sortOrder;

	@OneToOne
	@JoinColumn(name = "PARENT_VALUE_ID", nullable = true)
	private DescriptorValue parentValue;

	@OneToMany(mappedBy = "descriptorValue")
	@OrderBy("descriptorValueAttrId")
	private List<DescriptorValueAttr> valueAttrList;

	@OneToMany(mappedBy = "descriptorValue", fetch = FetchType.EAGER,targetEntity=DescValueInternational.class)
	private List<DescValueInternational> multilangList;

	@OneToMany(mappedBy = "parentValue",fetch=FetchType.LAZY, targetEntity = DescriptorValue.class)
	// @JoinColumn(name = "PARENT_VALUE_ID")
	private List<DescriptorValue> childValues;

	@ManyToMany
	@JoinTable(name = "UNIV_DATA_LANGUAGE", joinColumns = { @JoinColumn(name = "doc_language_id") }, 
	inverseJoinColumns = { @JoinColumn(name = "univ_data_unit_id") })
	private List<UnivDataUnit> langToDocs;
	
	
	public DescriptorValue() {
		childValues = new LinkedList<DescriptorValue>();
		valueAttrList = new LinkedList<DescriptorValueAttr>();
		multilangList = new LinkedList<DescValueInternational>();
		langToDocs = new ArrayList<UnivDataUnit>();
	}

	
	
	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public DescriptorGroup getDescriptorGroup() {
		return descriptorGroup;
	}

	public void setDescriptorGroup(DescriptorGroup descriptorGroup) {
		this.descriptorGroup = descriptorGroup;
	}

	public DescriptorValue getParentValue() {
		return parentValue;
	}

	public void setParentValue(DescriptorValue parentValue) {
		this.parentValue = parentValue;
	}

	public List<UnivDataUnit> getLangToDocs() {
		return langToDocs;
	}

	public void setLangToDocs(List<UnivDataUnit> langToDocs) {
		this.langToDocs = langToDocs;
	}

	public Long getDescriptorValueId() {
		return descriptorValueId;
	}

	public void setDescriptorValueId(Long descriptorValueId) {
		this.descriptorValueId = descriptorValueId;
	}

	public DescriptorGroup getDescriptorGroupId() {
		return descriptorGroup;
	}

	public void setDescriptorGroupId(DescriptorGroup descriptorGroupId) {
		this.descriptorGroup = descriptorGroupId;
	}

	public String getFullValue() {
		return fullValue;
	}

	public void setFullValue(String fullValue) {
		this.fullValue = fullValue;
	}

	public String getShortValue() {
		return shortValue;
	}

	public void setShortValue(String shortValue) {
		this.shortValue = shortValue;
	}

	public String getValueCode() {
		return valueCode;
	}

	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}

	public boolean isActual() {
		return isActual;
	}

	public void setActual(boolean isActual) {
		this.isActual = isActual;
	}

	public DescriptorValue getParentValueId() {
		return parentValue;
	}

	public void setParentValueId(DescriptorValue parentValueId) {
		this.parentValue = parentValueId;
	}

	public List<DescriptorValueAttr> getValueAttrList() {
		return valueAttrList;
	}

	public void setValueAttrList(List<DescriptorValueAttr> valueAttrList) {
		this.valueAttrList = valueAttrList;
	}

	public List<DescValueInternational> getMultilangList() {
		return multilangList;
	}

	public void setMultilangList(List<DescValueInternational> multilangList) {
		this.multilangList = multilangList;
	}

	public List<DescriptorValue> getChildValues() {
		return childValues;
	}

	public void setChildValues(List<DescriptorValue> childValues) {
		this.childValues = childValues;
	}

}
