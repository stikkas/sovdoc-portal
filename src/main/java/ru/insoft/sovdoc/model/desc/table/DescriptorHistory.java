package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DESCRIPTOR_HISTORY")
public class DescriptorHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "descriptorhistidgen")
	@SequenceGenerator(name = "descriptorhistidgen", sequenceName = "SEQ_DESCRIPTOR_HISTORY")
	@Column(name = "DESCRIPTOR_HISTORY_ID")
	private Long descriptorHistoryId;
	
	@Column(name = "ACTUAL_VALUE_ID")
	private Long actualValueId;
	
	@Column(name = "OLD_VALUE_ID")
	private Long oldValueId;
	
	@Column(name = "SORT_ORDER")
	private Integer sortOrder;
	
	@Column(name = "CHANGE_DATE")
	private String changeDate;

	public Long getDescriptorHistoryId() {
		return descriptorHistoryId;
	}

	public void setDescriptorHistoryId(Long descriptorHistoryId) {
		this.descriptorHistoryId = descriptorHistoryId;
	}

	public Long getActualValueId() {
		return actualValueId;
	}

	public void setActualValueId(Long actualValueId) {
		this.actualValueId = actualValueId;
	}

	public Long getOldValueId() {
		return oldValueId;
	}

	public void setOldValueId(Long oldValueId) {
		this.oldValueId = oldValueId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
}
