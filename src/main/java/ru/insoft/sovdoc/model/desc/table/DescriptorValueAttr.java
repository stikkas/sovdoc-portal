package ru.insoft.sovdoc.model.desc.table;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DESCRIPTOR_VALUE_ATTR")
public class DescriptorValueAttr {

	@Id
	@GeneratedValue(generator="dvaidgen")
	@SequenceGenerator(allocationSize=1,name="dvaidgen",sequenceName="SEQ_DESCRIPTOR_VALUE_ATTR")
	@Column(name = "DESCRIPTOR_VALUE_ATTR_ID")
	private Long descriptorValueAttrId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DESCRIPTOR_VALUE_ID")
	private DescriptorValue descriptorValue;
	
	@Column(name = "DESCRIPTOR_GROUP_ATTR_ID")
	private Long descriptorGroupAttrId;
	
	@Column(name = "ATTR_VALUE")
	private String attrValue;
	
	@Column(name = "REF_DESCRIPTOR_VALUE_ID")
	private Long refDescriptorValueId;
	
	@OneToMany(mappedBy = "descriptorValueAttr")
	private List<DescAttrInternational> multilangList;

	public Long getDescriptorValueAttrId() {
		return descriptorValueAttrId;
	}

	public void setDescriptorValueAttrId(Long descriptorValueAttrId) {
		this.descriptorValueAttrId = descriptorValueAttrId;
	}

	public DescriptorValue getDescriptorValue() {
		return descriptorValue;
	}

	public void setDescriptorValue(DescriptorValue descriptorValue) {
		this.descriptorValue = descriptorValue;
	}

	public Long getDescriptorGroupAttrId() {
		return descriptorGroupAttrId;
	}

	public void setDescriptorGroupAttrId(Long descriptorGroupAttrId) {
		this.descriptorGroupAttrId = descriptorGroupAttrId;
	}

	public String getAttrValue() {
		return attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public Long getRefDescriptorValueId() {
		return refDescriptorValueId;
	}

	public void setRefDescriptorValueId(Long refDescriptorValueId) {
		this.refDescriptorValueId = refDescriptorValueId;
	}

	public List<DescAttrInternational> getMultilangList() {
		return multilangList;
	}

	public void setMultilangList(List<DescAttrInternational> multilangList) {
		this.multilangList = multilangList;
	}
}
