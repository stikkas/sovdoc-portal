package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.insoft.sovdoc.ui.desc.MultilingualTable;

@Entity
@Table(name = "DESC_ATTR_INTERNATIONAL")
public class DescAttrInternational implements MultilingualTable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "descattrinternationalidgen")
	@SequenceGenerator(name = "descattrinternationalidgen", sequenceName = "SEQ_DESC_ATTR_INTERNATIONAL")
	@Column(name = "DESC_ATTR_INTERNATIONAL_ID",nullable=false)
	private Long descAttrInternationalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DESCRIPTOR_VALUE_ATTR_ID")
	private DescriptorValueAttr descriptorValueAttr;
	
	@Column(name = "LANGUAGE_CODE")
	private String languageCode;
	
	@Column(name = "ATTR_INTERNATIONAL_VALUE")
	private String attrInternationalValue;
	
	@Column(name = "DESCRIPTOR_VALUE_ATTR_ID", insertable = false, updatable = false)
	private Long rootId;

	public Long getDescAttrInternationalId() {
		return descAttrInternationalId;
	}

	public void setDescAttrInternationalId(Long descAttrInternationalId) {
		this.descAttrInternationalId = descAttrInternationalId;
	}

	public DescriptorValueAttr getDescriptorValueAttr() {
		return descriptorValueAttr;
	}

	public void setDescriptorValueAttr(DescriptorValueAttr descriptorValueAttr) {
		this.descriptorValueAttr = descriptorValueAttr;
	}

	@Override
	public String getLanguageCode() {
		return languageCode;
	}

	@Override
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getAttrInternationalValue() {
		return attrInternationalValue;
	}

	public void setAttrInternationalValue(String attrInternationalValue) {
		this.attrInternationalValue = attrInternationalValue;
	}

	@Override
	public Long getRootId() {
		return rootId;
	}

	@Override
	public void setRootId(Long rootId) {
		this.rootId = rootId;
	}
}
