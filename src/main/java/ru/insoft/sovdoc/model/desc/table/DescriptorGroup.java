package ru.insoft.sovdoc.model.desc.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DESCRIPTOR_GROUP")
public class DescriptorGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "descgroupidgen")
	@SequenceGenerator(name = "descgroupidgen", sequenceName = "SEQ_DESCRIPTOR_GROUP")
	
	@Column(name = "DESCRIPTOR_GROUP_ID",nullable=false)
	private Long descriptorGroupId;
	
	@Column(name = "SUBSYSTEM_NUMBER")
	private Long subsystemNumber;
	
	@Column(name = "GROUP_NAME")
	private String groupName;
	
	@Column(name = "GROUP_CODE")
	private String groupCode;
	
	@Column(name = "SORT_ORDER")
	private Long sortOrder;
	
	@Column(name = "IS_SYSTEM")
	private boolean isSystem;
	
	@Column(name = "IS_HIERARCHICAL")
	private boolean isHierarchical;
	
	@Column(name="SHORT_VALUE_SUPPORTED",nullable=false)
	private Long isShortValueSupported;
	
	
	@Column(name="HISTORY_SUPPORTED",nullable=false)
	private Long historySupported;

	public Long getDescriptorGroupId() {
		return descriptorGroupId;
	}

	public void setDescriptorGroupId(Long descriptorGroupId) {
		this.descriptorGroupId = descriptorGroupId;
	}

	public Long getSubsystemNumber() {
		return subsystemNumber;
	}

	public void setSubsystemNumber(Long subsystemNumber) {
		this.subsystemNumber = subsystemNumber;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isSystem() {
		return isSystem;
	}

	public void setSystem(boolean isSystem) {
		this.isSystem = isSystem;
	}

	public boolean isHierarchical() {
		return isHierarchical;
	}

	public void setHierarchical(boolean isHierarchical) {
		this.isHierarchical = isHierarchical;
	}

	public Long getShortValueSupported() {
		return isShortValueSupported;
	}

	public void setShortValueSupported(Long isShortValueSupported) {
		this.isShortValueSupported = isShortValueSupported;
	}

	public Long getHistorySupported() {
		return historySupported;
	}

	public void setHistorySupported(Long historySupported) {
		this.historySupported = historySupported;
	}
	
	
	
	
}
