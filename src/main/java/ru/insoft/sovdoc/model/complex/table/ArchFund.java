package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Entity
@Table(name = "ARCH_FUND")
public class ArchFund implements Serializable {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long dataUnitId;
	@OneToOne
	@JoinColumn(name = "FUND_TYPE_ID")
	private DescriptorValue fundType;
	
	@Column(name = "SERIES_COUNT")
	private int seriesCount;
	
	@Column(name = "STORAGE_UNIT_COUNT")
	private int storageUnitCount;
	
	@Column(name = "NOTES")
	private String notes;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "HISTORICAL_NOTE")
	private String historicalNote;
	
	@OneToMany(mappedBy = "fund")
	@OrderBy("sortOrder")
	private List<ArchFundName> fundNames;

	public Long getDataUnitId() {
		return dataUnitId;
	}

	public void setDataUnitId(Long dataUnit) {
		this.dataUnitId = dataUnit;
	}

	public int getSeriesCount() {
		return seriesCount;
	}

	public void setSeriesCount(int seriesCount) {
		this.seriesCount = seriesCount;
	}

	public int getStorageUnitCount() {
		return storageUnitCount;
	}

	public void setStorageUnitCount(int storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getHistoricalNote() {
		return historicalNote;
	}

	public void setHistoricalNote(String historicalNote) {
		this.historicalNote = historicalNote;
	}

	public List<ArchFundName> getFundNames() {
		return fundNames;
	}

	public void setFundNames(List<ArchFundName> fundNames) {
		this.fundNames = fundNames;
	}

	public DescriptorValue getFundType() {
		return fundType;
	}

	public void setFundType(DescriptorValue fundType) {
		this.fundType = fundType;
	}
}
