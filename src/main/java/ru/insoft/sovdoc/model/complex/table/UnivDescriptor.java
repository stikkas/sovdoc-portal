package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "UNIV_DESCRIPTOR")
public class UnivDescriptor {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "univdescidgen")
	@SequenceGenerator(name = "univdescidgen", sequenceName = "SEQ_UNIV_DESCRIPTOR")
	@Column(name = "UNIV_DESCRIPTOR_ID",nullable=false)
	private Long univDescriptorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIV_DATA_UNIT_ID")
	private UnivDataUnit dataUnit;
	
	@Column(name = "DESCRIPTOR_VALUE_ID1")
	private Long descriptorValueId1;
	
	@Column(name = "DESCRIPTOR_VALUE_ID2")
	private Long descriptorValueId2;
	
	@Column(name = "DESCRIPTOR_RELATION_ID")
	private Long descriptorRelationId;

	public Long getUnivDescriptorId() {
		return univDescriptorId;
	}

	public void setUnivDescriptorId(Long univDescriptorId) {
		this.univDescriptorId = univDescriptorId;
	}

	public UnivDataUnit getDataUnit() {
		return dataUnit;
	}

	public void setDataUnit(UnivDataUnit dataUnit) {
		this.dataUnit = dataUnit;
	}

	public Long getDescriptorValueId1() {
		return descriptorValueId1;
	}

	public void setDescriptorValueId1(Long descriptorValueId1) {
		this.descriptorValueId1 = descriptorValueId1;
	}

	public Long getDescriptorValueId2() {
		return descriptorValueId2;
	}

	public void setDescriptorValueId2(Long descriptorValueId2) {
		this.descriptorValueId2 = descriptorValueId2;
	}

	public Long getDescriptorRelationId() {
		return descriptorRelationId;
	}

	public void setDescriptorRelationId(Long descriptorRelationId) {
		this.descriptorRelationId = descriptorRelationId;
	}
}
