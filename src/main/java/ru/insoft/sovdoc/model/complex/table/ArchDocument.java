package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARCH_DOCUMENT")
public class ArchDocument {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@Column(name = "DOCUMENT_TYPE_ID")
	private Long documentTypeId;
	
	@Column(name = "DOCUMENT_NUMBER")
	private String documentNumber;
	
	@Column(name = "DOCUMENT_NAME")
	private String documentName;
	
	@Column(name = "PAGE_COUNT")
	private Integer pageCount;
	
	@Column(name = "DATES_NOTE")
	private String datesNote;
	
	@Column(name = "REPRODUCTION_TYPE_ID")
	private Long reproductionTypeId;
	
	@Column(name = "BASE_MATERIAL_ID")
	private Long baseMaterialId;
	
	@Column(name = "AUTHENTICITY_TYPE_ID")
	private Long authenticityTypeId;
	
	@Column(name = "NOTES")
	private String notes;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public Long getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Long documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public String getDatesNote() {
		return datesNote;
	}

	public void setDatesNote(String datesNote) {
		this.datesNote = datesNote;
	}

	public Long getReproductionTypeId() {
		return reproductionTypeId;
	}

	public void setReproductionTypeId(Long reproductionTypeId) {
		this.reproductionTypeId = reproductionTypeId;
	}

	public Long getBaseMaterialId() {
		return baseMaterialId;
	}

	public void setBaseMaterialId(Long baseMaterialId) {
		this.baseMaterialId = baseMaterialId;
	}

	public Long getAuthenticityTypeId() {
		return authenticityTypeId;
	}

	public void setAuthenticityTypeId(Long authenticityTypeId) {
		this.authenticityTypeId = authenticityTypeId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
}
