package ru.insoft.sovdoc.model.complex.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "V_CARD_LINKS")
public class VComplexCardLinks {

  @Id
  @Column(name = "ID_LINKED_CARDS")
  private Long idLinkes;

  @Column(name = "UNIT_ID_ORIGINAL")
  private Long unitIdOriginal;

  @Column(name = "UNIT_ID_LINKED")
  private Long unitIdLinked;

  @Column(name = "UNIT_FULL_NAME")
  private String unitFullName;

  @Column(name = "UNIT_TYPE_CODE")
  private String unitTypeCode;
  
  @Column(name = "PORTAL_SECTION_ID")
  private Long portalSectionId;

  /**
   * @return the idLinkes
   */
  public Long getIdLinkes() {
    return idLinkes;
  }

  /**
   * @param idLinkes the idLinkes to set
   */
  public void setIdLinkes(Long idLinkes) {
    this.idLinkes = idLinkes;
  }

  /**
   * @return the unitIdOriginal
   */
  public Long getUnitIdOriginal() {
    return unitIdOriginal;
  }

  /**
   * @param unitIdOriginal the unitIdOriginal to set
   */
  public void setUnitIdOriginal(Long unitIdOriginal) {
    this.unitIdOriginal = unitIdOriginal;
  }

  /**
   * @return the unitIdLinked
   */
  public Long getUnitIdLinked() {
    return unitIdLinked;
  }

  /**
   * @param unitIdLinked the unitIdLinked to set
   */
  public void setUnitIdLinked(Long unitIdLinked) {
    this.unitIdLinked = unitIdLinked;
  }

  /**
   * @return the unitFullName
   */
  public String getUnitFullName() {
    return unitFullName;
  }

  /**
   * @param unitFullName the unitFullName to set
   */
  public void setUnitFullName(String unitFullName) {
    this.unitFullName = unitFullName;
  }

  /**
   * @return the unitTypeCode
   */
  public String getUnitTypeCode() {
    return unitTypeCode;
  }

  /**
   * @param unitTypeCode the unitTypeCode to set
   */
  public void setUnitTypeCode(String unitTypeCode) {
    this.unitTypeCode = unitTypeCode;
  }

    public Long getPortalSectionId() {
        return portalSectionId;
    }

    public void setPortalSectionId(Long portalSectionId) {
        this.portalSectionId = portalSectionId;
    }
}
