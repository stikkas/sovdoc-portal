package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.adm.table.AdmUser;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Entity
@Table(name = "UNIV_DATA_UNIT")
public class UnivDataUnit implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "univdataunitidgen")
	@SequenceGenerator(name = "univdataunitidgen", sequenceName = "SEQ_UNIV_DATA_UNIT")
	@Column(name = "UNIV_DATA_UNIT_ID", nullable = false)
	private Long univDataUnitId;

	@OneToOne
	@JoinColumn(name = "RES_OWNER_ID")
	private DescriptorValue resOwner;

	@OneToOne
	@JoinColumn(name = "DESCRIPTION_LEVEL_ID")
	private DescriptorValue descriptionLevel;

	@OneToOne
	@JoinColumn(name = "PORTAL_SECTION_ID")
	private DescriptorValue section;

	@Column(name = "PARENT_UNIT_ID")
	private Long parentUnitId;

	@OneToOne
	@JoinColumn(name = "UNIT_TYPE_ID")
	private DescriptorValue unitType;

	@Column(name = "UNIT_NAME")
	private String unitName;

	@Column(name = "NUMBER_NUMBER")
	private Integer numberNumber;

	@Column(name = "NUMBER_PREFIX")
	private String numberPrefix;

	@Column(name = "NUMBER_TEXT")
	private String numberText;

	@Column(name = "ARCH_NUMBER_CODE")
	private String archNumberCode;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "ANNOTATION")
	private String annotation;

	@Column(name = "CONTENTS")
	private String contents;

	@Column(name = "IS_ACCESS_LIMITED")
	private boolean isAccessLimited;

	@OneToOne
	@JoinColumn(name = "ADD_USER_ID")
	private AdmUser addUser;

	@OneToOne
	@JoinColumn(name = "MOD_USER_ID")
	private AdmUser modUser;

	@Column(name = "INSERT_DATE")
	private Date insertDate;

	@Column(name = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	@Column(name = "full_access_to_images")
	private boolean fullAccessToImages;

	@OneToMany(mappedBy = "dataUnit")
	private List<UnivDateInterval> dateIntervals;

	@OneToMany(mappedBy = "dataUnit", targetEntity = UnivImagePath.class)
	@OrderBy(value = "sortOrder")
	private List<UnivImagePath> imagePathes;

	@ManyToMany
	@JoinTable(name = "UNIV_DATA_LANGUAGE", joinColumns = {
		@JoinColumn(name = "univ_data_unit_id")},
			inverseJoinColumns = {
				@JoinColumn(name = "doc_language_id")})
	private List<DescriptorValue> langs = new ArrayList<DescriptorValue>();

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public DescriptorValue getDescriptionLevel() {
		return descriptionLevel;
	}

	public void setDescriptionLevel(DescriptorValue descriptionLevel) {
		this.descriptionLevel = descriptionLevel;
	}

	public Long getParentUnitId() {
		return parentUnitId;
	}

	public void setParentUnitId(Long parentUnitId) {
		this.parentUnitId = parentUnitId;
	}

	public DescriptorValue getUnitType() {
		return unitType;
	}

	public void setUnitType(DescriptorValue unitType) {
		this.unitType = unitType;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Integer getNumberNumber() {
		return numberNumber;
	}

	public void setNumberNumber(Integer numberNumber) {
		this.numberNumber = numberNumber;
	}

	public String getNumberText() {
		return numberText;
	}

	public void setNumberText(String numberText) {
		this.numberText = numberText;
	}

	public String getArchNumberCode() {
		return archNumberCode;
	}

	public void setArchNumberCode(String archNumberCode) {
		this.archNumberCode = archNumberCode;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public boolean isAccessLimited() {
		return isAccessLimited;
	}

	public void setAccessLimited(boolean isAccessLimited) {
		this.isAccessLimited = isAccessLimited;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public List<UnivDateInterval> getDateIntervals() {
		return dateIntervals;
	}

	public void setDateIntervals(List<UnivDateInterval> dateIntervals) {
		this.dateIntervals = dateIntervals;
	}

	public AdmUser getAddUser() {
		return addUser;
	}

	public void setAddUser(AdmUser addUser) {
		this.addUser = addUser;
	}

	public AdmUser getModUser() {
		return modUser;
	}

	public void setModUser(AdmUser modUser) {
		this.modUser = modUser;
	}

	public DescriptorValue getResOwner() {
		return resOwner;
	}

	public void setResOwner(DescriptorValue resOwner) {
		this.resOwner = resOwner;
	}

	public List<UnivImagePath> getImagePathes() {
		return imagePathes;
	}

	public void setImagePathes(List<UnivImagePath> imagePathes) {
		this.imagePathes = imagePathes;
	}

	public List<DescriptorValue> getLangs() {
		return langs;
	}

	public void setLangs(List<DescriptorValue> langs) {
		this.langs = langs;
	}

	public DescriptorValue getSection() {
		return section;
	}

	public void setSection(DescriptorValue section) {
		this.section = section;
	}

	public boolean isFullAccessToImages() {
		return fullAccessToImages;
	}

	public void setFullAccessToImages(boolean fullAccessToImages) {
		this.fullAccessToImages = fullAccessToImages;
	}

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public void setNumberPrefix(String numberPrefix) {
        this.numberPrefix = numberPrefix;
    }
    
}
