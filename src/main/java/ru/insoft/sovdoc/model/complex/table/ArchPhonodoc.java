package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.FetchType.EAGER;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * Таблица для фонодокументов
 *
 * @author stikkas<stikkas@yandex.ru>
 */
@Entity
@Table(name = "ARCH_PHONODOC")
public class ArchPhonodoc implements Serializable {

	/**
	 * ID единицы информационного ресурса
	 */
	@Id
	@Column(name = "UNIV_DATA_UNIT_ID", insertable = false, updatable = false)
	private Long id;

	/**
	 * Вид записанного материала
	 */
	@Column(name = "RECORD_TYPE_ID", insertable = false, updatable = false)
	private Long recordTypeId;

	/**
	 * Вид фонодокумента
	 */
	@Column(name = "PHONODOC_TYPE_ID", insertable = false, updatable = false)
	private Long phonodocTypeId;

	/**
	 * Заголовок фонодокумента
	 */
	@Column(name = "PHONODOC_NAME", insertable = false, updatable = false)
	private String title;

	/**
	 * Авторы
	 */
	@Column(name = "AUTHORS", insertable = false, updatable = false)
	private String authors;

	/**
	 * Место события
	 */
	@Column(name = "EVENT_PLACE", insertable = false, updatable = false)
	private String place;

	/**
	 * Исполнитель (оркестр)
	 */
	@Column(name = "PERFORMER_ORCHESTRA", insertable = false, updatable = false)
	private String orchestra;

	/**
	 * Исполнитель (хор)
	 */
	@Column(name = "PERFORMER_CHOIR", insertable = false, updatable = false)
	private String choir;

	/**
	 * Исполнители
	 */
	@Column(name = "PERFORMERS", insertable = false, updatable = false)
	private String performers;

	/**
	 * Рубрика
	 */
	@Column(name = "RUBRICS", insertable = false, updatable = false)
	private String rubrics;

	/**
	 * Дата записи
	 */
	@Column(name = "RECORD_DATE", insertable = false, updatable = false, columnDefinition = "DATE")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date recordDate;

	/**
	 * Дата перезаписи
	 */
	@Column(name = "REWRITE_DATE", insertable = false, updatable = false, columnDefinition = "DATE")
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date rewriteDate;

	/**
	 * Состав комплекта
	 */
	@Column(name = "KIT_INFO", insertable = false, updatable = false)
	private String kitInfo;

	/**
	 * Наименование изготовителя
	 */
	@Column(name = "MANUFACTURER_NAME", insertable = false, updatable = false)
	private String manufacturer;

	/**
	 * Аннотация
	 */
	@Lob
	@Column(name = "ANNOTATION", insertable = false, updatable = false)
	private String annotation;

	/**
	 * Примечание
	 */
	@Column(name = "NOTES", insertable = false, updatable = false)
	private String notes;

	/**
	 * Единицы хранения
	 */
	@OneToMany(mappedBy = "phonodoc", fetch = EAGER)
	List<ArchPhonoStorageUnit> storageUnits;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRecordTypeId() {
		return recordTypeId;
	}

	public void setRecordTypeId(Long recordTypeId) {
		this.recordTypeId = recordTypeId;
	}

	public Long getPhonodocTypeId() {
		return phonodocTypeId;
	}

	public void setPhonodocTypeId(Long phonodocTypeId) {
		this.phonodocTypeId = phonodocTypeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getOrchestra() {
		return orchestra;
	}

	public void setOrchestra(String orchestra) {
		this.orchestra = orchestra;
	}

	public String getChoir() {
		return choir;
	}

	public void setChoir(String choir) {
		this.choir = choir;
	}

	public String getPerformers() {
		return performers;
	}

	public void setPerformers(String performers) {
		this.performers = performers;
	}

	public String getRubrics() {
		return rubrics;
	}

	public void setRubrics(String rubrics) {
		this.rubrics = rubrics;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Date getRewriteDate() {
		return rewriteDate;
	}

	public void setRewriteDate(Date rewriteDate) {
		this.rewriteDate = rewriteDate;
	}

	public String getKitInfo() {
		return kitInfo;
	}

	public void setKitInfo(String kitInfo) {
		this.kitInfo = kitInfo;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<ArchPhonoStorageUnit> getStorageUnits() {
		return storageUnits;
	}

	public void setStorageUnits(List<ArchPhonoStorageUnit> storageUnits) {
		this.storageUnits = storageUnits;
	}

}
