package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Единица хранения аудиоданных
 *
 * @author stikkas<stikkas@yandex.ru>
 */
@Entity
@Table(name = "ARCH_PHONO_STORAGE_UNIT")
public class ArchPhonoStorageUnit implements Serializable, Comparable<ArchPhonoStorageUnit> {

	/**
	 * ID единицы хранения аудиоданных
	 */
	@Id
	@Column(name = "PHONO_STORAGE_UNIT_ID", insertable = false, updatable = false)
	private Long id;

	/**
	 * Единица информационного ресурса
	 */
	@ManyToOne
	@JoinColumn(name="UNIV_DATA_UNIT_ID", referencedColumnName = "UNIV_DATA_UNIT_ID", insertable = false, updatable = false)
	private ArchPhonodoc phonodoc;

	/**
	 * Номер единицы хранения
	 */
	@Column(name = "NUMBER_NUMBER", insertable = false, updatable = false)
	private Long number;

	/**
	 * Литера единицы хранения
	 */
	@Column(name = "NUMBER_TEXT", insertable = false, updatable = false)
	private String litera;

	/**
	 * Время звучания
	 */
	@Column(name = "PLAYTIME", insertable = false, updatable = false)
	private Long time;

	/**
	 * Аудио файл для единицы хранения
	 * Слишком запутана структура таблиц (очень много зациклено на UNIV_DATA_UNIT_ID)
	 * поэтому Hibernate не может создать запрос, и приходится, не использовать отношения
	 */
	@Column(name = "UNIV_IMAGE_PATH_ID", insertable = false, updatable = false)
	private Long pathId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getLitera() {
		return litera;
	}

	public void setLitera(String litera) {
		this.litera = litera;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public ArchPhonodoc getPhonodoc() {
		return phonodoc;
	}

	public void setPhonodoc(ArchPhonodoc phonodoc) {
		this.phonodoc = phonodoc;
	}

	public Long getPathId() {
		return pathId;
	}

	public void setPathId(Long pathId) {
		this.pathId = pathId;
	}

	
	public String getFullNumber() {
		return number + (litera != null && !litera.isEmpty() ? "-" + litera : "");
	}

	@Override
	public int compareTo(ArchPhonoStorageUnit o) {
		return getFullNumber().compareTo(o.getFullNumber());
	}

}
