package ru.insoft.sovdoc.model.complex.table;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

/**
 * Аннотации к единицам хранения фонодокументов
 *
 * @author stikkas<stikkas@yandex.ru>
 */
@Entity
@Table(name = "ARCH_PERFORMANCE_ANNOTATION")
@NamedQueries(
		@NamedQuery(name = "ArchPerformanceAnnotation.findAllAnnosByUnit", query
				= "SELECT a FROM ArchPerformanceAnnotation a WHERE a.storageUnitId = :suid ORDER BY a.number"))
public class ArchPerformanceAnnotation implements Serializable {

	/**
	 * ID аннотации выступления
	 */
	@Id
	@Column(name = "ANNOTATION_ID", insertable = false, updatable = false)
	private Long id;

	/**
	 * ID единицы хранения аудиоданных
	 */
	@Column(name = "PHONO_STORAGE_UNIT_ID", insertable = false, updatable = false)
	private Long storageUnitId;

	/**
	 * Номер выступления
	 */
	@Column(name = "PERFORMANCE_NUMBER", insertable = false, updatable = false)
	private Integer number;

	/**
	 * Сторона/трек
	 */
	@Column(name = "SIDE_OR_TRACK", insertable = false, updatable = false)
	private String sideTrack;

	/**
	 * Аннотация выступления
	 */
	@Lob
	@Column(name = "ANNOTATION", insertable = false, updatable = false)
	private String annotation;

	/**
	 * Докладчик
	 */
	@Column(name = "SPOKESPERSON", insertable = false, updatable = false)
	private String speaker;

	/**
	 * Язык
	 */
	@ManyToOne
	@JoinColumn(name = "LANGUAGE_ID", referencedColumnName = "DESCRIPTOR_VALUE_ID", insertable = false, updatable = false)
	private DescriptorValue language;

	/**
	 * Время звучания
	 */
	@Column(name = "PLAYTIME", insertable = false, updatable = false)
	private Long time;

	/**
	 * запись о файле
	 */
	@ManyToOne
	@JoinColumn(name = "UNIV_IMAGE_PATH_ID", referencedColumnName = "UNIV_IMAGE_PATH_ID", insertable = false, updatable = false)
	private UnivImagePath path;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStorageUnit() {
		return storageUnitId;
	}

	public void setStorageUnit(Long storageUnitId) {
		this.storageUnitId = storageUnitId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getSideTrack() {
		return sideTrack;
	}

	public void setSideTrack(String sideTrack) {
		this.sideTrack = sideTrack;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	public DescriptorValue getLanguage() {
		return language;
	}

	public void setLanguage(DescriptorValue language) {
		this.language = language;
	}

	public Long getTime() {
		return time;
	}

	/**
	 * Возвращает время проигрывания в формате hh:mm:ss . Предполагается что в
	 * базе время хранится в секундах
	 * @return 
	 */
	public String getFormattedTime() {
		long minutes = time / 60;
		long hours = minutes / 60;
		minutes %= 60;
		long secs = time % 60;
		return String.format("%02d:%02d:%02d", hours, minutes, secs);
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public UnivImagePath getPath() {
		return path;
	}

	public void setPath(UnivImagePath path) {
		this.path = path;
	}

}
