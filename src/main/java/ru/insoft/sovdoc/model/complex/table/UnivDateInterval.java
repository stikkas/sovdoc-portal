package ru.insoft.sovdoc.model.complex.table;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "UNIV_DATE_INTERVAL")
public class UnivDateInterval {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dateintervalidgen")
  @SequenceGenerator(name = "dateintervalidgen", sequenceName = "SEQ_UNIV_DATE_INETERVAL")
  @Column(name = "DATE_INTERVAL_ID")
  private Long dateInetrvalId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "UNIV_DATA_UNIT_ID")
  private UnivDataUnit dataUnit;

  @Column(name = "BEGIN_DATE")
  private Date beginDate;

  @Column(name = "END_DATE")
  private Date endDate;

  public Long getDateInetrvalId() {
    return dateInetrvalId;
  }

  public void setDateInetrvalId(Long dateInetrvalId) {
    this.dateInetrvalId = dateInetrvalId;
  }

  public UnivDataUnit getDataUnit() {
    return dataUnit;
  }

  public void setDataUnit(UnivDataUnit dataUnit) {
    this.dataUnit = dataUnit;
  }

  public Date getBeginDate() {
    return beginDate;
  }

  public void setBeginDate(Date beginDate) {
    this.beginDate = beginDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getPrintableBeginDate() {
    if (dataUnit.getDescriptionLevel().getValueCode().equals("SERIES") || dataUnit.getDescriptionLevel().getValueCode().equals("FUND")) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
      return sdf.format(beginDate);
    } else {
      SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
      return sdf.format(beginDate);
    }
  }

  public String getPrintableEndDate() {
    if (dataUnit.getDescriptionLevel().getValueCode().equals("SERIES") || dataUnit.getDescriptionLevel().getValueCode().equals("FUND")) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
      return sdf.format(endDate);
    } else {
      SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
      return sdf.format(endDate);
    }
  }
}
