package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Entity
@Table(name = "ARCH_SERIES")
public class ArchSeries {

	@Id
	@Column(name = "UNIV_DATA_UNIT_ID")
	private Long univDataUnitId;
	
	@OneToOne
	@JoinColumn(name = "DOCUMENT_KIND_ID")
	private DescriptorValue documentKind;
	
	@Column(name = "SERIES_NAME")
	private String seriesName;
	
	@Column(name = "STORAGE_UNIT_COUNT")
	private int storageUnitCount;
	
	@OneToOne
	@JoinColumn(name = "REPRODUCTION_TYPE_ID")
	private DescriptorValue reproductionType;
	
	@Column(name = "NOTES")
	private String notes;

	public Long getUnivDataUnitId() {
		return univDataUnitId;
	}

	public void setUnivDataUnitId(Long univDataUnitId) {
		this.univDataUnitId = univDataUnitId;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}

	public Integer getStorageUnitCount() {
		return storageUnitCount;
	}

	public void setStorageUnitCount(Integer storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public DescriptorValue getDocumentKind() {
		return documentKind;
	}

	public void setDocumentKind(DescriptorValue documentKind) {
		this.documentKind = documentKind;
	}

	public DescriptorValue getReproductionType() {
		return reproductionType;
	}

	public void setReproductionType(DescriptorValue reproductionType) {
		this.reproductionType = reproductionType;
	}

	public void setStorageUnitCount(int storageUnitCount) {
		this.storageUnitCount = storageUnitCount;
	}
	
}
