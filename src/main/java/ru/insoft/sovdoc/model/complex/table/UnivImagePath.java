package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ru.insoft.sovdoc.model.desc.table.DescriptorValue;

@Entity
@Table(name = "UNIV_IMAGE_PATH")
public class UnivImagePath {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "imagepathidgen")
	@SequenceGenerator(name = "imagepathidgen", sequenceName = "SEQ_UNIV_IMAGE_PATH")
	@Column(name = "UNIV_IMAGE_PATH_ID", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "UNIV_DATA_UNIT_ID", referencedColumnName = "UNIV_DATA_UNIT_ID")
	private UnivDataUnit dataUnit;

	@Column(name = "SORT_ORDER", nullable = false)
	private Long sortOrder;

	@Column(name = "FILE_PATH", nullable = false)
	private String filePath;

	@Column(name = "FILE_NAME", nullable = false)
	private String fileName;

	@OneToOne
	@JoinColumn(name = "FORMAT_ID", nullable = false)
	private DescriptorValue dvFormat;

	@OneToOne
	@JoinColumn(name = "IMAGE_CATEGORY_ID", nullable = false)
	private DescriptorValue category;

	@Column(name = "CAPTION", length = 500, nullable = true)
	private String caption;

	public String getCaption() {
		return this.caption;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UnivDataUnit getDataUnit() {
		return dataUnit;
	}

	public void setDataUnit(UnivDataUnit dataUnit) {
		this.dataUnit = dataUnit;
	}

	public Long getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Long sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public DescriptorValue getDvFormat() {
		return dvFormat;
	}

	public void setDvFormat(DescriptorValue dvFormat) {
		this.dvFormat = dvFormat;
	}

	public DescriptorValue getCategory() {
		return category;
	}

	public void setCategory(DescriptorValue category) {
		this.category = category;
	}
	
}
