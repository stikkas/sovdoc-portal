package ru.insoft.sovdoc.model.complex.table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "APPLICATION_TO_DOCUMENT")
public class ApplicationToDocument {

  @Id
  @SequenceGenerator(sequenceName = "SEQ_APPLICATION_TO_DOCUMENT", name = "applToDoc", allocationSize = 1)
  @GeneratedValue(generator = "applToDoc", strategy = GenerationType.SEQUENCE)
  @Column(name = "APPLICATION_TO_DOCUMENT_ID")
  private Long applToDocId;

  @Column(name = "UNIV_DATA_UNIT_ID")
  private Long univDataUnitId;

  @Column(name = "APPLICATION_NUMBER")
  private String applNumber;

  @Column(name = "APPLICATION_NAME")
  private String applName;

  @Column(name = "PAGES")
  private String pages;

  @Column(name = "PAGES_QUANTITY")
  private Long pagesQuantity;

  /**
   * @return the applToDocId
   */
  public Long getApplToDocId() {
    return applToDocId;
  }

  /**
   * @param applToDocId the applToDocId to set
   */
  public void setApplToDocId(Long applToDocId) {
    this.applToDocId = applToDocId;
  }

  /**
   * @return the univDataUnitId
   */
  public Long getUnivDataUnitId() {
    return univDataUnitId;
  }

  /**
   * @param univDataUnitId the univDataUnitId to set
   */
  public void setUnivDataUnitId(Long univDataUnitId) {
    this.univDataUnitId = univDataUnitId;
  }

  /**
   * @return the applNumber
   */
  public String getApplNumber() {
    return applNumber;
  }

  /**
   * @param applNumber the applNumber to set
   */
  public void setApplNumber(String applNumber) {
    this.applNumber = applNumber;
  }

  /**
   * @return the applName
   */
  public String getApplName() {
    return applName;
  }

  /**
   * @param applName the applName to set
   */
  public void setApplName(String applName) {
    this.applName = applName;
  }

  /**
   * @return the pages
   */
  public String getPages() {
    return pages;
  }

  /**
   * @param pages the pages to set
   */
  public void setPages(String pages) {
    this.pages = pages;
  }

  /**
   * @return the pagesQuantity
   */
  public Long getPagesQuantity() {
    return pagesQuantity;
  }

  /**
   * @param pagesQuantity the pagesQuantity to set
   */
  public void setPagesQuantity(Long pagesQuantity) {
    this.pagesQuantity = pagesQuantity;
  }



}
