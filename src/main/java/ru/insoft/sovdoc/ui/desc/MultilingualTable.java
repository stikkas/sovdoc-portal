package ru.insoft.sovdoc.ui.desc;

public interface MultilingualTable {

	public Long getRootId();
	void setRootId(Long rootId);
	
	public String getLanguageCode();
	void setLanguageCode(String languageCode);
}
