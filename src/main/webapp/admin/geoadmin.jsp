﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="ext/ext-theme-classic-all.css">
<script src="ext/ext-all.js" type="text/javascript"></script>
<script src="ext/ext-lang-ru.js" type="text/javascript"></script>
<script type="text/javascript" src="js/worldmap.src.js"></script>
<script type="text/javascript" src="js/countrycodes.js"></script>
<base href="<%=basePath%>">
<title>Настройки доступа к образам по ip</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<style type="text/css">
.worldmap {
	fill: white;
	stroke: black;
	stroke-width: 0.5px;
}

.worldmap:hover {
	fill: red;
	stroke: black;
	stroke-width: 0.5px;
}
</style>
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
</head>

<body>
	<h1>Управление доступом к образам документов на портале "Документы
		советской эпохи"</h1>
	<P>Доступ к образам документов, размещенных на портале, возможен только для посетителей из тех стран, которые включены в список.</P>
	<p>Для добавления страны в список - выберите её на политической карте мира и подтвердите выбор.</p>
	<p>После завершения формирования списка, нажмите кнопку "Сохранить" для сохранения параметров конфигурации на сервере.</p>
	<div id="worldmap" width="640" height="400" style="overflow:hidden;"
		align="center"></div>
	<div id="extgrid" width="640" height="300" align="center"></div>
	<div id="exttoolbar" width="640" height="30" align="center"></div>
	<script type="text/javascript">
		var click = null;
        var over = null;
        Ext.onReady(function() {
	        var stoore = Ext.create('Ext.data.Store', {
	            storeId : 'simpsonsStore',
	            fields : [ 'code', 'runame' ],
	            data : {
		            'items' : []
	            },
	            proxy : {
	                type : 'memory',
	                reader : {
	                    type : 'json',
	                    root : 'items'
	                }
	            }
	        });
	        click = function(code) {
		        var isExist = false;
		        for ( var j = 0; j < stoore.getCount(); j++) {
			        if (stoore.getAt(j).data.code == code) {
				        isExist = true;
			        }
		        }
		        if (!isExist) {
			        var mBoxConfirm = function() {
				        if (arguments[0] != 'no') {
					        var addRow = {
					            code : code,
					            runame : window.array[code]
					        };
					        stoore.add(addRow);
				        }
			        };
			        Ext.MessageBox.confirm('Подтверждение операции', 'Подтвердите добавление страны ' + window.array[code] + ' в список', mBoxConfirm);
		        } else {
			        Ext.Msg.alert('Сообщение', 'Страна уже находится в списке');
		        }
	        }
	        var tip = Ext.create('Ext.tip.ToolTip', {
	            target : 'worldmap',
	            html : ''
	        });
	        over = function(code) {
		        tip.update(window.array[code]);
		        tip.show();
	        }
	        var map = new WorldMap({
	            element : 'worldmap',
	            width : 800,
	            height : 600,
	            clickhandler : 'click',
	            overhandler : 'over'
	        });
	        var gridSelection = null;
	        var grid = Ext.create('Ext.grid.Panel', {
	            sortableColumns : false,
	            enableColumnHide : false,
	            width : 640,
	            renderTo : 'extgrid',
	            columns : [ {
	                text : 'Код страны',
	                dataIndex : 'code',
	                width : 15
	            }, {
	                text : 'Название страны',
	                dataIndex : 'runame'
	            } ],
	            store : stoore,
	            title : 'Страны из которых разрешен доступ к образам документов',
	            forceFit : true,
	            listeners : {
		            select : function(th, rec, ind) {
			            gridSelection = rec;
		            }
	            }
	        });
	        var generateValueForServer = function() {
		        var resultValue = "";
		        for ( var k = 0; k < stoore.getCount(); k++) {
			        resultValue += stoore.getAt(k).data.code;
			        if (k != stoore.getCount() - 1) {
				        resultValue += ",";
			        }
		        }
		        return resultValue;
	        }
	        var saveBtn = Ext.create('Ext.Button', {
	            text : 'Сохранить',
	            handler : function() {
		            var me = this;
		            me.setDisabled(true)
		            Ext.Ajax.request({
		                url : 'admin/AdminImages',
		                params : {
		                    action : 'save',
		                    countries : generateValueForServer()
		                },
		                success : function(response) {
			                var text = Ext.decode(response.responseText);
			                Ext.Msg.alert('Результат', text.msg);
			                me.setDisabled()
		                },
		                failure : function() {
			                Ext.Msg.alert('fail', text.msg);
		                }
		            });
	            }
	        });
	        var loadAllowedCountries = function() {
		        stoore.removeAll();
		        Ext.Ajax.request({
		            url : 'admin/AdminImages',
		            params : {
			            action : 'get'
		            },
		            success : function(response) {
			            var text = Ext.decode(response.responseText);
			            var countries = text.msg;
			            if (countries) {
				            var arrayCountries = countries.split(',');
				            for ( var l = 0; l < arrayCountries.length; l++) {
					            var tempObj = {
					                code : arrayCountries[l],
					                runame : window.array[arrayCountries[l]]
					            }
					            stoore.add(tempObj);
				            }
			            }
		            },
		            failure : function() {
			            Ext.Msg.alert('fail', text.failMessage);
		            }
		        });
	        };
	        var cancelBtn = Ext.create('Ext.Button', {
	            text : 'Отмена',
	            handler : function() {
		            loadAllowedCountries();
	            }
	        });
	        var delBtn = Ext.create('Ext.Button', {
	            text : 'Удалить выбранную запись из списка',
	            handler : function() {
		            if (gridSelection != null) {
			            stoore.remove(gridSelection);
		            }
	            }
	        });
	        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
	            width : 640,
	            items : [ saveBtn, cancelBtn, delBtn ],
	            renderTo : 'exttoolbar'
	        });
	        loadAllowedCountries();
        });
	</script>

</body>
</html>
