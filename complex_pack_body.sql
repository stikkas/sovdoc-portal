--------------------------------------------------------
--  File created - пятница-Ноябрь-20-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body COMPLEX_PACK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "SOVDOC"."COMPLEX_PACK" 
/* Formatted on 4-мар-2015 11:31:05 (QP5 v5.126) */
IS
    --Возвращает часть хранимого архивного шифра (номер/литеру фонда/описи/дела)
    FUNCTION extract_arch_code_part (
/*ADVICE(5): Function with more than one RETURN statement in the
                  executable section [512] */
        p_arch_number_code   IN univ_data_unit.arch_number_code%TYPE,
        p_part_number        IN NUMBER)
        RETURN VARCHAR2
    IS
        v_pos1   NUMBER (5);
        v_pos2   NUMBER (5);
    BEGIN
        IF p_part_number = 1
        THEN
            v_pos1 := 1;
        ELSE
            v_pos1 :=
                INSTR (p_arch_number_code,
                       CHR (1),
                       1,
                       p_part_number - 1)
                + 1;

            IF v_pos1 = 1
            THEN
                RETURN NULL;
            END IF;
        END IF;

        v_pos2 :=
            INSTR (p_arch_number_code,
                   CHR (1),
                   1,
                   p_part_number);

        IF v_pos2 = 0
        THEN
            RETURN SUBSTR (p_arch_number_code, v_pos1);
        ELSE
            RETURN SUBSTR (p_arch_number_code, v_pos1, v_pos2 - v_pos1);
        END IF;
    END;

    --Возвращает данные по делу (ед.хр.) в читабельном виде
    FUNCTION get_readable_unit_info (
        p_arch_number_code IN univ_data_unit.arch_number_code%TYPE)

        RETURN VARCHAR2
    IS
        v_unit_num      NUMBER (10);
        v_unit_letter   VARCHAR2 (10);
        v_unit_volume   NUMBER (5);
        v_unit_part     NUMBER (5);

        v_res           VARCHAR2 (500);
    BEGIN
        v_unit_num := extract_arch_code_part (p_arch_number_code, 5);
        v_unit_letter := extract_arch_code_part (p_arch_number_code, 6);
        v_unit_volume := extract_arch_code_part (p_arch_number_code, 7);
        v_unit_part := extract_arch_code_part (p_arch_number_code, 8);

        IF v_unit_num IS NOT NULL
        THEN
            v_res := v_unit_num || v_unit_letter;

            IF v_unit_volume IS NOT NULL
            THEN
                v_res := v_res || '. Т.' || v_unit_volume;
            END IF;

            IF v_unit_part IS NOT NULL
            THEN
                v_res := v_res || '. Ч.' || v_unit_part;
            END IF;
        END IF;

        RETURN v_res;
    END;


    --Возвращает архивный шифр для отображения в результатах поиска
    FUNCTION get_readable_arch_number (
        p_arch_number_code IN univ_data_unit.arch_number_code%TYPE,
        doc_prefix IN VARCHAR2)

        RETURN VARCHAR2
    IS
        v_fund_num        NUMBER (10);
        v_fund_letter     VARCHAR2 (10);
        v_series_num      NUMBER (10);
        v_series_letter   VARCHAR2 (10);
        v_unit_num        NUMBER (10);
        v_pages           VARCHAR2 (250);

        v_res             VARCHAR2 (500);
    BEGIN
        v_fund_num := extract_arch_code_part (p_arch_number_code, 1);
        v_fund_letter := extract_arch_code_part (p_arch_number_code, 2);
        v_series_num := extract_arch_code_part (p_arch_number_code, 3);
        v_series_letter := extract_arch_code_part (p_arch_number_code, 4);
        v_unit_num := extract_arch_code_part (p_arch_number_code, 5);
        v_pages := extract_arch_code_part (p_arch_number_code, 10);

        IF v_fund_num IS NOT NULL
        THEN
            v_res := v_res || 'Ф.' || v_fund_num || v_fund_letter;
        END IF;

        IF v_series_num IS NOT NULL
        THEN
		    if v_res is not null then
			    v_res := v_res || '. ';
			end if;
            v_res := v_res || 'Оп.' || v_series_num || v_series_letter;
        END IF;

        IF v_unit_num IS NOT NULL
        THEN
		    if v_res is not null then
			    v_res := v_res || '. ';
			end if;
            v_res :=
                   v_res
                || doc_prefix
                || get_readable_unit_info (p_arch_number_code);
        END IF;

        IF v_pages IS NOT NULL
        THEN
		    if v_res is not null then
			    v_res := v_res || '. ';
			end if;
            v_res := v_res || 'Л.' || v_pages;
        END IF;

        RETURN v_res;
    END;


    --Возвращает диапазоны дат материалов данной карточки
    FUNCTION get_date_intervals (
        p_univ_data_unit_id IN univ_data_unit.univ_data_unit_id%TYPE)

        RETURN VARCHAR2
    IS
        v_res    VARCHAR2 (200);
        v_part   VARCHAR2 (30);
    BEGIN
        FOR r
        IN (  SELECT   a.begin_date AS begin_date,
                       a.end_date AS end_date,
                       uv.description_level_id AS description_level_id
                FROM       univ_date_interval a
                       LEFT JOIN
                           univ_data_unit uv
                       ON uv.univ_data_unit_id = a.univ_data_unit_id
               WHERE   a.univ_data_unit_id = p_univ_data_unit_id
            ORDER BY   a.begin_date)

        LOOP
            IF (r.description_level_id IN (221, 222))
            THEN
                IF r.begin_date = r.end_date
                THEN
                    v_part := TO_CHAR (r.begin_date, 'dd.mm.yyyy');
                ELSE
                    v_part :=
                           TO_CHAR (r.begin_date, 'dd.mm.yyyy')
                        || '-'
                        || TO_CHAR (r.end_date, 'dd.mm.yyyy');
                END IF;
            ELSE
                IF TO_CHAR (r.begin_date, 'yyyy') = TO_CHAR (r.end_date, 'yyyy')
                THEN
                    v_part := TO_CHAR (r.begin_date, 'yyyy');
                ELSE
                    v_part :=
                           TO_CHAR (r.begin_date, 'yyyy')
                        || '-'
                        || TO_CHAR (r.end_date, 'yyyy');
                END IF;
            END IF;

            IF v_res IS NULL
            THEN
                v_res := v_part;
            ELSE
                v_res := v_res || ', ' || v_part;
            END IF;
        END LOOP;

        RETURN v_res;
    END;


   /* --Возвращает диапазоны дат материалов данной карточки old_variant*/
  /* function GET_DATE_INTERVALS(
        p_univ_data_unit_id in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE)
    return varchar2
    is
      v_res   varchar2(200);
      v_part  varchar2(30);
    begin
      for R in (select BEGIN_DATE, END_DATE
                  from UNIV_DATE_INTERVAL
                 where UNIV_DATA_UNIT_ID = p_univ_data_unit_id
                 order by BEGIN_DATE)
      loop
        if to_char(R.BEGIN_DATE, 'ddmm') = '0101' and to_char(R.END_DATE, 'ddmm') = '3112' then
          if to_char(R.BEGIN_DATE, 'yyyy') = to_char(R.END_DATE, 'yyyy') then
            v_part := to_char(R.BEGIN_DATE, 'yyyy');
          else
            v_part := to_char(R.BEGIN_DATE, 'yyyy') || '-' || to_char(R.END_DATE, 'yyyy');
          end if;
        else
          if R.BEGIN_DATE = R.END_DATE then
            v_part := to_char(R.BEGIN_DATE, 'dd.mm.yyyy');
          else
            v_part := to_char(R.BEGIN_DATE, 'dd.mm.yyyy') || '-' || to_char(R.END_DATE, 'dd.mm.yyyy');
          end if;
        end if;
        if v_res is null then
          v_res := v_part;
        else
          v_res := v_res || ', ' || v_part;
        end if;
      end loop;
      return v_res;
    end;*/

    --Возвращает список авторов (или главного редактора)
    FUNCTION get_authors (
        p_univ_data_unit_id IN univ_data_unit.univ_data_unit_id%TYPE)
        RETURN VARCHAR2
    IS
        v_res   VARCHAR2 (500);
    BEGIN
        FOR r IN (SELECT   dv.short_value
                    FROM       arch_ebook_author aea
                           INNER JOIN
                               descriptor_value dv
                           ON aea.author_id = dv.descriptor_value_id
                   WHERE   aea.univ_data_unit_id = p_univ_data_unit_id)
        LOOP
            IF v_res IS NULL
            THEN
                v_res := r.short_value;
            ELSE
                v_res := v_res || ', ' || r.short_value;
            END IF;
        END LOOP;

        IF v_res IS NULL
        THEN
            SELECT   editor || ' (редактор)'
              INTO   v_res
              FROM   arch_ebook
             WHERE   univ_data_unit_id = p_univ_data_unit_id;
        END IF;

        RETURN v_res;
    END;

    --Обновляет архивный шифр у дочерних элементов в случае изменения
    --архивного шифра вышестоящего
    PROCEDURE update_arch_code (
        p_univ_data_unit_id   IN univ_data_unit.univ_data_unit_id%TYPE,
        p_desc_level_code     IN descriptor_value.value_code%TYPE,
        p_arch_number_code    IN univ_data_unit.arch_number_code%TYPE)
    IS
    BEGIN
        UPDATE   univ_data_unit
           SET   arch_number_code =
                     p_arch_number_code
                     || SUBSTR (arch_number_code,
                                INSTR (arch_number_code,
                                       CHR (1),
                                       1,
                                       DECODE (p_desc_level_code,
                                               'FUND',
                                               2,
                                               'SERIES',
                                               4,
                                               'STORAGE_UNIT',
                                               8)))
         WHERE   univ_data_unit_id IN
                         (    SELECT   univ_data_unit_id
                                FROM   univ_data_unit
                          CONNECT BY   PRIOR univ_data_unit_id =
                                           parent_unit_id
                          START WITH   parent_unit_id = p_univ_data_unit_id);
    END;


    --Обновляет принадлежность к тематическому разделу портала у всех
    --дочерних элементов по отношению к переданному
    PROCEDURE update_portal_section (
        p_univ_data_unit_id   IN univ_data_unit.univ_data_unit_id%TYPE,
        p_portal_section_id   IN univ_data_unit.portal_section_id%TYPE)
    IS
    BEGIN
        UPDATE   univ_data_unit
           SET   portal_section_id = p_portal_section_id
         WHERE   univ_data_unit_id IN
                         (    SELECT   univ_data_unit_id
                                FROM   univ_data_unit
                          CONNECT BY   PRIOR univ_data_unit_id =
                                           parent_unit_id
                          START WITH   parent_unit_id = p_univ_data_unit_id);
    END;


    --занесение в журнал принадлежности к тематическому разделу портала у всех
    --дочерних элементов по отношению к переданному
    PROCEDURE journalInsert_portal_section (
        p_univ_data_unit_id   IN univ_data_unit.univ_data_unit_id%TYPE,
        p_portal_section_id_old   IN NUMBER,
        p_portal_section_id_new   IN NUMBER)
    AS
        new_id   NUMBER;
        old_Portal_Section VARCHAR2 (500);
        new_Portal_Section VARCHAR2 (500);

    BEGIN
        IF(p_portal_section_id_old != 0)
        THEN old_Portal_Section:= get_portal_section(p_portal_section_id_old);
        END IF;

        IF(p_portal_section_id_new != 0)
        THEN new_Portal_Section:= get_portal_section(p_portal_section_id_new);
        END IF;

        FOR c
        IN (SELECT univ_data_unit_id as univ_data_unit_id,
                   decode (unit_type_id, 241, 241,
                                         242, 233792,
                                         242, 233792,
                                         243, 233791,
                                         244, 233788,
                                         245, 233789) as unit_type_id
               FROM   univ_data_unit
            CONNECT BY   PRIOR univ_data_unit_id = parent_unit_id
            START WITH   parent_unit_id = p_univ_data_unit_id
            )
        LOOP
            BEGIN
                SELECT   seq_univ_correct_journal.NEXTVAL INTO new_id FROM DUAL;

                IF(c.unit_type_id !=241)
                THEN
                INSERT INTO univ_correct_journal (univ_correct_journal_id,
                                                  user_id,
                                                  archive_id,
                                                  univ_data_unit_id,
                                                  descriptor_value_id,
                                                  operation_type_id,
                                                  changed_entity_type_id,
                                                  operation_date,
                                                  data_block)
                    SELECT   new_id,
                             37,
                             125,
                             c.univ_data_unit_id,
                             NULL,
                             233786,
                             c.unit_type_id,
                             SYSDATE,
                                '<b><u><font color="#B72C36">Редактируемые значения</font></u></b><br/>'
                             || '<b>Тематический раздел портала</b><br/>'
                             || decode(old_Portal_Section, null, '&nbsp;&nbsp;&nbsp;<s>' || '&lt;&lt;пусто&gt;&gt;' || '</s><br/>','&nbsp;&nbsp;&nbsp;<s>' || old_Portal_Section || '</s><br/>')
                             || decode(new_Portal_Section, null, '&nbsp;&nbsp;&nbsp;'    || '&lt;&lt;пусто&gt;&gt;' || '<b/>',  '&nbsp;&nbsp;&nbsp;' || new_Portal_Section || '<br/>')
                      FROM   DUAL;
                END IF;

            END;
        END LOOP;

        COMMIT;
    END;


    --функция получает тематический раздел портала
    FUNCTION get_portal_section (
        p_portal_section_id    IN NUMBER)
        RETURN VARCHAR2
    IS
        result   VARCHAR2 (600);

   BEGIN
        SELECT  dv.full_value
        INTO   result
        FROM   descriptor_value dv
        WHERE   dv.descriptor_value_id = p_portal_section_id;
        RETURN result;
    END;

    --Функция подсчитывает количество дочерних элементов заданного типа
    FUNCTION get_typed_children_count (
        p_univ_data_unit_id    IN univ_data_unit.univ_data_unit_id%TYPE,
        p_children_type_code   IN descriptor_value.value_code%TYPE)
        RETURN NUMBER
    IS
        v_cnt   NUMBER (10);
    BEGIN
            SELECT   COUNT ( * )
/*ADVICE(293): SELECT COUNT used to obtain number of rows for specified
                  WHERE clause [309] */
              INTO   v_cnt
              FROM       univ_data_unit udu
                     INNER JOIN
                         descriptor_value dv
                     ON udu.description_level_id = dv.descriptor_value_id
             WHERE   dv.value_code = p_children_type_code
        CONNECT BY   PRIOR udu.univ_data_unit_id = udu.parent_unit_id
        START WITH   udu.parent_unit_id = p_univ_data_unit_id;

        RETURN v_cnt;
    END;


    --Уменьшает на 1 порядковые номера файлов, прикреплённых к данной карточке,
    --с данной категорией и с порядковым номером больше переданного
    --Вызывается при удалении прикреплённого файла
    PROCEDURE file_order_decrease (
        p_univ_data_unit_id   IN univ_data_unit.univ_data_unit_id%TYPE,
        p_category_id         IN univ_image_path.image_category_id%TYPE,
        p_sort_order          IN univ_image_path.sort_order%TYPE)
    IS
    BEGIN
        UPDATE   univ_image_path
           SET   sort_order = sort_order - 1
         WHERE       univ_data_unit_id = p_univ_data_unit_id
                 AND image_category_id = p_category_id
                 AND sort_order > p_sort_order;
    END;

    --Функция, извлекающая из строки только цифры (числовое значение)
    FUNCTION getnumfromstr (p_string IN VARCHAR2)
        RETURN NUMBER
    AS
        result   NUMBER;
    BEGIN
        result :=
            TO_NUMBER(TRANSLATE (
                          p_string,
                          '1234567890'
                          || TRANSLATE ('#' || p_string, '#1234567890', '#'),
                          '1234567890'));
        RETURN result;
    END;

END;

/
