--------------------------------------------------------
--  File created - пятница-Ноябрь-20-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package SOVDOCPORTAL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "SOVDOC"."SOVDOCPORTAL" AS
FUNCTION getFullArchiveCode(univDataUnitId IN NUMBER)
RETURN VARCHAR2;
FUNCTION getAuthors(univDataUnitId in NUMBER)
RETURN VARCHAR2;
FUNCTION getChildsCount(univDataUnitId in NUMBER)
RETURN NUMBER;
FUNCTION checkInterval(dateA in DATE, dateB in Date, unitId in NUMBER)
return NUMBER;
FUNCTION chekIntervalAonly(dateA in DATE, unitId in NUMBER)
return NUMBER;
END sovdocportal;
 
 
 

/
