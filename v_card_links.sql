--------------------------------------------------------
--  File created - пятница-Ноябрь-20-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View V_CARD_LINKS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "SOVDOC"."V_CARD_LINKS" ("ID_LINKED_CARDS", "UNIT_ID_ORIGINAL", "UNIT_ID_LINKED", "UNIT_FULL_NAME", "UNIT_TYPE_CODE", "PORTAL_SECTION_ID") AS 
  SELECT   ucl.id_linked_cards,
             ucl.unit_id_original,
             ucl.unit_id_linked,
                complex_pack.get_readable_arch_number (udu.arch_number_code, 'Д.')
             || ' '
             || udu.unit_name AS UNIT_FULL_NAME,
             dv.value_code AS unit_type_code,
             udu.portal_section_id
      FROM           univ_cards_links ucl
                 INNER JOIN
                     univ_data_unit udu
                 ON ucl.unit_id_linked = udu.univ_data_unit_id
             INNER JOIN
                 descriptor_value dv
             ON udu.unit_type_id = dv.descriptor_value_id;
