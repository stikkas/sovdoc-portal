--------------------------------------------------------
--  File created - пятница-Ноябрь-20-2015   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package COMPLEX_PACK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "SOVDOC"."COMPLEX_PACK" 
  IS
    --Возвращает часть хранимого архивного шифра (номер/литеру фонда/описи/дела)
    function EXTRACT_ARCH_CODE_PART(
        p_arch_number_code  in  UNIV_DATA_UNIT.ARCH_NUMBER_CODE%TYPE,
        p_part_number       in  number)
    return varchar2;

    --Возвращает данные по делу (ед.хр.) в читабельном виде
    function GET_READABLE_UNIT_INFO(
        p_arch_number_code  in  UNIV_DATA_UNIT.ARCH_NUMBER_CODE%TYPE)
    return varchar2;

    --Возвращает архивный шифр для отображения в результатах поиска
    function GET_READABLE_ARCH_NUMBER(
        p_arch_number_code  in  UNIV_DATA_UNIT.ARCH_NUMBER_CODE%TYPE, doc_prefix IN VARCHAR2)
    return varchar2;

    --Возвращает диапазоны дат материалов данной карточки
    function GET_DATE_INTERVALS(
        p_univ_data_unit_id in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE)
    return varchar2;

    --Возвращает список авторов (или главного редактора)
    function GET_AUTHORS(
        p_univ_data_unit_id in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE)
    return varchar2;

    --Обновляет архивный шифр у дочерних элементов в случае изменения
    --архивного шифра вышестоящего
    procedure UPDATE_ARCH_CODE(
        p_univ_data_unit_id in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE,
        p_desc_level_code   in  DESCRIPTOR_VALUE.VALUE_CODE%TYPE,
        p_arch_number_code  in  UNIV_DATA_UNIT.ARCH_NUMBER_CODE%TYPE);

    --Обновляет принадлежность к тематическому разделу портала у всех
    --дочерних элементов по отношению к переданному
    procedure UPDATE_PORTAL_SECTION(
        p_univ_data_unit_id in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE,
        p_portal_section_id in  UNIV_DATA_UNIT.PORTAL_SECTION_ID%TYPE);

    --занесение в журнал принадлежности к тематическому разделу портала у всех
    --дочерних элементов по отношению к переданному
    PROCEDURE journalInsert_portal_section (
        p_univ_data_unit_id       IN univ_data_unit.univ_data_unit_id%TYPE,
        p_portal_section_id_old   NUMBER,
        p_portal_section_id_new   NUMBER);

    --функция получает тематический раздел портала
    FUNCTION get_portal_section (
        p_portal_section_id    IN NUMBER)
    return VARCHAR2;

    --Функция подсчитывает количество дочерних элементов заданного типа
    function GET_TYPED_CHILDREN_COUNT(
        p_univ_data_unit_id     in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE,
        p_children_type_code    in  DESCRIPTOR_VALUE.VALUE_CODE%TYPE)
    return number;

    --Уменьшает на 1 порядковые номера файлов, прикреплённых к данной карточке,
    --с данной категорией и с порядковым номером больше переданного
    --Вызывается при удалении прикреплённого файла
    procedure FILE_ORDER_DECREASE(
        p_univ_data_unit_id in  UNIV_DATA_UNIT.UNIV_DATA_UNIT_ID%TYPE,
        p_category_id       in  UNIV_IMAGE_PATH.IMAGE_CATEGORY_ID%TYPE,
        p_sort_order        in  UNIV_IMAGE_PATH.SORT_ORDER%TYPE);



    --Функция, извлекающая из строки только цифры (числовое значение) (использую для библиотеки ISBN)
    FUNCTION getnumfromstr (p_string IN VARCHAR2)
        RETURN NUMBER;

END; -- Package spec

/
