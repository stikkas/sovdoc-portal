-- Start of DDL Script for Procedure SOVDOC.IRINDEXER
-- Generated 5-���-2013 12:34:18 from SOVDOC@MGOA_UNC

CREATE OR REPLACE 
procedure irindexer(row_Id in rowid, tlob in out nocopy clob) is
clob_temp CLOB;
count_temp NUMBER;
begin
    for row_ in (select
    UNIV_DATA_UNIT_ID udui,  --for selecting from ARCH_SERIES,ARCH_FUND etc.
    UNIT_TYPE_ID uti,        --
    UNIT_NAME un,               -- UNIT_NAME
    NUMBER_TEXT nt,             -- NUMBER_TEXT
    ARCH_NUMBER_CODE anc,       -- ARCH_NUMBER_CODE
    CONTENTS cts                -- CONTENTS
    from UNIV_DATA_UNIT where rowid=row_Id)
      loop
        tlob:=appendField(row_.un,tlob);      -- UNIT_NAME
        tlob:=appendField(row_.nt,tlob);      -- NUMBER_TEXT
        tlob:=appendField(row_.anc,tlob);     -- ARCH_NUMBER_CODE
        tlob:=appendField(row_.cts,tlob);     -- CONTENTS
        case row_.uti
            when 242 then --(FUND)
                begin
                  declare
                  v_annotation ARCH_FUND.ANNOTATION%TYPE;
                  v_notes ARCH_FUND.NOTES%TYPE;
                  v_historical_note ARCH_FUND.HISTORICAL_NOTE%TYPE;
                  begin
                        select count(*) into count_temp from ARCH_FUND
                        where UNIV_DATA_UNIT_ID = row_.udui;
                        if count_temp>0 then
                        select ANNOTATION, NOTES,HISTORICAL_NOTE
                        into v_annotation,v_notes,v_historical_note
                        from ARCH_FUND where UNIV_DATA_UNIT_ID=row_.udui;
                        tlob:=appendClobField(v_annotation,tlob);
                        tlob:=appendClobField(v_historical_note,tlob);
                        tlob:=appendField(v_notes,tlob);
                        end if;
                  end;
                end;
            when 243 then --(SERIES)
                begin
                    declare
                    v_series_name ARCH_SERIES.SERIES_NAME%TYPE;
                    v_annotation ARCH_SERIES.ANNOTATION%TYPE;
                    v_notes ARCH_SERIES.NOTES%TYPE;
                    begin
                        select count(*) into count_temp from ARCH_SERIES
                        where UNIV_DATA_UNIT_ID=row_.udui;
                        if count_temp>0 then
                             select SERIES_NAME,ANNOTATION,NOTES
                             into v_series_name,v_annotation,v_notes
                             from ARCH_SERIES
                             where UNIV_DATA_UNIT_ID=row_.udui;
                             tlob:=appendField(v_series_name,tlob);
                             tlob:=appendField(v_annotation,tlob);
                             tlob:=appendField(v_notes,tlob);
                            end if;
                    end;
                end;
            when 244 then -- ARCH_STORAGE_UNIT
                begin
                     declare
                        su_file_caption ARCH_STORAGE_UNIT.FILE_CAPTION%TYPE;
                        su_dates_note ARCH_STORAGE_UNIT.DATES_NOTE%TYPE;
                        su_annotation ARCH_STORAGE_UNIT.ANNOTATION%TYPE;
                        su_work_index ARCH_STORAGE_UNIT.WORK_INDEX%TYPE;
                        su_notes ARCH_STORAGE_UNIT.NOTES%TYPE;
                     begin
                        select count(*) into count_temp from ARCH_STORAGE_UNIT
                        where UNIV_DATA_UNIT_ID=row_.udui;
                        if count_temp >0 then
                        select FILE_CAPTION,DATES_NOTE, ANNOTATION, WORK_INDEX,
                        NOTES into
                        su_file_caption,su_dates_note,su_annotation,su_work_index,
                        su_notes
                        from ARCH_STORAGE_UNIT
                        where UNIV_DATA_UNIT_ID = row_.udui;
                        tlob:=appendClobField(su_annotation,tlob);
                        tlob:=appendField(su_file_caption,tlob);
                        tlob:=appendField(su_dates_note,tlob);
                        tlob:=appendField(su_work_index,tlob);
                        tlob:=appendField(su_notes,tlob);
                        end if;
                     end;
                end;
            when 245 then --ARCH_DOCUMENT
                begin
                    declare
                        doc_document_number ARCH_DOCUMENT.DOCUMENT_NUMBER%TYPE;
                        doc_document_name ARCH_DOCUMENT.DOCUMENT_NAME%TYPE;
                        doc_dates_note ARCH_DOCUMENT.DATES_NOTE%TYPE;
                        doc_annotation ARCH_DOCUMENT.ANNOTATION%TYPE;
                        doc_notes ARCH_DOCUMENT.NOTES%TYPE;
                    begin
                        select count(*) into count_temp from ARCH_DOCUMENT
                        where UNIV_DATA_UNIT_ID=row_.udui;
                        if count_temp>0 then
                            select DOCUMENT_NUMBER,DOCUMENT_NAME,DATES_NOTE,
                            ANNOTATION,NOTES
                            into doc_document_number, doc_document_name,
                            doc_dates_note,doc_annotation,doc_notes
                            from ARCH_DOCUMENT where
                                UNIV_DATA_UNIT_ID=row_.udui;
                            tlob:=appendField(doc_document_number,tlob);
                            tlob:=appendField(doc_document_name,tlob);
                            tlob:=appendField(doc_dates_note,tlob);
                            tlob:=appendClobField(doc_annotation,tlob);
                            tlob:=appendField(doc_notes,tlob);
                        end if;
                    end;
                end;
            when 487 then --ARCH_EBOOK
                begin
                    declare
                        eb_authors ARCH_EBOOK.AUTHORS%TYPE;
                        eb_title_note ARCH_EBOOK.TITLE_NOTE%TYPE;
                        eb_edition_note ARCH_EBOOK.EDITION_NOTE%TYPE;
                        eb_publishing_place ARCH_EBOOK.PUBLISHING_PLACE%TYPE;
                        eb_publisher ARCH_EBOOK.PUBLISHER%TYPE;
                        eb_publishing_year ARCH_EBOOK.PUBLISHING_YEAR%TYPE;
                        eb_edition_size ARCH_EBOOK.EDITION_SIZE%TYPE;
                        eb_series_info ARCH_EBOOK.SERIES_INFO%TYPE;
                        eb_isbn ARCH_EBOOK.ISBN%TYPE;
                        eb_udc ARCH_EBOOK.UDC%TYPE;
                        eb_notes ARCH_EBOOK.NOTES%TYPE;
                        eb_annotation ARCH_EBOOK.ANNOTATION%TYPE;
                    begin
                    select count(*) into count_temp from ARCH_EBOOK
                    where UNIV_DATA_UNIT_ID=row_.udui;
                    if count_temp>0 then
                        select authors, title_note,edition_note,publishing_place,
                        publisher,publishing_year, edition_size,series_info,
                        isbn,udc,notes,annotation into
                        eb_authors,eb_title_note,eb_edition_note, eb_publishing_place,
                        eb_publisher,eb_publishing_year,eb_edition_size,eb_series_info,
                        eb_isbn,eb_udc,eb_notes,eb_annotation from
                        ARCH_EBOOK where
                        UNIV_DATA_UNIT_ID = row_.udui;
                        tlob:=appendField(eb_authors,tlob);
                        tlob:=appendField(eb_title_note,tlob);
                        tlob:=appendField(eb_edition_note,tlob);
                        tlob:=appendField(eb_publishing_place,tlob);
                        tlob:=appendField(eb_publisher,tlob);
                        tlob:=appendField(eb_publishing_year,tlob);
                        tlob:=appendField(eb_edition_size,tlob);
                        tlob:=appendField(eb_series_info,tlob);
                        tlob:=appendField(eb_isbn,tlob);
                        tlob:=appendField(eb_udc,tlob);
                        tlob:=appendField(eb_notes,tlob);
                        tlob:=appendClobField(eb_annotation,tlob);
                    end if;
                    end;
                end;
            else continue;
          end case;
      end loop;
end;
/



-- End of DDL Script for Procedure SOVDOC.IRINDEXER

