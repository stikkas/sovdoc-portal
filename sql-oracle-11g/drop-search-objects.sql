exec CTX_DDL.drop_preference('ds_pref');
exec CTX_DDL.drop_preference('wordlist_pref');
exec CTX_DDL.drop_preference('lexer_pref');
exec CTX_DDL.drop_stoplist('sovdoc_stoplist');
drop index IDX_SEARCH;
drop procedure IRINDEXER;
drop function GETCLOBFORFIELD;
drop index IDX_EDITION_NOTE;
drop index IDX_PUBLISHING_PLACE;
drop index IDX_PUBLISHER;
drop index IDX_EDITION_SIZE;
drop index IDX_ISBN;
drop index IDX_UDC;
drop index IDX_NOTES;
drop index IDX_ANNOTATION;

