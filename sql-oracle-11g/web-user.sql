﻿/*==============================================================*/
/* Table: WEB_USER                                              */
/*==============================================================*/
CREATE TABLE WEB_USER 
(
   "WEB_USER_ID"        NUMBER(10)           NOT NULL,
   "USER_ID"            NUMBER(10),
   "LAST_NAME"          VARCHAR2(150)        NOT NULL,
   "FIRST_NAME"         VARCHAR2(150)        NOT NULL,
   "MIDDLE_NAME"        VARCHAR2(150),
   "BIRTH_DATE"         DATE                 NOT NULL,
   "CITIZENSHIP_ID"     NUMBER(10),
   "OCCUPATION_INFO"    VARCHAR2(350),
   "EDUCATION_LEVEL_ID" NUMBER(10),
   "RANK_ID"            NUMBER(10),
   "STATUS"             NUMBER(10)			 NOT NULL,
   "PHONE"              VARCHAR2(20),
   "EMAIL"              VARCHAR2(200)         NOT NULL,
   "CONFIRMATION_UUID"  VARCHAR(4000),
   CONSTRAINT PK_WEB_USER PRIMARY KEY ("WEB_USER_ID")
);

COMMENT ON TABLE WEB_USER IS
'Пользователь портала';

COMMENT ON COLUMN WEB_USER."WEB_USER_ID" IS
'ID пользователя портала';

COMMENT ON COLUMN WEB_USER."USER_ID" IS
'ID пользователя';

COMMENT ON COLUMN WEB_USER."LAST_NAME" IS
'Фамилия';

COMMENT ON COLUMN WEB_USER."FIRST_NAME" IS
'Имя';

COMMENT ON COLUMN WEB_USER."MIDDLE_NAME" IS
'Отчество';

COMMENT ON COLUMN WEB_USER."BIRTH_DATE" IS
'Дата рождения';

COMMENT ON COLUMN WEB_USER."CITIZENSHIP_ID" IS
'Гражданство';

COMMENT ON COLUMN WEB_USER."OCCUPATION_INFO" IS
'Место работы (учёбы) и должность';

COMMENT ON COLUMN WEB_USER."EDUCATION_LEVEL_ID" IS
'Образование (степень)';

COMMENT ON COLUMN WEB_USER."RANK_ID" IS
'Звание';

COMMENT ON COLUMN WEB_USER."STATUS" IS
'Статус учетной записи';

COMMENT ON COLUMN WEB_USER."PHONE" IS
'Телефон';

COMMENT ON COLUMN WEB_USER."EMAIL" IS
'Адрес электронной почты';

COMMENT ON COLUMN WEB_USER."CONFIRMATION_UUID" IS
'Идентификатор подтверждения';

ALTER TABLE WEB_USER
   ADD CONSTRAINT FK_WEB_USER_FK_WEB_US_DESCRIPT FOREIGN KEY ("STATUS")
      REFERENCES DESCRIPTOR_VALUE ("DESCRIPTOR_VALUE_ID");

ALTER TABLE WEB_USER
   ADD CONSTRAINT "FK_ADMUSR_WEBUSR" FOREIGN KEY ("USER_ID")
      REFERENCES ADM_USER ("USER_ID");

ALTER TABLE WEB_USER
   ADD CONSTRAINT "FK_CIT_WEBUSR" FOREIGN KEY ("CITIZENSHIP_ID")
      REFERENCES DESCRIPTOR_VALUE ("DESCRIPTOR_VALUE_ID");

ALTER TABLE WEB_USER
   ADD CONSTRAINT "FK_EDU_WEBUSR" FOREIGN KEY ("EDUCATION_LEVEL_ID")
      REFERENCES DESCRIPTOR_VALUE ("DESCRIPTOR_VALUE_ID");

ALTER TABLE WEB_USER
   ADD CONSTRAINT "FK_RNK_WEBUSR" FOREIGN KEY ("RANK_ID")
      REFERENCES DESCRIPTOR_VALUE ("DESCRIPTOR_VALUE_ID");
