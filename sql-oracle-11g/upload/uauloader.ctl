load data
  CHARACTERSET UTF8
  infile 'D:\workspace\sovdoc-portal\sql-oracle-11g\upload\testunits.csv'
  Append
  into table UNIV_DATA_UNIT
  fields terminated by ";" optionally enclosed by '"'
  TRAILING NULLCOLS
(RES_OWNER_ID,UNIT_TYPE_ID, UNIT_NAME,NUMBER_TEXT,ARCH_NUMBER_CODE,CONTENTS,IS_ACCESS_LIMITED,ADD_USER_ID,MOD_USER_ID,INSERT_DATE "SYSDATE",LAST_UPDATE_DATE "SYSDATE")