--drop DIRECTORY SOVDOC_SEARCH_FILES_DIR;
create or replace DIRECTORY SOVDOC_SEARCH_FILES_DIR as 'C:\\tomcat/sovdoc-search-test/';
/
SET SERVEROUTPUT ON
create or replace function getClobForStoredFile(image_path_id in NUMBER) 
return CLOB as
fn VARCHAR2(4000);
clobStartTag CLOB;
clobEndTag CLOB;
clobFileContent CLOB;
bfileTarget BFILE;
numberWarning NUMBER;
numberCtxlang NUMBER;
numberOne NUMBER;
i NUMBER;
begin
select FILE_NAME into fn from univ_image_path
where UNIV_IMAGE_PATH_ID=TO_NUMBER(image_path_id);
bfileTarget:=BFILENAME('SOVDOC_SEARCH_FILES_DIR', fn);
numberOne:=1;
numberCtxlang:=dbms_lob.default_lang_ctx;
clobStartTag:=TO_CLOB('@field@');
clobEndTag:=TO_CLOB('@/field@');
dbms_lob.createtemporary(clobFileContent, TRUE); 
DBMS_LOB.OPEN(bfileTarget);
DBMS_LOB.LOADCLOBFROMFILE(clobFileContent,bfileTarget,
DBMS_LOB.LOBMAXSIZE,numberOne,numberOne,dbms_lob.default_csid,numberCtxlang,numberWarning);
DBMS_LOB.CLOSE(bfileTarget);
dbms_lob.WRITEAPPEND(clobStartTag,length(clobFileContent),clobFileContent);
dbms_lob.WRITEAPPEND(clobStartTag,length(clobEndTag),clobEndTag);
DBMS_LOB.FREETEMPORARY(clobFileContent);
return clobStartTag;
end;
/
select getClobForStoredFile(4) tt from dual;
