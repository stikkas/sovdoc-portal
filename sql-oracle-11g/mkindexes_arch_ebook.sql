create index IDX_EDITION_NOTE on ARCH_EBOOK(EDITION_NOTE) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_PUBLISHING_PLACE on ARCH_EBOOK(PUBLISHING_PLACE) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_PUBLISHER on ARCH_EBOOK(PUBLISHER) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_EDITION_SIZE on ARCH_EBOOK(EDITION_SIZE) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_ISBN on ARCH_EBOOK(ISBN) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_UDC on ARCH_EBOOK(UDC) indextype is CTXSYS.CONTEXT 
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_NOTES on ARCH_EBOOK(NOTES) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

create index IDX_ANNOTATION on ARCH_EBOOK(ANNOTATION) indextype is CTXSYS.CONTEXT
parameters ('DATASTORE CTXSYS.DIRECT_DATASTORE sync (on commit)');

