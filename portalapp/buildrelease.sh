#!/bin/sh -e

dir=`pwd`/..
webappDir=$dir/src/main/webapp
sencha=$HOME/Sencha/Cmd/5.1.3.61/sencha

rm -rf build/* $webappDir/app.js $webappDir/css/* $webappDir/images/* $webappDir/resources/* $webappDir/sovdoc-all.scss $webappDir/browsers.html
$sencha app refresh
$sencha app build -c production
mkdir -p build/production/sovdoc/css
cp -R css/* build/production/sovdoc/css
mkdir -p build/production/sovdoc/images
cp -R images/* build/production/sovdoc/images
cd build/production/sovdoc
tar -c --exclude index.html . | tar -x -C $webappDir
cd -
cp browsers.html $webappDir
cd ..
rm -rf $webappDir/.sass-cache
rm -f  $webappDir/config.rb
mvn clean package


