// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides
Ext.application({
	name: 'app',
	extend: 'app.view.Application',
	requires: ['Ext.util.HashMap', 'Ext.util.History', 'app.view.Viewport',
		'Ext.layout.container.Absolute', 'app.js.CFooter'],
	autoCreateViewport: false,
	launch: function () {
		if (Ext.isIE8m || Ext.isOpera && Ext.operaVersion < 12) {
			window.location.href = 'browsers.html';
			return;
		}
		Ext.util.History.init();
		Ext.History.on('change', function (token) {
			if ((token == 'undefined')) {
				return;
			}
			var titleKey = token;
			if (titleKey.indexOf('&') !== -1) {
				titleKey = titleKey.split('&')[0];
			}
			var titleText = vp.titlesMap.get(titleKey);
			if (!titleText) {
				var msg = 'titleText for token: ' + titleKey + ' NOT FOUND';
			} else {
				document.title = titleText;
			}
			var sum = 0;
			sum += token.indexOf('advsearch');
			sum += token.indexOf('elib');
			if (sum == -2) {
				if (token != window.vp.currentChapter) {
					vp.navigate(token);
				}
				return;
			}
			var pageFromHistoryMap = vp.histMap.get(token);
			if (!pageFromHistoryMap) {
				window.vp.navigate('#' + token);
			} else {
				if (pageFromHistoryMap != vp.portal) {
					vp.removeAll(false);
					if (token == 'advsearch') {
						pageFromHistoryMap.add(vp.headerSearch);
					} else {
						pageFromHistoryMap.add(vp.header);
					}
					vp.add(pageFromHistoryMap);
					vp.portal = pageFromHistoryMap;
					vp.currentChapter = token;
					pageFromHistoryMap.panHead.menu.markSElectedChapterImmidetly(titleKey);
				}
			}
			if (vp.items.length == 0) {
				Ext.Msg.alert('Error', 'vp.items.length==0');
			}
		});
		Ext.Ajax.timeout = 1000 * 60 * 3;
		var vp = Ext.create('app.view.Viewport');
		window.vp = vp;
		var token = '';
		var callNavigate = function () {
			var initChapter = window.location.hash;
			if (!initChapter) {
				initChapter = window.location + '#main';
			}
			vp.navigate(initChapter);
		};
		vp.loadStaticContent(function ()
		{
			var footer = Ext.create('app.js.CFooter');
			footer.loadData(callNavigate);
		});
	}
});
