set webappDir="..\src\main\webapp"

del /Q build\
del /Q %webappDir%\app.js
del /Q %webappDir%\css\
del /Q %webappDir%\images\
del /Q %webappDir%\resources\
del /Q %webappDir%\sovdoc-all.scss
del /Q %webappDir%\browsers.html
sencha app refresh
sencha app build -c production
mkdir build\production\sovdoc\css
xcopy /V /E /Y css build\production\sovdoc\css
mkdir build\production\sovdoc\images
xcopy /V /E /Y images build\production\sovdoc\images
xcopy /V /E /Y  build\production\sovdoc %webappDir% /EXCLUDE:excl.txt
xcopy /V /E /Y browsers.html %webappDir%
cd ..
del /Q src\main\webapp\.sass-cache\
del /Q src\main\webapp\config.rb
mvn clean package
