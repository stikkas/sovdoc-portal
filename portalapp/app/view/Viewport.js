Ext.define('app.view.Viewport', {
    id : 'sys-viewport',
//    extend : 'Ext.container.Viewport',
    extend: 'Ext.container.Container',
    requires: ['app.js.CHead','app.js.main.CHeadSearch','app.js.pages.Main','app.js.pages.News','app.js.CNewsChapter','app.js.pages.Search','Ext.window.MessageBox',
               'app.js.pages.TematicChilds','app.js.pages.AdvSearch','app.js.pages.Elib','app.js.pages.htmlpage','app.js.pages.Exhibitions','app.js.pages.SearchResults'],
    histMap : Ext.create('Ext.util.HashMap'),
    titlesMap : Ext.create('Ext.util.HashMap'),
    layout:'vbox',
    renderTo:'main_container',
    loadMask : null,
    autoRender : true,
    autoScroll : false,
    autoDestroy : false,
    currentChapter : null,
    header : null,
    headerSearch : null,
    portal : 'init portal string',
    l10nDatav : null,
    loginedUserId : null,
    loginedUserName : null,
    failHandlerCallBack:function(rec,op,succ){
    	if (!succ){
    		Ext.Msg.alert(op.getError().statusText,'Код ошибки: '+op.getError().status);
    	}
    },
    failHandler:function(resp,op){
    	if (resp.status){
    		Ext.Msg.alert('Ошибка',resp.status+':'+resp.statusText);
    	}else{
    		Ext.Msg.alert(op.statusText+':'+op.status,op.responseText);
    	}
    },
    getConfigForSearchStore : function(fieldsArray, pageSiize, params) {
    	var me = this;
	    var cfg = {
	        extend : 'Ext.data.Store',
	        pageSize : pageSiize,
	        fields : fieldsArray,
	        autoLoad : false,
	        proxy : {
	            type : 'ajax',
	            url : 'SearchHandler',
	            reader : {
	                type : 'json',
	                root : 'items',
	                totalProperty : 'itemsCount'
	            },
	            extraParams : {},
	            totalProperty : 'itemsCount',
	            listeners : {
		            'exception' : me.failHandler
	            }
	        },
	        listeners : {
	            beforeload : function(store, operation, eOpts) {
		            window.vp.loadMask.show();
	            },
	            load : function(t, records, successfull, eOpts) {
		            window.vp.loadMask.hide();
	            }
	        }
	    };
	    if (params) {
		    cfg.proxy.extraParams = params;
	    }
	    return cfg;
    },
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.initTitlesMap();
//	    function getScrollWidth() {
//		    // http://learn.javascript.ru/task/uznat-shirinu-polosy-prokrutki
//		    var div = document.createElement('div');
//		    div.style.overflowY = 'scroll';
//		    div.style.width = '50px';
//		    div.style.height = '50px';
//		    div.style.visibility = 'hidden';
//		    document.body.appendChild(div);
//		    var scrollWidth = div.offsetWidth - div.clientWidth;
//		    document.body.removeChild(div);
//		    return scrollWidth;
//	    }
//	    ;
	    var fieldsArrSearch = [ 'dataUnitId', 'dataUnitName', 'archiveCode', 'mathesTextBlock' ];
	    var fieldsArrElib = [ 'dataUnitId', 'bookName', 'tom', 'author', 'publisher', 'publYear', 'coverurl' ];
	    var fieldsRoots = [ 'dataUnitId', 'unitName', 'archiveCode', 'goToCard', 'goToChilds', 'mathesTextBlock' ];
        var fieldsChildren = [ 'dataUnitId', 'unitName', 'goToCard', 'goToChilds', 'mathesTextBlock' ];
        var searchStoreCfg = me.getConfigForSearchStore(fieldsArrSearch, 50);
	    var elibStoreParams = {
	        action : 'elib',
	        params : Ext.encode({
	            elibQuery : 1,
	            elibGetAll : 2
	        })
	    };
	    var elibStoreCfg = me.getConfigForSearchStore(fieldsArrElib, 2, elibStoreParams);
          var rootsCfg = me.getConfigForSearchStore(fieldsRoots, 50);
	    var childrenCfg = me.getConfigForSearchStore(fieldsChildren, 50);
	    Ext.regStore('js.search.SearchStore', searchStoreCfg);
	    Ext.regStore('js.elib.ElibStore', elibStoreCfg);
	    Ext.regStore('js.topic.Children', childrenCfg);
	    Ext.regStore('js.topic.Roots', rootsCfg);
	    me.loadMask = {
	    		show:function(){
	    			//stop warnings in browser's console
//	    			console.log('mask body');
	    			Ext.getBody().mask('Пожалуйста, подождите..');
	    		},
	    		hide:function(){
//	    			console.log('unmask body');
	    			Ext.getBody().unmask();
	    		}
	    		
	    }
    },
    loadL10nData : function(callBack) {
	    var me = this;
	    Ext.Ajax.request({
	        async : false,
	        url : 'PortalDataProvider',
	        params : {
		        action : 'getLocaleConsts'
	        },
	        success : function(response, opts) {
		        me.l10nDatav = Ext.decode(response.responseText);
	        },
	        failure : function(response, opts) {
		        if (me.loadMask) {
			        loadMask.hide();
		        }
		        window.vp.failHandler
	        },
	        callback : callBack
	    });
    },
    /**
	 * Последовательная загрузка шапки, подвала и констант-локализации
	 * 
	 * @param {}
	 *            callBack
	 */
    loadStaticContent : function(callBack) {
	    var me = this;
	    // 2. После загрузки подвала, грузим константы локализации
	    var loadConsts = function() {
		    // 4. О чудо. Дергаем кaллБэк, переданный параметром главной
		    // фукнции.
		    // К моменту её вызова - последовательно будут загружены:
		    // подвал, шапка, константы локализации
		    me.loadL10nData(callBack);
	    };

	    me.header = Ext.create('app.js.CHead', {
		    html : 'Документы советской эпохи'
	    });
	    // 1. Грузим содержимое шапки. после загрузки, попадаем в 2
	    me.header.load(loadConsts);
    },
    initTitlesMap : function() {
	    var me = this;
	    var siteName = 'Документы советской эпохи: ';
	    me.titlesMap.add('main', siteName + 'Главная');
	    me.titlesMap.add('userhome', siteName + 'Личный кабинет');
	    me.titlesMap.add('feedback', siteName + 'Обратная связь');
	    me.titlesMap.add('nsa', siteName + 'Научно-справочный аппарат');
	    me.titlesMap.add('!changeaccountinfo', siteName + 'Редактирование учетной записи');
	    me.titlesMap.add('!chpass', siteName + 'Редактирование пароля');
	    me.titlesMap.add('search', siteName + 'Документальные комплексы');
	    me.titlesMap.add('advsearch', siteName + 'Расширенный поиск');
	    me.titlesMap.add('!tematicsection', siteName + 'Тематический раздел');
	    me.titlesMap.add('tematicchilds', siteName + 'Содержимое тематического раздела');
	    me.titlesMap.add('userreg', siteName + 'Регистрация пользователя');
	    me.titlesMap.add('news', siteName + 'Новости');
	    me.titlesMap.add('shownews', siteName + 'Новость');
	    me.titlesMap.add('exhblist', siteName + 'Выставки');
	    me.titlesMap.add('showexbpage', siteName + 'Выставка');
	    me.titlesMap.add('members', siteName + 'Участники проекта');
	    me.titlesMap.add('elib', siteName + 'Электронная библиотека');
	    me.titlesMap.add('searchresults', siteName + 'Результаты поиска');
	    me.titlesMap.add('showunit', siteName + 'Просмотр единицы хранения');
	    me.titlesMap.add('aboutproject', siteName + 'О проекте');
	    me.titlesMap.add('aboutsite', siteName + 'О сайте');
	    me.titlesMap.add('usingconditions', siteName + 'Условия использования');
	    me.titlesMap.add('showebookunit', siteName + 'Книга');
	    me.titlesMap.add('help', siteName + 'Помощь');
    },
    navigate : function(chapterr) {
	    var me = this;
	    if (typeof chapterr == 'undefined' || chapterr == '#undefined') {
		    throw 'chapter for navigation can not be undefined';
		    return;
	    }
	    var chapter = chapterr.split('&')[0];
	    var param = chapterr.split('&')[1];
	    var param2 = null;
	    var param3 = null;
	    if (chapter.indexOf('#') != -1) {
		    // если есть # - убираю её и все что левее
		    chapter = chapter.split('#')[1];
	    }
	    if (param == null) {
		    // для работы поиска
		    param = arguments[1];
		    param2 = arguments[2];
		    param3 = arguments[3];
	    }
	    // Если нужная страница уже есть в кеше - ничего делать не надо.
	    // обработчик хистори получит ивент при навигации, и обработает так же,
	    // как при обработке
	    // кнопки браузера "назад"
	    // var cachedSamePage = this.histMap.get(chapterr);
	    // if (!cachedSamePage){
	    // return false;
	    // }
	    // for any user: logined or not
	    if (!me.portal){
	    	Ext.Msg.alert('Error','me.portal is undefined!');
	    }
	    me.currentChapter = chapter;
	    switch (chapter) {
	    case 'feedback':
		    me.portal = Ext.create('app.js.feedback.FeedPanel');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'nsa':
		    me.portal = Ext.create('app.js.pages.htmlpage', {
		        loadAction : 'getNsaPage',
		        clazz : 'nsa'
		    });
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'main':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.Main');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'news':
		    if (!(me.portal instanceof app.js.pages.News)) {
			    // если и так установлен нужный компонент, зачем
			    // инициализировать такой же заново?
			    me.portal = Ext.create('app.js.pages.News');
			    me.portal.initChildsAfterLocaleDataLoaded();
		    }
		    me.portal.loadNews(param);
		    break;
	    case 'search':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.Search');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'advsearch':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.AdvSearch');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'searchresults':
		    me.portal = Ext.create('app.js.pages.SearchResults');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'showunit':
		    me.portal = Ext.create('app.js.pages.UnitView');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    var unitViewParams = param.split(';');
		    var tabMode = "info";
		    var paramUnitId = null;
		    if (unitViewParams.length == 2) {
			    paramUnitId = unitViewParams[0];
			    tabMode = unitViewParams[1].split('=')[1];
		    } else {
			    paramUnitId = param;
		    }
		    var paramArr = paramUnitId.split('=');
		    me.portal.loadUnitInfo(paramArr[1], tabMode);
		    break;
	    case 'showebookunit':
		    me.portal = Ext.create('app.js.pages.EbookUnitView');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    var paramArr = param.split('=');
		    me.portal.loadUnitInfo(paramArr[1]);
		    break;
	    case 'exhblist':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadList();
		    break;
	    case 'showexbpage':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSameExhibition(param);
		    break;
	    case 'members':
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSysPage('getMembers');
		    break;
	    case 'userreg':
		    me.portal = Ext.create('app.js.pages.UserReg');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'shownews':
		    me.portal = Ext.create('app.js.pages.News');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadOneNews(param);
		    break;
	    case 'aboutsite':
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSysPage('aboutSitePage');
		    break;
	    case 'aboutproject':
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSysPage('aboutProjectPage');
		    break;
	    case 'help':
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSysPage('help');
		    break;
	    case 'regok':
		    me.portal = Ext.create('app.js.pages.htmlpage', {
		        loadAction : 'getRegOkPage',
		        clazz : 'reg_let'
		    });
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'elib':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.Elib');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case '!tematicsection':
		    me.portal = Ext.create('app.js.pages.TematicSection');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSection(param);
		    break;
	    case 'tematicchilds':
		    this.loadMask.show();
		    me.portal = Ext.create('app.js.pages.TematicChilds');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadChilds(param);
		    break;
	    case 'regconfirmok':
		    me.portal = Ext.create('app.js.pages.htmlpage', {
		        loadAction : 'getActivationOkPage',
		        clazz : 'reg_let'
		    });
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'regconfirmfail':
		    me.portal = Ext.create('app.js.pages.htmlpage', {
			    loadAction : 'getActivationFail'
		    });
		    me.portal.initChildsAfterLocaleDataLoaded();
		    break;
	    case 'usingconditions':
		    me.portal = Ext.create('app.js.pages.Exhibitions');
		    me.portal.initChildsAfterLocaleDataLoaded();
		    me.portal.loadSysPage('usingConditions');
		    break;
	    case 'userhome':
		    if (!this.isUserLogined()) {
			    this.loadMask.show();
			    me.portal = Ext.create('app.js.users.UserLoginChapter');
			    me.portal.initChildsAfterLocaleDataLoaded();
		    } else {
			    me.portal = Ext.create('app.js.users.UserHomeChapter');
			    me.portal.initChildsAfterLocaleDataLoaded();
		    }
		    break;
	    case '!changeaccountinfo':
		    if (!this.isUserLogined()) {
			    me.portal = Ext.create('app.js.users.UserLoginChapter')
			    me.portal.initChildsAfterLocaleDataLoaded();
		    } else {
			    me.portal.currentCenterCont.goEditAccountInfo();
		    }
		    break;
	    case '!feedback':
		    if (!this.isUserLogined()) {
			    me.portal = Ext.create('app.js.users.UserLoginChapter')
			    me.portal.initChildsAfterLocaleDataLoaded();
		    } else {
			    me.portal.currentCenterCont.goFeedBack();
		    }
		    break;
	    case '!chpass':
		    if (!this.isUserLogined()) {
			    me.portal = Ext.create('app.js.users.UserHomeChapter');
			    me.portal.initChildsAfterLocaleDataLoaded();
		    } else {
			    me.portal.currentCenterCont.goChangePasswordInfo();
		    }
		    break;
	    default:
		    return;
	    }
	    var vp = this;
	    var pt = me.portal;
	    me.removeAll(false);
	    me.add(pt);
	    me.header.menu.markSElectedChapterImmidetly(chapter);
	    me.pageChanged(pt, chapterr);
	    return true;
    },
    pageChanged : function(portal, chapter) {
	    if (Ext.History.getToken() == chapter) {
		    return;
	    }
	    var name = chapter.split('#')[1];
	    Ext.util.History.add(name, true);
	    var value = portal;
	    // console.debug('put ' + name + ' into histMap');
	    var sum = chapter.indexOf('advsearch');
	    sum += chapter.indexOf('elib');
	    if (sum == -2) {
		    // не найдены искомые паттерны во всех трех случаях, в кеш ничего не
		    // помещаем
		    return;
	    } else {
		    // console.debug('put page to history map: '+name);
		    this.histMap.add(name, value);
	    }
    },
//    changeLocale : function(newLocale) {
//	    Ext.Ajax.request({
//	        async : false,
//	        url : 'PortalLocalizationDataProvider',
//	        params : {
//	            action : 'changeLocale',
//	            lang : newLocale
//	        },
//	        success : function(response, opts) {
//	        },
//	        failure : window.vp.failHandler,
//	        callback : function(option, success, response) {
//		        // это выполнится только после получения
//		        // ответа.
//		        window.location.reload();
//	        }
//	    });
//    },
    isUserLogined : function() {
	    return this.loginedUserId != null;
    },
    add : function() {
	    var me = this;
	    if (me.items.length >= 2) {
		    Ext.Msg.alert('Ошибка', 'Viewport already contains content, me.items.lenght=' + me.items.length);
	    }
	    me.callParent(arguments);
    },
    showPopup : function() {
	    var popupContent = Ext.create('app.js.CExhibitionsChapter');
	    popupContent.loadSysPageWithoutFoot(popupContent, 'SEARCH_HELP',function(){
	    	//показываю окно только после загрузки контента
		    var wnd = Ext.create('Ext.window.Window', {
			    items : [ popupContent ]
		    });
		    wnd.setWidth(Ext.getBody().getWidth()-300);
		    wnd.show();
	    });

	    
	    
	    return false;
    },
    setTargetAttr: function(container, selector)
    {
        var contentLinks = container.select(selector);
        contentLinks.each(function(el)
        {
            el.set(
            {
                target: '_blank'
            });
        });
    }
});