Ext.define('app.js.CNewsChapter', {
    extend : 'Ext.container.Container',
    width : '90%',
    cls : 'news_chap',
    autoScroll : false,
    height : 'auto',
    loadOneNews : function(itself, newsId) {
	    var i = itself;
	    var me = i.ownerCt;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
	            action : 'getOneNews',
	            id : newsId
	        },
	        success : function(response, opts) {
	        	window.vp.loadMask.hide();
		        i.update(response.responseText, false, i.updLayout);                        
		        window.vp.doLayout();
	        },
	        failure : window.vp.failHandler
	    });
    },
    updLayout : function() {
            window.vp.setTargetAttr(this, 'blockquote a:not([target])');
	    window.vp.doLayout();
    },
    loadNews : function(itself, pageid) {
	    var i = itself;
	    var me = i.ownerCt;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
	            action : 'getNews',
	            p : pageid
	        },
	        success : function(response, opts) {
	        	window.vp.loadMask.hide();
		        i.update(response.responseText, false, i.updLayout);
                        i.ownerCt.updateLayout();
	        },
	        failure : window.vp.failHandler
	    });
    }
});