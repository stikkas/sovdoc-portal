Ext.define('app.js.pages.Exhibitions', {
    extend : 'app.js.page',
    cls:'exh-cl',
    require : [ 'app.js.CExhibitionsChapter' ],
    initComponent : function() {
	    var m = this;
	    m.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);

    },
    loadList : function() {
	    var me = this;
	    me.currentCenterCont = Ext.create('app.js.CExhibitionsChapter', {
	        x : 0,
	        y : 350,
	        layout:{
	        	type:'hbox'
	        }
	    });
	    me.add(me.currentCenterCont);
	    me.currentCenterCont.loadExhibitionsList(me.currentCenterCont);
    },
    loadSameExhibition : function(param) {
	    var me = this;
	    var idParm = param.split('=')[1];
	    me.currentCenterCont = Ext.create('app.js.CExhibitionsChapter', {
	        x : 0,
	        y : 330
	    });
	    me.add(me.currentCenterCont);
	    me.currentCenterCont.loadExhibition(me.currentCenterCont, idParm);
    },
    loadSysPage : function(pageCode) {
	    var me = this;
	    me.currentCenterCont = Ext.create('app.js.CExhibitionsChapter', {
	        x : 0,
	        y : 330
	    });
	    me.add(me.currentCenterCont);
	    me.currentCenterCont.loadSystemPage(pageCode);
    }
});