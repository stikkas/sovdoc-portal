Ext.define('app.js.pages.Elib', {
    extend : 'app.js.page',
    cls:'elib-cl',
    requires:['app.js.elib.CEBookListCard','app.js.elib.CSearchFieldAuthor','app.js.elib.CSearchFieldEbookSeries','app.js.elib.CSearchFieldPublYear'],
    initComponent : function() {
	    var me = this;
	    this.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    // если есть второй параметр 'noclean', значит переход
	    // выполняется из результатов поиска
	    // кнопка "Вернуться к параметрам поиска"
	    me.panElib = Ext.create('app.js.search.CAdvSearchChapter', {
	        mode : 'elib',
                cls:'e-lib search_style',
	        l10nData : window.vp.l10nDatav,
	        x : 0,
	        y : 345
	    });
	    me.currentCenterCont = me.panElib;
	    me.add(me.currentCenterCont);
    }
});