Ext.define('app.js.pages.EbookUnitView', {
    extend : 'app.js.page',
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    loadUnitInfo : function(param) {
	    var me = this;
	    me.currentCenterCont.loadInfo(param);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.currentCenterCont = Ext.create('app.js.elib.PDataUnitViewModeUrl', {
	        l10nConsts : window.vp.l10nDatav,
	        x : 0,
	        y : 340
	    });
	    me.add(me.currentCenterCont);
    }
});