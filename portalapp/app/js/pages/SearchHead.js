Ext.define('app.js.pages.SearchHead', {
    extend : 'app.js.page',
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
	    if (!window.vp.headerSearch) {
		    window.vp.headerSearch = Ext.create('app.js.main.CHeadSearch', {
		        html : window.vp.l10nDatav.sys_title,
		        panPortal : me,
		        l10nData : window.vp.l10nDatav,
		        x : 0,
		        y : 0
		    });
		    window.vp.headerSearch.load();
	    }
	    me.add(window.vp.headerSearch);
    }
});