Ext.define('app.js.pages.Main', {
    extend : 'app.js.page',
    cls:'main-cl',
    requires: ['app.js.main.CNewsBlock','app.js.main.CMainContent','app.js.main.CMainExhb'],
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    this.callParent(arguments);
	    var me = this;
            var updLayout = function()
            {
                me.doLayout();
            };
            
            var t1 = Ext.create('app.js.main.CNewsBlock', {
		    l10nData : window.vp.l10nDatav
	    });
	    t1.loadNews(t1, updLayout);
	    //me.add(t1);
	    var t2 = Ext.create('app.js.main.CMainExhb', {
		    l10nData : window.vp.l10nDatav
	    });
	    t2.loadExhibitions(t2, updLayout);
	    //me.add(t2);
	    var banners = Ext.create('app.js.search.CMainLinksArea', {
	        loadAction : 'getBanners',
	        cls : 'banners_cls',
                //x:25,
                //y:0,
                height:100
	    });
	    banners.loadHtml(updLayout);
	    //me.add(banners);
            me.add(Ext.create('Ext.container.Container',
            {
               layout: 'vbox',
               items: [t1, t2, banners],
               x: 25,
               y: 350
            }));
	    
	    var t3 = Ext.create('app.js.main.CMainContent', {
		    l10nData : window.vp.l10nDatav
	    });
	    me.add(t3);            
            
	    t3.loadContent(t3);
    }
});