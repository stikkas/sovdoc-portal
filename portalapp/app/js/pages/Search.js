Ext.define('app.js.pages.Search', {
    extend : 'app.js.pages.SearchHead',
    requires:['app.js.topics.CTematicSection','app.js.CExhibitionsChapter','app.js.pages.TematicSection'],
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
            var updLayout = function() {
		    window.vp.doLayout();
	    };
	    me.currentCenterCont = Ext.create('app.js.topics.CTematicSection', {
	        x : 0,
	        y : 400
	    });
	    var popupContent = Ext.create('app.js.CExhibitionsChapter');
	    popupContent.loadSysPageWithoutFoot(popupContent, 'portal_doccomplex', updLayout);
	    me.staticPanel = Ext.create('Ext.container.Container', {
	        width : '80%',
	        items : [ popupContent ],
                cls:'tematic_content'
	    });
	    me.currentCenterCont.add(me.staticPanel);	    
	    me.add(me.currentCenterCont);
	    me.currentCenterCont.load(me.currentCenterCont, updLayout);
    }
});