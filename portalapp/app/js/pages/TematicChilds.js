Ext.define('app.js.pages.TematicChilds', {
    extend : 'app.js.page',
    cls:'tem-child-cl',
    requires: ['app.js.topics.ChildsGrid','Ext.form.Label'],
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    if (!window.vp.headerSearch) {
		    window.vp.headerSearch = Ext.create('app.js.main.CHeadSearch', {
		        html : window.vp.l10nDatav.sys_title,
		        panPortal : me,
		        l10nData : window.vp.l10nDatav,
		        x : 0,
		        y : 0
		    });
		    window.vp.headerSearch.load();
	    }
	    me.add(window.vp.headerSearch);
	    me.currentCenterCont = Ext.create('app.js.topics.ChildsGrid', {
	        x : 0,
	        y : 427
	    });
	    me.add(me.currentCenterCont);
    },
    loadChilds : function(param) {
	    var me = this;
	    if (param) {
		    var paramsSplitted = param.split(';');
		    var param1 = paramsSplitted[0];
		    var param1Split = param1.split('=');
		    var parm1Key = param1Split[0];
		    var parm1Val = param1Split[1];
		    me.currentCenterCont.loadChilds(parm1Val);
	    }
    }
});