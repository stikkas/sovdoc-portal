/**
 * Страница отображения результатов поиска
 */
Ext.define('app.js.pages.SearchResults', {
    extend : 'app.js.page',
    layout:'vbox',
    initChildsAfterLocaleDataLoaded:function(){
	    var em = this;
	    em.add(window.vp.header);
	    em.gridSearchResults = Ext.create('app.js.search.SearchResultsGrid', {
		    l10nData : window.vp.l10nDatav,
		    x:0,
		    y:320
	    });
	    em.currentCenterCont = em.gridSearchResults;
	    em.add(em.gridSearchResults);
    }
});