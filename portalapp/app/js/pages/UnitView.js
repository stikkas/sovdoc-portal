Ext.define('app.js.pages.UnitView', {
    extend : 'app.js.page',
    cls:'card-cl',
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    loadUnitInfo : function(param, tabMode) {
	    var me = this;
	    me.currentCenterCont.loadInfo(param, tabMode == 'panno');
	    if (tabMode == "img") {
		    if (me.currentCenterCont.goToImagesHandler) {
			    me.currentCenterCont.goToImagesHandler();
		    }
	    }
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.currentCenterCont = Ext.create('app.js.elib.PDataUnitViewMode', {
	        l10nConsts : window.vp.l10nDatav,
	        x : 0,
	        y : 340
	    });
	    me.add(me.currentCenterCont);
    }
});