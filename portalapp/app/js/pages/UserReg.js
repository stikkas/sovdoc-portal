Ext.define('app.js.pages.UserReg', {
    extend : 'app.js.page',
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.currentCenterCont = Ext.create('app.js.users.CUserCreationForm', {
	        l10nData : window.vp.l10nDatav,
	        x : 0,
	        y : 370
	    });
	    me.add(me.currentCenterCont);
    }
});