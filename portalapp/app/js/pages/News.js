Ext.define('app.js.pages.News', {
    extend : 'app.js.page',
    cls:'news-cl',
    // id:'js-pages-News',
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    goToChapter : function() {
	    throw 'Эта функция не должна вызываться у экземпляров данного класса!';
    },
    loadOneNews : function(params) {
	    window.vp.loadMask.show();
	    var me = this;
	    var idParm = params.split('=')[1];
	    me.currentCenterCont.loadOneNews(me.currentCenterCont, idParm);
    },
    loadNews : function(params) {
	    window.vp.loadMask.show();
	    var me = this;
	    if (params) {
		    // вместо p=X, параметром нужно передавать просто номер страницы баг
		    params = params.split('=')[1];
	    }
	    me.currentCenterCont.loadNews(me.currentCenterCont, params);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.currentCenterCont = Ext.create('app.js.CNewsChapter', {
	        l10nData : window.vp.l10nDatav,
	        x : 0,
	        y : 340
	    });
	    me.add(me.currentCenterCont);
    }
});