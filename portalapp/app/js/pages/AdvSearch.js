Ext.define('app.js.pages.AdvSearch', {
    extend : 'app.js.page', 
    requires:['app.js.search.CAdvSearchChapter','app.js.search.PAdvSearch','Ext.form.field.ComboBox','app.js.search.CSearchField',
              'app.js.search.CSearchFieldLanguage','app.js.search.CSearchFieldDateInterval','app.js.search.PortalDateField',
              'app.js.search.ArchiveCode','Ext.data.TreeStore','app.js.search.TreeCombo','Ext.tree.Panel','Ext.form.FieldSet',
              'app.js.search.CSearchFieldElementType','app.js.search.CSearchFieldDescriptorsGrid','app.js.search.PDescTable',
              'app.js.search.WChoseDesc'],
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    if (!window.vp.headerSearch) {
		    window.vp.headerSearch = Ext.create('app.js.main.CHeadSearch', {
		        html : window.vp.l10nDatav.sys_title,
		        panPortal : me,
		        l10nData : window.vp.l10nDatav,
		        x : 0,
		        y : 0
		    });
		    window.vp.headerSearch.load();
	    }
	    me.add(window.vp.headerSearch);
	    me.currentCenterCont = Ext.create('app.js.search.CAdvSearchChapter', {
	        mode : 'advsearch',
	        l10nData : window.vp.l10nDatav,
	        x : 0,
	        y : 390
	    });
	    me.add(me.currentCenterCont);
    }
});