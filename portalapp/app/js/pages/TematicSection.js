Ext.define('app.js.pages.TematicSection', {
    extend : 'app.js.page',
    requires:['app.js.topics.RootsGrid','Ext.toolbar.Paging','app.js.topics.SectionHierarchy'],
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    if (!window.vp.headerSearch) {
		    window.vp.headerSearch = Ext.create('app.js.main.CHeadSearch', {
		        html : window.vp.l10nDatav.sys_title,
		        panPortal : me,
		        l10nData : window.vp.l10nDatav,
		        x : 0,
		        y : 0
		    });
		    window.vp.headerSearch.load();
	    }
	    me.add(window.vp.headerSearch);
	    me.currentCenterCont = Ext.create('app.js.topics.CTematicSection', {
	        x : 0,
	        y : 400
	    });
	    me.add(me.currentCenterCont);
    },
    loadSection : function(params) {
	    var me = this;
	    var loadGrid = function() {
		    if (params) {
			    var paramsSplitted = params.split('=');
			    var paramKey = paramsSplitted[0];
			    var paramValue = paramsSplitted[1];
			    if (paramKey == 'sectionId') 
                            {
                                // помечаем выбранный раздел
				me.currentCenterCont.markCelected(paramValue);
                                
                                //Старый код для отображения корневых элементов раздела портала
                                //без иерархии тематических разделов
				/*var rootsGrid = Ext.create('app.js.topics.RootsGrid', {
				    x : 200,
				    y : 411
				});
				rootsGrid.loadRoots(paramValue);				    
				rootsGrid.setTitle('Содержимое раздела');
				me.add(rootsGrid);*/
                                
                                //Новый код (отображение с иерархией)
                                var secHierarchy = Ext.create('app.js.topics.SectionHierarchy',
                                {
                                    x: 240,
                                    y: 411,
                                    rootPage: me
                                });
                                secHierarchy.loadData(paramValue);
                                me.add(secHierarchy);
			    }
		    }
	    };
	    me.currentCenterCont.load(me.currentCenterCont, loadGrid);
    }
});