Ext.define('app.js.pages.htmlpage', {
    extend : 'app.js.page',
    loadAction : '',
    cls : 'htmlpage-cls',
    initComponent : function() {
	    var me = this;
	    me.currentCenterCont = Ext.create('app.js.CExhibitionsChapter', {
	        cls : me.clazz,
	        y : 310,
	        x : 0,
	        styleHtml:true
	    });
	    Ext.applyIf(me, {
		    items : [ me.currentCenterCont ]
	    });
	    me.callParent(arguments);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.currentCenterCont.loadSystemPage(me.loadAction);
    }
});