Ext.define('app.js.CHead', {
    extend : 'Ext.container.Container',
    requires: ['app.js.main.MainMenu','app.js.search.CMainLinksArea'],
    layout : 'absolute',
    width:'100%',
    height : 350,
    maxHeight: 350,
    minHeight: 350,
    x : 0,
    y : 0,
    autoScroll : false,
    cls : 'title_',
    menu : null,
    romb : null,
    initPanels : function() {
	    var me = this;
	    me.menu = Ext.create('app.js.main.MainMenu', {
	        x : 0,
	        y : 255
	    });
	    me.romb = Ext.create('app.js.search.CMainLinksArea', {
	        x : 0,
	        y : 255,
	        cls : 'srch_all'
	    });
    },
    initComponent : function() {
	    var me = this;
	    me.initPanels();
	    Ext.applyIf(me, {
		    items : [ me.menu, me.romb ]
	    });
	    me.callParent(arguments);
    },
    load : function(callBack) {
	    /*
		 * Последовательная загрузка контента шапки: сначала разделы меню, потом
		 * область ссылок.
		 */
	    var me = this;
//	    var loadRomb = function() {
//		    me.romb.loadHtml(callBack);
//	    };
	    me.menu.loadChapters(me.menu,callBack);
    }
});