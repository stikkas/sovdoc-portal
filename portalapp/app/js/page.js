Ext.define('app.js.page', {
    extend : 'Ext.container.Container',
    width : '100%',
    minWidth:600,
    cls:'body_pages',
    renderTo: 'main_container',
    layout : 'absolute',
    autoDestroy : false,
    autoRender : true,
    autoShow : true,
    currentCenterCont : null,
    panHead : null,
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.panHead = window.vp.header;
	    me.add(window.vp.header);
    },
    //не понятно зачем нужны эти функции
    /*scro : null,
    restoreScroll : function() {
	    if (this.scro != null) {
		    this.getEl().scrollTo('top', this.scro.top);
	    }
    },
    saveScroll : function() {
	    if (this.getEl() != null) {
		    this.scro = this.getEl().getScroll();
	    } else {
		    throw 'this.getEl() is null. Container was not rendered';
	    }
    }*/
});