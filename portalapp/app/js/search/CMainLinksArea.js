Ext.define('app.js.search.CMainLinksArea', {
    extend : 'Ext.container.Container',
    width : 1140,
    height : 48,
    maxHeight: 68,
    styleHtmlContent : true,
    autoScroll : false,
    cls : 'srch',
    // loadAction : 'getMainLinksBlock',
    loadHtml : function(callBack) {
	    var me = this;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : this.loadAction
	        },
	        success : function(response, opts) {
		        me.update(response.responseText);
		        window.vp.loadMask.hide();
	        },
	        failure : window.vp.failHandler,
	        callback : callBack
	    });
    }
});
