/**
 * Компонент для выбора диапазона дат в качестве критерия расширенного поиска
 */
Ext.define('app.js.search.CSearchFieldDateInterval', {
    extend : 'Ext.container.Container',
    layout : {
        type : 'hbox',
        align : 'left'
    },
    fromField : null,
    toField : null,
    initComponent : function() {
	    // labelWidth у компонентов наименование и комбо - 150, значит делаю
	    // метку 120 и метку текстовую у компонента 30:
	    // компоненты должны выстраиваться в красивый столбец. Со стилями все
	    // ломается, надо из стилей убирать задание размеров компонентов
	    // в расширенном поиске
	    var me = this;
	    var delPanId = me.id;
	    var delBtn = Ext.create('Ext.button.Button', {
	        text : '',
	        cls : 'del_but',
	        handler : function() {
		        me.panel.remove(delPanId, true);
		        me.panel.combo.getStore().add({
		            "fieldId" : me.fieldId,
		            "fieldName" : me.fieldName,
		            "orderIndex" : me.orderIndex,
		            tp : me.tp
		        });
		        me.panel.combo.getStore().sort('orderIndex', 'ASC');
	        }
	    });
	    var label = Ext.create('Ext.form.Label');
	    label.setText('Даты');
	    label.setWidth(145);
	    me.fromField = Ext.create('app.js.search.PortalDateField', {
	        labelWidth : 18,
	        cls : 'mar_t-2 mar_r5',
	        width:120,
	        name : 'fromDate',
	        allowBlank : false,
                labelSeparator: ''
	    });
	    me.fromField.setFieldLabel('с');
	    me.toField = Ext.create('app.js.search.PortalDateField', {
	        labelWidth : 25,
	        cls : 'mar_t-2',
	        width:125,
	        name : 'toDate',
	        allowBlank : false,
            labelSeparator: ''
	    });
	    me.toField.setFieldLabel('по&nbsp;');
	    Ext.applyIf(me, {
		    items : [ label, me.fromField, me.toField, delBtn ]
	    });
	    me.callParent(arguments);
    },
    clear : function() {
	    var me = this;
	    me.fromField.setRawValue('');
	    me.toField.setRawValue('');
    },
    getName : function() {
	    return 'dates';
    },
    getValue : function() {
	    var me = this;
	    var descMap = new Ext.util.HashMap();
	    if (me.fromField.isValid()) {
		    descMap.add(me.fromField.getName(), me.fromField.getValue());
	    }
	    if (me.toField.isValid()) {
		    descMap.add(me.toField.getName(), me.toField.getValue());
	    }
	    return descMap;
    },
    getErr : function() {
	    var me = this;
	    var fromValid = me.fromField.isValid();
	    var toValid = me.toField.isValid();
	    
	    var emptyField = 0;
	    if (me.fromField.getRawValue()==''){
	    	emptyField+=1;
	    	fromValid = true;
	    }
	    if (me.toField.getRawValue()==''){
	    	toValid = true;
	    	emptyField+=1;
	    }

	    
	    var result = fromValid & toValid;
	    if (emptyField == 2 ){
	    	result = false;
	    }
	    if (!result) {
		    return 'Для поиска по диапазону дат необходимо ввести хотя бы одну корректную дату';
	    if (fromValid) {
		    if (!toValid) {
			    if (me.toField.getRawValue() == '') {
				    // если вторая дата совсем не введена, то искать типа можно.
				    return '';
			    } else {
				    return 'Дата "по" введена некорректно';
			    }
		    } else {
			    return '';
		    }
	    } else {
		    if (me.fromField.getRawValue() == '') {
			    return 'Дата "с" обязательна для заполнения';
		    } else {
			    return 'Дата "с" введена некорректно';
		    }
	    }
    }
}});