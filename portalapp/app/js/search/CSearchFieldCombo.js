Ext.define('app.js.search.CSearchFieldCombo', {
    extend : 'Ext.container.Container',
    layout : {
        type : 'hbox',
        align : 'left'
    },
    selected : '',
    l10nConsts : {},
    action : 'ABSTRACT',
    field : {},
    name : this.fieldId,
    getName : function() {
	    return this.fieldId;
    },
    getValue : function() {
	    var me = this;
	    return me.selected;
    },
    initComponent : function() {
	    var me = this;
	    var delPanId = me.id;
	    var delBtn = Ext.create('Ext.button.Button', {
	        text : '',
	        cls : 'del_but',
	        handler : function() {
		        me.panel.remove(delPanId, true);
		        me.panel.combo.getStore().add({
		            "fieldId" : me.fieldId,
		            "fieldName" : me.fieldName,
		            "orderIndex" : me.orderIndex,
		            tp : me.tp
		        });
		        me.panel.combo.getStore().sort('orderIndex', 'ASC');
	        }
	    });
	    var comboStore = Ext.create('Ext.data.Store', {
	        fields : [ 'id', 'name' ],
	        proxy : {
	            type : 'ajax',
	            url : 'DescLoader?action=' + me.action,
	            autoLoad : false,
	            reader : {
	                type : 'json',
	                root : 'items',
	                totalProperty : 'totalCount'
	            }
	        }
	    });
	    var combo = Ext.create('Ext.form.field.ComboBox', {
	        store : comboStore,
	        labelWidth : 146,
	        fieldLabel : me.fieldName,
            labelSeparator : '',
	        // fieldLabel:me.l10nConsts.as_desc_level,
	        displayField : 'name',
	        //width:400,
	        valueField : 'id',
	        editable : false,
	        emptyText : me.l10nConsts.as_notSelected,
	        listeners : {
		        select : function(combo, rec) {
			        me.selected = rec[0].data.id;
		        }
	        }
	    });
	    me.field = combo;
	    Ext.applyIf(me, {
		    items : [ combo, delBtn ]
	    });
	    me.callParent(arguments);
    },
    getErr : function() {
	    var me = this;
	    if (me.selected == '') {
		    return this.errConsts.as_err_desc_l_not_selected;
	    } else {
		    return '';
	    }
    }
});
