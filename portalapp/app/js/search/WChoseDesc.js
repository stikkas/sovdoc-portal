Ext.define('app.js.search.WChoseDesc', {
    extend : 'Ext.window.Window',
    width : 500,
    height : 470,
    resizable : false,
    layout : {
        type : 'vbox',
        align : 'left',
        flex : 1
    },
    selectedGroupId : null,
    currentCont : {},
    searchTf : {},
    cls : 'descVal',
    descValueName : '',
    initComponent : function() {
	    var me = this;
	    me.title = me.l10nData.vcw_title;
	    var comboStore = Ext.create('Ext.data.Store', {
	        fields : [ 'id', 'namee' ],
	        proxy : {
	            type : 'ajax',
	            url : 'DescLoader?action=getGroups',
	            autoLoad : false,
	            reader : {
	                type : 'json',
	                root : 'items',
	                totalProperty : 'totalCount'
	            }
	        }
	    });
	    var combo = Ext.create('Ext.form.field.ComboBox', {
	        store : comboStore,
	        displayField : 'namee',
	        valueField : 'id',
	        editable : false,
	        width : 412,
	        emptyText : me.l10nData.vcw_comboEmptyText
	    });
	    comboStore.load();
	    var treeStore = Ext.create('Ext.data.TreeStore', {
	        fields : [ 'id', 'text', 'isDescValue' ],
	        autoLoad : false,
	        clearOnLoad : true,
	        defaultRootId : 'root',
	        sortOnLoad : false,
	        nodeParam: 'id',
	        proxy : {
	        	timeout: 60*1000,
	            url : 'DescLoader',
	            method : 'post',
	            type : 'ajax',
	            extraParams : {
		            action : 'getValuesTree'
	            },
	            root : {
	                id : 'root',
	                expanded : 'false'
	            }
	        }
	    });
	    var gridStore = Ext.create('Ext.data.Store', {
	        fields : [ 'id', 'text' ],
	        autoLoad : false,
	        proxy : {
	            type : 'ajax',
	            url : 'DescLoader',
	            reader : {
	                type : 'json',
	                root : 'items'
	            }
	        }
	    });
	    var pathField = Ext.create('Ext.form.field.TextArea', {
	        width : '100%',
	        readOnly : true
	    });
	    var printPath = function(rowmodel, rec, index) {
		    if (me.currentCont instanceof Ext.tree.Panel) {
			    if (!rec.data.isDescValue) {
				    return;
			    }
		    }
		    Ext.Ajax.request({
		        url : 'DescLoader',
		        params : {
		            action : 'getPath',
		            valueId : rec.data.id
		        },
		        success : function(response) {
		        	var respObj = Ext.decode(response.responseText);
		        	var text = respObj.msg;
			        pathField.setValue(text);
		        },
		        failure : window.vp.failHandler
		    });
	    };
	    var treePan = Ext.create('Ext.tree.Panel', {
	        title : me.l10nData.vcw_treePanelTitle,
	        store : treeStore,
	        rootVisible : false,
	        width : '100%',
	        autoScroll : true,
	        height : 235,
	        //cls : 'height128',
	        listeners : {
		        select : printPath
	        },
	        plugins: {
	            ptype: 'bufferedrenderer'
	        },
	        emptyText : 'Нет значений'
	    });
	    treePan.getView().loadMask = true;
	    treePan.getView().preserveScrollOnRefresh = true;
	    var grid = Ext.create('Ext.grid.Panel', {
	        store : gridStore,
	        width : '100%',
	        //height : 240,
	        autoScroll : true,
	        hideHeaders : true,
	        title : me.l10nData.vcw_gridPanelTitle,
	        columns : [ {
	            text : 'id',
	            dataIndex : 'id',
	            hidden : true,
	            hideable : false
	        }, {
	            text : me.l10nData.vcw_gridValueColumnName,
	            dataIndex : 'text',
	            flex : 1
	        } ]
	    });
	    var showAllBtn = null;
	    var f_Search = function() {
		    Ext.apply(gridStore.getProxy(), {
			    extraParams : {
			        action : 'getValuesGrid',
			        groupId : combo.getValue(),
			        q : Ext.encode(searchTf.getValue())
			    }
		    });
		    if (me.currentCont != grid) {
			    me.remove(me.currentCont, false);
			    me.currentCont = grid;
			    me.insert(2, me.currentCont);
		    }
		    gridStore.load({
		    	callback:window.vp.failHandlerCallBack
		    });
		    pathField.setValue("");
		    if (showAllBtn != null) {
			    showAllBtn.enable();
		    }
	    };
	    var searchTf = Ext.create('Ext.form.field.Text', {
	        flex : 1,
	        width : 415,
	        maxWidth : 415,
	        enableKeyEvents : true,
	        listeners : {
		        keyup : function(tf, eventObject) {
			        if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
				        f_Search();
				        showAllBtn.enable();
			        }
		        }
	        }
	    });
	    var showAll = function() {
		    me.remove(me.currentCont, false);
		    me.currentCont = treePan;
		    me.insert(2, me.currentCont);
		    me.searchTf.setValue('');
	    };
	    showAllBtn = Ext.create('Ext.button.Button', {
	        text : ' ',
	        cls : 'show_tree',
	        handler : function() {
		        this.disable();
		        showAll();
	        }
	    });
	    combo.addListener('select', function(combo, rec) {
		    this.selectedGroupId = rec[0].data.id;
		    Ext.apply(treeStore.getProxy(), {
			    extraParams : {
			        action : 'getValuesTree',
			        groupId : this.selectedGroupId
			    }
		    });
		    if (me.currentCont != treePan) {
			    me.remove(me.currentCont, false);
			    me.currentCont = treePan;
			    me.insert(2, me.currentCont);
		    }
		    showAllBtn.disable();
		    searchTf.setValue('');
		    if (rec[0].data.id != null) {
			    me.descValueName = rec[0].data.namee;
			    pathField.setValue("");
			    treePan.getStore().load();
		    }
	    }, this);
	    me.searchTf = searchTf;
	    var searchBtn = Ext.create('Ext.button.Button', {
	        text : '',
	        handler : f_Search,
	        cls : 'srch_but'
	    });
	    var hboxSearchField = Ext.create('Ext.container.Container', {
	        layout : {
		        type : 'hbox'
	        },
	        width : '100%',
	        items : [ searchTf, searchBtn, showAllBtn ]
	    });
	    grid.addListener("select", printPath);
	    var label = Ext.create('Ext.form.Label', {
	        text : me.l10nData.vcw_pathLabel,
	        width : '100%'
	    });
	    var choseBtn = Ext.create('Ext.button.Button', {
	        text : me.l10nData.vcw_selectBtn,
	        handler : function() {
		        var activePanel = me.currentCont;
		        if (activePanel.getSelectionModel().getSelection().length == 0) {
			        Ext.Msg.alert('', me.l10nData.vcw_notSelectedWarning);
		        } else {
			        var selectedValue = activePanel.getSelectionModel().getSelection()[0].data;
			        if (activePanel instanceof Ext.tree.Panel) {
				        if (!selectedValue.isDescValue) {
					        Ext.Msg.alert('Ошибка', 'Выбранный элемент не является указателем');
					        return;
				        }
			        }
			        //window.vp.getComponent(0).saveScroll();
			        var dataForGrid = {
			            descValueId : selectedValue.id,
			            descValueGroupText : me.descValueName,
			            descValue : selectedValue.text
			        };
			        me.choseHandler(dataForGrid);
			        me.close();
			        //window.vp.getComponent(0).restoreScroll();
			        window.vp.doLayout();
                    window.scrollTo(0,document.body.scrollHeight);
		        }
	        }
	    });
	    var cancelBtn = Ext.create('Ext.button.Button', {
	        text : me.l10nData.vcw_cancelBtn,
	        handler : function() {
		        me.close();
	        }
	    });
	    me.currentCont = treePan;
	    Ext.applyIf(me, {
	        items : [ combo, hboxSearchField, treePan, label, pathField ],
	        buttons : [ choseBtn, cancelBtn ]
	    });
	    showAllBtn.disable();
	    me.callParent(arguments);
    }
});