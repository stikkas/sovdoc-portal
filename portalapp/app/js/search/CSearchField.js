Ext.define('app.js.search.CSearchField', {
    extend : 'Ext.container.Container',
    layout : {
        type : 'hbox',
        align : 'left'
    },
    field : '',
    name : '',
    getName : function() {
	    return this.fieldId;
    },
    getValue : function() {
	    return this.field.getValue();
    },
    initComponent : function() {
	    var mee = this;
	    var delPanId = mee.id;
	    var defWidth = 800;
	    if (mee.fieldId == "isbn") {
		    defWidth = 400;
	    }
	    mee.field = Ext.create(mee.tp, {
	        fieldLabel : mee.fieldName,
            labelSeparator : '',
	        labelWidth : 146,
	      //  regex : new RegExp('^[\\sа-яА-ЯёЁa-zA-Z0-9"()№»«\\-]+$'),
	        width : defWidth
	    });
	    var nameFieldDelBtn = Ext.create('Ext.button.Button', {
	        text : '',
	        cls : 'del_but',
	        handler : function() {
		        mee.panel.remove(delPanId, true);
		        mee.panel.combo.getStore().add({
		            "fieldId" : mee.fieldId,
		            "fieldName" : mee.fieldName,
		            "orderIndex" : mee.orderIndex,
		            tp : mee.tp
		        });
		        mee.panel.combo.getStore().sort('orderIndex', 'ASC');
	        }
	    });
	    Ext.applyIf(mee, {
		    items : [ mee.field, nameFieldDelBtn ]
	    });
	    mee.callParent(arguments);
    },
    getErr : function() {
	    var value = this.field.getValue();
	    if (!this.field.validate()) {
		    return this.field.getErrors(value);
	    }
	    if ((value == '') || (typeof value == 'undefined') || (value == null)) {
		    if (this.tp == 'Ext.form.field.Date') {
			    return this.errConsts.as_err_date_not_filled;
		    } else {
			    if (this.name == 'name') {
				    return this.errConsts.as_err_name_not_filled;
			    } else {
				    return this.errConsts;
			    }
		    }
	    } else {
		    if (this.tp == 'Ext.form.field.Date') {
			    if (!this.field.validate()) {
				    return this.errConsts.as_err_date_invalid_format;
			    } else {
				    return '';
			    }
		    }
	    }
    }
});