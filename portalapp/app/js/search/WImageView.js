Ext.define('app.js.search.WImageView', {
	requires: "Ext.layout.container.Border",
	extend: 'Ext.window.Window',
	layout: {
		type: 'border'
	},
	height: '90%',
	width: '90%',
	autoScroll: true,
	data: null,
	currentIdx: null,
	setCurrentIndex: function (idx) {
		this.currentIdx = idx;
		this.showImageWidthIndex(idx);
		this.configureButtons(this.currentIdx);
		this.updateLabel(this.currentIdx);
	},
	setData: function (dt) {
		this.data = dt;
	},
	showImageWidthIndex: function (idx) {
		var me = this,
				item = this.data.get(idx).data,
				tpy = item.url.split(".")[1],
				url = item.url,
				mapUrl = item.mapUrl;
		if (mapUrl) {
			me.update('<object data="' + mapUrl + '" width="99%" height="100%"><embed src="' + mapUrl + '" /></object>');
		} else {
			switch (tpy) {
				case 'tif':
					var tifStr = '<embed src="' + url + '" type="image/tiff" width="100%" height="100%">';
					me.update(tifStr);
					break;
				case 'pdf':
					// width="80%" height="80%"
					var tifStr = '<embed width="100%" height="100%"' + 'src="' + url + '" />';
					me.update(tifStr);
					break;
				default:
					me.update('<img src="' + url + '"/>');
					break;
			}
		}
	},
	btnFirst: null,
	btnLast: null,
	btnNext: null,
	btnPrev: null,
	labelInfo: null,
	initComponent: function () {
		var me = this;
		me.labelInfo = Ext.create('Ext.form.Label', {
			text: ''
		});
		me.btnFirst = Ext.create('Ext.Button', {
			cls: 'x-tbar-page-first',
			handler: function () {
				me.currentIdx = 0;
				me.showImageWidthIndex(me.currentIdx);
				me.configureButtons(me.currentIdx);
				me.updateLabel(me.currentIdx);
			}
		});
		me.btnNext = Ext.create('Ext.Button', {
			cls: 'x-tbar-page-next',
			handler: function () {
				me.currentIdx = me.currentIdx + 1;
				me.showImageWidthIndex(me.currentIdx);
				me.configureButtons(me.currentIdx);
				me.updateLabel(me.currentIdx);
			}
		});
		me.btnPrev = Ext.create('Ext.Button', {
			cls: 'x-tbar-page-prev',
			handler: function () {
				me.currentIdx = me.currentIdx - 1;
				me.showImageWidthIndex(me.currentIdx);
				me.configureButtons(me.currentIdx);
				me.updateLabel(me.currentIdx);
			}
		});
		me.btnLast = Ext.create('Ext.Button', {
			cls: 'x-tbar-page-last',
			handler: function () {
				me.currentIdx = me.data.length - 1;
				me.showImageWidthIndex(me.currentIdx);
				me.configureButtons(me.currentIdx);
				me.updateLabel(me.currentIdx);
			}
		});
		var toolBar = Ext.create('Ext.toolbar.Toolbar', {
			region: 'south',
			items: [me.btnFirst, me.btnPrev, me.btnNext, me.btnLast, me.labelInfo]
		});
		Ext.applyIf(me, {
			dockedItems: [toolBar]
		});
		me.callParent(arguments);
	},
	updateLabel: function (idx) {
		var me = this;
		me.setTitle(me.data.get(idx).data.fileName);
		idx = idx + 1;
		me.labelInfo.setText('Изображение ' + idx + ' из ' + me.data.length);
	},
	configureButtons: function (idx) {
		var me = this;
		if (idx <= 0) {
			me.btnFirst.disable();
			me.btnPrev.disable();
		} else {
			me.btnFirst.enable();
			me.btnPrev.enable();
		}
		if (idx >= me.data.length - 1) {
			me.btnLast.disable();
			me.btnNext.disable();
		} else {
			me.btnLast.enable();
			me.btnNext.enable();
		}
	}
});