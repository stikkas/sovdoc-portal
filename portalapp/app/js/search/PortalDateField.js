Ext.define('app.js.search.PortalDateField', {
    extend : 'Ext.form.field.Date',
    format : 'd.m.Y',
    altFormats : '',
    validateOnBlur : false,
    validateOnChange : false,
    enforceMaxLength : true,
    allowBlank : true,
    msgTarget: 'none',
    maxLength : 10,
    cls : 'width50',
    enforceMaxLength : true,
    maxLength : 10,
    maskRe : /[0-9\.]/,
    dirtyCls : '',
    invalidCls : '',
    invalidCls : '',   
    listeners : {
        blur : function(comp, Th, opts) {
	        var rawNewValue = comp.getRawValue();
	        if (rawNewValue) {
		        var isValie = comp.validateValue(rawNewValue);
		        if (!isValie) {
			        Ext.Msg.alert('Ошибка', 'Неверно задана дата (формат ввода: дд.мм.гггг)');
		        }
	        }
	        
        },
        change : function(comp, nVal, oVal, opts) {
	        if (oVal && nVal) {
		        if (oVal.length && nVal.length) {
			        if (nVal.length <= oVal.length) {
				        // При удалении символов код установке точек не нужен.
				        return;
			        } else {
				        var diff = nVal.substr(oVal.length, nVal.length - oVal.length);
			        }
		        }
	        }
	        if (nVal) {
		        if (nVal.length == 5) {
			        var rawNewValue = '';
			        if (nVal.substr(2, 1) != '.') {
				        rawNewValue = nVal.substr(0, 2) + '.' + nVal.substr(2, nVal.length - 2);
			        } else {
				        rawNewValue = nVal;
			        }
			        if (rawNewValue.substr(4, 1) != '.') {
				        rawNewValue = rawNewValue.substr(0, 5) + '.' + rawNewValue.substr(5, rawNewValue.length - 5);
			        } else {
			        }
			        if (rawNewValue instanceof Date) {
				        comp.setValue(rawNewValue);
			        } else {
				        comp.setRawValue(rawNewValue);
			        }
		        }
	        }
        }
    },
    createPicker : function() {
	    var me = this, format = Ext.String.format;
	    var me = this;
	    var picker = new Ext.picker.Date({
	        pickerField : me,
	        ownerCt : me.ownerCt,
	        renderTo : document.body,
	        floating : true,
	        hidden : true,
	        focusOnShow : true,
	        minDate : me.minValue,
	        maxDate : me.maxValue,
	        disabledDatesRE : me.disabledDatesRE,
	        disabledDatesText : me.disabledDatesText,
	        disabledDays : me.disabledDays,
	        disabledDaysText : me.disabledDaysText,
	        format : me.format,
	        showToday : me.showToday,
	        startDay : me.startDay,
	        minText : format(me.minText, me.formatDate(me.minValue)),
	        maxText : format(me.maxText, me.formatDate(me.maxValue)),
            value: new Date('01/01/1940'),
	        listeners : {
	            scope : me,
	            select : me.onSelect,
	            show : function(th, eop) {
		            th.showMonthPicker();
	            }
	        },
	        keyNavConfig : {
		        esc : function() {
			        me.collapse();
		        }
	        }
	    });
	    return picker;
    }
});