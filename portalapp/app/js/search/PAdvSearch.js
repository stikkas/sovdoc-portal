Ext.define('app.js.search.PAdvSearch', {
    extend : 'Ext.form.Panel',
    name : 'searchpanel',
    combo : '',
    width : '100%',
    searchFieldsMap : '',
//  scrollValueBody : 0,
    padding: '0 16 0 0',
    storeData : {},
    mode : '',
    reset : function() {
	    var me = this;
	    me.combo.getStore().load();
	    me.combo.reset();
	    var i = me.items.length;
	    for (i = me.items.length - 1; i > 0; i--) {
		    me.remove(i, true);
	    }
    },
    addSearchField : function(parm) {
	    fieldCode = parm[0].data.fieldId;
	    var fieldClass = this.searchFieldsMap.get(fieldCode).tp;
	    switch (fieldClass) {
	    case 'app.js.search.CSearchFieldDateInterval':
		    var parmObj = this.searchFieldsMap.get(fieldCode);
		    parmObj.l10nData = this.l10nData;
		    parmObj.panel = this;
		    comp = Ext.create(fieldClass,parmObj);
		    break;
	    case 'app.js.search.ArchiveCode':
		    var params = this.searchFieldsMap.get(fieldCode);
		    params.l10nData = this.l10nData;
		    comp = Ext.create(fieldClass, params);
		    break;
	    case 'app.js.search.CSearchFieldElementType':
		    var parmObj = this.searchFieldsMap.get(fieldCode);
		    parmObj.l10nData = this.l10nData;
		    parmObj.panel = this;
		    comp = Ext.create("app.js.search.CSearchFieldElementType", parmObj);
		    comp.setWidth(420);
		    break;
	    case 'app.js.search.CSearchFieldDescriptorsGrid':
		    var parms = this.searchFieldsMap.get(fieldCode);
		    parms.l10nData = this.l10nData;
		    comp = Ext.create(fieldClass, parms);
		    break;
	    case 'app.js.elib.CSearchFieldAuthor':
		    var parms = this.searchFieldsMap.get(fieldCode);
		    parms.l10nData = this.l10nData;
		    parms.panel = this;
		    comp = Ext.create(fieldClass, parms);
		    comp.setWidth(420);
		    break;
	    case 'app.js.elib.CSearchFieldPublYear':
		    var parms = this.searchFieldsMap.get(fieldCode);
		    parms.l10nData = this.l10nData;
		    comp = Ext.create(fieldClass, parms);
		    comp.setWidth(410);
		    break;
	    case 'app.js.search.CSearchFieldLanguage':
		    var parmObj = this.searchFieldsMap.get(fieldCode);
		    parmObj.l10nData = this.l10nData;
		    parmObj.panel = this;
		    comp = Ext.create(fieldClass, parmObj);
		    comp.setWidth(420);
		    break;
	    case 'app.js.elib.CSearchFieldEbookSeries':
		    var parmObj = this.searchFieldsMap.get(fieldCode);
		    parmObj.l10nData = this.l10nData;
		    parmObj.panel = this;
		    comp = Ext.create(fieldClass, parmObj);
		    comp.setWidth(830);
		    break;
	    default:
		    var parms = this.searchFieldsMap.get(fieldCode);
		    comp = Ext.create('app.js.search.CSearchField', parms);
	    }
	    this.add(comp);
	    this.combo.getStore().remove(parm);
	    this.combo.reset();
	    this.updateLayout();
	    window.vp.doLayout();
        window.scrollTo(0,document.body.scrollHeight);
    },
    listeners : {
    },
    mkFieldInfoItem : function(fId, fName, order, ftp, errConst) {
	    if (typeof fName == 'undefined' || fName == null) {
		    throw "Field Name cannot be null or undefined (fieldUd=" + fId + ")";
	    }
	    var t = {
	        fieldId : fId,
	        fieldName : fName,
	        orderIndex : order,
	        tp : ftp,
	        errConsts : errConst
	    };
	    return t;
    },
    initComponent : function() {
	    Ext.fly(window).on("scroll", function() {
		    var topScrollBodyPosition = Ext.getBody().getScroll().top;
		    this.topScrollPos = topScrollBodyPosition;
	    });
	    var me = this;
	    var storeDataBibl = [ me.mkFieldInfoItem("author", me.l10nData.elib_author, 1, "app.js.elib.CSearchFieldAuthor", me.l10nData.elib_err_author), me.mkFieldInfoItem("name", me.l10nData.elib_book_title, 1, "Ext.form.field.Text", me.l10nData.elib_err_book_title), me.mkFieldInfoItem("unitLang", me.l10nData.as_unit_lang, 2, "app.js.search.CSearchFieldLanguage", me.l10nData), me.mkFieldInfoItem("publisher", me.l10nData.elib_publisher, 3, "Ext.form.field.Text", me.l10nData.elib_err_publisher),
	            me.mkFieldInfoItem("publYear", me.l10nData.elib_publ_year, 4, "app.js.elib.CSearchFieldPublYear", me.l10nData.elib_err_publ_year), me.mkFieldInfoItem("seriesInfo", me.l10nData.elib_series_info, 5, "app.js.elib.CSearchFieldEbookSeries", me.l10nData.elib_err_series_info), me.mkFieldInfoItem("isbn", me.l10nData.elib_isbn, 6, "Ext.form.field.Text", me.l10nData.elib_err_isbn),
	            me.mkFieldInfoItem("nsadescriptors", me.l10nData.elib_keywords, 7, "app.js.search.CSearchFieldDescriptorsGrid", me.l10nData) ];
	    var storeDataAdvSearch = [ me.mkFieldInfoItem("name", me.l10nData.as_name, 1, "Ext.form.field.Text", me.l10nData.as_err_name_not_filled), me.mkFieldInfoItem("unitLang", me.l10nData.as_unit_lang, 2, "app.js.search.CSearchFieldLanguage", me.l10nData), me.mkFieldInfoItem("date", me.l10nData.as_date, 3, "app.js.search.CSearchFieldDateInterval", me.l10nData), me.mkFieldInfoItem("archeiveCode", me.l10nData.as_archiveCode, 4, "app.js.search.ArchiveCode", me.l10nData),
	            me.mkFieldInfoItem("elemType", me.l10nData.as_desc_level, 5, "app.js.search.CSearchFieldElementType", me.l10nData), me.mkFieldInfoItem("nsadescriptors", me.l10nData.as_NsaDescriptors, 6, "app.js.search.CSearchFieldDescriptorsGrid", me.l10nData) ];
	    if (me.mode == 'advsearch') {
		    me.storeData = storeDataAdvSearch;
		    me.title = me.l10nData.as_title;
	    }
	    if (me.mode == 'elib') {
		    me.storeData = storeDataBibl;
		    me.title = me.l10nData.elib_chapter_title;
	    }
	    me.searchFieldsMap = new Ext.util.HashMap();
	    me.combo = Ext.create('Ext.form.field.ComboBox', {
	        name : 'combo',
	        labelWidth : 146,
	        width : 396,
	        editable : false,
	        fieldLabel : me.l10nData.as_choseAttrLabel,
            labelSeparator : '',
	        store : Ext.create('Ext.data.Store', {
	            fields : [ 'fieldId', 'fieldName', 'orderIndex', 'tp', 'errConsts' ],
	            data : me.storeData
	        }),
	        displayField : 'fieldName',
	        valueField : 'fieldId',
	        blankText : me.l10nData.as_notSelected,
	        emptyText : me.l10nData.as_notSelected,
	        listeners : {
		        select : function(combo, rec) {
			        me.addSearchField(rec);
		        }
	        }
	    });
	    for (i = 0; i < me.combo.getStore().data.getCount(); i++) {
		    var strName = me.combo.getStore().getAt(i).get("fieldId");
		    var row = me.combo.getStore().getAt(i);
		    me.searchFieldsMap.add(strName, {
		        fieldId : row.get('fieldId'),
		        fieldName : row.get('fieldName'),
		        panel : me,
		        orderIndex : row.get('orderIndex'),
		        tp : row.get('tp'),
		        errConsts : row.get('errConsts')
		    });
	    }
	    Ext.applyIf(me, {
		    items : [ me.combo ]
	    });
	    me.callParent(arguments);
      },
    getValuesMap : function() {
	    var valuesMap = {};
	    var i = 0;
	    for (i = 1; i < this.items.length; i++) {
		    var value = this.getComponent(i).getValue();
		    if (!(value instanceof Ext.util.HashMap)) {
			    valuesMap[this.getComponent(i).getName()] = value;
		    } else {
			    value.each(function(key, value, length) {
				    valuesMap[key] = value;
			    });
		    }
	    }
	    // Флаг, указывающий, что запрос поиска осуществляется для электронной
		// библиотеки
	    // поэтому на сервере надо искать по тарблицам ARCH_EBOOK
	    if (this.mode == 'elib') {
		    valuesMap['elibQuery'] = 1;
	    }
	    return valuesMap;
    },
    getErr : function() {
	    if (this.items.length == 1) {
		    return this.l10nData.as_err_parms_not_spec;
	    }
	    var err = '';
	    var j = 1;
	    var compCount = this.items.length;
	    var i = j;
	    while (i < compCount) {
		    var val = this.getComponent(i).getErr();
		    var bool = (val != '') && (val);
		    if (bool) {
			    err += '<p>';
			    err += val;
			    err += '</p>';
		    }
		    i++;
	    }
	    return err;
    }
});