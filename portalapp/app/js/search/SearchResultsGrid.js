Ext.define('app.js.search.SearchResultsGrid', {
    extend : 'Ext.grid.Panel',
    forceFit : true,
    dockedItems : [],
    mode : '',
    viewConfig : {
	    loadMask : false
    },
    autoHeight:true,
    width : '100%',
    padding: '0 16 0 0',
    toolBar : {},
    pagingTb : null,
    sortableColumns : false,
    enableColumnHide : false,
    storeName : 'js.search.SearchStore',
    needBackButton : true,
    initComponent : function() {
	    var me = this;
	    me.store = Ext.getStore(me.storeName);
	    me.toolBar = Ext.create('Ext.toolbar.Toolbar', {
		    items : [ Ext.create('Ext.Button', {
		        cls : 'back_res',
		        text : 'Назад',
		        width : 200,
		        tooltip : 'Вернуться к критерию поиска',
		        tooltipType : 'title',
		        handler : function() {
			        // возврат к разделу "Расширенный поиск" или "Электронная
			        // библиотека"
			        Ext.util.History.back();
		        }
		    }) ]
	    });
	    me.pagingTb = Ext.create('Ext.toolbar.Paging', {
	        dock : 'bottom',
	        displayInfo : true,
	        store : me.getStore(),
			listeners: {
				change: function() {
					window.document.body.scrollTop = 0;
				}
			}
	    });
	    me.dockedItems[0] = me.pagingTb;
	    for ( var io = 0; io < me.pagingTb.items.length; io++) {
		    var obj = me.pagingTb.items.get(io);
		    if (obj.overflowText) {
			    obj.tooltipType = 'title';
			    obj.setTooltip(obj.overflowText);
		    }
	    }
	    me.callParent(arguments);
	    if (me.needBackButton) {
		    me.addDocked(me.toolBar);
	    }
    },
    cls : 'srch_res mar_t-25',
    top : 405,
    bottom: 50,
    columns : [ {
        dataIndex : 'dataUnitId',   
        text : '',
        hidden : true,
        hideable : false,
        width : 30,
        hideable : false,
        renderer : function(value) {
	        return '<a href="navigation.jsp?page=showdataunit&id=' + value + '">Просмотр</a>';
        }
    }, {
        dataIndex : 'archiveCode',
        text : 'Архивный шифр',
        width : 20
    }, {
        dataIndex : 'dataUnitName',
        text : 'Заголовок',
        width : 58
    }, {
        dataIndex : 'mathesTextBlock',
        text : 'Крайние даты',
        width : 20
    } ],
    listeners : {
	    cellclick : function(ref, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		    var nvUrl = '#showunit' + '&id=' + record.data.dataUnitId;
		    window.vp.navigate(nvUrl);
	    }
    }
});