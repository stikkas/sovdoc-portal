Ext.define('app.js.search.CSimpleSearch', {
    extend : 'Ext.container.Container',
    width : '100%',
    height : 100,
    autoScroll : false,
    layout : 'absolute',
    l10nData : {},
    searchField : {},
    clear : function() {
	    var me = this;
	    me.searchField.setValue('');
    },
    initComponent : function() {
	    var me = this;
	    var goSearchHandler = function() {
		    var errors = searchTxtField.getErrors(searchTxtField.getValue());
		    if (errors.length != 0) {
			    Ext.Msg.alert('Ошибка', errors);
			    return;
		    }
		    var searchStore = Ext.getStore('js.search.SearchStore');
		    searchStore.getProxy().extraParams = {
			    q : Ext.encode(searchTxtField.getValue())
		    };
		    searchStore.currentPage = 1;
		    searchStore.load({
			    callback : function(recs, ops, suc) {
				    if (suc) {
					    if (recs.length == 0) {
						    Ext.Msg.alert('Информация', 'К сожалению, по Вашему запросу ничего не найдено');
						    return;
					    } else {
						    window.vp.navigate('#searchresults', Ext.encode(searchTxtField.getValue()));
					    }
				    }
				
			    },
			    failure:window.vp.failHandler
		    });
	    };
	    me.searchField = Ext.create('Ext.form.field.Text', {
	        xtype : 'textfield',
	        fieldLabel : me.l10nData.ss_searchlabel,
            labelSeparator : '',
	        labelWidth : 235,
	        validateOnChange : false,
	        validateOnBlur : false,
	        allowBlank : false,
	      //  regex : new RegExp('^[\\sа-яА-ЯёЁa-zA-Z0-9"()№»«\\-]+$'),
	        width : 600,
	        enableKeyEvents : true,
	        value : '',
	        listeners : {
		        keyup : function(tf, eventObject) {
			        if (eventObject.getCharCode() == Ext.EventObject.ENTER) {
				        goSearchHandler();
			        }
		        }
	        }
	    });
	    var searchTxtField = me.searchField;
	    var searchTаcont = Ext.create('Ext.container.Container', {
	        width : 670,
	        height : 25,
	        layout : {
		        type : 'hbox'
	        },
	        cls : 'srch_fld',
	        items : [ me.searchField, {
	            width : 70,
	            xtype : 'button',
	            text : me.l10nData.ss_searchbtn,
	            handler : goSearchHandler
	        } ],
	        x : 240,
	        y : 30
	    });
	    var labelsCont = Ext.create('Ext.container.Container', {
	        width : 300,
	        cls : 'srch_link',
	        items : [ {
	            xtype : 'displayfield',
	            value : '<a href="#advsearch" onclick=window.vp.navigate(this.href)>' + me.l10nData.ss_advsearch + '</a> ' + '<a href="#search" onclick=window.vp.showPopup(this.href)>' + me.l10nData.ss_help + '</a>'
	        } ],
	        x : 475,
	        y : 55
	    });
	    Ext.applyIf(me, {
		    items : [ searchTаcont, labelsCont ]
	    });
	    me.callParent(arguments);
    }
});