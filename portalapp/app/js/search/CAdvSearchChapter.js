Ext.define('app.js.search.CAdvSearchChapter', {
    extend : 'Ext.container.Container',
    // width : '95%',
    autoScroll : false,
    height : 'auto',
    layout : {
        type : 'vbox',
        align : 'left'
    },
    mode : '',
    cls : 'search_style',
    items : [],
    panAdvSearch : {},
    gridAdvSearch : {},
    btnSearch : {},
    btnReset : {},
    searchStore : null,
    initComponent : function() {
	    var me = this;
	    me.panAdvSearch = Ext.create('app.js.search.PAdvSearch', {
	        mode : me.mode,
	        l10nData : me.l10nData
	    });
	    me.btnSearch = Ext.create('Ext.Button', {
	        text : me.l10nData.as_searchBtn,
	        cls : 'btn_cls',
	        maxWidth : 150,
	        handler : function() {
		        var err = me.panAdvSearch.getErr();
		        if (err != '') {
			        Ext.Msg.alert(me.l10nData.sys_error, err);
			        return;
		        }
		        window.vp.loadMask.show();
		        var prms = me.panAdvSearch.getValuesMap();
		        var descParms = {};
		        if (prms['descGrid']) {
			        descParms = Ext.encode(prms['descGrid']);
			        delete prms['descGrid'];
		        }
		        var parms = Ext.encode(prms);
		        me.searchStore = null;
		        if (me.mode == 'elib') {
			        me.searchStore = Ext.getStore('js.elib.ElibStore');
		        } else {
			        me.searchStore = Ext.getStore('js.search.SearchStore');
		        }
		        var loadParams = {
			        callback : function(recs, ops, suc) {
				        if (suc) {
					        if (recs.length != 0) {
						        // переход к странице отображения результатов
						        // поиска выполняется только если режим не
						        // "электронная библиотека"
						        // в электронной библиотеки результаты поиска
						        // отображаются на той же странице, где
						        // расположены критерии и список.
						        if (me.mode != 'elib') {
							        window.vp.navigate('#searchresults');
						        }
					        } else {
						        Ext.Msg.alert('Информация', 'К сожалению, по Вашему запросу ничего не найдено');
						        return;
					        }
				        } 
			        },
			        failure:window.vp.failHandler
		        };
		        me.searchStore.getProxy().extraParams = {
		            action : me.mode,
		            params : parms,
		            descValueParams : descParms
		        };
		        me.searchStore.currentPage = 1;
		        me.searchStore.load(loadParams);
	        }
	    });
	    me.btnReset = Ext.create('Ext.Button', {
	        text : me.l10nData.as_cleanBtn,
	        cls : 'btn_cls',
	        handler : function() {
		        if (me.mode == 'elib') {
			        // если режим - "электронная библиотека", то при "очистке"
			        // результатов поиска нужно
			        // отображать весь список электронный книг (Оля показывала
			        // как это работает на примере оракловых приложений)
		        	
			        me.searchStore.getProxy().extraParams = {
			            action : 'elib',
			            params : Ext.encode({
			                elibQuery : 1,
			                elibGetAll : 2
			            })
			        };
		        	
		        	
			        me.searchStore.load();
		        }
		        me.panAdvSearch.reset();
	        }
	    });
	    var btnsBar = Ext.create('Ext.toolbar.Toolbar', {
		    items : [ me.btnSearch, me.btnReset ]
	    });
	    var barCont = Ext.create('Ext.container.Container', {
	        width : '100%',
	        height : 28,
	        layout : {
	            type : 'vbox',
	            align : 'center'
	        },
	        items : [ btnsBar ]
	    });
	    me.items = [];
	    if (me.mode == 'elib') {
		    // Если раздел - "Электронная библиотека", выводится до кучи еще и
			// список всех книг,
		    // что бы их не искать (баг: 6069)
		    var bookList = Ext.create('app.js.elib.CEBookListCard');
		    me.items[me.items.length] = bookList;
		    bookList.loadAll();
	    }
	    me.items[me.items.length] = me.panAdvSearch;
	    me.items[me.items.length] = barCont;
	    me.callParent(arguments);
	    if (me.mode != 'elib')
		    window.vp.loadMask.hide();
    }
});