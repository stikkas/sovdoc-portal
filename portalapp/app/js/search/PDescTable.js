Ext.define('app.js.search.PDescTable', {
    extend : 'Ext.grid.Panel',
    cls : 'desc_tab',
    valueIds : '',
    selectedRow : '',
    // width:'100%',
    autoScroll : false,
    hideHeaders : true,
    isDataExist : function() {
	    return this.getStore().data.length != 0;
    },
    getDataMap : function() {
	    var me = this;
	    var descMap = {};
	    for (i = 0; i < me.getStore().data.length; i++) {
		    descMap[i] = me.getStore().data.get(i).data.descValueId;
	    }
	    return descMap;
    },
    reset : function() {
	    var me = this;
	    var i = 0;
	    for (i = me.getStore().data.length; i >= 0; i--) {
		    me.getStore().removeAt(i);
	    }
    },
    initComponent : function() {
	    var me = this;
	    me.valueIds = new Array();
	    var gridStore = Ext.create('Ext.data.Store', {
	        fields : [ 'descValueId', 'descValueGroupText', 'descValue' ],
	        proxy : {
	            type : 'memory',
	            reader : {
	                type : 'json',
	                root : 'items'
	            }
	        }
	    });
	    var addValueHandler = function(rec) {
		    me.valueIds[me.valueIds.length] = rec.id;
		    gridStore.add(rec);
	    };
	    var addBtn = Ext.create('Ext.Button', {
	        text : '',
	        cls : 'add_but',
	        handler : function() {
		        var addWnd = Ext.create('app.js.search.WChoseDesc', {
		            l10nData : me.l10nData,
		            choseHandler : addValueHandler
		        });
		        addWnd.show();
	        }
	    });
	    var delBtn = Ext.create('Ext.Button', {
	        text : '',
	        cls : 'del_but_tab',
	        handler : function() {
		        var isNotSelected = (me.getSelectionModel().getSelection().length == 0);
		        if (isNotSelected) {
			        Ext.Msg.alert('', me.windowL10nConsts.notSelectedWarning);
		        } else {
			        gridStore.removeAt(me.selectedRow);
		        }
	        }
	    });
	    var tb = Ext.create('Ext.toolbar.Toolbar', {
		    items : [ addBtn, delBtn ]
	    });
	    Ext.applyIf(me, {
	        dockedItems : [ tb ],
	        store : gridStore,
	        columns : [ {
	            text : 'id',
	            dataIndex : 'descValueId',
	            hidden : true,
	            hideable : false
	        }, {
	            text : me.l10nData.asg_gridName,
	            dataIndex : 'descValueGroupText',
	            flex : 1
	        }, {
	            text : me.l10nData.asg_gridValue,
	            dataIndex : 'descValue',
	            flex : 2
	        } ],
	        listeners : {
		        'select' : function(rowmodel, rec, index) {
			        me.selectedRow = index;
		        }
	        }
	    });
	    me.callParent(arguments);
    }
});