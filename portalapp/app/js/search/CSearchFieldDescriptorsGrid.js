Ext.define('app.js.search.CSearchFieldDescriptorsGrid', {
    extend : 'Ext.container.Container',
    cls : 'desc_nsa',
    layout : {
        type : 'hbox',
        align : 'left'
    },
    field : '',
    name : '',
    minHeight : 50,
    getName : function() {
	    return 'descGrid';
    },
    getValue : function() {
	    return this.field.getDataMap();
    },
    initComponent : function() {
	    var me = this;
	    var delPanId = me.id;
	    me.field = me.gridAdvSearch = Ext.create('app.js.search.PDescTable', {
		    l10nData : me.l10nData
	    });
	    var delBtn = Ext.create('Ext.button.Button', {
	        text : '',
	        cls : 'del_but',
	        handler : function() {
		        me.panel.remove(delPanId, true);
		        me.panel.combo.getStore().add({
		            "fieldId" : me.fieldId,
		            "fieldName" : me.fieldName,
		            "orderIndex" : me.orderIndex,
		            tp : me.tp
		        });
		        me.panel.combo.getStore().sort('orderIndex', 'ASC');
	        }
	    });
	    Ext.applyIf(me, {
		    items : [ {
		        xtype : 'fieldset',
		        width : '80%',
		        title : me.l10nData.as_fieldSetTitle,
		        items : [ me.field ]
		    }, delBtn ]
	    });
	    me.callParent(arguments);
    },
    getErr : function() {
	    if (!this.field.isDataExist()) {
		    return this.l10nData.as_err_nsa_desc_not_selected;
	    } else {
		    return '';
	    }
    }
});