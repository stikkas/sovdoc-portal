Ext.define('app.js.search.ArchiveCode', {
	extend: 'Ext.container.Container',
	fsCont: '',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	initComponent: function () {
		var me = this;
		var btn = Ext.create('Ext.Button', {
			text: '',
			cls: 'del_but_arch',
			maxHeight: 25,
			handler: function () {
				var t = me.getValue();
				me.panel.remove(me, true);
				me.panel.doLayout();
				me.panel.combo.getStore().add({
					"fieldId": me.fieldId,
					"fieldName": me.fieldName,
					"orderIndex": me.orderIndex,
					tp: me.tp
				});
			}
		});
		me.fsCont = Ext.create('Ext.container.Container', {
			width: 380,
			items: [{
					xtype: 'fieldset',
					defaultType: 'textfield',
					defaults: {
						labelWidth: 145,
						width: 380
					},
					title: me.l10nData.as_archiveCode,
					cls: 'arch_search',
					items: [Ext.create('Ext.form.field.ComboBox', {
							labelWidth: 145,
							displayField: 'name',
							valueField: 'id',
							allowBlank: true,
							fieldLabel: me.l10nData.as_Archive,
							labelSeparator: '',
							emptyText: me.l10nData.as_notSelected,
							name: 'archiveCode',
							editable: false,
							store: Ext.create('Ext.data.Store', {
								fields: ['id', 'name'],
								autoLoad: true,
								proxy: {
									type: 'ajax',
									url: 'DescLoader?action=getOrgStructure',
									reader: {
										type: 'json',
										root: 'items'
									}
								}
							}),
							listeners: {
								select: function (comp, rec, index) {
									if (comp.getValue() == "" || comp.getValue() == -1) {
										comp.reset();
									}
								}
							}
						}),{
							fieldLabel: me.l10nData.as_fondPrefix,
							labelSeparator: '',
							name: 'fondPrefix',
							errMsg: me.l10nData.as_fund_prefix_not_filled
						}, {
							fieldLabel: me.l10nData.as_fondNumber,
							labelSeparator: '',
							name: 'fondNumber',
							xtype: 'numberfield',
							width: 300,
							errMsg: me.l10nData.as_err_fund_not_filled
						}, {
							fieldLabel: me.l10nData.as_fondLitera,
							labelSeparator: '',
							name: 'fondLitera',
							errMsg: me.l10nData.as_fund_letter_not_filled
						}, {
							fieldLabel: me.l10nData.as_opisNumber,
							labelSeparator: '',
							name: 'opisNumber',
							xtype: 'numberfield',
							width: 300,
							errMsg: me.l10nData.as_err_inv_num_not_filled
						}, {
							fieldLabel: me.l10nData.as_opisLitera,
							labelSeparator: '',
							name: 'opisLitera',
							errMsg: me.l10nData.as_err_inv_let_not_filled
						}, {
							fieldLabel: me.l10nData.as_deloNumber,
							labelSeparator: '',
							name: 'deloNumber',
							xtype: 'numberfield',
							width: 300,
							errMsg: me.l10nData.as_err_file_num_not_filled
						}, {
							fieldLabel: me.l10nData.as_deloLitera,
							labelSeparator: '',
							name: 'deloLitera',
							errMsg: me.l10nData.as_err_file_let_not_filled
						}]
				}]
		});
		Ext.applyIf(me, {
			items: [me.fsCont, btn]
		});
		me.callParent(arguments);
	},
	getErr: function () {
		var isFill = false,
				err = '',
				me = this;
		/*
		for (var j = me.fsCont.getComponent(0).items.length - 1; j > 0; j--) {
			if (me.fsCont.getComponent(0).getComponent(j).name == 'fondLitera' || this.fsCont.getComponent(0).getComponent(j).name == 'opisLitera') {
				continue; // skip cheking of litera fond & opis litera
			}
			var value = this.fsCont.getComponent(0).getComponent(j).getValue();
			if ((value != '') && (value) && (value != -1)) {
				isFill = true;
			} else {
				if (isFill) {
					var msg = '<p>' + this.fsCont.getComponent(0).getComponent(j).errMsg + '</p>';
					err += msg;
				}
			}
		}
		*/
		var comboValue = this.fsCont.getComponent(0).getComponent(0).getValue();
		if ((!comboValue || comboValue == -1) && !isFill) {
			err += this.errConsts.as_err_archive_not_selected;
		}
		return err;
	},
	getValue: function () {
		var hm = new Ext.util.HashMap();
		for (i = 0; i < this.fsCont.getComponent(0).items.length; i++) {
			var name = this.fsCont.getComponent(0).getComponent(i).getName();
			var value = this.fsCont.getComponent(0).getComponent(i).getValue();
			if ((name == 'archiveCode') && (value == -1)) {
				continue;
			}
			hm.add(name, value);
		}
		return hm;
	}
});
