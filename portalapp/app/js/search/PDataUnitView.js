Ext.define('app.js.search.PDataUnitView', {
	minHeight: 500,
	height: 'auto',
	width: '100%',
	extend: 'Ext.form.Panel',
	autoScroll: false,
	cls: 'card_page',
	link: null,
	tb: null,
	padding: '0 16 10 0',
	layout: {
		type: 'vbox',
		align: 'stretch',
		pack: 'start'
	},
	l10nConsts: {},
	items: [],
	currentPanel: {},
	htmlPanel: Ext.create('Ext.form.Panel', {
		autoEl: true,
		cls: 'card',
		autoRender: true,
		flex: 1,
		pollForChanges: true
	}),
	initComponent: function () {

		//проверяю ссылка это или нет, если да - то блокирую кнопку НАЗАД
		var queryString = function () {
			var query_string = {};
			var query = window.location.href;
			var vars = query.split("&");
			for (var i = 0; i < vars.length; i++) {

				var pair = vars[i].split("=");
				if (pair[0] === "isRef" && pair[1] === "ssylka") {
					query_string = pair[1];
				}
			}
			return query_string;
		}();

		var me = this;
		me.callParent(arguments);
		me.tb = Ext.create('Ext.toolbar.Toolbar', {
			items: [me.nazad = Ext.create('Ext.Button', {
//		        text : me.l10nConsts.card_BackToGrid,
					text: 'Назад',
					width: 'auto',
					cls: 'back_res',
					tooltip: 'Назад',
					tooltipType: 'title',
					// maxWidth : 200,
					handler: function () {
						Ext.util.History.back();
					}
				})]
		});
		me.addDocked(me.tb);
		if (queryString === "ssylka") {
			me.nazad.setDisabled(true);
		}
		;
		me.currentPanel = me.htmlPanel;
		me.htmlPanel.hide();
		me.add(me.htmlPanel);
		window.vp.loadMask.show();
	},
	loadInfo: function (duId, action) {
		var me = this;
		Ext.Ajax.request({
			url: 'SearchHandler',
			params: {
				action: action || 'getUnitInfo',
				unitId: duId
			},
			success: function (response) {
				// Для реализации функции 6172, ответ прилетает в виде json
				// объекта, где
				// в поле cardhtml передается html содержимое для карточки, а в
				// поле
				// ebookurl - ссылка электронную книгу из базы.
				var data = Ext.decode(response.responseText),
						toolBarItems = me.tb.items;
				if (data.isPhonoDoc) {
					// вообще эта панель очень плохо расширяема, поэтому такие охуенные костыли
					// (см. ниже тоже, и в других файлах тоже, везде одним словом)
					// Я не знаю чему тут мог научиться Ф. только гавнокодить.
					toolBarItems.getAt(1).hide();
					toolBarItems.getAt(2).hide();
				}
				// Показываем или скрываем заголовок для аннотаций
				toolBarItems.getAt(3).setVisible(action);

				me.htmlPanel.update(data.cardhtml);
				me.htmlPanel.show();
				if (action) { // загрузили страничку с плеерами
					$('.audio-player').each(function (i, e) {
						var el = $(e);
						$(el.attr('data-player')).jPlayer({
							ready: function () {
								var file = el.attr('data-file');
								if (!file) {
									var dc = $(el.attr('data-container'));
									dc.find('button')
											.each(function () {
												$(this).prop('disabled', true);
											});
									dc.find('div.jp-volume-bar').each(function () {
										$(this).addClass('noactive');
									});
								} else {
									$(this).jPlayer("setMedia", {
										title: el.attr('data-title'),
										mp3: file
									});
								}
							},
							cssSelectorAncestor: el.attr('data-container'),
							swfPath: "/player",
							supplied: "mp3",
							useStateClassSkin: true,
							autoBlur: false,
							smoothPlayBar: false,
							keyEnabled: false,
							remainingDuration: true,
							toggleDuration: true
						});
					});
				}
				window.vp.doLayout();
				window.vp.loadMask.hide();
			},
			failure: function (response) {
				var text = response.responseText;
				me.htmlPanel.update(text);
				me.htmlPanel.show();
				window.vp.loadMask.hide();
			}
		});
	}
});
