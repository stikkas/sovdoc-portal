Ext.define('app.js.search.CSearchFieldLanguage', {
    extend : 'app.js.search.CSearchFieldCombo',
    action : 'getUnitLanguages',
    cls:'width228_f',
    getErr : function() {
	    var me = this;
	    if (me.selected == '') {
		    return this.errConsts.as_err_lang_not_sel;
	    } else {
		    return '';
	    }
    }
});