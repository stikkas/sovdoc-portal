Ext.define('app.js.CExhibitionsChapter', {
    extend : 'Ext.container.Container',
    width : '99%',
    height:'auto',
    cls : 'text_n exh_cls',
//    layout : {
//	    type : 'hbox'
//    },
    autoRender:true,
    loadExhibition : function(itself, exhId) {
	    var owner = itself.ownerCt;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
	            action : 'getOneExhibition',
	            id : exhId
	        },
	        success : function(response, opts) {
                        var cb = function()
                        {
                            window.vp.setTargetAttr(this, 'a:not([target])');
                        }
		        itself.update(response.responseText, false, cb);
                        
		        window.vp.doLayout();
		        window.vp.loadMask.hide();
	        },
	        failure : window.vp.failHandler
	    });
    },
    loadExhibitionsList : function(itself) {
    	var me = this;
	    var owner = itself.ownerCt;
	    var leftListBlock = Ext.create('Ext.container.Container', {
	        width : '25%',
	        minHeight : 100,
	        cls : 'left_exh'
	    });
	    var rightBlock = Ext.create('Ext.container.Container', {
	        minHeight : 100,
	        height : 500,
                cls:'right_exh',
	        autoScroll : true
	    });
	    itself.add(leftListBlock);
	    itself.add(rightBlock);
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : 'getExhibitionsList'
	        },
	        success : function(response, opts) {
		        var respObject = Ext.decode(response.responseText);
		        var hideCb = function() {
                                window.vp.setTargetAttr(this, 'a:not([target])');
			        window.vp.updateLayout();
			        window.vp.loadMask.hide();
		        }
		        var cb = function() {
			        rightBlock.update(respObject.staticInfo, false, hideCb);
		        }
		        leftListBlock.update(respObject.list, false, cb);
	        },
	        failure : window.vp.failHandler
	    });
    },
    loadSysPageWithoutFoot : function(itself, pageCode,cb) {
    	var me = this;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : pageCode
	        },
	        success : function(response, opts) {
		        me.update(response.responseText);
		        window.vp.loadMask.hide();
		        if (cb){
		        	cb();
		        }
	        },
	        failure : window.vp.failHandler
	    });
    },
    loadSystemPage : function(pageCode) {
    	var me = this;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : pageCode
	        },
	        success : function(response, opts) {
		        me.update(response.responseText);
		        window.vp.doLayout();
	        },
	        failure : window.vp.failHandler
	    });
    }
});