Ext.define('app.js.users.CUserHome', {
    extend : 'Ext.container.Container',
    layout : 'absolute',
    autoScroll : false,
    height : 'auto',
    // minHeight:500,
    cls : 'user_home',
    chaptersBlock : null,
    selectedChapter : null,
    centerComp : null,
    listeners : {
        activate : function(cnt) {
	        cnt.chaptersBlock.mark();
        },
        afterrender : function(cnt) {
	        cnt.chaptersBlock.mark();
        }
    },
    initComponent : function() {
	    var me = this;
	    this.chaptersBlock = Ext.create('app.js.main.MainMenu', {
	        loadAction : 'getUserHomeChapters',
	        height : 200,
	        cls : 'userChapters',
	        highlightCls : 'user_menu_selected',
	        width : 200,
	        x : 0,
	        y : 0
	    });
	    this.chaptersBlock.loadChapters(this.chaptersBlock);
	    Ext.applyIf(this, {
		    items : [ this.chaptersBlock ]
	    });
	    this.callParent(arguments);
    },
    goEditAccountInfo : function() {
	    var me = this;
	    this.selectedChapter = 'changeaccountinfo';
	    this.chaptersBlock.markSElectedChapterImmidetly(this.selectedChapter);
	    this.chaptersBlock.mark();
	    if (this.centerComp != null) {
		    this.remove(this.centerComp);
	    }
	    this.centerComp = Ext.create('app.js.users.CUserEditAccountInfo', {
	        l10nData : this.l10nData,
	        // править эти координаты, для перемещения панели редактирования
			// личных данных
	        x : 250,
	        y : 0
	    });
	    this.add(this.centerComp);
	    this.centerComp.loadInfo();
    },
    goChangePasswordInfo : function() {
	    var me = this;
	    this.selectedChapter = 'chpass';
	    this.chaptersBlock.markSElectedChapterImmidetly(this.selectedChapter);
	    this.chaptersBlock.mark();
	    if (this.centerComp != null) {
		    this.remove(this.centerComp);
	    }
	    this.centerComp = Ext.create('app.js.users.CUserChangePasswd', {
	        // Править эти координаты, для перемещения панели смены пароля
	        x : 450,
	        y : 20
	    });
	    this.add(this.centerComp);
    },
    goFeedBack : function() {
	    var me = this;
	    this.selectedChapter = 'feedback';
	    this.chaptersBlock.markSElectedChapterImmidetly(this.selectedChapter);
	    if (this.centerComp != null) {
		    this.remove(this.centerComp);
	    }
	    this.centerComp = Ext.create('app.js.users.CUserFeedback', {
	        // Править эти координаты, для перемещения панели обратной связи
	        x : 450,
	        y : 0
	    });
	    this.add(this.centerComp);
    }
});