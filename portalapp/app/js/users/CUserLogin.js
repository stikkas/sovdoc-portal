Ext.define('app.js.users.CUserLogin', {
    extend : 'Ext.container.Container',
    layout : {
        type : 'vbox',
        align : 'center'
    },
    width : 700,
    height : 400,
    cls : 'user_login',
    initComponent : function() {
	    var textArea = Ext.create('app.js.search.CMainLinksArea', {
		    loadAction : 'getLoginPageText'
	    });
	    textArea.loadHtml();
	    var loginPan = Ext.create('app.js.main.PLoginForm', {
		    l10nData : this.l10nData
	    });
	    Ext.applyIf(this, {
		    items : [ textArea, loginPan ]
	    });
	    this.callParent(arguments);
    }
});