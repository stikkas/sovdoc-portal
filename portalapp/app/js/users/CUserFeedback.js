Ext.define('app.js.users.CUserFeedback', {
    extend : 'Ext.container.Container',
    layout : 'absolute',
    height : 'auto',
    ta : null,
    sendBtn : null,
    sendFeedBack : function() {
	    var me = this;
	    if (me.up().ta.getValue() == '' || me.up().ta.getValue() == null) {
		    Ext.Msg.alert('Ошибка', 'Сообщение не может быть пустым');
		    return;
	    }
	    Ext.Ajax.request({
	        url : 'FeedBackHandler',
	        params : {
		        msg : me.up().ta.getValue()
	        },
	        success : function(response) {
		        var resp = Ext.decode(response.responseText);
		        if (resp.success) {
			        me.up().ta.setValue('');
			        Ext.Msg.alert('OK', resp.failMessage);
		        } else {
			        Ext.Msg.alert('FAIL', resp.failMessage);
		        }
	        },
	        failure : function(response) {
		        var text = response.responseText;
		        Ext.Msg.alert('FAIL', 'Ошибка ' + text);
	        }
	    });
    },
    initComponent : function() {
	    var me = this;
	    me.ta = Ext.create('Ext.form.field.TextArea', {
	        fieldLabel : 'Текст сообщения',
	        grow : true,
	        labelCls : 'feedBackLabel',
	        name : 'feedback_msg',
	        height : 300,
	        width : 400,
	        labelAlign : 'top',
	        x : 0,
	        y : 0
	    });
	    me.sendBtn = Ext.create('Ext.Button', {
	        text : 'Отправить',
	        width : 70,
	        y : 305,
	        x : 330,
	        handler : me.sendFeedBack
	    });
	    Ext.applyIf(me, {
		    items : [ me.ta, me.sendBtn ]
	    });
	    me.callParent(arguments);
    }
});