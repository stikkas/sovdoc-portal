Ext.define('app.js.users.CUserEditAccountInfo', {
    extend : 'app.js.users.CUserCreationForm',
    height : 400,
    width : 480,
    init : function() {
	    var me = this;
	    this.tfEmail = this.getTextField('email', window.vp.l10nDatav.reg_mail, false, 'email');
	    this.tfLastName = this.getTextField('last_name', window.vp.l10nDatav.reg_sec_name, false);
	    this.tfFirstName = this.getTextField('first_name', window.vp.l10nDatav.reg_first_name, false);
	    this.tfMiddleName = this.getTextField('middle_name', window.vp.l10nDatav.reg_patronymic, false);
	    this.tfWorkPlace = this.getTextField('work_place', window.vp.l10nDatav.reg_work_place, false);
	    this.tfPhone = this.getTextField('phone', window.vp.l10nDatav.reg_phone, false);
	    this.dfBirthday = Ext.create('Ext.form.field.Date', {
	        fieldLabel : window.vp.l10nDatav.reg_birthday,
	        name : 'birthday',
	        maxValue : new Date(),
	        labelWidth : 150,
	        inputWidth : 100,
	        width : 480,
	        format : 'd.m.Y'
	    });
	    this.cmbState = this.getCombo('citizenship', window.vp.l10nDatav.reg_citizenship, 'getCitizenship');
	    this.cmbEducation = this.getCombo('education', window.vp.l10nDatav.reg_education, 'getDegriees');
	    this.cmbRank = this.getCombo('rank', window.vp.l10nDatav.reg_rank, 'getRanks');
	    var submitBtn = Ext.create('Ext.Button', {
	        text : window.vp.l10nDatav.user_acc_save,
	        bindForm : true,
	        handler : function() {
		        var form = this.up('form').getForm();
		        if (form.isValid()) {
			        form.submit({
			            params : {
			                action : 'updateAccountInfo',
			                userId : window.vp.loginedUserId
			            },
			            success : function(form, action) {
				            Ext.Msg.alert(' ', window.vp.l10nDatav.user_acc_changes_saved);
			            },
			            failure : function(form, action) {
				            Ext.Msg.alert('Failure', action.result.failMessage);
			            }
			        });
		        } else {
			        var errors = '';
			        var i = 0;
			        for (i = 0; i < me.items.length; i++) {
				        var currentFieldErrs = me.getComponent(i).getErrors(me.getComponent(i).getValue());
				        var j = 0;
				        for (j = 0; j < currentFieldErrs.length; j++) {
					        errors += '<p>';
					        errors += me.getComponent(i).getFieldLabel();
					        errors += ' ';
					        errors += currentFieldErrs[j];
					        errors += '</p>';
				        }
			        }
			        Ext.Msg.alert('ERROR', errors);
		        }
	        }
	    });
	    var btns = [ submitBtn ];
	    Ext.applyIf(this, {
	        items : [ this.tfEmail, this.tfLastName, this.tfFirstName, this.tfMiddleName, this.dfBirthday, this.cmbState, this.tfWorkPlace, this.cmbEducation, this.cmbRank, this.tfPhone ],
	        buttons : btns
	    });
    },
    loadInfo : function() {
	    var me = this;
	    Ext.Ajax.request({
	        url : 'UsersManager',
	        params : {
	            action : 'getAccountInfo',
	            userId : window.vp.loginedUserId
	        },
	        success : function(response) {
		        var text = Ext.decode(response.responseText);
		        var birthday = Ext.Date.parse(text.birthday, 'd.m.Y');
		        var mail = text.mail;
		        var middleName = text.middleName;
		        var firstName = text.firstName;
		        var secondName = text.secondName;
		        var citizenchipId = text.citizenchipId;
		        var workPlace = text.workPlace;
		        var education = text.education;
		        var rank = text.rank;
		        var phone = text.phone;
		        me.tfEmail.setValue(mail);
		        me.tfLastName.setValue(secondName);
		        me.tfFirstName.setValue(firstName);
		        me.tfMiddleName.setValue(middleName);
		        me.dfBirthday.setValue(birthday);
		        me.tfWorkPlace.setValue(workPlace);
		        me.tfPhone.setValue(phone);
		        me.cmbState.getStore().load();
		        me.cmbState.setValue(citizenchipId);
		        me.cmbEducation.getStore().load();
		        me.cmbEducation.setValue(education);
		        me.cmbRank.getStore().load();
		        me.cmbRank.setValue(rank);
	        },
	        failure : function(response) {
		        Ext.Msg.alert('FAIL', response.failMessage);
	        }
	    });
    }
});