Ext.define('app.js.users.CUserCreationForm', {
    extend : 'Ext.form.Panel',
    height : 500,
    cls : 'us_reg',
    width : 480,
    tfLogin : null,
    tfLastName : null,
    tfFirstName : null,
    tfMiddleName : null,
    tfWorkPlace : null,
    cmbEducation : null,
    cmbRank : null,
    tfPhone : null,
    dfBirthday : null,
    tfPass : null,
    tfPassConfirm : null,
    tfEmail : null,
    cmbfState : null,
    btnRegister : null,
    url : 'UsersManager',
    defaults : {
	    validateOnChange : false
    },
    getTextField : function(nam, label, blankAllowed, inpType, minLengtht, maxLengtht) {
	    if (typeof inpType == 'undefined') {
		    inpType = 'text';
	    }
	    if (typeof minLength == 'undefined') {
		    minLengtht = 3;
	    }
	    if (typeof maxLength == 'undefined') {
		    maxLengtht = 150;
	    }
	    var field = Ext.create('Ext.form.field.Text', {
	        name : nam,
	        fieldLabel : label,
	        allowBlank : blankAllowed,
	        labelWidth : 150,
	        width : 480,
	        inputType : inpType,
	        validateOnChange : false,
	        minLength : minLengtht,
	        maxLength : maxLengtht
	    });
	    return field;
    },
    getCombo : function(cmbName, cmbLabel, actionParm) {
	    return Ext.create('Ext.form.field.ComboBox', {
	        name : cmbName,
	        fieldLabel : cmbLabel,
	        displayField : 'name',
	        valueField : 'id',
	        labelWidth : 150,
	        editable : false,
	        width : 480,
	        store : Ext.create('Ext.data.Store', {
	            fields : [ 'id', 'name' ],
	            proxy : {
	                type : 'ajax',
	                url : 'DescLoader?action=' + actionParm,
	                autoLoad : true,
	                reader : {
	                    type : 'json',
	                    root : 'items'
	                }
	            }
	        })
	    });
    },
    init : function() {
	    var me = this;
	    this.tfLogin = this.getTextField('login', this.l10nData.reg_login, false, 6, 30);
	    this.tfPass = this.getTextField('pass', this.l10nData.reg_pass, false, 'password', 6, 30);
	    this.tfPassConfirm = this.getTextField('passConfirm', this.l10nData.reg_pass_confirm, false, 'password', 6, 30);
	    this.tfEmail = this.getTextField('email', this.l10nData.reg_mail, false, 'email');
	    this.tfLastName = this.getTextField('last_name', this.l10nData.reg_sec_name, false);
	    this.tfFirstName = this.getTextField('first_name', this.l10nData.reg_first_name, false);
	    this.tfMiddleName = this.getTextField('middle_name', this.l10nData.reg_patronymic, true);
	    this.tfWorkPlace = this.getTextField('work_place', this.l10nData.reg_work_place, true, 3, 350);
	    this.tfPhone = this.getTextField('phone', this.l10nData.reg_phone, true, 3, 20);
	    this.title = this.l10nData.reg_title;
	    this.dfBirthday = Ext.create('Ext.form.field.Date', {
	        fieldLabel : this.l10nData.reg_birthday,
	        name : 'birthday',
	        maxValue : new Date(),
	        allowBlank : false,
	        labelWidth : 150,
	        inputWidth : 100,
	        width : 480,
	        format : 'd.m.Y'
	    });
	    this.cmbState = this.getCombo('citizenship', this.l10nData.reg_citizenship, 'getCitizenship');
	    this.cmbEducation = this.getCombo('education', this.l10nData.reg_education, 'getDegriees');
	    this.cmbRank = this.getCombo('rank', this.l10nData.reg_rank, 'getRanks');
	    var submitBtn = Ext.create('Ext.Button', {
	        text : me.l10nData.reg_register,
	        bindForm : true,
	        handler : function() {
		        var form = this.up('form').getForm();
		        if (form.isValid()) {
			        if (me.tfPass.getValue() != me.tfPassConfirm.getValue()) {
				        Ext.Msg.alert('ERROR', me.l10nData.reg_password_not_equals);
				        return;
			        }
			        form.submit({
			            params : {
				            action : 'regUser'
			            },
			            success : function(form, action) {
				            window.vp.navigate('#regok');
			            },
			            failure : function(form, action) {
				            Ext.Msg.alert('Failure', action.result.failMessage);
			            }
			        });
		        } else {
			        var errors = '';
			        var i = 0;
			        for (i = 0; i < me.items.length; i++) {
				        var currentFieldErrs = me.getComponent(i).getErrors(me.getComponent(i).getValue());
				        var j = 0;
				        for (j = 0; j < currentFieldErrs.length; j++) {
					        errors += '<p>';
					        errors += me.getComponent(i).getFieldLabel();
					        errors += ' ';
					        errors += currentFieldErrs[j];
					        errors += '</p>';
				        }
			        }
			        Ext.Msg.alert('ERROR', errors);
		        }
	        }
	    });
	    var btns = [ submitBtn ];
	    Ext.applyIf(this, {
	        items : [ this.tfLogin, this.tfPass, this.tfPassConfirm, this.tfEmail, this.tfLastName, this.tfFirstName, this.tfMiddleName, this.dfBirthday, this.cmbState, this.tfWorkPlace, this.cmbEducation, this.cmbRank, this.tfPhone ],
	        buttons : btns
	    });
    },
    initComponent : function() {
	    this.init();
	    this.callParent(arguments);
    }
});