Ext.define('app.js.users.UserRegChapter', {
    extend : 'app.js.page',
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    this.currentCenterCont = Ext.create('app.js.CUserCreationForm', {
	        l10nData : me.l10nDatav,
	        x : 0,
	        y : 370
	    });
	    this.add(this.currentCenterCont);
    }
});
