Ext.define('app.js.users.UserLoginChapter', {
    extend : 'app.js.page',
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    var xPos = (Ext.getBody().getWidth() - 400) / 2;
	    me.currentCenterCont = Ext.create('app.js.users.CUserLogin', {
	        l10nData : window.vp.l10nDatav,
	        x : xPos,
	        y : 370
	    });
	    me.add(me.currentCenterCont);
    }
});