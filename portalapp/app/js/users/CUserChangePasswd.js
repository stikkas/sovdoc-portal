Ext.define('js.users.CUserChangePasswd', {
    extend : 'Ext.container.Container',
    layout : 'absolute',
    curPassTf : null,
    newPassTf : null,
    newPassTfConfirm : null,
    saveBtn : null,
    savePass : function() {
	    var me = this;
	    var pan = me.up();
	    if (pan.newPassTf.getValue() != pan.newPassTfConfirm.getValue()) {
		    Ext.Msg.alert('Ошибка', 'Введенные пароли не совпадают. Пароль не изменен');
		    return;
	    } else {
		    var noError = (pan.curPassTf.isValid() & pan.newPassTf.isValid()) & pan.newPassTfConfirm.isValid();
		    if (!noError) {
			    Ext.Msg.alert('ERROR', 'Некорректно заполнены поля. Длина пароля должна быть от 6 до 30 символов');
			    return;
		    }
	    }
	    Ext.Ajax.request({
	        url : 'UsersManager',
	        params : {
	            action : 'changePass',
	            oldPwd : pan.curPassTf.getValue(),
	            newPwd : pan.newPassTf.getValue()
	        },
	        success : function(response) {
		        var resp = Ext.decode(response.responseText);
		        if (resp.success) {
			        Ext.Msg.alert('OK', resp.failMessage);
			        pan.curPassTf.setValue('');
			        pan.newPassTf.setValue('');
			        pan.newPassTfConfirm.setValue('');
		        } else {
			        Ext.Msg.alert('FAIL', resp.failMessage);
		        }
	        },
	        failure : function(response) {
		        var text = response.responseText;
		        Ext.Msg.alert('FAIL', 'Ошибка ' + text);
	        }
	    });
    },
    getPwdField : function(fieldLabel_, x_, y_) {
	    return Ext.create('Ext.form.field.Text', {
	        fieldLabel : fieldLabel_,
	        width : 300,
	        labelWidth : 100,
	        inputType : 'password',
	        x : x_,
	        y : y_,
	        minLength : 6,
	        maxLength : 30,
	        validateOnChange : false
	    });
    },
    initComponent : function() {
	    var me = this;
	    me.curPassTf = me.getPwdField('Текущий пароль', 0, 0);
	    me.newPassTf = me.getPwdField('Новый пароль', 0, 25);
	    me.newPassTfConfirm = me.getPwdField('Подтверждение', 0, 50);
	    me.saveBtn = Ext.create('Ext.Button', {
	        text : 'Сохранить',
	        handler : me.savePass,
	        width : 70,
	        x : 230,
	        y : 75
	    });
	    Ext.applyIf(me, {
		    items : [ me.curPassTf, me.newPassTf, me.newPassTfConfirm, me.saveBtn ]
	    });
	    me.callParent(arguments);
    }
});