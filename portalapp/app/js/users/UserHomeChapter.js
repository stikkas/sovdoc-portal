Ext.define('app.js.users.UserHomeChapter', {
    extend : 'app.js.page',
    minHeight : 500,
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
	    this.currentCenterCont = Ext.create('app.js.users.CUserHome', {
	        l10nData : me.l10nDatav,
	        x : 0,
	        y : 370
	    });
	    this.add(this.currentCenterCont);
    }
});