Ext.define('app.js.CFooter',
{
   extend: 'Ext.container.Container',
   renderTo: 'foot_container',
   cls: 'foot',
   loadData: function(callback)
   {
       var me = this;
       Ext.Ajax.request(
               {
                   url: 'PortalDataProvider',
                   params:
                   {
                       action: 'getFooter'
                   },
                   success: function(response)
                   {
                       me.update(response.responseText);
                       if (callback)
                           callback();
                   },
                   failure: window.vp.failHandler
               });
   }
});