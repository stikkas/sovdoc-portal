Ext.define('app.js.topics.ChildsGrid', {
    extend : 'app.js.search.SearchResultsGrid',
    btnSection : null,
    btnParent : null,
    btnUp : null,
    toolBar : null,
    sectionId : null,
    minHeight: 250,
    title : 'Архивный шифр',
    tools:[{
       xtype: 'button',
       text : 'Вверх',
	   width : 'auto',
	   tooltip : 'Перейти на уровень выше',
	   tooltipType : 'title',
	   baseCls : 'childsGridButtons_up back_ress',
	   handler : function(){ 
         Ext.util.History.back();
       }
     }],

    header : {
        baseCls : 'gridHeaderBaseCls',
        cls : 'gridHeaderCls'
    },
    columns : [],
    needBackButton : false,
    storeName : 'js.topic.Children',
    listeners : {
    // здесь не должно быть листереов - клик не обрабатывается, навигация -
	// ссылками!
    },
    rootId : null,
    loadChilds : function(sectionRootId) {
	    var me = this;
	    me.rootId = sectionRootId;
	    me.getStore().currentPage = 1;
	    me.getStore().getProxy().extraParams = {
	        action : 'getTematicChilds',
	        tematicSectionRootId : sectionRootId
	    };
	    me.getStore().load({
	        totalProperty : 'itemsCount',
	        callback : function(recs, op, succ) {
		        // данные о секции, код и наименовнаия загружаются в том же
				// запросе, что и Store
		        if (succ) {
			        var rawSectionInfo = this.getProxy().getReader().rawData.sectionInfo;
			        var sectionInfo = Ext.decode(rawSectionInfo);
			        var sectionName = sectionInfo.sectionName;
			        me.btnSection.setText(sectionName);
			        var sectionId = sectionInfo.sectionId;
			        me.sectionId = sectionInfo.sectionId;
			        var unitName = sectionInfo.unitName;
			        me.btnParent.setText(unitName);
			        me.setTitle(sectionInfo.unitArchiveCode);
		        }
		        window.vp.doLayout();
	        }
	    });
    },
    sectionBtnHandler : function() {
	    var me = this;
	    var navigateUrl = '#!tematicsection&sectionId=' + me.ownerCt.ownerCt.ownerCt.sectionId;
	    window.vp.navigate(navigateUrl);
    },
    parentBtnHandler : function() {
	    var me = this;
	    var navigateUrl = '';
    },
    upBtnHandler : function() {
	    Ext.util.History.back();
    },
    initComponent : function() {
	    var me = this;
	    me.btnSection = Ext.create('Ext.Button', {
	        baseCls : 'childsGridButtons',
	        width : 'auto',
	        cls:'card_page',
	        tooltip : 'Вернуться к содержимому тематического раздела',
	        tooltipType : 'title',
	        handler : me.sectionBtnHandler
	    });
	    me.btnParent = Ext.create('Ext.form.Label', {
	        baseCls : 'childsGridParentLabel',
	        width : 'auto',
	        text : ''
	    });
	    me.columns = [ {
	        dataIndex : 'dataUnitId',
	        text : '',
	        hidden : true,
	        hideable : false,
	        width : 30,
	        hideable : false
	    }, {
	        dataIndex : 'unitName',
	        text : 'Заголовок',
	        width : 300,
	        renderer : function(value, metadata, record) {
		        metadata.style = 'cursor: pointer;';
		        var nvUrl = '#showunit&id=' + record.data.dataUnitId;
		        var fullUrl = '<a href="' + nvUrl + '">' + value + '</a>';
		        return fullUrl;
	        }
	    }, {
	        dataIndex : 'goToCard',
	        text : 'Карточка',
	        hidden : true,
	        hideable : false,
	        width : 60,
	        renderer : function(value, metadata, record) {
		        metadata.style = 'cursor: pointer;'; // http://www.sencha.com/forum/showthread.php?173876-Grid-Row-change-row-color-and-cursor
		        var nvUrl = '#showunit&id=' + value;
		        return '<a href="' + nvUrl + '" >Просмотр</a>';
	        }
	    }, {
	        dataIndex : 'mathesTextBlock',
	        text : 'Крайние даты'
	    }, {
	        dataIndex : 'goToChilds',
	        text : 'След. уровень',
	        width : 40,
	        height:51,
	        renderer : function(value, metadata, record, row, col, store, gridView) {
		        if (value == 0) {
			        return 'Нет';
		        } else {
			        return '<a href="#!" onclick="window.vp.navigate(\'#tematicchilds&rootId=' + record.data.dataUnitId + '\'); return false;" >' + value + '</a>';
		        }
	        }
	    } ];
	    me.toolBar = Ext.create('Ext.toolbar.Toolbar', {
	        width : '95%',
	        items : [ me.btnSection, me.btnParent, me.btnUp ]
	    });
	    Ext.applyIf(me, {
		    tbar : [ me.toolBar ]
	    });
	    me.callParent(arguments);
    }
});