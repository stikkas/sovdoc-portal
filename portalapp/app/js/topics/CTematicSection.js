Ext.define('app.js.topics.CTematicSection', {
    extend : 'Ext.container.Container',
    autoScroll : false,
    layout : 'hbox',
    sectionList : null,
    staticPanel : null,
    height : 'auto',
    title : 'Содержимое раздела ',
    initComponent : function() {
	    var me = this;
	    me.sectionList = Ext.create('app.js.main.MainMenu', {
	        loadAction : 'getTematicSections',
	        cls : 'tematicSections',
	        styleHtmlCls : 'tematicSections',
	        highlightCls : 'tematicSectionSelected',
	        width : 200,
	        height : 'auto',
	        x : 0,
	        y : 0
	    });
	    Ext.applyIf(me, {
		    items : [ me.sectionList ]
	    });
	    me.callParent(arguments);
    },
    load : function(itself, callBack) {
	    itself.sectionList.loadChapters(itself.sectionList, callBack);
    },
    markCelected : function(linkOrPid) {
	    var me = this;
	    me.sectionList.markTematicSection(linkOrPid);
    }
});