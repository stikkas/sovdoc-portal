Ext.define('app.js.topics.SectionHierarchy', 
{
    extend: 'Ext.panel.Panel',
    layout: 'vbox',
    width: '100% - 16',
    margin: '0 16 0 0',
    height: 405,
    autoScroll: true,
    cls: 'tematic-grid',
    initComponent: function()
    {
        var me = this;
        me.rootGrid = Ext.create('app.js.topics.TemSectionValuesGrid');
        me.items = [me.rootGrid];
        me.callParent(arguments);
    },
    loadData: function(tematicSectionId)
    {
        var me = this;
	me.sectionId = tematicSectionId;
	Ext.Ajax.request({
	    url : 'PortalDataProvider',
	    params : {
	        action : 'getTematicSectionRootTitle',
	        sectionId : tematicSectionId
	    },
	    success : function(response) {
	        var text = Ext.decode(response.responseText);
	        me.setTitle(text.titleText);
	    }
	});     
        window.vp.loadMask.show();
        Ext.Ajax.request(
        {
            url: 'SearchHandler',
            params:
            {
                action : 'getTematicRoots',
	        tematicSectionId : tematicSectionId
            },
            success: function(response)
            {
                me.processData(Ext.decode(response.responseText));
                window.vp.loadMask.hide();
            }
        });
    },
    processData: function(data)
    {
        var me = this;
        me.rootGrid.getStore().loadData(data.values);
        Ext.each(data.sections, function(section)
        {
            me.add(Ext.create('app.js.topics.SectionPanel',
            {
                sectionData: section,
                rootPage: me.rootPage
            }));
        });
        me.rootPage.doLayout();
    }
});