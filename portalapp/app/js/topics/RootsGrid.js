Ext.define('app.js.topics.RootsGrid', {
    extend : 'app.js.search.SearchResultsGrid',
    header : {
        baseCls : 'gridHeaderBaseCls',
        cls : 'gridHeaderCls',
        height : 35
    },
    minHeight:400,
    autoScroll : false,
    title : 'Содержимое раздела',
    titleAlign : 'center',
    sectionId : null,
    storeName : 'js.topic.Roots',
    needBackButton : false,
    initComponent : function() {
	    var me = this;
	    me.columns = [ {
	        dataIndex : 'dataUnitId',
	        text : '',
	        hidden : true,
	        hideable : false,
	        width : 30,
	        hideable : false
	    }, {
	        dataIndex : 'archiveCode',
	        text : 'Архивный шифр',
	        width : 150
	    }, {
	        dataIndex : 'unitName',
	        text : 'Заголовок',
	        width : 300,
	        renderer : function(value, metadata, record, row, col, store) {
		        metadata.style = 'cursor: pointer;';
		        var nvUrl = '#showunit&id=' + record.data.goToCard;
		        var fullUrl = '<a href="' + nvUrl + '">' + value + '</a>';
		        return fullUrl;
	        }
	    }, {
	        dataIndex : 'goToCard',
	        text : 'Карточка',
	        width : 60,
	        hidden : true,
	        hideable : false,
	        renderer : function(value, metadata, record, row, col, store) {
		        metadata.style = 'cursor: pointer;'; // http://www.sencha.com/forum/showthread.php?173876-Grid-Row-change-row-color-and-cursor
		        var nvUrl = '#showunit&id=' + value;
		        return '<a href="' + nvUrl + '">Просмотр</a>';
	        }
	    }, {
	        dataIndex : 'mathesTextBlock',
	        text : 'Крайние даты'
	    }, {
	        dataIndex : 'goToChilds',
	        text : 'След. уровень',
	        width : 50,
	        height:51,
	        renderer : function(value, metadata, record, row, col, store, gridView) {
		        if (value == 0) {
			        return 'Нет';
		        } else {
			        var url = "#tematicchilds&rootId=" + record.data.dataUnitId;
			        return '<a href="' + url + '" onclick="window.vp.navigate(\'#tematicchilds&rootId=' + record.data.dataUnitId + '\'); return false;" >' + value + '</a>';
		        }
	        }
	    } ];
	    me.callParent(arguments);
    },
    columns : [],
    listeners : {},
    loadRoots : function(tematicSectionId, cb) {
	    var me = this;
	    me.sectionId = tematicSectionId;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
	            action : 'getTematicSectionRootTitle',
	            sectionId : tematicSectionId
	        },
	        success : function(response) {
		        var text = Ext.decode(response.responseText);
		        me.setTitle(text.titleText);
	        }
	    });
	    me.getStore().getProxy().extraParams = {
	        action : 'getTematicRoots',
	        tematicSectionId : tematicSectionId,
	        tematicSectionRootsRequest : 1
	    };
	    me.getStore().currentPage = 1;
	    me.getStore().load({
	        totalProperty : 'itemsCount',
	        callback : cb
	    });
    }
});