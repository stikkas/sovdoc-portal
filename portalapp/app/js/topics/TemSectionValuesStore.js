Ext.define('app.js.topics.TemSectionValuesStore',
{
    extend: 'Ext.data.Store',
    fields: ['id', 'name', 'archCode', 'dates', 'childrenCnt'],
    autoLoad: false
});