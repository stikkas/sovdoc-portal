Ext.define('app.js.topics.TemSectionValuesGrid',
{
    extend: 'Ext.grid.Panel',
    requires: ['app.js.topics.TemSectionValuesStore'],
    width: '100%',
    forceFit: true,
    sortableColumns: false,
    enableColumnHide : false,
    store: Ext.create('app.js.topics.TemSectionValuesStore'),
    cls:'srch_res',
    top : 405,
    columns:
    [
        {
            dataIndex: 'archCode',
            text: 'Архивный шифр',
            width: 150
        },
        {
            dataIndex: 'name',
            text: 'Заголовок',
            width: 300,
            renderer: function(value, metadata, record) 
            {
	        metadata.style = 'cursor: pointer;';
	        var nvUrl = '#showunit&id=' + record.data.id;
	        var fullUrl = '<a href="' + nvUrl + '">' + value + '</a>';
	        return fullUrl;
	    }
        },
        {
            dataIndex: 'dates',
            text: 'Крайние даты'
        },
        {
            dataIndex: 'childrenCnt',
            text: 'След. уровень',
            width: 50,
            height: 51,
            renderer : function(value, metadata, record) 
            {
		if (value == 0) 
                    return 'Нет';
		else 
                {
		    var url = "#tematicchilds&rootId=" + record.data.id;
		    return '<a href="' + url + '" onclick="window.vp.navigate(\'#tematicchilds&rootId=' + record.data.id + '\'); return false;" >' + value + '</a>';
		}
	    }
        }
    ]        
});