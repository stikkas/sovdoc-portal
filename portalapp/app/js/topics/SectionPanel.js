Ext.define('app.js.topics.SectionPanel',
{
    extend: 'Ext.panel.Panel',
    layout: 'vbox',
    width: '100%',
    collapsible: true,
    leftMargin: 0,
    initComponent: function()
    {
        var me = this;
        me.items = [];
        if (me.sectionData)
        {
            var data = me.sectionData;
            me.header =
            {
                style: 'margin-left:' + me.leftMargin + 'px',
                title: data.section
            }
            //me.setTitle(data.section);
            if (data.values)
            {
                me.valuesGrid = Ext.create('app.js.topics.TemSectionValuesGrid',
                {
                    store: Ext.create('app.js.topics.TemSectionValuesStore'),
                    hideHeaders: true 
                });
                me.valuesGrid.getStore().loadData(data.values);
                me.items.push(me.valuesGrid);
            }
            Ext.each(data.sections, function(section)
            {
                me.items.push(Ext.create('app.js.topics.SectionPanel',
                {
                    sectionData: section,
                    rootPage: me.rootPage,
                    leftMargin: me.leftMargin + 20
                }));
            });
            me.on('collapse', me.doLayout);
            //me.on('expand', me.doPageLayout);
        }
        me.callParent(arguments);
    },
    doPageLayout: function()
    {
        this.rootPage.doLayout();
    }
});