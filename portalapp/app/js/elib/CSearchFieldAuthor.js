Ext.define('app.js.elib.CSearchFieldAuthor', {
    extend : 'app.js.search.CSearchFieldCombo',
    action : 'getElibAuthors',
    cls:'width228_f',
    getErr : function() {
	    var vl = this.getValue();
	    if (typeof vl == 'undefined') {
		    return this.l10nData.elib_err_author;
	    }
	    if (vl == '') {
		    return this.l10nData.elib_err_author;
	    }
	    return '';
    }
});