Ext.define('app.js.elib.CEbookList', {
    extend : 'app.js.search.SearchResultsGrid',
    title : 'Перечень электронных книг',
    cls : 'ebook-list-grid search_style ',
    width : '100%',
    height : 'auto',
    y : 360,
    gridFields : [ 'dataUnitId', 'bookName', 'tom', 'author', 'publisher', 'publYear' ],
   // baseCls:'ebook-list-grid-base',
    minHeight : 250,
    prepareForAdvSearch : function() {
	    // ничего не делаю: в этой табличке, кнопка возврата не нужна
    },
    columns : [ {
        dataIndex : 'dataUnitId',
        text : '',
        hidden : true,
        hideable : false,
        width : 30,
        renderer : function(value) {
	        return '<a href="navigation.jsp?page=showdataunit&id=' + value + '">Просмотр</a>';
        }
    }, {
        dataIndex : 'bookName',
        text : 'Название книги',
        width : 150
    }, {
        dataIndex : 'tom',
        text : 'Том',
        width : 100
    }, {
        dataIndex : 'author',
        text : 'Автор',
        width : 100
    }, {
        dataIndex : 'publisher',
        text : 'Издательство',
        width : 50
    }, {
        dataIndex : 'publYear',
        text : 'Год издания',
        width : 30
    } ],
    loadAll : function() {
	    var me = this;
	    Ext.apply(me.getStore().getProxy(), {
	        extraParams : {
	            action : 'elib',
	            params : Ext.encode({
	                elibQuery : 1,
	                elibGetAll : 2
	            })
	        },
	        success : function(response) {
		        var text = response.responseText;
	        },
	        failure : function(response) {
		        Ext.Msg.alert('Ошибка', response.responseText);
		        window.vp.loadMask.hide();
	        }
	    });
	    me.getStore().load({
	        params : {
	            start : 0,
	            limit : 50
	        },
	        callback : window.vp.doLayout()
	    });
    },
    listeners : {
	    cellclick : function(ref, td, cellIndex, record, tr, rowIndex, e, eOpts) {
		    var nvUrl = '#showebookunit' + '&id=' + record.data.dataUnitId;
		    window.vp.navigate(nvUrl);
	    }
    }
});