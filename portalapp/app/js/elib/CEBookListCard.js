Ext.define('app.js.elib.CEBookListCard', {
    extend : 'Ext.container.Container',
    dataView : null,
    pagingTb : null,
    label : null,
    minHeight : 300,
    width : '100%',
    cls : 'elib_list',
    initComponent : function() {
	    var me = this;
	    me.label = Ext.create('Ext.form.Label', {
		    baseCls : 'booklist'
	    });
	    me.label.setText('Перечень электронных книг');
	    me.dataView = Ext.create('Ext.view.View', {
	        store : 'js.elib.ElibStore',
	        cls : 'ebook_list_cont',
	        loadMask : false,
	        tpl : [ '<tpl for=".">', 
                        '<table class="list_books"><tr><td class="col-1"><div class="thumb"><a href="#showebookunit&id={dataUnitId}"><img src="{coverurl}"/></a></div></td>', 
                        '<td class="col-2"><table class="attr_book"><tr><td class="attr_name">Название книги</td><td class="attr_val"><a href="#showebookunit&id={dataUnitId}">{bookName}</a></td></tr>', 
                        '<tr><td class="attr_name">Том</td><td>{tom}</td></tr>', 
                        '<tr><td class="attr_name">Автор</td><td>{author}</td></tr>', 
                        '<tr><td class="attr_name">Издательство</td><td>{publisher}</td></tr>',
	                '<tr><td class="attr_name">Год издания</td><td>{publYear}</td></tr></table></td></tr></table>', 
                        '</tpl>' ],
	        itemSelector : 'div.thumb-wrap'
	    });
	    me.pagingTb = Ext.create('Ext.toolbar.Paging', {
		    store : me.dataView.getStore(),
		    displayInfo: true
	    });
	    Ext.applyIf(me, {
		    items : [ me.label, me.pagingTb, me.dataView ]
	    });
	    me.callParent(arguments);
    },
    loadAll : function() {
	    var me = this;
	    var store = me.dataView.getStore();
	    store.load({
	        params : {
	            action : 'elib',
	            params : Ext.encode({
	                elibQuery : 1,
	                elibGetAll : 2
	            })
	        },
	        callback : function(recs, ops, suc) {
	        }
	    });
    }
});