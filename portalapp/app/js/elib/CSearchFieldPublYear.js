Ext.define('app.js.elib.CSearchFieldPublYear', {
    extend : 'app.js.search.CSearchField',
    cls:'width40',
    initComponent : function() {
	    this.tp = 'Ext.form.field.Text';
	    this.callParent(arguments);
    },
    getErr : function() {
	    var reg = new RegExp("^\\d{4}$", "i");
	    var value = this.getValue();
	    var err = '';
	    if (reg.exec(value) == null || value.length > 4) {
		    err = this.l10nData.elib_err_publ_year;
	    }
	    return err;
    }
});