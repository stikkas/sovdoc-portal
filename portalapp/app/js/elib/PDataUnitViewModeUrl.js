Ext.define('app.js.elib.PDataUnitViewModeUrl', {
    extend : 'app.js.search.PDataUnitView',
    l10nConsts : {},
    loadInfo : function(duId) {
	    var me = this;
	    Ext.Ajax.request({
	        url : 'SearchHandler',
	        params : {
	            action : 'getUnitInfo',
	            unitId : duId
	        },
	        success : function(response) {
		        var textt = response.responseText;
		        var decodedValue = Ext.decode(textt);
		        var text = decodedValue.cardhtml;
                        //Убирается по просьбе задачи #10362
		        /*var ebookurl = decodedValue.ebookurl;
		        if (ebookurl != '') {
			        var link = Ext.create('Ext.Component', {
				        autoEl : {
				            tag : 'a',
				            href : ebookurl,
				            target : '_blank',
				            html : 'Читать книгу',
				            cls : 'read_book'
				        }
			        });
			        me.tb.add(link);
		        }*/
		        me.htmlPanel.update(text);
		        me.htmlPanel.show();
		        window.vp.doLayout();
		        window.vp.loadMask.hide();
	        },
	        failure : function(response) {
		        var text = response.responseText;
		        me.htmlPanel.update(text);
		        me.htmlPanel.show();
		        window.vp.loadMask.hide();
	        }
	    });
    }
});