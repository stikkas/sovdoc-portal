Ext.define('app.js.elib.PDataUnitViewMode', {
	extend: 'app.js.search.PDataUnitView',
	l10nConsts: {},
	previewsPanel: null,
	goToImagesHandler: true,
	cardBtn: null,
	newBtn: null,
	unitId: null,
	initComponent: function () {
		var me = this;
		Ext.define('Image', {
			extend: 'Ext.data.Model',
			fields: [{
					name: 'fileName',
					type: 'string'
				}, {
					name: 'url',
					type: 'string'
				}, {
					name: 'mapUrl',
					type: 'string'
				}]
		});
		me.previewsPanel = Ext.create('Ext.view.View', {
			loadMask: false,
			overflowX: 'none',
			overflowY: 'auto',
			store: Ext.create('Ext.data.Store', {
				autoLoad: false,
				model: 'Image',
				pageSize: 20,
				proxy: {
					type: 'ajax',
					url: 'SearchHandler',
					reader: {
						type: 'json',
						root: 'items',
						totalProperty: 'itemsCount'
					},
					totalProperty: 'itemsCount'
				}
			}),
			tpl: ['<tpl for=".">', 
				'<div class="thumb-wrap" id="{fileName}">', 
				'<div class="thumb"><img src="{url}" title="{fileName}"></div>', 
				'<span class="x-editable">{fileName}</span></div>', 
				'</tpl>', 
				'<div class="x-clear"></div>'],
			multiSelect: true,
			height: 430,
			cls: 'img_panel',
			trackOver: true,
			overItemCls: 'x-item-over',
			itemSelector: 'div.thumb-wrap',
			listeners: {
				itemdblclick: function (vw, rec, htmlel, idx, eventObject) {
					var wnd = Ext.create('app.js.search.WImageView');
					wnd.setData(me.previewsPanel.getStore().data);
					wnd.setCurrentIndex(idx);
					wnd.show();
					wnd.setHeight(wnd.getHeight() + 25);
				}
			},
			prepareData: function (data) {
				Ext.apply(data, {
					shortName: Ext.util.Format.ellipsis(data.name, 15),
					sizeString: Ext.util.Format.fileSize(data.size),
					dateString: Ext.util.Format.date(data.lastmod, "m/d/Y g:i a")
				});
				return data;
			}
		});
		me.pagingTb = Ext.create('Ext.toolbar.Paging', {
			store: me.previewsPanel.getStore(),
			displayInfo: true
		});
		me.callParent(arguments);
		me.previewsPanel.emptyText = me.l10nConsts.card_NoImages;
		// обработка нажатия кнопки "Образы"
		me.goToImagesHandler = function (btn, event) {
			window.location.hash = "#showunit&id=" + me.unitId + ';tab=img';
			me.newBtn.setDisabled(true);
			me.cardBtn.setDisabled(false);
			var unitView = me;
			if (unitView.currentPanel == unitView.previewsPanel) {
				return;
			}
			unitView.remove(unitView.htmlPanel, false);
			unitView.add(unitView.previewsPanel);
			unitView.add(unitView.pagingTb);
			unitView.previewsPanel.store.load({
				params: {
					start: 0,
					limit: 20
				},
				callback: function (rec, op, suc) {
					if (!suc) {
						me.previewsPanel.update(op.request.proxy.reader.rawData.msg);
					}
					window.vp.doLayout();
				}
			});
		};

		// Обработка нажатия кнопки "Карточка"
		var goToCardHandler = function (btn, event) {
			window.location.hash = "#showunit&id=" + me.unitId;
			me.cardBtn.setDisabled(true);
			me.newBtn.setDisabled(false);
			var unitView = me;
			if (unitView.currentPanel == unitView.htmlPanel) {
				return;
			}
			unitView.remove(unitView.previewsPanel, false);
			unitView.add(unitView.htmlPanel);
			unitView.currentPanel = unitView.htmlPanel;
		};
		me.cardBtn = Ext.create('Ext.Button', {
			text: me.l10nConsts.card_ShowCard,
			cls: 'card_pict',
			tooltip: 'Перейти к просмотру карточки', // Интересно, а на нерусском языке эта надпись будет непорусски?
			// Определение Russisch Schwein подходит к этому творению как нельзя лучше.
			tooltipType: 'title',
			width: 'auto',
			// maxwidth:200,
			handler: goToCardHandler
		});
		me.tb.add(me.cardBtn);
		me.newBtn = Ext.create('Ext.Button', {
			text: me.l10nConsts.card_Images,
			width: 'auto',
			tooltip: 'Перейти к просмотру образов',
			tooltipType: 'title',
			// maxWidth:200,
			cls: 'img_but',
			handler: me.goToImagesHandler
		});
		me.tb.add(me.newBtn);
		me.tb.add({
			xtype: 'box',
			autoEl: {
				tag: 'span',
				html: me.l10nConsts.annos_presents
			},
			hidden: true
		});
		me.cardBtn.setDisabled(true);
		// tb.remove('hyperlink');
	},
	loadInfo: function (duId, anno) {
		var me = this;
		if (anno) {
			me.callParent([duId, 'getPhonoSUInfo']);
			return;
		}
		me.unitId = duId;
		me.callParent(arguments);
		var previewViewProxy = me.previewsPanel.store.getProxy();
		Ext.apply(previewViewProxy, {
			extraParams: {
				action: 'getImageInfo',
				unitId: duId
			}
		});
	}
});