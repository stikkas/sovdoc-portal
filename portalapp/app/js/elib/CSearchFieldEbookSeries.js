Ext.define('app.js.elib.CSearchFieldEbookSeries', {
    extend : 'app.js.search.CSearchFieldCombo',
    action : 'getEbookSeries',
    cls : 'width830',
    getErr : function() {
	    var me = this;
	    if (me.selected == '') {
		    return this.l10nData.elib_err_series_not_sel;
	    } else {
		    return '';
	    }
    }
});