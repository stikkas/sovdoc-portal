Ext.define('app.js.main.PLoginForm', {
    extend : 'Ext.form.Panel',
    height : 180,
    width : 297,
    y : 540,
    x : 0,
    url : 'UsersManager',
    cls : 'login_box',
    initComponent : function() {
	    var me = this;
	    this.title = me.l10nData.p_sys_login;
	    var iitems = [ {
	        name : 'login',
	        xtype : 'textfield',
	        fieldLabel : me.l10nData.p_login,
	        allowBlank : false,
	        labelAlign : 'top'
	    }, {
	        xtype : 'textfield',
	        name : 'pass',
	        inputType : 'password',
	        fieldLabel : me.l10nData.p_pass,
	        allowBlank : false,
	        labelAlign : 'top'
	    } ];
	    var bbuttons = [ {
	        text : me.l10nData.p_go,
	        handler : function() {
		        var form = this.up('form').getForm();
		        if (form.isValid()) {
			        form.submit({
			            params : {
				            action : 'login'
			            },
			            success : function(form, action) {
				            window.vp.loginedUserId = action.result.userId;
				            window.vp.loginedUserName = action.result.fullName;
				            window.vp.navigate('#userhome');
			            },
			            failure : function(form, action) {
				            Ext.Msg.alert('Failure', action.result.failMessage);
				            window.vp.loadMask.hide();
			            }
			        });
		        }
	        }
	    } ];
	    Ext.applyIf(me, {
	        items : iitems,
	        buttons : bbuttons
	    });
	    me.callParent(arguments);
    }
});