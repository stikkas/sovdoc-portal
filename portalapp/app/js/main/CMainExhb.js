Ext.define('app.js.main.CMainExhb', {
    extend : 'Ext.container.Container',
    width :310,
    //x: 25,
    //y: 0,
    styleHtml : true,
    styleHtmlCls : 'news_block_styled',
    cls : 'news_block_r',
    loadExhibitions : function(itself, callback) {
	    var i = itself;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : 'getExhibitionShort'
	        },
	        success : function(response, opts) {
		        i.update(response.responseText);
		        window.vp.loadMask.hide();
	        },
	        failure : window.vp.failHandler,
                callback: callback
	    });
    }
});