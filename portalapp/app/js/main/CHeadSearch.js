Ext.define('app.js.main.CHeadSearch', {
    extend : 'app.js.CHead',
    requires: ['app.js.main.MainMenu','app.js.search.CSimpleSearch','Ext.form.field.Text','Ext.form.field.Display'],
    initPanels : function() {
	    var me = this;
	    me.menu = Ext.create('app.js.main.MainMenu', {
	        x :0,
	        y : 255
	    });
	    me.romb = Ext.create('app.js.search.CSimpleSearch', {
	        x : 0,
	        y : 295,
	        panPortal : me,
	        l10nData : me.l10nData,
	        cls : 'srch'
	    });
    },
    load : function() {
	    var me = this;
	    var markF = function() {
		    me.menu.markSElectedChapterImmidetly('search');
	    };
	    me.menu.loadChapters(me.menu, markF);
    },
    markSelectedChapter : function(chapter) {
	    return;
    }
});