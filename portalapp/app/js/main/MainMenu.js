Ext.define('app.js.main.MainMenu', {
    extend : 'Ext.container.Container',
    width : '100%',
    height : 30,
    styleHtmlContent : true,
    styleHtmlCls : 'menu',
    cls:'menu',
    loadAction : 'getChapters',
    highlightCls : 'menu_selected',
    loaded : false,
    chapterForSelection : null,
    listeners : {
	    render : function(menu, eOpts) {
		    menu.mark();
	    }
    },
    loadChapters : function(itself, callBack) {
	    var i = itself;
	    var cb = callBack;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : i.loadAction
	        },
	        success : function(response, opts) {
		        i.update(response.responseText);
		        if (cb) {
			        cb();
		        }
		        if (i.chapterForSelection) {
			        i.mark();
		        }
	        },
	        failure : window.vp.failHandler
	    });
    },
    getChapterForMark : function(chapterCode) {
	    if (chapterCode == 'searchresults' || chapterCode == 'advsearch' || chapterCode == 'showunit' || chapterCode == 'showunit') {
		    chapterCode = 'search'
	    }
	    if (chapterCode == '!tematicsection') {
		    chapterCode = 'search';
	    }
	    if (chapterCode == 'tematicchilds') {
		    chapterCode = 'search';
	    }
	    if (chapterCode == 'advsearch') {
		    chapterCode = 'search';
	    }
	    if (chapterCode == 'showexbpage') {
		    chapterCode = 'exhblist';
	    }
	    if (chapterCode == 'shownews') {
		    chapterCode = 'news';
	    }
	    if (chapterCode == 'showebookunit') {
		    chapterCode = 'elib';
	    }
	    return chapterCode;
    },
    markSElectedChapterImmidetly : function(chapterCode) {
	    var me = this;
	    chapterCode = me.getChapterForMark(chapterCode);
	    this.chapterForSelection = chapterCode + '_';
	    this.mark();
    },
    mark : function() {
	    // window.vp.loadMask.show();
	    if (!this.chapterForSelection) {
		    return;
	    }
	    
	    if (!this.chapterForSelection) {
		    throw 'chapter for selection is undefined!';
	    }
	    if (this.chapterForSelection != null) {
		    this.clearSelection();
		    var selLink = Ext.fly(this.chapterForSelection);
		    if (selLink != null) {
			    selLink.addCls(this.highlightCls);
		    }
	    }
    },
    markTematicSection: function(chapterCode){
    	var me = this;
    	me.clearSelection();
    	chapterCode = me.getChapterForMark(chapterCode);
    	this.chapterForSelection = chapterCode;
    	if (this.getEl()){
    		this.mark();
    	}
    },
    markSelectedChapter : function(chapterCode) {
	    var me = this;
	    chapterCode = me.getChapterForMark(chapterCode);
	    this.chapterForSelection = chapterCode + '_';
	    if (this.getEl()) {
		    this.mark();
	    }
    },
    clearSelection : function() {
	    var allLinks;
	    if (this.getEl() != null) {
		    allLinks = this.getEl().query('a');
		    var ij = 0;
		    for (ij = 0; ij < allLinks.length; ij++) {
			    var curLinks = Ext.fly(allLinks[ij].id);
			    if (curLinks != null) {
				    curLinks.removeCls(this.highlightCls);
			    } else {
			    }
		    }
	    } else {
	    }
    }
});