Ext.define('app.js.main.CMainContent', {
    extend : 'Ext.container.Container',
    //minWidth : 560,
    x : 365,
    y : 340,
    styleHtml : true,
    cls : 'center_news',
    loadContent : function(itself) {
	    var i = itself;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : 'getMainContent'
	        },
	        success : function(response, opts) {
		        i.update(response.responseText);
		        window.vp.doLayout();
	        },
	        failure : window.vp.failHandler
	    });
    }
});