Ext.define('app.js.main.CNewsBlock', {
    extend : 'Ext.container.Container',
    width : 310,
    //x: 25,
    //y: 350,
    styleHtml : true,
    styleHtmlCls : 'news_block_styled',
    cls : 'news_block',
    loadNews : function(iitself, callback) {
	    var ii = iitself;
	    Ext.Ajax.request({
	        url : 'PortalDataProvider',
	        params : {
		        action : 'getNewsBlock'
	        },
	        success : function(response, opts) {
		        ii.update(response.responseText);
	        },
	        failure : window.vp.failHandler,
                callback: callback
	    });
    }
});