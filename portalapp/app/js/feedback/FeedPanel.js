Ext.define('app.js.feedback.FeedPanel', {
    extend : 'app.js.page',
    requires : ['app.js.page'],
    initComponent : function() {
	    var me = this;
	    me.callParent(arguments);
	    me.currentCenterCont = Ext.create('app.js.feedback.FeedContent', {
	        x : 0,
	        y : 370
	    });
	    me.add(me.currentCenterCont);
    },
    initChildsAfterLocaleDataLoaded : function() {
	    var me = this;
	    me.callParent(arguments);
    }
});